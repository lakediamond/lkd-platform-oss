package moltin

import (
	"encoding/json"
	"io/ioutil"
	"math/big"
	"net/http"

	"github.com/ethereum/go-ethereum/common"
	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

//Initialize moltin func
func InitMotlin(app *application.Application) {

	checkTokenValidity(app)

	//Setup event listener
	getAllEventListeners(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/flows/" + app.Moltin.TokenContractConfigEntrySlug + "/entries"
	log.Debug().Msgf("Sending GET request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	req, err := http.NewRequest("GET", moltinUrl, nil)
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("ioutil.ReadAll() error")
		return
	}

	log.Debug().Msgf("Response status: %s", resp.Status)

	var buffer application.EntitiesGenerated

	err = json.Unmarshal(data, &buffer)
	if err != nil {
		log.Error().Err(err).Msg("Error response parsing")
		return
	}

	if len(buffer.Data) == 0 {
		log.Debug().Msg("Entries is not initialised")
		return
	}

	var entry application.Entities

	entry.TokenContractAddress = buffer.Data[0].TokenContractAddress
	entry.LastBlockScanned = buffer.Data[0].LastBlockScanned
	entry.EntriesId = buffer.Data[0].ID
	entry.TokenSymbol = buffer.Data[0].TokenSymbol

	app.Moltin.EntriesData.Entities = append(app.Moltin.EntriesData.Entities, entry)

	log.Debug().Msgf("Entries count, must be 1: %d", len(app.Moltin.EntriesData.Entities))
	if len(app.Moltin.EntriesData.Entities) == 0 {
		log.Info().Msg("No entries data, returning...")
		return
	}

	log.Debug().Msgf("Contract taken from entries data %s", app.Moltin.EntriesData.Entities)

	app.EthConfig.EthFilter.Addresses = []common.Address{common.HexToAddress(app.Moltin.EntriesData.Entities[0].TokenContractAddress)}

	n := new(big.Int)
	n.SetString(app.Moltin.EntriesData.Entities[0].LastBlockScanned, 10)

	app.EthConfig.EthFilter.FromBlock = n
	log.Debug().Msgf("Setting last block number by: %s", n)
}
