package moltin

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

type EventListener struct {
	Data []EventListenerInternal `json:"data"`
}

type EventCreation struct {
	Data EventListenerInternal `json:"data"`
}

type EventListenerInternal struct {
	Type            string        `json:"type,omitempty"`
	Name            string        `json:"name,omitempty"`
	Description     string        `json:"description,omitempty"`
	Enabled         bool          `json:"enabled,omitempty"`
	Observes        []string      `json:"observes,omitempty"`
	IntegrationType string        `json:"integration_type,omitempty"`
	Configuration   Configuration `json:"configuration,omitempty"`
}

type Configuration struct {
	Url string `json:"url"`
}

//func to check moltin for needed event, if event not found => create it, else => do nothing
func getAllEventListeners(app *application.Application) {
	var currencyEvents bool

	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/integrations"
	log.Debug().Msgf("Sending GET request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	req, err := http.NewRequest("GET", moltinUrl, nil)
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("ioutil.ReadAll() error")
		return
	}

	var events EventListener

	err = json.Unmarshal(data, &events)
	if err != nil {
		log.Error().Err(err).Msg("Error response parsing")
	}

	for _, event := range events.Data {
		if event.Configuration.Url == app.Moltin.Events.EventHandlersURL {
			if event.Name == app.Moltin.Events.CurrencyEventHandlerName {
				currencyEvents = true
				log.Info().Msg("Currency event request already exists")
			}
		}
	}

	if !currencyEvents {
		log.Info().Msg("Creating currency event request")
		createCurrencyEvent(app)
	}
}

//forming event data
func createCurrencyEvent(app *application.Application) {
	var data EventCreation

	data.Data.Name = app.Moltin.Events.CurrencyEventHandlerName
	data.Data.Configuration.Url = app.Moltin.Events.EventHandlersURL

	data.Data.Description = "Tracks " + strings.Join(app.Moltin.Events.CurrencyEventHandlerObservers, " ") + " events"
	data.Data.Enabled = true
	data.Data.IntegrationType = "webhook"
	data.Data.Observes = app.Moltin.Events.CurrencyEventHandlerObservers
	data.Data.Type = "integration"

	sendNewEventCreation(&data, app)
}

//sending event to moltin
func sendNewEventCreation(eventData *EventCreation, app *application.Application) {
	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/integrations"
	log.Debug().Msgf("Sending POST request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	data, err := json.Marshal(eventData)
	if err != nil {
		log.Error().Err(err).Msg("event data parsing error")
	}

	req, err := http.NewRequest("POST", moltinUrl, bytes.NewReader(data))
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return
	}
	defer resp.Body.Close()

	log.Debug().Msgf("Moltin event listener creation status %s", resp.Status)
}
