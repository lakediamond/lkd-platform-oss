package httpserver

import (
	"fmt"
	"net/http"

	"github.com/justinas/alice"
	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
	"gitlab.com/lakediamond/ecommerce-backend/pkg/handlers"
)

// Serve will run HTTP Server on configured application PORT
func Serve(app *application.Application) {
	router := NewHTTPRouter(app)
	routes := alice.New(handlers.JSONContentTypeHandler, handlers.LoggingHandler, handlers.RecoveryHandler).Then(router)

	addr := fmt.Sprintf(":%s", app.Config.Port)
	log.Info().Msgf("Serving API on http://localhost%s", addr)
	if err := http.ListenAndServe(addr, routes); err != nil {
		log.Fatal().Err(err).Msg("")
	}
}
