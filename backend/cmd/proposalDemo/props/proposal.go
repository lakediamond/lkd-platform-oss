package props

import (
	"crypto/ecdsa"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"
	"math"
	"math/big"
)

type Proposal struct {
	Price  *big.Int
	Amount *big.Int
	Date   int
	From   *ecdsa.PrivateKey
	TxHash string
}

func InputToProposals() []Proposal {
	input := GetProposalsInput()

	var proposals []Proposal
	for _, prop := range input {

		key, err := crypto.HexToECDSA(prop.From)
		if err != nil {
			log.Fatal().Err(err).Msg("invalid ethereum private key")
		}

		price := big.NewInt(int64(prop.Price * math.Pow(10, 18)))

		proposals = append(proposals, Proposal{price, big.NewInt(prop.Amount), prop.Date, key, ""})
	}

	return proposals
}
