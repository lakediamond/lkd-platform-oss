package main

import (
	"lkd-platform-backend/cmd/matcher/initializer"
	"lkd-platform-backend/internal/app/matcherBackend"
	"lkd-platform-backend/internal/pkg/matcherApplication"
)

func main() {
	app := matcherApplication.NewMatcherApplication()

	initializer.InitMatcherApplication(app)

	matcherBackend.RunListener(app)
}
