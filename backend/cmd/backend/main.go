package main

import (
	"net"
	"os"

	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"

	"lkd-platform-backend/cmd/backend/initializer"
	"lkd-platform-backend/internal/app/backend/httpserver"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/googleCloud"
)

func main() {
	addr := net.JoinHostPort(
		os.Getenv("DD_AGENT_HOST"),
		os.Getenv("DD_TRACE_AGENT_PORT"),
	)
	tracer.Start(tracer.WithAgentAddr(addr), tracer.WithServiceName("lkd-platform-backend"))
	defer tracer.Stop()

	app := application.NewApplication()

	initializer.InitApplication(app)
	app.Mailjet = application.NewMailjetClient(false)

	app.GC = googleCloud.NewGCApplication()

	httpserver.Serve(app)
}
