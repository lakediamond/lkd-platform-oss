package initializer

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/crm"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/oauth"
	"lkd-platform-backend/internal/pkg/phoneNumber"
	"lkd-platform-backend/internal/pkg/phoneNumber/twilio"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/sms"
	"lkd-platform-backend/internal/pkg/sms/bitAccess"
	"lkd-platform-backend/pkg/enviroment"
)

func InitApplication(app *application.Application) {
	env := enviroment.GetEnv()

	app.Repo = initDB(env)
	app.IAMClient = initIAMClient(env)
	app.EthClient = initEthereum(env)
	app.Config.AuthRetries = initAuthRetries(env)
	app.Storage = initStorage(env)
	app.CRMData = initCRMData(env)
	app.SMSSender = initSMSSender(env)
	app.PhoneNumberValidator = initPhoneNumberValidator(env)
}

func initDB(env map[string]string) *repo.Repo {
	name := env["DATABASE_NAME"]
	user := env["DATABASE_USER"]
	pass := env["DATABASE_PASSWORD"]
	host := env["DATABASE_HOST"]
	port := env["DATABASE_PORT"]

	r, err := repo.GetDbClient(name, user, pass, host, port)
	if err != nil {
		log.Panic().Err(err).Msg("cannot connect to database")
	}

	return r
}

func initIAMClient(env map[string]string) oauth.IdentityManagementClient {
	loginRememberParsed, err := strconv.ParseInt(env["OAUTH_LOGIN_REMEMBER_FOR"], 10, 64)
	if err != nil {
		log.Panic().Err(err).Msg("$OAUTH_LOGIN_REMEMBER_FOR value processing failure")
	}

	consentRememberParsed, err := strconv.ParseInt(env["OAUTH_CONSENT_REMEMBER_FOR"], 10, 64)
	if err != nil {
		log.Panic().Err(err).Msg("$OAUTH_CONSENT_REMEMBER_FOR value processing failure %s")
	}

	config := oauth.IAMConfig{
		AdminURL:     env["OAUTH_HYDRA_ADMIN_API_URL"],
		PublicURL:    env["OAUTH_HYDRA_API_URL"],
		ClientID:     env["OAUTH_CLIENT_ID"],
		ClientSecret: env["OAUTH_CLIENT_SECRET"],
		Scopes:       strings.Split(env["OAUTH_CLIENT_SCOPES"], ","),

		LoginRemember:   loginRememberParsed,
		ConsentRemember: consentRememberParsed,

		ClientLogoutRedirectURL: env["OAUTH_CLIENT_LOGOUT_REDIRECT_URL"],
		ClientRedirectURL:       env["OAUTH_CLIENT_REDIRECT_URL"],
		AuthTokenAudience:       strings.Split(env["OAUTH_AUTH_AUDIENCE"], ","),
		StateToken:              env["OAUTH_FLOW_STATE_TOKEN"],
	}

	client, err := oauth.GetIAMClient(&config)
	if err != nil {
		log.Panic().Err(err).Msg("cannot initialize IAM client")
	}

	return client
}

func initEthereum(env map[string]string) *etherPkg.PlatformEthereumApplication {
	md := initializeMatcherData(env)
	ethApp := etherPkg.InitializePlatformEthereumApplication(md)
	return ethApp
}

//InitializeMatcherData initializing matcher data for event listener
func initializeMatcherData(env map[string]string) *etherPkg.MatcherData {
	return &etherPkg.MatcherData{
		StorageAddress:        env["ETHEREUM_IO_STORAGE_CONTRACT_ADDRESS"],
		LakeDiamond:           env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		NewOrder:              env["ETHEREUM_NEW_ORDER_EVENT"],
		CloseOrder:            env["ETHEREUM_CLOSE_ORDER_EVENT"],
		NewProposal:           env["ETHEREUM_NEW_PROPOSAL_EVENT"],
		WithdrawProposal:      env["ETHEREUM_WITHDRAW_PROPOSAL_EVENT"],
		CompleteProposal:      env["ETHEREUM_COMPLETE_PROPOSAL_EVENT"],
		NewOrderType:          env["ETHEREUM_NEW_ORDER_TYPE_EVENT"],
		CompleteOrder:         env["ETHEREUM_ORDER_COMPLETE_EVENT"],
		NewSubOwner:           env["ETHEREUM_NEW_SUBOWNER_EVENT"],
		RemovedSubOwner:       env["ETHEREUM_REMOVED_SUBOWNER_EVENT"],
		IOContractAddress:     env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		RPCPort:               env["RPC_PORT"],
		PrivateKeyStorageKey:  env["ETHEREUM_PRIVATE_KEY_STORAGE_KEY"],
		PrivateKeyStorageData: env["ETHEREUM_PRIVATE_KEY_STORAGE_DATA"],
	}
}

func initAuthRetries(env map[string]string) *application.AuthRetries {
	limitRetries, err := strconv.ParseInt(env["AUTH_LIMIT_RETRIES"], 10, 64)
	if err != nil {
		log.Fatal().Err(err).Msg("invalid AUTH_LIMIT_RETRIES")
	}

	timeFrame, err := strconv.ParseInt(env["AUTH_TIME_FRAME"], 10, 64)
	if err != nil {
		log.Fatal().Err(err).Msg("invalid AUTH_TIME_FRAME")
	}

	lockTime, err := strconv.ParseInt(env["AUTH_LOCK_TIME"], 10, 64)
	if err != nil {
		log.Fatal().Err(err).Msg("invalid AUTH_LOCK_TIME")
	}

	return &application.AuthRetries{
		LimitRetries: limitRetries,
		TimeFrame:    timeFrame,
		LockTime:     lockTime,
		LockReason:   env["AUTH_LOCK_REASON"],
	}
}

func initStorage(env map[string]string) *application.Storage {
	return application.NewStorage(env["GCP_PROJECT_ID_FULL"], env["STORAGE_BUCKET_NAME"])
}

func initCRMData(env map[string]string) crm.DataInterface {
	token := env["CRM_TOKEN"]

	if len(token) < 0 {
		log.Fatal().Msg("CRM_TOKEN not found in config")
	}

	data := crm.InitData(token)

	err := loadDealDataFromConfig(data.GetDeal(), env)
	if err != nil {
		log.Fatal().Err(err).Msg(err.Error())
	}

	return data
}

func loadDealDataFromConfig(crmDeal *crm.Deal, env map[string]string) error {

	IDDocumentBitaccessVerificationFailureStageParsed, idDocParseErr := strconv.ParseInt(env["CRM_ID_DOCUMENT_FAILURE_STAGE"], 10, 0)
	if idDocParseErr != nil {
		return errors.WithMessage(idDocParseErr, getInvalidValueMsg("CRM_ID_DOCUMENT_FAILURE_STAGE"))
	}
	crmDeal.IDDocumentBitaccessVerificationFailureStage = IDDocumentBitaccessVerificationFailureStageParsed

	IDDocumentFromAllowedCountryFailureStageParsed, idDocAllowedCountryParseErr := strconv.ParseInt(env["CRM_ID_RESTRICTED_COUNTRY_FAILURE_STAGE"], 10, 0)
	if idDocAllowedCountryParseErr != nil {
		return errors.WithMessage(idDocAllowedCountryParseErr, getInvalidValueMsg("CRM_ID_RESTRICTED_COUNTRY_FAILURE_STAGE"))
	}
	crmDeal.IDDocumentFromAllowedCountryFailureStage = IDDocumentFromAllowedCountryFailureStageParsed

	SelfieBitaccessVerificationFailureStageParsed, selfieBtcsParseErr := strconv.ParseInt(env["CRM_SELFIE_FAILURE_STAGE"], 10, 0)
	if selfieBtcsParseErr != nil {
		return errors.WithMessage(selfieBtcsParseErr, getInvalidValueMsg("CRM_SELFIE_FAILURE_STAGE"))
	}
	crmDeal.SelfieBitaccessVerificationFailureStage = SelfieBitaccessVerificationFailureStageParsed

	EthereumAddressScorechainScreeningFailureStageParsed, ethScreeningParseErr := strconv.ParseInt(env["CRM_ETH_ADDRESS_SCREENING_FAILURE_STAGE"], 10, 0)
	if ethScreeningParseErr != nil {
		return errors.WithMessage(ethScreeningParseErr, getInvalidValueMsg("CRM_ETH_ADDRESS_SCREENING_FAILURE_STAGE"))
	}
	crmDeal.EthereumAddressScorechainScreeningFailureStage = EthereumAddressScorechainScreeningFailureStageParsed

	MultipleFailuresStageParsed, multiFailuresParseErr := strconv.ParseInt(env["CRM_MULTIPLE_FAILURES_STAGE"], 10, 0)
	if multiFailuresParseErr != nil {
		return errors.WithMessage(multiFailuresParseErr, getInvalidValueMsg("CRM_MULTIPLE_FAILURES_STAGE"))
	}
	crmDeal.MultipleFailuresStage = MultipleFailuresStageParsed

	return nil
}

func getInvalidValueMsg(valueName string) string {
	return fmt.Sprintf("invalid %s value", valueName)
}

func initSMSSender(env map[string]string) sms.Sender {
	return bitAccess.NewSender(
		env["COMMUNICATIONS_API_URL"],
		env["COMMUNICATIONS_API_KEY"],
		env["COMMUNICATIONS_API_SECRET"],
	)
}

func initPhoneNumberValidator(env map[string]string) phoneNumber.Validator {
	return twilio.NewValidator(
		env["COMMUNICATIONS_TWILIO_API_ROOT"],
		env["COMMUNICATIONS_TWILIO_ACCOUNT_SID"],
		env["COMMUNICATIONS_TWILIO_SECRET"],
	)
}
