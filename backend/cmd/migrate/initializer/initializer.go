package initializer

import (
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/pkg/enviroment"

	"github.com/rs/zerolog/log"
)

func InitMigration(app *application.Application) {
	env := enviroment.GetEnv()

	app.Repo = initDB(env)
}

func initDB(env map[string]string) *repo.Repo {
	name := env["DATABASE_NAME"]
	user := env["DATABASE_USER"]
	pass := env["DATABASE_PASSWORD"]
	host := env["DATABASE_HOST"]
	port := env["DATABASE_PORT"]

	r, err := repo.GetDbClient(name, user, pass, host, port)
	if err != nil {
		log.Panic().Err(err).Msg("cannot connect to database")
	}

	return r
}
