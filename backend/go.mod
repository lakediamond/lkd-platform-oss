module lkd-platform-backend

require (
	cloud.google.com/go v0.32.0
	github.com/Jeffail/gabs v1.1.1
	github.com/aristanetworks/goarista v0.0.0-20181109020153-5faa74ffbed7 // indirect
	github.com/btcsuite/btcd v0.0.0-20181013004428-67e573d211ac // indirect
	github.com/cespare/cp v1.0.0 // indirect
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20181014144952-4e0d7dc8888f // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgryski/dgoogauth v0.0.0-20171123172744-fd153d5969d3
	github.com/edsrzf/mmap-go v0.0.0-20170320065105-0bce6a688712 // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/ethereum/go-ethereum v1.8.18
	github.com/facebookgo/clock v0.0.0-20150410010913-600d898af40a // indirect
	github.com/facebookgo/grace v0.0.0-20180706040059-75cf19382434
	github.com/facebookgo/httpdown v0.0.0-20180706035922-5979d39b15c2 // indirect
	github.com/facebookgo/stats v0.0.0-20151006221625-1b76add642e4 // indirect
	github.com/fjl/memsize v0.0.0-20180929194037-2a09253e352a // indirect
	github.com/fourcube/goiban v0.0.0-20180521073646-b0e41b277c7f
	github.com/fourcube/goiban-data v0.0.0-20180515151907-ad39e148bb33 // indirect
	github.com/fourcube/goiban-data-loader v0.0.0-20181011090144-a8148a664f13 // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/gorilla/sessions v0.0.0-20160922145804-ca9ada445741
	github.com/gorilla/websocket v1.4.0
	github.com/goware/emailx v0.0.0-20171023230436-0bae9679d4e3
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/huin/goupnp v1.0.0 // indirect
	github.com/jackpal/go-nat-pmp v1.0.1 // indirect
	github.com/jinzhu/gorm v1.9.1
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v0.0.0-20181116074157-8ec929ed50c3 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/justinas/alice v0.0.0-20171023064455-03f45bd4b7da
	github.com/karalabe/hid v0.0.0-20180420081245-2b4488a37358 // indirect
	github.com/lib/pq v1.0.0
	github.com/mailjet/mailjet-apiv3-go v0.0.0-20180215102658-9a7900801c1e
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/minio/minio-go v6.0.11+incompatible
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/onsi/gomega v1.4.2 // indirect
	github.com/ory/go-convenience v0.1.0
	github.com/ory/hydra v0.0.0-20181121194553-1295663ada90
	github.com/ory/x v0.0.32
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/rjeczalik/notify v0.9.2 // indirect
	github.com/rs/zerolog v1.10.3
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24
	github.com/stretchr/testify v1.2.2
	github.com/syndtr/goleveldb v0.0.0-20181105012736-f9080354173f // indirect
	github.com/tealeg/xlsx v1.0.3 // indirect
	github.com/tidwall/pretty v0.0.0-20180105212114-65a9db5fad51
	github.com/tinylib/msgp v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20181112202954-3d3f9f413869
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a // indirect
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890
	golang.org/x/sys v0.0.0-20181107165924-66b7b1311ac8 // indirect
	google.golang.org/api v0.0.0-20180910000450-7ca32eb868bf
	google.golang.org/genproto v0.0.0-20190201180003-4b09977fb922
	gopkg.in/DataDog/dd-trace-go.v1 v1.9.0
	gopkg.in/gormigrate.v1 v1.2.1
	gopkg.in/natefinch/npipe.v2 v2.0.0-20160621034901-c1b8fa8bdcce // indirect
	gopkg.in/resty.v1 v1.10.2 // indirect
	gopkg.in/stretchr/testify.v1 v1.2.2 // indirect
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
)
