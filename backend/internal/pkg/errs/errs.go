package pkg

import "errors"

var (
	ErrInvalid2FACode  = errors.New("invalid 2FA code")
	ErrInvalidPassword = errors.New("invalid password")
	ErrDBError         = "invalid database response"
)
