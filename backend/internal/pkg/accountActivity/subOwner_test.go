package accountActivity_test

import (
	"crypto/ecdsa"
	"encoding/json"
	"math/big"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/matcherBackend"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/storageContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/tokenContract"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type SubOwnerActivitySuite struct {
	suite.Suite
	App         *application.Application
	MockServer  *httptest.Server
	PrivateKey1 *ecdsa.PrivateKey
	Address1    common.Address
	Auth        *bind.TransactOpts

	Token   *tokenContract.Token
	Storage *storageContract.Storage
	IO      *ioContract.IO

	EthMock *etherPkg.EthMock

	StorageAddress common.Address
}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_SubOwnerActivitySuite(t *testing.T) {
	suite.Run(t, new(SubOwnerActivitySuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *SubOwnerActivitySuite) SetupTest() {
	app, _, _ := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	ethMock, privKey := etherPkg.GetMockEthClient()

	suite.EthMock = ethMock

	app.EthClient = ethMock
	app.MatcherData = etherPkg.InitializeMatcherData()

	auth := bind.NewKeyedTransactor(privKey)

	suite.Auth = auth
	suite.Address1 = crypto.PubkeyToAddress(privKey.PublicKey)

	app.Repo.DB.Exec("DELETE FROM users;")
	app.Repo.DB.Exec("DELETE FROM jwt_tokens;")
	app.Repo.DB.Exec("DELETE FROM orders;")
	app.Repo.DB.Exec("DELETE FROM proposals;")
	app.Repo.DB.Exec("DELETE FROM support_data;")
	app.Repo.DB.Exec("DELETE FROM order_types;")
	app.Repo.DB.Exec("DELETE FROM account_activities;")
	app.Repo.DB.Exec("DELETE FROM sub_owners;")

	tokenAddress, _, token, err := tokenContract.DeployToken(auth, ethMock)
	if err != nil {
		log.Debug().Err(err).Msg("DeployToken error")
	}

	suite.EthMock.Commit()

	storageAddress, _, storage, err := storageContract.DeployStorage(auth, ethMock, tokenAddress)
	if err != nil {
		log.Debug().Err(err).Msg("DeployStorage error")
	}
	suite.EthMock.Commit()

	ioAddress, _, io, err := ioContract.DeployIO(auth, ethMock, storageAddress, suite.Address1)
	if err != nil {
		log.Debug().Err(err).Msg("DeployIo error")
	}

	suite.EthMock.Commit()

	_, err = storage.SetContractManager(auth, ioAddress)
	if err != nil {
		log.Debug().Err(err).Msg("SetContractManager error")
	}

	suite.EthMock.Commit()

	app.MatcherData.StorageAddress = storageAddress.String()
	app.MatcherData.LakeDiamond = ioAddress.String()

	suite.Token = token
	suite.Storage = storage
	suite.IO = io

	suite.StorageAddress = storageAddress

	suite.App = app
	suite.PrivateKey1 = privKey

	priv.Migration(app.Repo.DB)
	go matcherBackend.ListenEvents(app)
	go matcherBackend.ListenSubOwnerEvents(app)
}

func (suite *SubOwnerActivitySuite) TestAccountActivityNewSubOwner() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	_, err := suite.IO.AddSubOwners(suite.Auth, []common.Address{userAddress})
	if err != nil {
		log.Debug().Err(err).Msg("Add subOwner error")
	}

	suite.EthMock.Commit()

	for i := 0; i < 20; i++ {
		suite.Token.Approve(suite.Auth, suite.Address1, big.NewInt(100))
		suite.EthMock.Commit()
	}

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("IO_CONTRACT_SUBOWNER_ADDED", activities[0].Type)

	var objmap map[string]*json.RawMessage

	var buf string

	json.Unmarshal([]byte(activities[0].Content), &objmap)

	json.Unmarshal(*objmap["eth_address"], &buf)
	suite.Assert().Equal(strings.ToLower(userAddress.String()), buf)

}

func (suite *SubOwnerActivitySuite) TestAccountActivityRemoveSubOwner() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	_, err := suite.IO.RemoveSubOwners(suite.Auth, []common.Address{userAddress})
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()

	for i := 0; i < 20; i++ {
		suite.Token.Approve(suite.Auth, suite.Address1, big.NewInt(100))
		suite.EthMock.Commit()
	}

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("IO_CONTRACT_SUBOWNER_REMOVED", activities[0].Type)

	var objmap map[string]*json.RawMessage

	var buf string

	json.Unmarshal([]byte(activities[0].Content), &objmap)

	json.Unmarshal(*objmap["eth_address"], &buf)
	suite.Assert().Equal(strings.ToLower(userAddress.String()), buf)
}
