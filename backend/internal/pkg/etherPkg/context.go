package etherPkg

import (
	"context"
	"net/http"
)

//WithApplicationContext store context into each http request
func (client *PlatformEthereumApplication) WithEthereumContext(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), 1, client)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
