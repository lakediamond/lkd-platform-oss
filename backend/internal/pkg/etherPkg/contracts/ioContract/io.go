package ioContract

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// IoContractABI is the input ABI used to generate the binding from.
const IoContractABI = "[{\"constant\":true,\"inputs\":[],\"name\":\"backendAddress\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newAddress\",\"type\":\"address\"}],\"name\":\"setBackendAddress\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"subOwnerColumnKey\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"userAddress\",\"type\":\"address\"}],\"name\":\"getAllowedAmountTokens\",\"outputs\":[{\"name\":\"allowedTokenAmount\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"whiteListKey\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"subOwnerAddress\",\"type\":\"address\"}],\"name\":\"getIsSubOwner\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"price\",\"type\":\"uint256\"},{\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"createProposal\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"id\",\"type\":\"uint256\"},{\"name\":\"description\",\"type\":\"string\"}],\"name\":\"setOrderType\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addresses\",\"type\":\"address[]\"}],\"name\":\"addSubOwners\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"isOwner\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"activeOrderExists\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addresses\",\"type\":\"address[]\"}],\"name\":\"removeSubOwners\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"typeId\",\"type\":\"uint256\"},{\"name\":\"metadata\",\"type\":\"string\"},{\"name\":\"price\",\"type\":\"uint256\"},{\"name\":\"tknAmount\",\"type\":\"uint256\"}],\"name\":\"createOrder\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"userAddress\",\"type\":\"address\"},{\"name\":\"allowedTokenAmount\",\"type\":\"uint256\"}],\"name\":\"addAddressToWhitelist\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"withdrawProposal\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"lakeDiamondStorage\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"orderId\",\"type\":\"uint256\"},{\"name\":\"ids\",\"type\":\"uint256[]\"}],\"name\":\"matchOrder\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"storageAddress\",\"type\":\"address\"},{\"name\":\"backend\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"subOwner\",\"type\":\"address\"}],\"name\":\"NewSubOwner\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"subOwner\",\"type\":\"address\"}],\"name\":\"RemovedSubOwner\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"desctiption\",\"type\":\"string\"}],\"name\":\"NewOrderType\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"ProposalCreated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"isActive\",\"type\":\"bool\"}],\"name\":\"ProposalWithdrawn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"tokenAmount\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"orderId\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"etherAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"isActive\",\"type\":\"bool\"}],\"name\":\"ProposalCompleted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"ethAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"tknAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"typeId\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"metadata\",\"type\":\"string\"},{\"indexed\":false,\"name\":\"orderId\",\"type\":\"uint256\"}],\"name\":\"NewOrder\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"rest\",\"type\":\"uint256\"}],\"name\":\"OrderProcessed\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"userAddress\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"allowedTokenAmount\",\"type\":\"uint256\"}],\"name\":\"AddressAddedToWhiteList\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"}]"

// IoContract is an auto generated Go binding around an Ethereum contract.
type IoContract struct {
	IoContractCaller     // Read-only binding to the contract
	IoContractTransactor // Write-only binding to the contract
	IoContractFilterer   // Log filterer for contract events
}

// IoContractCaller is an auto generated read-only Go binding around an Ethereum contract.
type IoContractCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IoContractTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IoContractTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IoContractFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IoContractFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IoContractSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IoContractSession struct {
	Contract     *IoContract       // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IoContractCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IoContractCallerSession struct {
	Contract *IoContractCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// IoContractTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IoContractTransactorSession struct {
	Contract     *IoContractTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// IoContractRaw is an auto generated low-level Go binding around an Ethereum contract.
type IoContractRaw struct {
	Contract *IoContract // Generic contract binding to access the raw methods on
}

// IoContractCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IoContractCallerRaw struct {
	Contract *IoContractCaller // Generic read-only contract binding to access the raw methods on
}

// IoContractTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IoContractTransactorRaw struct {
	Contract *IoContractTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIoContract creates a new instance of IoContract, bound to a specific deployed contract.
func NewIoContract(address common.Address, backend bind.ContractBackend) (*IoContract, error) {
	contract, err := bindIoContract(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IoContract{IoContractCaller: IoContractCaller{contract: contract}, IoContractTransactor: IoContractTransactor{contract: contract}, IoContractFilterer: IoContractFilterer{contract: contract}}, nil
}

// NewIoContractCaller creates a new read-only instance of IoContract, bound to a specific deployed contract.
func NewIoContractCaller(address common.Address, caller bind.ContractCaller) (*IoContractCaller, error) {
	contract, err := bindIoContract(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IoContractCaller{contract: contract}, nil
}

// NewIoContractTransactor creates a new write-only instance of IoContract, bound to a specific deployed contract.
func NewIoContractTransactor(address common.Address, transactor bind.ContractTransactor) (*IoContractTransactor, error) {
	contract, err := bindIoContract(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IoContractTransactor{contract: contract}, nil
}

// NewIoContractFilterer creates a new log filterer instance of IoContract, bound to a specific deployed contract.
func NewIoContractFilterer(address common.Address, filterer bind.ContractFilterer) (*IoContractFilterer, error) {
	contract, err := bindIoContract(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IoContractFilterer{contract: contract}, nil
}

// bindIoContract binds a generic wrapper to an already deployed contract.
func bindIoContract(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IoContractABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IoContract *IoContractRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _IoContract.Contract.IoContractCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IoContract *IoContractRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IoContract.Contract.IoContractTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IoContract *IoContractRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IoContract.Contract.IoContractTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IoContract *IoContractCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _IoContract.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IoContract *IoContractTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IoContract.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IoContract *IoContractTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IoContract.Contract.contract.Transact(opts, method, params...)
}

// ActiveOrderExists is a free data retrieval call binding the contract method 0xb4846ef6.
//
// Solidity: function activeOrderExists() constant returns(bool)
func (_IoContract *IoContractCaller) ActiveOrderExists(opts *bind.CallOpts) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "activeOrderExists")
	return *ret0, err
}

// ActiveOrderExists is a free data retrieval call binding the contract method 0xb4846ef6.
//
// Solidity: function activeOrderExists() constant returns(bool)
func (_IoContract *IoContractSession) ActiveOrderExists() (bool, error) {
	return _IoContract.Contract.ActiveOrderExists(&_IoContract.CallOpts)
}

// ActiveOrderExists is a free data retrieval call binding the contract method 0xb4846ef6.
//
// Solidity: function activeOrderExists() constant returns(bool)
func (_IoContract *IoContractCallerSession) ActiveOrderExists() (bool, error) {
	return _IoContract.Contract.ActiveOrderExists(&_IoContract.CallOpts)
}

// BackendAddress is a free data retrieval call binding the contract method 0x0d72d57f.
//
// Solidity: function backendAddress() constant returns(address)
func (_IoContract *IoContractCaller) BackendAddress(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "backendAddress")
	return *ret0, err
}

// BackendAddress is a free data retrieval call binding the contract method 0x0d72d57f.
//
// Solidity: function backendAddress() constant returns(address)
func (_IoContract *IoContractSession) BackendAddress() (common.Address, error) {
	return _IoContract.Contract.BackendAddress(&_IoContract.CallOpts)
}

// BackendAddress is a free data retrieval call binding the contract method 0x0d72d57f.
//
// Solidity: function backendAddress() constant returns(address)
func (_IoContract *IoContractCallerSession) BackendAddress() (common.Address, error) {
	return _IoContract.Contract.BackendAddress(&_IoContract.CallOpts)
}

// GetAllowedAmountTokens is a free data retrieval call binding the contract method 0x3d491466.
//
// Solidity: function getAllowedAmountTokens(userAddress address) constant returns(allowedTokenAmount uint256)
func (_IoContract *IoContractCaller) GetAllowedAmountTokens(opts *bind.CallOpts, userAddress common.Address) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "getAllowedAmountTokens", userAddress)
	return *ret0, err
}

// GetAllowedAmountTokens is a free data retrieval call binding the contract method 0x3d491466.
//
// Solidity: function getAllowedAmountTokens(userAddress address) constant returns(allowedTokenAmount uint256)
func (_IoContract *IoContractSession) GetAllowedAmountTokens(userAddress common.Address) (*big.Int, error) {
	return _IoContract.Contract.GetAllowedAmountTokens(&_IoContract.CallOpts, userAddress)
}

// GetAllowedAmountTokens is a free data retrieval call binding the contract method 0x3d491466.
//
// Solidity: function getAllowedAmountTokens(userAddress address) constant returns(allowedTokenAmount uint256)
func (_IoContract *IoContractCallerSession) GetAllowedAmountTokens(userAddress common.Address) (*big.Int, error) {
	return _IoContract.Contract.GetAllowedAmountTokens(&_IoContract.CallOpts, userAddress)
}

// GetIsSubOwner is a free data retrieval call binding the contract method 0x4c9d2ceb.
//
// Solidity: function getIsSubOwner(subOwnerAddress address) constant returns(bool)
func (_IoContract *IoContractCaller) GetIsSubOwner(opts *bind.CallOpts, subOwnerAddress common.Address) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "getIsSubOwner", subOwnerAddress)
	return *ret0, err
}

// GetIsSubOwner is a free data retrieval call binding the contract method 0x4c9d2ceb.
//
// Solidity: function getIsSubOwner(subOwnerAddress address) constant returns(bool)
func (_IoContract *IoContractSession) GetIsSubOwner(subOwnerAddress common.Address) (bool, error) {
	return _IoContract.Contract.GetIsSubOwner(&_IoContract.CallOpts, subOwnerAddress)
}

// GetIsSubOwner is a free data retrieval call binding the contract method 0x4c9d2ceb.
//
// Solidity: function getIsSubOwner(subOwnerAddress address) constant returns(bool)
func (_IoContract *IoContractCallerSession) GetIsSubOwner(subOwnerAddress common.Address) (bool, error) {
	return _IoContract.Contract.GetIsSubOwner(&_IoContract.CallOpts, subOwnerAddress)
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() constant returns(bool)
func (_IoContract *IoContractCaller) IsOwner(opts *bind.CallOpts) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "isOwner")
	return *ret0, err
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() constant returns(bool)
func (_IoContract *IoContractSession) IsOwner() (bool, error) {
	return _IoContract.Contract.IsOwner(&_IoContract.CallOpts)
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() constant returns(bool)
func (_IoContract *IoContractCallerSession) IsOwner() (bool, error) {
	return _IoContract.Contract.IsOwner(&_IoContract.CallOpts)
}

// LakeDiamondStorage is a free data retrieval call binding the contract method 0xe851ddd2.
//
// Solidity: function lakeDiamondStorage() constant returns(address)
func (_IoContract *IoContractCaller) LakeDiamondStorage(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "lakeDiamondStorage")
	return *ret0, err
}

// LakeDiamondStorage is a free data retrieval call binding the contract method 0xe851ddd2.
//
// Solidity: function lakeDiamondStorage() constant returns(address)
func (_IoContract *IoContractSession) LakeDiamondStorage() (common.Address, error) {
	return _IoContract.Contract.LakeDiamondStorage(&_IoContract.CallOpts)
}

// LakeDiamondStorage is a free data retrieval call binding the contract method 0xe851ddd2.
//
// Solidity: function lakeDiamondStorage() constant returns(address)
func (_IoContract *IoContractCallerSession) LakeDiamondStorage() (common.Address, error) {
	return _IoContract.Contract.LakeDiamondStorage(&_IoContract.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() constant returns(address)
func (_IoContract *IoContractCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "owner")
	return *ret0, err
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() constant returns(address)
func (_IoContract *IoContractSession) Owner() (common.Address, error) {
	return _IoContract.Contract.Owner(&_IoContract.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() constant returns(address)
func (_IoContract *IoContractCallerSession) Owner() (common.Address, error) {
	return _IoContract.Contract.Owner(&_IoContract.CallOpts)
}

// SubOwnerColumnKey is a free data retrieval call binding the contract method 0x30f4f8df.
//
// Solidity: function subOwnerColumnKey() constant returns(string)
func (_IoContract *IoContractCaller) SubOwnerColumnKey(opts *bind.CallOpts) (string, error) {
	var (
		ret0 = new(string)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "subOwnerColumnKey")
	return *ret0, err
}

// SubOwnerColumnKey is a free data retrieval call binding the contract method 0x30f4f8df.
//
// Solidity: function subOwnerColumnKey() constant returns(string)
func (_IoContract *IoContractSession) SubOwnerColumnKey() (string, error) {
	return _IoContract.Contract.SubOwnerColumnKey(&_IoContract.CallOpts)
}

// SubOwnerColumnKey is a free data retrieval call binding the contract method 0x30f4f8df.
//
// Solidity: function subOwnerColumnKey() constant returns(string)
func (_IoContract *IoContractCallerSession) SubOwnerColumnKey() (string, error) {
	return _IoContract.Contract.SubOwnerColumnKey(&_IoContract.CallOpts)
}

// WhiteListKey is a free data retrieval call binding the contract method 0x4bfae20d.
//
// Solidity: function whiteListKey() constant returns(string)
func (_IoContract *IoContractCaller) WhiteListKey(opts *bind.CallOpts) (string, error) {
	var (
		ret0 = new(string)
	)
	out := ret0
	err := _IoContract.contract.Call(opts, out, "whiteListKey")
	return *ret0, err
}

// WhiteListKey is a free data retrieval call binding the contract method 0x4bfae20d.
//
// Solidity: function whiteListKey() constant returns(string)
func (_IoContract *IoContractSession) WhiteListKey() (string, error) {
	return _IoContract.Contract.WhiteListKey(&_IoContract.CallOpts)
}

// WhiteListKey is a free data retrieval call binding the contract method 0x4bfae20d.
//
// Solidity: function whiteListKey() constant returns(string)
func (_IoContract *IoContractCallerSession) WhiteListKey() (string, error) {
	return _IoContract.Contract.WhiteListKey(&_IoContract.CallOpts)
}

// AddAddressToWhitelist is a paid mutator transaction binding the contract method 0xe17039b8.
//
// Solidity: function addAddressToWhitelist(userAddress address, allowedTokenAmount uint256) returns()
func (_IoContract *IoContractTransactor) AddAddressToWhitelist(opts *bind.TransactOpts, userAddress common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "addAddressToWhitelist", userAddress, allowedTokenAmount)
}

// AddAddressToWhitelist is a paid mutator transaction binding the contract method 0xe17039b8.
//
// Solidity: function addAddressToWhitelist(userAddress address, allowedTokenAmount uint256) returns()
func (_IoContract *IoContractSession) AddAddressToWhitelist(userAddress common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.AddAddressToWhitelist(&_IoContract.TransactOpts, userAddress, allowedTokenAmount)
}

// AddAddressToWhitelist is a paid mutator transaction binding the contract method 0xe17039b8.
//
// Solidity: function addAddressToWhitelist(userAddress address, allowedTokenAmount uint256) returns()
func (_IoContract *IoContractTransactorSession) AddAddressToWhitelist(userAddress common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.AddAddressToWhitelist(&_IoContract.TransactOpts, userAddress, allowedTokenAmount)
}

// AddSubOwners is a paid mutator transaction binding the contract method 0x8d1fd1af.
//
// Solidity: function addSubOwners(addresses address[]) returns()
func (_IoContract *IoContractTransactor) AddSubOwners(opts *bind.TransactOpts, addresses []common.Address) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "addSubOwners", addresses)
}

// AddSubOwners is a paid mutator transaction binding the contract method 0x8d1fd1af.
//
// Solidity: function addSubOwners(addresses address[]) returns()
func (_IoContract *IoContractSession) AddSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.AddSubOwners(&_IoContract.TransactOpts, addresses)
}

// AddSubOwners is a paid mutator transaction binding the contract method 0x8d1fd1af.
//
// Solidity: function addSubOwners(addresses address[]) returns()
func (_IoContract *IoContractTransactorSession) AddSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.AddSubOwners(&_IoContract.TransactOpts, addresses)
}

// CreateOrder is a paid mutator transaction binding the contract method 0xce02bbe2.
//
// Solidity: function createOrder(typeId uint256, metadata string, price uint256, tknAmount uint256) returns()
func (_IoContract *IoContractTransactor) CreateOrder(opts *bind.TransactOpts, typeId *big.Int, metadata string, price *big.Int, tknAmount *big.Int) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "createOrder", typeId, metadata, price, tknAmount)
}

// CreateOrder is a paid mutator transaction binding the contract method 0xce02bbe2.
//
// Solidity: function createOrder(typeId uint256, metadata string, price uint256, tknAmount uint256) returns()
func (_IoContract *IoContractSession) CreateOrder(typeId *big.Int, metadata string, price *big.Int, tknAmount *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.CreateOrder(&_IoContract.TransactOpts, typeId, metadata, price, tknAmount)
}

// CreateOrder is a paid mutator transaction binding the contract method 0xce02bbe2.
//
// Solidity: function createOrder(typeId uint256, metadata string, price uint256, tknAmount uint256) returns()
func (_IoContract *IoContractTransactorSession) CreateOrder(typeId *big.Int, metadata string, price *big.Int, tknAmount *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.CreateOrder(&_IoContract.TransactOpts, typeId, metadata, price, tknAmount)
}

// CreateProposal is a paid mutator transaction binding the contract method 0x6935bda6.
//
// Solidity: function createProposal(price uint256, amount uint256) returns()
func (_IoContract *IoContractTransactor) CreateProposal(opts *bind.TransactOpts, price *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "createProposal", price, amount)
}

// CreateProposal is a paid mutator transaction binding the contract method 0x6935bda6.
//
// Solidity: function createProposal(price uint256, amount uint256) returns()
func (_IoContract *IoContractSession) CreateProposal(price *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.CreateProposal(&_IoContract.TransactOpts, price, amount)
}

// CreateProposal is a paid mutator transaction binding the contract method 0x6935bda6.
//
// Solidity: function createProposal(price uint256, amount uint256) returns()
func (_IoContract *IoContractTransactorSession) CreateProposal(price *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.CreateProposal(&_IoContract.TransactOpts, price, amount)
}

// MatchOrder is a paid mutator transaction binding the contract method 0xff312fba.
//
// Solidity: function matchOrder(orderId uint256, ids uint256[]) returns()
func (_IoContract *IoContractTransactor) MatchOrder(opts *bind.TransactOpts, orderId *big.Int, ids []*big.Int) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "matchOrder", orderId, ids)
}

// MatchOrder is a paid mutator transaction binding the contract method 0xff312fba.
//
// Solidity: function matchOrder(orderId uint256, ids uint256[]) returns()
func (_IoContract *IoContractSession) MatchOrder(orderId *big.Int, ids []*big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.MatchOrder(&_IoContract.TransactOpts, orderId, ids)
}

// MatchOrder is a paid mutator transaction binding the contract method 0xff312fba.
//
// Solidity: function matchOrder(orderId uint256, ids uint256[]) returns()
func (_IoContract *IoContractTransactorSession) MatchOrder(orderId *big.Int, ids []*big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.MatchOrder(&_IoContract.TransactOpts, orderId, ids)
}

// RemoveSubOwners is a paid mutator transaction binding the contract method 0xbf9a4067.
//
// Solidity: function removeSubOwners(addresses address[]) returns()
func (_IoContract *IoContractTransactor) RemoveSubOwners(opts *bind.TransactOpts, addresses []common.Address) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "removeSubOwners", addresses)
}

// RemoveSubOwners is a paid mutator transaction binding the contract method 0xbf9a4067.
//
// Solidity: function removeSubOwners(addresses address[]) returns()
func (_IoContract *IoContractSession) RemoveSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.RemoveSubOwners(&_IoContract.TransactOpts, addresses)
}

// RemoveSubOwners is a paid mutator transaction binding the contract method 0xbf9a4067.
//
// Solidity: function removeSubOwners(addresses address[]) returns()
func (_IoContract *IoContractTransactorSession) RemoveSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.RemoveSubOwners(&_IoContract.TransactOpts, addresses)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_IoContract *IoContractTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_IoContract *IoContractSession) RenounceOwnership() (*types.Transaction, error) {
	return _IoContract.Contract.RenounceOwnership(&_IoContract.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_IoContract *IoContractTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _IoContract.Contract.RenounceOwnership(&_IoContract.TransactOpts)
}

// SetBackendAddress is a paid mutator transaction binding the contract method 0x1815ce7d.
//
// Solidity: function setBackendAddress(newAddress address) returns()
func (_IoContract *IoContractTransactor) SetBackendAddress(opts *bind.TransactOpts, newAddress common.Address) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "setBackendAddress", newAddress)
}

// SetBackendAddress is a paid mutator transaction binding the contract method 0x1815ce7d.
//
// Solidity: function setBackendAddress(newAddress address) returns()
func (_IoContract *IoContractSession) SetBackendAddress(newAddress common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.SetBackendAddress(&_IoContract.TransactOpts, newAddress)
}

// SetBackendAddress is a paid mutator transaction binding the contract method 0x1815ce7d.
//
// Solidity: function setBackendAddress(newAddress address) returns()
func (_IoContract *IoContractTransactorSession) SetBackendAddress(newAddress common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.SetBackendAddress(&_IoContract.TransactOpts, newAddress)
}

// SetOrderType is a paid mutator transaction binding the contract method 0x72cda48a.
//
// Solidity: function setOrderType(id uint256, description string) returns()
func (_IoContract *IoContractTransactor) SetOrderType(opts *bind.TransactOpts, id *big.Int, description string) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "setOrderType", id, description)
}

// SetOrderType is a paid mutator transaction binding the contract method 0x72cda48a.
//
// Solidity: function setOrderType(id uint256, description string) returns()
func (_IoContract *IoContractSession) SetOrderType(id *big.Int, description string) (*types.Transaction, error) {
	return _IoContract.Contract.SetOrderType(&_IoContract.TransactOpts, id, description)
}

// SetOrderType is a paid mutator transaction binding the contract method 0x72cda48a.
//
// Solidity: function setOrderType(id uint256, description string) returns()
func (_IoContract *IoContractTransactorSession) SetOrderType(id *big.Int, description string) (*types.Transaction, error) {
	return _IoContract.Contract.SetOrderType(&_IoContract.TransactOpts, id, description)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(newOwner address) returns()
func (_IoContract *IoContractTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(newOwner address) returns()
func (_IoContract *IoContractSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.TransferOwnership(&_IoContract.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(newOwner address) returns()
func (_IoContract *IoContractTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _IoContract.Contract.TransferOwnership(&_IoContract.TransactOpts, newOwner)
}

// WithdrawProposal is a paid mutator transaction binding the contract method 0xe1f02ffa.
//
// Solidity: function withdrawProposal(id uint256) returns()
func (_IoContract *IoContractTransactor) WithdrawProposal(opts *bind.TransactOpts, id *big.Int) (*types.Transaction, error) {
	return _IoContract.contract.Transact(opts, "withdrawProposal", id)
}

// WithdrawProposal is a paid mutator transaction binding the contract method 0xe1f02ffa.
//
// Solidity: function withdrawProposal(id uint256) returns()
func (_IoContract *IoContractSession) WithdrawProposal(id *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.WithdrawProposal(&_IoContract.TransactOpts, id)
}

// WithdrawProposal is a paid mutator transaction binding the contract method 0xe1f02ffa.
//
// Solidity: function withdrawProposal(id uint256) returns()
func (_IoContract *IoContractTransactorSession) WithdrawProposal(id *big.Int) (*types.Transaction, error) {
	return _IoContract.Contract.WithdrawProposal(&_IoContract.TransactOpts, id)
}

// IoContractAddressAddedToWhiteListIterator is returned from FilterAddressAddedToWhiteList and is used to iterate over the raw logs and unpacked data for AddressAddedToWhiteList events raised by the IoContract contract.
type IoContractAddressAddedToWhiteListIterator struct {
	Event *IoContractAddressAddedToWhiteList // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractAddressAddedToWhiteListIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractAddressAddedToWhiteList)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractAddressAddedToWhiteList)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractAddressAddedToWhiteListIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractAddressAddedToWhiteListIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractAddressAddedToWhiteList represents a AddressAddedToWhiteList event raised by the IoContract contract.
type IoContractAddressAddedToWhiteList struct {
	UserAddress        common.Address
	AllowedTokenAmount *big.Int
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterAddressAddedToWhiteList is a free log retrieval operation binding the contract event 0x9e8465dbefda7fb214bd4b472dbb7074b89587fd8aa8537b0be6f1c05c75b2ee.
//
// Solidity: e AddressAddedToWhiteList(userAddress indexed address, allowedTokenAmount indexed uint256)
func (_IoContract *IoContractFilterer) FilterAddressAddedToWhiteList(opts *bind.FilterOpts, userAddress []common.Address, allowedTokenAmount []*big.Int) (*IoContractAddressAddedToWhiteListIterator, error) {

	var userAddressRule []interface{}
	for _, userAddressItem := range userAddress {
		userAddressRule = append(userAddressRule, userAddressItem)
	}
	var allowedTokenAmountRule []interface{}
	for _, allowedTokenAmountItem := range allowedTokenAmount {
		allowedTokenAmountRule = append(allowedTokenAmountRule, allowedTokenAmountItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "AddressAddedToWhiteList", userAddressRule, allowedTokenAmountRule)
	if err != nil {
		return nil, err
	}
	return &IoContractAddressAddedToWhiteListIterator{contract: _IoContract.contract, event: "AddressAddedToWhiteList", logs: logs, sub: sub}, nil
}

// WatchAddressAddedToWhiteList is a free log subscription operation binding the contract event 0x9e8465dbefda7fb214bd4b472dbb7074b89587fd8aa8537b0be6f1c05c75b2ee.
//
// Solidity: e AddressAddedToWhiteList(userAddress indexed address, allowedTokenAmount indexed uint256)
func (_IoContract *IoContractFilterer) WatchAddressAddedToWhiteList(opts *bind.WatchOpts, sink chan<- *IoContractAddressAddedToWhiteList, userAddress []common.Address, allowedTokenAmount []*big.Int) (event.Subscription, error) {

	var userAddressRule []interface{}
	for _, userAddressItem := range userAddress {
		userAddressRule = append(userAddressRule, userAddressItem)
	}
	var allowedTokenAmountRule []interface{}
	for _, allowedTokenAmountItem := range allowedTokenAmount {
		allowedTokenAmountRule = append(allowedTokenAmountRule, allowedTokenAmountItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "AddressAddedToWhiteList", userAddressRule, allowedTokenAmountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractAddressAddedToWhiteList)
				if err := _IoContract.contract.UnpackLog(event, "AddressAddedToWhiteList", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractNewOrderIterator is returned from FilterNewOrder and is used to iterate over the raw logs and unpacked data for NewOrder events raised by the IoContract contract.
type IoContractNewOrderIterator struct {
	Event *IoContractNewOrder // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractNewOrderIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractNewOrder)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractNewOrder)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractNewOrderIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractNewOrderIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractNewOrder represents a NewOrder event raised by the IoContract contract.
type IoContractNewOrder struct {
	From      common.Address
	Price     *big.Int
	EthAmount *big.Int
	TknAmount *big.Int
	TypeId    *big.Int
	Metadata  string
	OrderId   *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterNewOrder is a free log retrieval operation binding the contract event 0x4a34279b3cb8e1cfa27650e508a7b0d79b9022740507f3ce83aee0026d7547d8.
//
// Solidity: e NewOrder(from indexed address, price indexed uint256, ethAmount indexed uint256, tknAmount uint256, typeId uint256, metadata string, orderId uint256)
func (_IoContract *IoContractFilterer) FilterNewOrder(opts *bind.FilterOpts, from []common.Address, price []*big.Int, ethAmount []*big.Int) (*IoContractNewOrderIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var priceRule []interface{}
	for _, priceItem := range price {
		priceRule = append(priceRule, priceItem)
	}
	var ethAmountRule []interface{}
	for _, ethAmountItem := range ethAmount {
		ethAmountRule = append(ethAmountRule, ethAmountItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "NewOrder", fromRule, priceRule, ethAmountRule)
	if err != nil {
		return nil, err
	}
	return &IoContractNewOrderIterator{contract: _IoContract.contract, event: "NewOrder", logs: logs, sub: sub}, nil
}

// WatchNewOrder is a free log subscription operation binding the contract event 0x4a34279b3cb8e1cfa27650e508a7b0d79b9022740507f3ce83aee0026d7547d8.
//
// Solidity: e NewOrder(from indexed address, price indexed uint256, ethAmount indexed uint256, tknAmount uint256, typeId uint256, metadata string, orderId uint256)
func (_IoContract *IoContractFilterer) WatchNewOrder(opts *bind.WatchOpts, sink chan<- *IoContractNewOrder, from []common.Address, price []*big.Int, ethAmount []*big.Int) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var priceRule []interface{}
	for _, priceItem := range price {
		priceRule = append(priceRule, priceItem)
	}
	var ethAmountRule []interface{}
	for _, ethAmountItem := range ethAmount {
		ethAmountRule = append(ethAmountRule, ethAmountItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "NewOrder", fromRule, priceRule, ethAmountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractNewOrder)
				if err := _IoContract.contract.UnpackLog(event, "NewOrder", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractNewOrderTypeIterator is returned from FilterNewOrderType and is used to iterate over the raw logs and unpacked data for NewOrderType events raised by the IoContract contract.
type IoContractNewOrderTypeIterator struct {
	Event *IoContractNewOrderType // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractNewOrderTypeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractNewOrderType)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractNewOrderType)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractNewOrderTypeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractNewOrderTypeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractNewOrderType represents a NewOrderType event raised by the IoContract contract.
type IoContractNewOrderType struct {
	Id          *big.Int
	Desctiption string
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterNewOrderType is a free log retrieval operation binding the contract event 0xdfd316104bf24f7956e6d696e6173feefc619f5b4365679a70f5e6f9a38f46ea.
//
// Solidity: e NewOrderType(id indexed uint256, desctiption string)
func (_IoContract *IoContractFilterer) FilterNewOrderType(opts *bind.FilterOpts, id []*big.Int) (*IoContractNewOrderTypeIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "NewOrderType", idRule)
	if err != nil {
		return nil, err
	}
	return &IoContractNewOrderTypeIterator{contract: _IoContract.contract, event: "NewOrderType", logs: logs, sub: sub}, nil
}

// WatchNewOrderType is a free log subscription operation binding the contract event 0xdfd316104bf24f7956e6d696e6173feefc619f5b4365679a70f5e6f9a38f46ea.
//
// Solidity: e NewOrderType(id indexed uint256, desctiption string)
func (_IoContract *IoContractFilterer) WatchNewOrderType(opts *bind.WatchOpts, sink chan<- *IoContractNewOrderType, id []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "NewOrderType", idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractNewOrderType)
				if err := _IoContract.contract.UnpackLog(event, "NewOrderType", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractNewSubOwnerIterator is returned from FilterNewSubOwner and is used to iterate over the raw logs and unpacked data for NewSubOwner events raised by the IoContract contract.
type IoContractNewSubOwnerIterator struct {
	Event *IoContractNewSubOwner // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractNewSubOwnerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractNewSubOwner)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractNewSubOwner)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractNewSubOwnerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractNewSubOwnerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractNewSubOwner represents a NewSubOwner event raised by the IoContract contract.
type IoContractNewSubOwner struct {
	SubOwner common.Address
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterNewSubOwner is a free log retrieval operation binding the contract event 0xd0c7bd0476da2f9fe0cc58fd97b202c056d63a9a341d9bce34706a3efb2ca58d.
//
// Solidity: e NewSubOwner(subOwner address)
func (_IoContract *IoContractFilterer) FilterNewSubOwner(opts *bind.FilterOpts) (*IoContractNewSubOwnerIterator, error) {

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "NewSubOwner")
	if err != nil {
		return nil, err
	}
	return &IoContractNewSubOwnerIterator{contract: _IoContract.contract, event: "NewSubOwner", logs: logs, sub: sub}, nil
}

// WatchNewSubOwner is a free log subscription operation binding the contract event 0xd0c7bd0476da2f9fe0cc58fd97b202c056d63a9a341d9bce34706a3efb2ca58d.
//
// Solidity: e NewSubOwner(subOwner address)
func (_IoContract *IoContractFilterer) WatchNewSubOwner(opts *bind.WatchOpts, sink chan<- *IoContractNewSubOwner) (event.Subscription, error) {

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "NewSubOwner")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractNewSubOwner)
				if err := _IoContract.contract.UnpackLog(event, "NewSubOwner", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractOrderProcessedIterator is returned from FilterOrderProcessed and is used to iterate over the raw logs and unpacked data for OrderProcessed events raised by the IoContract contract.
type IoContractOrderProcessedIterator struct {
	Event *IoContractOrderProcessed // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractOrderProcessedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractOrderProcessed)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractOrderProcessed)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractOrderProcessedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractOrderProcessedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractOrderProcessed represents a OrderProcessed event raised by the IoContract contract.
type IoContractOrderProcessed struct {
	Id   *big.Int
	Rest *big.Int
	Raw  types.Log // Blockchain specific contextual infos
}

// FilterOrderProcessed is a free log retrieval operation binding the contract event 0xa72954bf82a02979408df189cd39e5f8ced72a87fc687cbfb63201dcd0c95c32.
//
// Solidity: e OrderProcessed(id indexed uint256, rest uint256)
func (_IoContract *IoContractFilterer) FilterOrderProcessed(opts *bind.FilterOpts, id []*big.Int) (*IoContractOrderProcessedIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "OrderProcessed", idRule)
	if err != nil {
		return nil, err
	}
	return &IoContractOrderProcessedIterator{contract: _IoContract.contract, event: "OrderProcessed", logs: logs, sub: sub}, nil
}

// WatchOrderProcessed is a free log subscription operation binding the contract event 0xa72954bf82a02979408df189cd39e5f8ced72a87fc687cbfb63201dcd0c95c32.
//
// Solidity: e OrderProcessed(id indexed uint256, rest uint256)
func (_IoContract *IoContractFilterer) WatchOrderProcessed(opts *bind.WatchOpts, sink chan<- *IoContractOrderProcessed, id []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "OrderProcessed", idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractOrderProcessed)
				if err := _IoContract.contract.UnpackLog(event, "OrderProcessed", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the IoContract contract.
type IoContractOwnershipTransferredIterator struct {
	Event *IoContractOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractOwnershipTransferred represents a OwnershipTransferred event raised by the IoContract contract.
type IoContractOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: e OwnershipTransferred(previousOwner indexed address, newOwner indexed address)
func (_IoContract *IoContractFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*IoContractOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &IoContractOwnershipTransferredIterator{contract: _IoContract.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: e OwnershipTransferred(previousOwner indexed address, newOwner indexed address)
func (_IoContract *IoContractFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *IoContractOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractOwnershipTransferred)
				if err := _IoContract.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractProposalCompletedIterator is returned from FilterProposalCompleted and is used to iterate over the raw logs and unpacked data for ProposalCompleted events raised by the IoContract contract.
type IoContractProposalCompletedIterator struct {
	Event *IoContractProposalCompleted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractProposalCompletedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractProposalCompleted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractProposalCompleted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractProposalCompletedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractProposalCompletedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractProposalCompleted represents a ProposalCompleted event raised by the IoContract contract.
type IoContractProposalCompleted struct {
	Id          *big.Int
	TokenAmount *big.Int
	OrderId     *big.Int
	EtherAmount *big.Int
	IsActive    bool
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterProposalCompleted is a free log retrieval operation binding the contract event 0x6a656d34dcc18dc84f88a839cf55a39e73969b663381896230bf831ca13c9ac8.
//
// Solidity: e ProposalCompleted(id indexed uint256, tokenAmount indexed uint256, orderId indexed uint256, etherAmount uint256, isActive bool)
func (_IoContract *IoContractFilterer) FilterProposalCompleted(opts *bind.FilterOpts, id []*big.Int, tokenAmount []*big.Int, orderId []*big.Int) (*IoContractProposalCompletedIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var tokenAmountRule []interface{}
	for _, tokenAmountItem := range tokenAmount {
		tokenAmountRule = append(tokenAmountRule, tokenAmountItem)
	}
	var orderIdRule []interface{}
	for _, orderIdItem := range orderId {
		orderIdRule = append(orderIdRule, orderIdItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "ProposalCompleted", idRule, tokenAmountRule, orderIdRule)
	if err != nil {
		return nil, err
	}
	return &IoContractProposalCompletedIterator{contract: _IoContract.contract, event: "ProposalCompleted", logs: logs, sub: sub}, nil
}

// WatchProposalCompleted is a free log subscription operation binding the contract event 0x6a656d34dcc18dc84f88a839cf55a39e73969b663381896230bf831ca13c9ac8.
//
// Solidity: e ProposalCompleted(id indexed uint256, tokenAmount indexed uint256, orderId indexed uint256, etherAmount uint256, isActive bool)
func (_IoContract *IoContractFilterer) WatchProposalCompleted(opts *bind.WatchOpts, sink chan<- *IoContractProposalCompleted, id []*big.Int, tokenAmount []*big.Int, orderId []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var tokenAmountRule []interface{}
	for _, tokenAmountItem := range tokenAmount {
		tokenAmountRule = append(tokenAmountRule, tokenAmountItem)
	}
	var orderIdRule []interface{}
	for _, orderIdItem := range orderId {
		orderIdRule = append(orderIdRule, orderIdItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "ProposalCompleted", idRule, tokenAmountRule, orderIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractProposalCompleted)
				if err := _IoContract.contract.UnpackLog(event, "ProposalCompleted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractProposalCreatedIterator is returned from FilterProposalCreated and is used to iterate over the raw logs and unpacked data for ProposalCreated events raised by the IoContract contract.
type IoContractProposalCreatedIterator struct {
	Event *IoContractProposalCreated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractProposalCreatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractProposalCreated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractProposalCreated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractProposalCreatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractProposalCreatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractProposalCreated represents a ProposalCreated event raised by the IoContract contract.
type IoContractProposalCreated struct {
	From   common.Address
	Price  *big.Int
	Amount *big.Int
	Id     *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterProposalCreated is a free log retrieval operation binding the contract event 0xc9b07488d313faaa6bc24f782e87a9511347a63829422a6778a8ef9d8b636806.
//
// Solidity: e ProposalCreated(from indexed address, price indexed uint256, amount indexed uint256, id uint256)
func (_IoContract *IoContractFilterer) FilterProposalCreated(opts *bind.FilterOpts, from []common.Address, price []*big.Int, amount []*big.Int) (*IoContractProposalCreatedIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var priceRule []interface{}
	for _, priceItem := range price {
		priceRule = append(priceRule, priceItem)
	}
	var amountRule []interface{}
	for _, amountItem := range amount {
		amountRule = append(amountRule, amountItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "ProposalCreated", fromRule, priceRule, amountRule)
	if err != nil {
		return nil, err
	}
	return &IoContractProposalCreatedIterator{contract: _IoContract.contract, event: "ProposalCreated", logs: logs, sub: sub}, nil
}

// WatchProposalCreated is a free log subscription operation binding the contract event 0xc9b07488d313faaa6bc24f782e87a9511347a63829422a6778a8ef9d8b636806.
//
// Solidity: e ProposalCreated(from indexed address, price indexed uint256, amount indexed uint256, id uint256)
func (_IoContract *IoContractFilterer) WatchProposalCreated(opts *bind.WatchOpts, sink chan<- *IoContractProposalCreated, from []common.Address, price []*big.Int, amount []*big.Int) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var priceRule []interface{}
	for _, priceItem := range price {
		priceRule = append(priceRule, priceItem)
	}
	var amountRule []interface{}
	for _, amountItem := range amount {
		amountRule = append(amountRule, amountItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "ProposalCreated", fromRule, priceRule, amountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractProposalCreated)
				if err := _IoContract.contract.UnpackLog(event, "ProposalCreated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractProposalWithdrawnIterator is returned from FilterProposalWithdrawn and is used to iterate over the raw logs and unpacked data for ProposalWithdrawn events raised by the IoContract contract.
type IoContractProposalWithdrawnIterator struct {
	Event *IoContractProposalWithdrawn // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractProposalWithdrawnIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractProposalWithdrawn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractProposalWithdrawn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractProposalWithdrawnIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractProposalWithdrawnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractProposalWithdrawn represents a ProposalWithdrawn event raised by the IoContract contract.
type IoContractProposalWithdrawn struct {
	Amount   *big.Int
	Id       *big.Int
	IsActive bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterProposalWithdrawn is a free log retrieval operation binding the contract event 0x6f8327249e2509cc2b10c7d6ac7a6dbbaa90b3519d444c21a959fbfc2e6ebbfa.
//
// Solidity: e ProposalWithdrawn(amount indexed uint256, id indexed uint256, isActive bool)
func (_IoContract *IoContractFilterer) FilterProposalWithdrawn(opts *bind.FilterOpts, amount []*big.Int, id []*big.Int) (*IoContractProposalWithdrawnIterator, error) {

	var amountRule []interface{}
	for _, amountItem := range amount {
		amountRule = append(amountRule, amountItem)
	}
	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "ProposalWithdrawn", amountRule, idRule)
	if err != nil {
		return nil, err
	}
	return &IoContractProposalWithdrawnIterator{contract: _IoContract.contract, event: "ProposalWithdrawn", logs: logs, sub: sub}, nil
}

// WatchProposalWithdrawn is a free log subscription operation binding the contract event 0x6f8327249e2509cc2b10c7d6ac7a6dbbaa90b3519d444c21a959fbfc2e6ebbfa.
//
// Solidity: e ProposalWithdrawn(amount indexed uint256, id indexed uint256, isActive bool)
func (_IoContract *IoContractFilterer) WatchProposalWithdrawn(opts *bind.WatchOpts, sink chan<- *IoContractProposalWithdrawn, amount []*big.Int, id []*big.Int) (event.Subscription, error) {

	var amountRule []interface{}
	for _, amountItem := range amount {
		amountRule = append(amountRule, amountItem)
	}
	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "ProposalWithdrawn", amountRule, idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractProposalWithdrawn)
				if err := _IoContract.contract.UnpackLog(event, "ProposalWithdrawn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IoContractRemovedSubOwnerIterator is returned from FilterRemovedSubOwner and is used to iterate over the raw logs and unpacked data for RemovedSubOwner events raised by the IoContract contract.
type IoContractRemovedSubOwnerIterator struct {
	Event *IoContractRemovedSubOwner // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IoContractRemovedSubOwnerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IoContractRemovedSubOwner)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IoContractRemovedSubOwner)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IoContractRemovedSubOwnerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IoContractRemovedSubOwnerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IoContractRemovedSubOwner represents a RemovedSubOwner event raised by the IoContract contract.
type IoContractRemovedSubOwner struct {
	SubOwner common.Address
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterRemovedSubOwner is a free log retrieval operation binding the contract event 0xc4adbb71fb21387c352881f205be328ff9125ae4c658ed8dcbe960a47dbf515a.
//
// Solidity: e RemovedSubOwner(subOwner address)
func (_IoContract *IoContractFilterer) FilterRemovedSubOwner(opts *bind.FilterOpts) (*IoContractRemovedSubOwnerIterator, error) {

	logs, sub, err := _IoContract.contract.FilterLogs(opts, "RemovedSubOwner")
	if err != nil {
		return nil, err
	}
	return &IoContractRemovedSubOwnerIterator{contract: _IoContract.contract, event: "RemovedSubOwner", logs: logs, sub: sub}, nil
}

// WatchRemovedSubOwner is a free log subscription operation binding the contract event 0xc4adbb71fb21387c352881f205be328ff9125ae4c658ed8dcbe960a47dbf515a.
//
// Solidity: e RemovedSubOwner(subOwner address)
func (_IoContract *IoContractFilterer) WatchRemovedSubOwner(opts *bind.WatchOpts, sink chan<- *IoContractRemovedSubOwner) (event.Subscription, error) {

	logs, sub, err := _IoContract.contract.WatchLogs(opts, "RemovedSubOwner")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IoContractRemovedSubOwner)
				if err := _IoContract.contract.UnpackLog(event, "RemovedSubOwner", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}
