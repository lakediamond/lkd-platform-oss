package crm

type DataInterface interface {
	GetRelatedKYCFlags() []string
	GetToken() string
	GetPerson() *Person
	GetDeal() *Deal
}

//Person contains information about CRM person keys
type Person struct {
	LastNameValue                  string `crmKey:"(LD_CF_1)"`
	FirstNameValue                 string `crmKey:"(LD_CF_2)"`
	BirthDateValue                 string `crmKey:"(LD_CF_3)"`
	ResidentialAddressValue        string `crmKey:"(LD_CF_5)"`
	EthereumAddressValue           string `crmKey:"(LD_CF_8)"`
	BankNameValue                  string `crmKey:"(LD_CF_9)"`
	BankAddressValue               string `crmKey:"(LD_CF_10)"`
	BankIbanValue                  string `crmKey:"(LD_CF_11)"`
	AgeOver18VerifiedValue         string `crmKey:"(LD_FL_2)"`
	EmailUserVerificationValue     string `crmKey:"(LD_FL_3)"`
	TelephoneUserVerificationValue string `crmKey:"(LD_FL_4)"`
}

//Deal contains information about CRM deal keys
type Deal struct {
	TelephoneFromAllowedCountryValue               string `crmKey:"(LD_FL_4)"`
	NameSanctionScreeningValue                     string `crmKey:"(LD_FL_1)"`
	ResidentialAddressFromAllowedCountryValue      string `crmKey:"(LD_FL_5)"`
	IDDocumentBitaccessVerificationValue           string `crmKey:"(LD_FL_6_a)"`
	IDDocumentBitaccessVerificationFailureStage    int64
	IDDocumentFromAllowedCountryValue              string `crmKey:"(LD_FL_6_b)"`
	IDDocumentFromAllowedCountryFailureStage       int64
	SelfieBitaccessVerificationValue               string `crmKey:"(LD_FL_7)"`
	SelfieBitaccessVerificationFailureStage        int64
	EthereumAddressScorechainScreeningValue        string `crmKey:"(LD_FL_8)"`
	EthereumAddressScorechainScreeningFailureStage int64
	IPConnectionFromAllowedCountryValue            string `crmKey:"(LD_FL_9)"`
	MultipleFailuresStage                          int64
}
