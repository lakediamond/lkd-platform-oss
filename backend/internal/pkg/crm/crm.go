package crm

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/pkg/enviroment"
	"lkd-platform-backend/pkg/httputil"
)

type crmDataResponse struct {
	Error          string `json:"error"`
	AdditionalData struct {
		Pagination struct {
			Limit                 int  `json:"limit"`
			MoreItemsInCollection bool `json:"more_items_in_collection"`
			Start                 int  `json:"start"`
		} `json:"pagination"`
	} `json:"additional_data"`
	Data []struct {
		ActiveFlag         bool        `json:"active_flag"`
		AddTime            string      `json:"add_time"`
		AddVisibleFlag     bool        `json:"add_visible_flag"`
		BulkEditAllowed    bool        `json:"bulk_edit_allowed"`
		DetailsVisibleFlag bool        `json:"details_visible_flag"`
		EditFlag           bool        `json:"edit_flag"`
		FieldType          string      `json:"field_type"`
		FilteringAllowed   bool        `json:"filtering_allowed"`
		ID                 int         `json:"id"`
		ImportantFlag      bool        `json:"important_flag"`
		IndexVisibleFlag   bool        `json:"index_visible_flag"`
		Key                string      `json:"key"`
		Link               string      `json:"link"`
		Name               string      `json:"name"`
		OrderNr            int         `json:"order_nr"`
		PicklistData       interface{} `json:"picklist_data"`
		SearchableFlag     bool        `json:"searchable_flag"`
		SortableFlag       bool        `json:"sortable_flag"`
		UpdateTime         string      `json:"update_time"`
		UseField           string      `json:"use_field"`
	} `json:"data"`
	Success bool `json:"success"`
}

//Data contains all CRM data information
type Data struct {
	Token  string
	Person *Person
	Deal   *Deal
}

//InitCRMData initializing new CRM data
func InitData(token string) *Data {
	var crmData Data

	crmData.Token = token

	person, err := initPerson(crmData.Token)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	crmData.Person = person

	deal, err := initDeal(crmData.Token)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}
	crmData.Deal = deal

	return &crmData
}

func initPerson(token string) (*Person, error) {
	var crmPerson Person

	url := fmt.Sprintf("https://api.pipedrive.com/v1/personFields?api_token=%s", token)

	val := reflect.ValueOf(&crmPerson).Elem()
	err := loadDataFromCRM(url, &val)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	return &crmPerson, nil
}

func initDeal(token string) (*Deal, error) {
	var crmDeal Deal

	err := loadDealDataFromConfig(&crmDeal)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("https://api.pipedrive.com/v1/dealFields?api_token=%s", token)
	val := reflect.ValueOf(&crmDeal).Elem()
	err = loadDataFromCRM(url, &val)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	return &crmDeal, nil
}

func loadDataFromCRM(crmURL string, val *reflect.Value) error {
	crmDataResponse, err := getCRMData(crmURL)
	if err != nil {
		return err
	}

	for _, crmDataResp := range crmDataResponse.Data {
		for i := 0; i < val.NumField(); i++ {
			field := val.Field(i)
			typeField := val.Type().Field(i)

			key := typeField.Tag.Get("crmKey")
			if len(key) != 0 && strings.Contains(crmDataResp.Name, key) {
				field.SetString(crmDataResp.Key)
				continue
			}
		}
	}

	err = checkForEmptyCRMFields(val)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}
	return nil
}

func loadDealDataFromConfig(crmDeal *Deal) error {
	myEnv := enviroment.GetEnv()

	IDDocumentBitaccessVerificationFailureStageParsed, idDocParseErr := strconv.ParseInt(myEnv["CRM_ID_DOCUMENT_FAILURE_STAGE"], 10, 0)
	if idDocParseErr != nil {
		return idDocParseErr
	}
	crmDeal.IDDocumentBitaccessVerificationFailureStage = IDDocumentBitaccessVerificationFailureStageParsed

	IDDocumentFromAllowedCountryFailureStageParsed, idDocAllowedCountryParseErr := strconv.ParseInt(myEnv["CRM_ID_RESTRICTED_COUNTRY_FAILURE_STAGE"], 10, 0)
	if idDocAllowedCountryParseErr != nil {
		return idDocAllowedCountryParseErr
	}
	crmDeal.IDDocumentFromAllowedCountryFailureStage = IDDocumentFromAllowedCountryFailureStageParsed

	SelfieBitaccessVerificationFailureStageParsed, selfieBtcsParseErr := strconv.ParseInt(myEnv["CRM_SELFIE_FAILURE_STAGE"], 10, 0)
	if selfieBtcsParseErr != nil {
		return selfieBtcsParseErr
	}
	crmDeal.SelfieBitaccessVerificationFailureStage = SelfieBitaccessVerificationFailureStageParsed

	EthereumAddressScorechainScreeningFailureStageParsed, ethScreeningParseErr := strconv.ParseInt(myEnv["CRM_ETH_ADDRESS_SCREENING_FAILURE_STAGE"], 10, 0)
	if ethScreeningParseErr != nil {
		return ethScreeningParseErr
	}
	crmDeal.EthereumAddressScorechainScreeningFailureStage = EthereumAddressScorechainScreeningFailureStageParsed

	MultipleFailuresStageParsed, multiFailuresParseErr := strconv.ParseInt(myEnv["CRM_MULTIPLE_FAILURES_STAGE"], 10, 0)
	if multiFailuresParseErr != nil {
		return multiFailuresParseErr
	}
	crmDeal.MultipleFailuresStage = MultipleFailuresStageParsed

	return nil
}

func getCRMData(url string) (*crmDataResponse, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	body := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	var crmResp crmDataResponse
	err = json.Unmarshal(body, &crmResp)
	if err != nil {
		return nil, err
	}

	if !crmResp.Success {
		log.Fatal().Msg(crmResp.Error)
	}
	return &crmResp, nil
}

func checkForEmptyCRMFields(val *reflect.Value) error {
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		typeField := val.Type().Field(i)

		key := typeField.Tag.Get("crmKey")

		if len(key) != 0 && len(field.String()) == 0 {
			return errors.New("empty " + typeField.Name)
		}
	}
	return nil
}

//GetRelatedKYCFlags returns all CRM custom flags
func (d *Data) GetRelatedKYCFlags() []string {
	return []string{d.Person.AgeOver18VerifiedValue, d.Person.EmailUserVerificationValue, d.Person.TelephoneUserVerificationValue, d.Deal.TelephoneFromAllowedCountryValue}
}

func (d *Data) GetToken() string {
	return d.Token
}

func (d *Data) GetPerson() *Person {
	return d.Person
}

func (d *Data) GetDeal() *Deal {
	return d.Deal
}
