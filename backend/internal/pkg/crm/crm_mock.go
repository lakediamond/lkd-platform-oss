package crm

//DataInitializerMock is a mock structure
type DataInitializerMock struct {
	Token  string
	Person *Person
	Deal   *Deal
}

//InitCRMData initializing mock data
func InitDataMock() *DataInitializerMock {
	return &DataInitializerMock{
		Deal: &Deal{
			TelephoneFromAllowedCountryValue:               "TelephoneFromAllowedCountryValue",
			NameSanctionScreeningValue:                     "NameSanctionScreeningValue",
			ResidentialAddressFromAllowedCountryValue:      "ResidencialAddressFromAllowedCountryValue",
			IDDocumentBitaccessVerificationValue:           "IDDocumentBitaccessVerificationValue",
			IDDocumentBitaccessVerificationFailureStage:    20,
			IDDocumentFromAllowedCountryValue:              "IDDocumentFromAllowedCountryValue",
			IDDocumentFromAllowedCountryFailureStage:       24,
			SelfieBitaccessVerificationValue:               "SelfieBitaccessVerificationValue",
			SelfieBitaccessVerificationFailureStage:        22,
			EthereumAddressScorechainScreeningValue:        "EthereumAddressScorechainScreeningValue",
			EthereumAddressScorechainScreeningFailureStage: 21,
			IPConnectionFromAllowedCountryValue:            "IPConnectionFromAllowedCountryValue",
			MultipleFailuresStage:                          26,
		},
		Person: &Person{
			LastNameValue:                  "LastNameValue",
			FirstNameValue:                 "FirstNameValue",
			BirthDateValue:                 "BirthDateValue",
			ResidentialAddressValue:        "ResidentialAddressValue",
			EthereumAddressValue:           "EthereumAddressValue",
			BankNameValue:                  "BankNameValue",
			BankAddressValue:               "BankAddressValue",
			AgeOver18VerifiedValue:         "AgeOver18VerifiedValue",
			EmailUserVerificationValue:     "EmailUserVerificationValue",
			TelephoneUserVerificationValue: "TelephoneUserVerificationValue",
		},
	}
}

//GetRelatedKYCFlags returns all CRM custom flags
func (d *DataInitializerMock) GetRelatedKYCFlags() []string {
	return []string{d.Person.AgeOver18VerifiedValue, d.Person.EmailUserVerificationValue, d.Person.TelephoneUserVerificationValue, d.Deal.TelephoneFromAllowedCountryValue}
}

func (d *DataInitializerMock) GetToken() string {
	return d.Token
}

func (d *DataInitializerMock) GetPerson() *Person {
	return d.Person
}

func (d *DataInitializerMock) GetDeal() *Deal {
	return d.Deal
}
