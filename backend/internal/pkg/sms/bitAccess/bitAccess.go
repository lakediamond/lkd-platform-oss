package bitAccess

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/pkg/httputil"
)

//Sender bit
type Sender struct {
	apiURL    string
	apiKey    string
	apiSecret string
}

//NewSender create new instance of bit access sms message sender client
func NewSender(apiURL string, apiKey string, apiSecret string) *Sender {
	return &Sender{
		apiKey:    apiKey,
		apiSecret: apiSecret,
		apiURL:    apiURL,
	}
}

//Send send sms message to phone number
func (connector *Sender) Send(phoneNumber string, message string) error {
	if err := makeSMSGatewayCall(
		connector.apiURL,
		connector.apiKey,
		connector.apiSecret,
		phoneNumber,
		message); err != nil {
		return err
	}

	log.Debug().Msgf("SMS to phone %s with code %s just sent", phoneNumber, message)
	return nil
}

func makeSMSGatewayCall(url, key, secret, phone string, text string) error {
	pvPayload := map[string]interface{}{
		"to":   phone,
		"body": text,
	}
	tmp, _ := json.Marshal(pvPayload)
	requestData := bytes.NewReader(tmp)
	client := &http.Client{}
	req, _ := http.NewRequest("POST", fmt.Sprintf("%s/sms/send", url), requestData)
	nonce, signature := makeAuthHeader([]byte(secret), tmp)
	req.Header.Add("authkey", key)
	req.Header.Add("content-type", "application/json")
	req.Header.Add("authsignature", signature)
	req.Header.Add("authnonce", nonce)
	req.Header.Add("authversion", "2")
	resp, err := client.Do(req)
	if err != nil {
		log.Debug().Err(err).Msg(err.Error())
		return err
	}
	// if resp.StatusCode != 200 {
	// 	return errPhoneVerificationSMSGatewayProblem
	// }
	log.Debug().Msgf("Signature %s", signature)
	log.Debug().Msgf("Nonce %s", nonce)
	body := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	var res string

	json.Unmarshal(body, &res)
	log.Debug().Msgf("Result %s", res)
	log.Debug().Msgf("Status %s", resp.Status)
	return nil
}

func makeAuthHeader(secret, data []byte) (string, string) {
	nonce := strconv.FormatInt(time.Now().Add(time.Minute*time.Duration(10)).UTC().Unix(), 10)
	key := secret
	h := hmac.New(sha256.New, key)
	h.Write([]byte(nonce))
	h.Write(data)
	r := hex.EncodeToString(h.Sum(nil))
	return nonce, r
}
