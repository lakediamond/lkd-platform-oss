package mailjet

type EmailConfig struct {
	TemplateID int                    `json:"templateID"`
	FromEmail  string                 `json:"fromEmail"`
	FromName   string                 `json:"fromName"`
	ToEmail    string                 `json:"toEmail"`
	ToName     string                 `json:"toName"`
	Variables  map[string]interface{} `json:"variables"`
}

func (cfg *EmailConfig) CopyVariables() map[string]interface{} {
	c := map[string]interface{}{}

	for key, value := range cfg.Variables {
		c[key] = value
	}

	return c
}

func (cfg *EmailConfig) GetCopy() *EmailConfig {
	return &EmailConfig{
		TemplateID: cfg.TemplateID,
		FromEmail:  cfg.FromEmail,
		FromName:   cfg.FromName,
		ToEmail:    cfg.ToEmail,
		ToName:     cfg.ToName,
		Variables:  cfg.CopyVariables(),
	}
}
