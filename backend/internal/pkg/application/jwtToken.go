package application

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/enviroment"
	"lkd-platform-backend/pkg/httputil"
)

func (app *Application) RequireTokenAuthentication(h http.Handler) http.Handler {
	e := enviroment.GetEnv()
	if e["ENV"] == enviroment.TestEnv {
		return app.requireTokenAuthenticationMock(h)
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		authHeader := r.Header.Get("Authorization")
		if len(authHeader) != 0 {
			authHeaderParts := strings.Split(authHeader, " ")
			if len(authHeaderParts) != 2 {
				err := fmt.Errorf("invalid auth header value - %s", authHeader)
				w.WriteHeader(http.StatusUnauthorized)
				httputil.WriteErrorMsg(w, err)
				return
			}

			accessToken := authHeaderParts[1]

			tokenDetails, _, err := app.IAMClient.GetOAuthClient().AdminApi.IntrospectOAuth2Token(accessToken, "")
			if err != nil {
				log.Debug().Msg("failed to introspect access token")
				w.WriteHeader(http.StatusUnauthorized)
				httputil.WriteErrorMsg(w, errors.New("failed to introspect access token"))
				return
			}

			if !tokenDetails.Active {
				err := fmt.Errorf("AccessToken is inactive or has been revoked  - %v", tokenDetails)
				w.WriteHeader(http.StatusUnauthorized)
				httputil.WriteErrorMsg(w, err)
				return
			}

			user, err := app.Repo.User.GetUserByEmail(tokenDetails.Sub)
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				httputil.WriteErrorMsg(w, err)
				return
			}

			SetUser(r, user)

			h.ServeHTTP(w, r)
			return
		}

		log.Info().Msg(fmt.Sprintf("Continue with general LKD auth flow, because no auth header value provided - %s", authHeader))
		oauthToken, err := GetOAuthTokenFromSession(r)
		if err != nil {
			log.Debug().Err(err).Msg("GetOAuthTokenFromSession error")
			w.WriteHeader(http.StatusUnauthorized)
			httputil.WriteErrorMsg(w, err)
			return
		}

		tokenDetails, _, err := app.IAMClient.GetOAuthClient().AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
		if err != nil {
			log.Debug().Msg("failed to introspect access token")
			w.WriteHeader(http.StatusUnauthorized)
			httputil.WriteErrorMsg(w, errors.New("failed to introspect access token"))
			return
		}

		if tokenDetails.Active {
			user, err := app.Repo.User.GetUserByEmail(tokenDetails.Sub)
			if err != nil {
				log.Debug().Err(err).Msg("Failed to get user record from database")
				w.WriteHeader(http.StatusUnauthorized)
				httputil.WriteErrorMsg(w, err)
				return
			}

			SetTokenDetails(r, oauthToken)
			SetUser(r, user)

			h.ServeHTTP(w, r)
		} else {
			err = errors.New("OAuth token is inactive, complete another auth flow roundLoop to get new one")
			httputil.RemoveCookies(w)
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusUnauthorized)
			httputil.WriteErrorMsg(w, err)
			return
		}
	})
}

func (app *Application) requireTokenAuthenticationMock(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		user, _ := app.Repo.User.GetUserByEmail(r.Header.Get("user_email"))
		SetUser(r, user)
		h.ServeHTTP(w, r)
	})
}

func (app *Application) RequireAdminAccess(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		user := GetUser(r)

		if user.Role != models.AdminRole {
			log.Debug().Msg("require admin permissions")
			w.WriteHeader(http.StatusForbidden)
			return
		}

		h.ServeHTTP(w, r)
	})
}
