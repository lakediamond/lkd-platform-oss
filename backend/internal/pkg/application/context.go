package application

import (
	"fmt"
	"net/http"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	"github.com/ory/hydra/sdk/go/hydra/swagger"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//GetApplicationContext returns application from http request
func GetApplicationContext(r *http.Request) *Application {
	return r.Context().Value(applicationKey).(*Application)
}

//GetRequestParams returns params from http request context
func GetRequestParams(r *http.Request) httprouter.Params {
	return httprouter.ParamsFromContext(r.Context())
}

//HandlingContext contains needed handling context
type HandlingContext struct {
	Configuration *Application
}

func SetUser(r *http.Request, user models.User) {
	context.Set(r, "user", user)
}

func GetUser(r *http.Request) models.User {
	return context.Get(r, "user").(models.User)
}

func UpdateSessionWithOAuthToken(w http.ResponseWriter, r *http.Request, token *oauth2.Token) error {
	app := GetApplicationContext(r)

	session, err := app.IAMClient.GetCookies().Get(r, "lkd-platform")
	if err != nil {
		log.Debug().Err(err).Msg("Failed to get session from request")
		return err
	}

	session.Values["oauth-token"] = token
	if err = session.Save(r, w); err != nil {
		log.Debug().Err(err).Msg("Failed to update session with OAuth token value")
		return err
	}

	return nil
}

func GetOAuthTokenFromSession(r *http.Request) (*oauth2.Token, error) {
	app := GetApplicationContext(r)

	session, err := app.IAMClient.GetCookies().Get(r, "lkd-platform")
	if err != nil {
		log.Debug().Err(err).Msg("get session error")
		return nil, err
	}

	log.Debug().Msgf("session: %v", session)

	tokenSessionRecord := session.Values["oauth-token"]

	log.Debug().Msgf("tokenSessionRecord: %v", tokenSessionRecord)
	if tokenSessionRecord == nil {
		return nil, errors.New("Failed to get token from session cookie, [recovery]")
	}

	oauthToken := tokenSessionRecord.(*oauth2.Token)
	log.Debug().Msg(fmt.Sprintf("OAuth token from session, before refresh call - %v", oauthToken))

	oauthTokenDetails, _, err := app.IAMClient.GetOAuthClient().AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
	if err != nil {
		return nil, err
	}

	err = RefreshOAuthToken(app, oauthToken)
	log.Debug().Msgf("OAuth token after refresh call - %v", oauthToken)
	if err != nil || !oauthToken.Valid() {
		return nil, fmt.Errorf("refresh call to Hydra for OAuth token %v failed or refreshed token %v is not valid, [recovery]", oauthTokenDetails, oauthToken)
	}

	log.Debug().Msgf("OAuth token found on session and has been refreshed - %v", oauthToken)
	return oauthToken, nil
}

//
//func UpdateSessionWithOAuthToken(w http.ResponseWriter, r *http.Request, token *oauth2.Token) error {
//	app := GetApplicationContext(r)
//
//	d, _ := json.Marshal(token)
//	encToken := crypto.Encrypt(d, app.Config.JwtSalt)
//
//	session, err := app.IAMClient.GetCookies().Get(r, "lkd-platform")
//	if err != nil {
//		log.Debug().Err(err).Msg("Failed to get session from request")
//		return err
//	}
//
//	session.Values["oauth-token"] = encToken
//	if err = session.Save(r, w); err != nil {
//		log.Debug().Err(err).Msg("Failed to update session with OAuth token value")
//		return err
//	}
//
//	return nil
//}
//
//func GetOAuthTokenFromSession(r *http.Request) (*oauth2.Token, error) {
//	app := GetApplicationContext(r)
//
//	session, err := app.IAMClient.GetCookies().Get(r, "lkd-platform")
//	if err != nil {
//		log.Debug().Err(err).Msg("get session error")
//		return nil, err
//	}
//
//	tokenSessionRecord := crypto.Decrypt(session.Values["oauth-token"].([]byte), app.Config.JwtSalt)
//	if tokenSessionRecord == nil {
//		return nil, errors.New("Failed to get token from session cookie [recovery]")
//	}
//
//	var oauthToken *oauth2.Token
//
//	err = json.Unmarshal(tokenSessionRecord, oauthToken)
//	if err != nil {
//		return nil, errors.New("Failed to decode token from session cookie [recovery]")
//	}
//
//	log.Debug().Msg(fmt.Sprintf("OAuth token from session - %v", oauthToken))
//
//	oauthTokenDetails, _, err := app.IAMClient.GetOAuthClient().AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
//	if err != nil {
//		return nil, err
//	}
//
//	err = RefreshOAuthToken(app, oauthToken)
//	if err != nil || !oauthToken.Valid() {
//		return nil, fmt.Errorf("Refresh call to Hydra for OAuth token %v failed or refreshed token %v is not valid [recovery]", oauthTokenDetails, oauthToken)
//	}
//
//	log.Debug().Msg(fmt.Sprintf("OAuth token found on session and has been refreshed - %v", oauthToken))
//	return oauthToken, nil
//}

//RefreshOAuthToken refreshing Hydra oauth token
func RefreshOAuthToken(app *Application, token *oauth2.Token) error {
	token, err := app.IAMClient.OAuth2Config().TokenSource(oauth2.NoContext, token).Token()
	if err != nil {
		return err
	}

	return nil
}

func SetTokenDetails(r *http.Request, oauthToken *oauth2.Token) (*swagger.OAuth2TokenIntrospection, error) {
	app := GetApplicationContext(r)
	oauthTokenDetails, _, err := app.IAMClient.GetOAuthClient().AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
	if err != nil {
		return nil, err
	}

	context.Set(r, "tokenDetails", oauthTokenDetails)
	return oauthTokenDetails, nil
}

func GetTokenDetails(r *http.Request) *swagger.OAuth2TokenIntrospection {
	return context.Get(r, "tokenDetails").(*swagger.OAuth2TokenIntrospection)
}
