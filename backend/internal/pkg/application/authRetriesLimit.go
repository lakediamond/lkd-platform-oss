package application

//AuthRetries contains information about authorization limit
type AuthRetries struct {
	LimitRetries int64
	TimeFrame    int64
	LockTime     int64
	LockReason   string
}
