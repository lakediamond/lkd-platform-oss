package application

import (
	"context"
	"fmt"

	"net/http"
	"strconv"

	"github.com/gorilla/websocket"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/crm"
	"lkd-platform-backend/internal/pkg/crypto"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/googleCloud"
	"lkd-platform-backend/internal/pkg/mailjet"
	"lkd-platform-backend/internal/pkg/oauth"
	"lkd-platform-backend/internal/pkg/phoneNumber"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/sms"
	"lkd-platform-backend/pkg/enviroment"
)

// Config as application Config type
type Config struct {
	Env      string
	LogLevel string
	Port     string
	JwtSalt  string

	ErrorsMap          map[string]int
	CORSStatusCode     int
	CORSAllowedHeaders string
	CORSAllowedOrigins string
	CORSAllowedMethods string

	LkdPlatformFrontendDNS                 string
	LkdPlatformFrontendRecoveryPasswordUrl string
	IOContractAddress                      string
	EthereumPrivateKey                     string

	MaxVerificationAttempts int
	MaxVerificationPerDay   int

	KYCRootURL  string
	AuthRetries *AuthRetries
	WSUpgrader  *websocket.Upgrader

	AuthUser     string
	AuthPassword string
}

//Application is a main structure. It contains all needed variables
type Application struct {
	Config Config

	Repo                 *repo.Repo
	Mailjet              *mailjet.Sender
	Storage *Storage
	CRMData              crm.DataInterface
	EthClient            *etherPkg.PlatformEthereumApplication
	routes               Routes
	IAMClient            oauth.IdentityManagementClient
	SMSSender            sms.Sender
	PhoneNumberValidator phoneNumber.Validator
	GC                   *googleCloud.GCApplication
}

type key int

const (
	applicationKey key = 0
)

//Route contains route variables
type Route struct {
	Alias   string
	Method  string
	Path    string
	Handler http.Handler
}

// Routes represents list of routes
type Routes []Route

// NewApplication return application instance
func NewApplication() *Application {
	config := compileConfig()
	app := Application{Config: config}
	app.SetLoggerLevel()

	return &app
}

//WithApplicationContext store context into each http request
func (app *Application) WithApplicationContext(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), applicationKey, app)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

//MountRoutes mounting all needed application routes
func (app *Application) MountRoutes(routes Routes) {
	app.routes = routes
}

//FindRoute returns http path to requested alias
func (app *Application) FindRoute(alias string) string {
	var found string
	var supportedAliases []string
	for _, route := range app.routes {
		supportedAliases = append(supportedAliases, route.Alias)
		if route.Alias == alias {
			found = route.Path
		}
	}
	if found == "" {
		panic(fmt.Errorf("route with alias %s not found. Supported aliases: %s", alias, supportedAliases))
	}
	return found
}

func compileConfig() Config {
	myEnv := enviroment.GetEnv()

	crypto.InitData(
		myEnv["CRYPTO_SALT"],
		myEnv["CRYPTO_ITERATIONS"],
		myEnv["CRYPTO_MEMORY"],
		myEnv["CRYPTO_PARALLELISM"],
		myEnv["CRYPTO_KEYLEN"],
	)

	statusCode, _ := strconv.ParseInt(myEnv["CORSStatusCode"], 10, 32)
	maxVerificationAttempts, _ := strconv.ParseInt(myEnv["MAX_VERIFCATION_ATTEMPS"], 10, 32)
	maxVerificationPerDay, _ := strconv.ParseInt(myEnv["MAX_VERFICATIONS_PER_DAY"], 10, 32)

	wsUpgrader := GetWSUpgrader()
	return Config{
		Env:                                    myEnv["ENV"],
		LogLevel:                               myEnv["LOG_LEVEL"],
		Port:                                   myEnv["PORT"],
		LkdPlatformFrontendDNS:                 myEnv["LKD_PLATFORM_FRONTEND_DNS"],
		LkdPlatformFrontendRecoveryPasswordUrl: myEnv["LKD_PLATFORM_FRONTEND_RECOVERY_PASSWORD_URL"],

		JwtSalt:            myEnv["JWT_SALT"],
		CORSAllowedHeaders: myEnv["CORSAllowedHeaders"],
		CORSAllowedOrigins: myEnv["CORSAllowedOrigins"],
		CORSAllowedMethods: myEnv["CORSAllowedMethods"],
		CORSStatusCode:     int(statusCode),

		IOContractAddress:  myEnv["ETHEREUM_IO_CONTRACT_ADDRESS"],
		EthereumPrivateKey: myEnv["ETHEREUM_PRIVATE_KEY"],

		MaxVerificationAttempts: int(maxVerificationAttempts),
		MaxVerificationPerDay:   int(maxVerificationPerDay),

		KYCRootURL: myEnv["KYC_ROOT_URL"],
		WSUpgrader: wsUpgrader,

		AuthUser:     myEnv["BASIC_AUTH_USERNAME"],
		AuthPassword: myEnv["BASIC_AUTH_PASSWORD"],
	}
}

func (app *Application) initErrorsMap() {
	app.Config.ErrorsMap = make(map[string]int)
	app.Config.ErrorsMap[ErrUserNotFound] = http.StatusNotFound
	app.Config.ErrorsMap[ErrAuthFailed] = http.StatusUnauthorized
	app.Config.ErrorsMap[ErrNoAuthHeader] = http.StatusUnauthorized
	app.Config.ErrorsMap[ErrInvalidAuthToken] = http.StatusUnauthorized
}

//SetLoggerLevel setting logger level
func (app *Application) SetLoggerLevel() {
	level, err := zerolog.ParseLevel(app.Config.LogLevel)
	if err != nil {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
	log.Info().Msgf("Log level set to %s", level.String())
}
