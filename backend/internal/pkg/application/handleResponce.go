package application

import (
	"encoding/json"
	"net/http"
)

//type HandlingContext struct {
//	Configuration *models.Configuration
//}
//
//func (ctx *HandlingContext) Init() {
//	ctx.Configuration = &models.Configuration{}
//	ctx.Configuration.Init()
//}

//HandleAPIResponse handling API response
func (app *Application) HandleAPIResponse(r *http.Request, rw http.ResponseWriter, statusCode int, result interface{}) {
	url, reqID := getRequestMeta(r)
	if statusCode == 0 {
		statusCode = http.StatusInternalServerError
	}
	apiResponseMarshaled, _ := json.Marshal(GetAPIResponseTemplate(statusCode, url, reqID, result))
	app.Write(r, rw, statusCode, apiResponseMarshaled)
}

//
//func (app *Application) HandleAPIError(r *http.Request, rw http.ResponseWriter, err *models.FactoryError) {
//	url, reqID := getRequestMeta(r)
//	statusCode := app.Config.ErrorsMap[err.Text]
//	if statusCode == 0 {
//		statusCode = http.StatusInternalServerError
//	}
//	apiResponseMarshalled, _ := json.Marshal(GetAPIErrorTemplate(statusCode, url, reqID, err))
//	app.Write(r, rw, statusCode, apiResponseMarshalled)
//}
//
//func (app *Application) HandleValidationError(r *http.Request, rw http.ResponseWriter, err *models.FactoryEntityValidationDetails) {
//	url, reqID := getRequestMeta(r)
//	statusCode := app.Config.ErrorsMap[err.Error.Text]
//	if statusCode == 0 {
//		statusCode = http.StatusInternalServerError
//	}
//	apiResponseMarshalled, _ := json.Marshal(GetValidationErrorTemplate(statusCode, url, reqID, err.Error, err.ValidationErrors))
//	app.Write(r, rw, statusCode, apiResponseMarshalled)
//}
//

//Write writing a response to http.ResponseWriter
func (app *Application) Write(r *http.Request, rw http.ResponseWriter, statusCode int, payload []byte) {
	rw.Header().Set("Content-Type", "application/json")
	if r.Method == "OPTIONS" {
		statusCode = app.Config.CORSStatusCode
	}
	rw.WriteHeader(statusCode)
	rw.Write(payload)
}
