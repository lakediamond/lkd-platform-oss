package application

import (
	"net/http"
	"strings"

	"github.com/rs/zerolog/log"

	"github.com/gorilla/websocket"

	"lkd-platform-backend/pkg/enviroment"
)

func GetWSUpgrader() *websocket.Upgrader {
	env := enviroment.GetEnv()
	origins := strings.Split(env["WS_ALLOWED_ORIGINS"], ", ")
	log.Debug().Msgf("origins: %s added to WS config", origins)

	return &websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			for _, origin := range origins {
				if r.Header.Get("Origin") == origin {
					return true
				}
			}
			return false
		},
	}
}
