package repo

import (
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//OrderRepository contains all using methods for orders table
type OrderRepository interface {
	UpdateOrder(order *models.Order) error
	GetOrderByID(id uint) (models.Order, error)
	GetOrderByBlockchainID(id uint) (models.Order, error)
	GetAllOrders() ([]models.Order, error)
	GetOrdersDetails() ([]OrderResponseDB, error)
	GetOrdersCumulativeData(daysToLoad int) ([]OrdersCumulativeDataDB, error)
}

//OrderRepo is OrderRepository main implementation
type OrderRepo struct {
	db *gorm.DB
}

//UpdateOrder creating new or updating order
func (repo *OrderRepo) UpdateOrder(order *models.Order) error {
	err := repo.db.Save(order)
	if err != nil {
		return err.Error
	}
	return nil
}

//GetOrderByID returns order finding by id, if no record with actual id - returns error
func (repo *OrderRepo) GetOrderByID(id uint) (models.Order, error) {
	var order models.Order

	err := repo.db.Where("id = ?", id).Find(&order)
	if err != nil {
		return order, err.Error
	}

	return order, nil
}

//GetOrderByBlockchainID returns order finding by blockchain_id, if no record with actual id - returns error
func (repo *OrderRepo) GetOrderByBlockchainID(id uint) (models.Order, error) {
	var order models.Order

	err := repo.db.Where("blockchain_id = ?", id).Find(&order)
	if err != nil {
		return order, err.Error
	}

	return order, nil
}

//GetAllOrders returns all orders
func (repo *OrderRepo) GetAllOrders() ([]models.Order, error) {
	var orders []models.Order

	err := repo.db.Find(&orders).Error
	if err != nil {
		return orders, err
	}

	return orders, nil
}

//GetOrdersDetails returns all orders with extra fields
func (repo *OrderRepo) GetOrdersDetails() ([]OrderResponseDB, error) {
	var orderResponsesDB []OrderResponseDB

	err := repo.db.
		Raw(`
select ord.id, ord.created_on, ord.created_by, ordt.name, ord.metadata, ord.price, ord.token_amount, ord.eth_amount, mt.id as matcher_id, mt.tokens as matcher_tokens, mt.ether as matcher_ether, mt.tx_hash as matcher_tx_hash, usr.id as user_id, usr.first_name as user_first_name, usr.last_name as user_last_name, usr.email as user_email, usr.eth_address as user_eth_address
from orders ord
            left join order_types ordt on ordt.id = ord.type_id
            left join matches mt on mt.order_blockchain_id = ord.blockchain_id
            left join proposals pr on pr.blockchain_id = mt.proposal_blockchain_id
            left join users usr on usr.eth_address = pr.created_by`).Scan(&orderResponsesDB).Error

	if err != nil {
		return orderResponsesDB, err
	}

	return orderResponsesDB, nil
}

//GetOrdersCumulativeData returns orders match statistic grouped by order price range
func (repo *OrderRepo) GetOrdersCumulativeData(daysToLoad int) ([]OrdersCumulativeDataDB, error) {
	var ordersCumulativeDataDB []OrdersCumulativeDataDB

	query := strings.Replace(ordersCumulativeDataQueryTemplate, "{daysToLoad}", strconv.Itoa(daysToLoad), -1)

	err := repo.db.Raw(query).Scan(&ordersCumulativeDataDB).Error

	if err != nil {
		return ordersCumulativeDataDB, err
	}

	return ordersCumulativeDataDB, nil
}

type OrderResponseDB struct {
	ID          uint            `json:"id"`
	CreatedOn   time.Time       `json:"created_on"`
	CreatedBy   string          `json:"created_by"`
	Type        string          `json:"type" gorm:"column:name"`
	Metadata    string          `json:"metadata"`
	TokenPrice  decimal.Decimal `json:"token_price" gorm:"column:price"`
	TokenAmount decimal.Decimal `json:"tokens_amount" gorm:"column:token_amount"`
	EthAmount   decimal.Decimal `json:"eth_amount" gorm:"column:eth_amount"`

	InvestmentID   *uuid.UUID      `json:"id" gorm:"type:uuid; column:matcher_id"`
	TxHash         string          `json:"tx_hash" gorm:"column:matcher_tx_hash"`
	TokensInvested decimal.Decimal `json:"tokens_invested" gorm:"column:matcher_tokens"`
	EthReceived    decimal.Decimal `json:"eth_received" gorm:"column:matcher_ether"`

	InvestorID *uuid.UUID `json:"id" gorm:"type:uuid; column:user_id"`
	FirstName  string     `json:"first_name" gorm:"column:user_first_name"`
	LastName   string     `json:"last_name" gorm:"column:user_last_name"`
	Email      string     `json:"email" gorm:"column:user_email"`
	EthAccount string     `json:"eth_account" gorm:"column:user_eth_address"`
}

//OrdersCumulativeDataDB type for describe orders cumulative data grouped by price range.
type OrdersCumulativeDataDB struct {
	MinValue        decimal.Decimal `json:"min_value" gorm:"column:min_value"`
	MaxValue        decimal.Decimal `json:"max_value" gorm:"column:max_value"`
	OwnersCount     decimal.Decimal `json:"lkd_owners" gorm:"column:lkd_owners"`
	TokenAmount     decimal.Decimal `json:"lkd_tokens" gorm:"column:lkd_tokens"`
	CumulativeValue decimal.Decimal `json:"cumulative_value" gorm:"column:cumulative_value"`
}

const ordersCumulativeDataQueryTemplate = `
	with order_prices as (
		select 
		orders.price as price	
		from orders
		inner join matches on matches.order_blockchain_id = orders.blockchain_id
		where orders.completed_on > current_timestamp - interval '{daysToLoad} days'
	),
	order_stats as (
		select round(cast((min(price) / pow(10, 18)) as numeric), 1) as min,
		round(cast((max(price) / pow(10, 18)) as numeric), 1) + 0.1 as max
		from order_prices
	),
	step as (select step_value from (values (0.1)) as step(step_value)),
	range_stats as (
		select CAST(((max - min) / step_value) as Integer) as stepsCount
		from order_stats, step
	),
	histogram as (
		select width_bucket(price / pow(10, 18), min, max, stepsCount) as bucket
		from order_prices, order_stats, range_stats
		group by bucket
		order by bucket
	)
	
	select
		(min + (bucket - 1) * step_value) as min_value,
		(min + bucket * step_value) as max_value,
		(
			select count(*) from 
			(
				select count(*) from orders
				inner join matches on matches.order_blockchain_id = orders.blockchain_id
				where price / pow(10, 18) >= (min + (bucket - 1) * step_value) and price / pow(10, 18) < (min + bucket * step_value)
				and orders.completed_on > current_timestamp - interval '{daysToLoad} days'
				group by created_by
			) as OwnerCount(records_in_group)
		) as lkd_owners,
		(
			select sum(matches.tokens) from orders
			inner join matches on matches.order_blockchain_id = orders.blockchain_id
			where price / pow(10, 18) >= (min + (bucket - 1) * step_value) and price / pow(10, 18) < (min + bucket * step_value)
			and orders.completed_on > current_timestamp - interval '{daysToLoad} days'
		) as lkd_tokens,
		(
			select sum(matches.tokens * proposals.price / pow(10, 18)) from orders
			inner join matches on matches.order_blockchain_id = orders.blockchain_id
			inner join proposals on matches.proposal_blockchain_id = proposals.blockchain_id
			where orders.price / pow(10, 18) >= (min + (bucket - 1) * step_value) and orders.price / pow(10, 18) < (min + bucket * step_value)
			and orders.completed_on > current_timestamp - interval '{daysToLoad} days'
		) as cumulative_value
	from histogram, order_stats, step`
