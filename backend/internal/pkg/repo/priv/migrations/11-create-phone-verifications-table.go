package migrations

import (
	"time"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"

	"github.com/satori/go.uuid"
)

type PhoneVerification struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key"`
	User        User
	UserID      *uuid.UUID `gorm:"type:uuid REFERENCES users(id) ON DELETE CASCADE; not null"`
	Phone       string     `gorm:"not null"`
	CountryCode string     `gorm:"not null"`
	Status      string     `gorm:"not null"`
	Code        int        `gorm:"not null"`
	Attempts    int        `gorm:"not null; default:0"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

// TableName return table name
func (PhoneVerification) TableName() string {
	return "phone_verifications"
}

var CreatePhoneVericationTable gormigrate.Migration = gormigrate.Migration{

	ID: "201812041530",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&PhoneVerification{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("phone_verifications").Error
	},
}
