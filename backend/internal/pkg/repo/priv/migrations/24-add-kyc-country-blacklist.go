package migrations

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"gopkg.in/gormigrate.v1"
)

type KYCCountryBlacklist struct {
	ID   *uuid.UUID `gorm:"type:uuid; primary_key"`
	Code string     `json:"code" gorm:"type:varchar(2)"`
	Name string     `json:"name"`
}

// TableName overrides gorm built-in table name definition
func (KYCCountryBlacklist) TableName() string {
	return "kyc_country_blacklist"
}

var CreateKYCCountryBlacklistTable gormigrate.Migration = gormigrate.Migration{

	ID: "201812251800",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&KYCCountryBlacklist{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("kyc_country_blacklist").Error
	},
}
