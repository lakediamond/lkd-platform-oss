package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var ChangeOrdersAndProposalsTablesToBigInt gormigrate.Migration = gormigrate.Migration{
	ID: "201811281400",
	Migrate: func(tx *gorm.DB) error {

		var req = `
ALTER TABLE proposals ALTER COLUMN price TYPE DECIMAL;
ALTER TABLE proposals ALTER COLUMN amount TYPE DECIMAL;

ALTER TABLE orders ALTER COLUMN price TYPE DECIMAL;
ALTER TABLE orders ALTER COLUMN eth_amount TYPE DECIMAL;
ALTER TABLE orders ALTER COLUMN token_amount TYPE DECIMAL;
  `

		return tx.Exec(req).Error
	},
}
