package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var MakeBlockchainIDUinque gormigrate.Migration = gormigrate.Migration{
	ID: "201812051100",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE proposals ADD UNIQUE (blockchain_id);
ALTER TABLE orders ADD UNIQUE (blockchain_id);
`
		return tx.Exec(req).Error
	},
}
