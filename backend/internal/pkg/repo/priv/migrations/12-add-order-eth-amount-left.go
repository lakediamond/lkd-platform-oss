package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddOrderEthAmountLeftField gormigrate.Migration = gormigrate.Migration{
	ID: "201812121700",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE orders ADD eth_amount_left DECIMAL;
`

		return tx.Exec(req).Error
	},
}
