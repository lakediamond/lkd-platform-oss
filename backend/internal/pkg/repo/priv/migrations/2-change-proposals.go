package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var ChangeOrderTable gormigrate.Migration = gormigrate.Migration{
	ID: "201811221600",
	Migrate: func(tx *gorm.DB) error {
		var req = `ALTER TABLE proposals
ADD created_by VARCHAR(50);
ALTER TABLE proposals
DROP COLUMN user_id;
  `

		return tx.Exec(req).Error
	},
}
