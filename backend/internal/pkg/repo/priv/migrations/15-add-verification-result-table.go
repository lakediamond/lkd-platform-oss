package migrations

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"gopkg.in/gormigrate.v1"
)

type VerificationResult struct {
	ID                        *uuid.UUID `gorm:"type:uuid; primary_key" json:"-"`
	UserID                    *uuid.UUID `gorm:"type:uuid"`
	CallBackType              string     `json:"callBackType"`
	CallbackDate              string     `json:"callbackDate"`
	CallbackUrl               string     `json:"callback_url"`
	FirstAttemptDate          string     `json:"firstAttemptDate"`
	IDCheckDataPositions      string     `json:"idCheckDataPositions"`
	IDCheckDocumentValidation string     `json:"idCheckDocumentValidation"`
	IDCheckHologram           string     `json:"idCheckHologram"`
	IDCheckMRZCode            string     `json:"idCheckMRZcode"`
	IDCheckMicroprint         string     `json:"idCheckMicroprint"`
	IDCheckSecurityFeatures   string     `json:"idCheckSecurityFeatures"`
	IDCheckSignature          string     `json:"idCheckSignature"`
	IDCountry                 string     `json:"idCountry"`
	IDDob                     string     `json:"idDob"`
	IDExpiry                  string     `json:"idExpiry"`
	IDFirstName               string     `json:"idFirstName"`
	IDLastName                string     `json:"idLastName"`
	IDNumber                  string     `json:"idNumber"`
	IDScanImage               string     `json:"idScanImage"`
	IDScanImageFace           string     `json:"idScanImageFace"`
	IDScanSource              string     `json:"idScanSource"`
	IDScanStatus              string     `json:"idScanStatus"`
	IDType                    string     `json:"idType"`
	IdentityVerification      string     `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB"`
	JumioIDScanReference      string     `json:"jumioIdScanReference"`
	MerchantIdScanReference   string     `json:"merchantIdScanReference"`
	PersonalNumber            string     `json:"personalNumber"`
	TransactionDate           string     `json:"transactionDate"`
	VerificationStatus        string     `json:"verificationStatus"`
}

var CreateVerificationResultTable gormigrate.Migration = gormigrate.Migration{

	ID: "201812181400",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&VerificationResult{})

		if err := tx.Model(VerificationResult{}).AddForeignKey("user_id", "users (id)", "RESTRICT", "RESTRICT").Error; err != nil {
			return err
		}

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("verification_results").Error
	},
}
