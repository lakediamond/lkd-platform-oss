package migrations

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"gopkg.in/gormigrate.v1"
)

type KYCInvestmentPlan struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key"`
	Code        int        `json:"code"`
	MaxLimit    int        `json:"max_limit"`
	Currency    string     `json:"currency"`
	Description string     `json:"description"`
}

// TableName overrides gorm built-in table name definition
func (KYCInvestmentPlan) TableName() string {
	return "kyc_investment_plan"
}

func newPlan(code, maxLimit int, currency, description string) *KYCInvestmentPlan {
	u := uuid.NewV4()
	plan := KYCInvestmentPlan{
		ID:          &u,
		Code:        code,
		MaxLimit:    maxLimit,
		Currency:    currency,
		Description: description,
	}
	return &plan
}

var CreateKYCInvestmentPlanTable gormigrate.Migration = gormigrate.Migration{

	ID: "201812251300",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(&KYCInvestmentPlan{})

		plan := newPlan(0, 3000, "CHF", "Up to 3000 CHF")
		tx.Model(&plan).Create(plan)

		plan = newPlan(1, 15000, "CHF", "CHF 3000 to CHF 15000")
		tx.Model(&plan).Create(plan)

		plan = newPlan(2, 100000, "CHF", "CHF 15000 to CHF 100000")
		tx.Model(&plan).Create(plan)

		plan = newPlan(3, 999999, "CHF", "More than CHF 100000")
		tx.Model(&plan).Create(plan)

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("kyc_investment_plan").Error
	},
}
