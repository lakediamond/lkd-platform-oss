package migrations

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"gopkg.in/gormigrate.v1"
)

type UserAccessRecoveryRequest struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	UserID    *uuid.UUID
	Status    string
	Token     string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

var CreateUserAccessRecoveryRequestTable gormigrate.Migration = gormigrate.Migration{
	ID: "201901161100",
	Migrate: func(tx *gorm.DB) error {
		tx.AutoMigrate(
			&UserAccessRecoveryRequest{})

		if err := tx.Model(UserAccessRecoveryRequest{}).AddForeignKey("user_id", "users (id)", "RESTRICT", "RESTRICT").Error; err != nil {
			return err
		}

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("user_access_recovery_requests").Error
	},
}
