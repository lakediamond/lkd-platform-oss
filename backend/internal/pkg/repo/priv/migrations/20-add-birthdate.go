package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddBirthDate gormigrate.Migration = gormigrate.Migration{
	ID: "201812241600",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE users ADD birth_date VARCHAR(10);
`

		return tx.Exec(req).Error
	},
}
