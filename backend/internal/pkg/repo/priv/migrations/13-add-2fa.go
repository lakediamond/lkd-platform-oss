package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var Add2FaForUser gormigrate.Migration = gormigrate.Migration{
	ID: "201812131300",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE users ADD google_2fa_secret VARCHAR(16);
ALTER TABLE users ADD google_2fa_enabled BOOLEAN;

`

		return tx.Exec(req).Error
	},
}
