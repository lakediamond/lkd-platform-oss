package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddTimeColumsToUsersTable gormigrate.Migration = gormigrate.Migration{
	ID: "201902151630",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE users ADD created_at timestamp;
ALTER TABLE users ADD updated_at timestamp;
ALTER TABLE users ADD deleted_at timestamp;
`

		return tx.Exec(req).Error
	},
}
