package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddIPInfoToAccountActivities gormigrate.Migration = gormigrate.Migration{
	ID: "201812201400",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE account_activities ADD cfip_address VARCHAR(16);
ALTER TABLE account_activities ADD cfip_country VARCHAR(50);
`

		return tx.Exec(req).Error
	},
}
