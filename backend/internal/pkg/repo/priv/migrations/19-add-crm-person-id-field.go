package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCRMPersonID gormigrate.Migration = gormigrate.Migration{
	ID: "201812211300",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE users ADD crm_person_id integer;
`

		return tx.Exec(req).Error
	},
}
