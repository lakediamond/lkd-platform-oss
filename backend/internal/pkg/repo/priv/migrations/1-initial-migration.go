package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var CreateAllTables gormigrate.Migration = gormigrate.Migration{
	ID: "201608301400",
	Migrate: func(tx *gorm.DB) error {
		// it's a good pratice to copy the struct inside the function,
		// so side effects are prevented if the original struct changes during the time
		tx.AutoMigrate(
			&User{},
			&Order{},
			&OrderType{},
			&Proposal{},
			&JwtToken{},
			&SupportData{})

		if err := tx.Model(Proposal{}).AddForeignKey("user_id", "users (id)", "RESTRICT", "RESTRICT").Error; err != nil {
			return err
		}

		if err := tx.Model(Order{}).AddForeignKey("type_id", "order_types (id)", "RESTRICT", "RESTRICT").Error; err != nil {
			return err
		}

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("proposals", "order_types", "users", "jwt_tokens, support_data, orders").Error
	},
}
