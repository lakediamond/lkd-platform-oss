package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddTargetTier gormigrate.Migration = gormigrate.Migration{
	ID: "201812251200",
	Migrate: func(tx *gorm.DB) error {
		var req = `ALTER TABLE users ADD target_tier UUID default null;`

		return tx.Exec(req).Error
	},
}
