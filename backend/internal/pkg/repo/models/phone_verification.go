package models

import (
	"time"

	"github.com/satori/go.uuid"
)

type PVStatus = string

const PVNew PVStatus = "NEW"
const PVVerified PVStatus = "VERIFIED"
const PVFailed PVStatus = "FAILED"
const PVCancelled PVStatus = "CANCELED"

// PhoneVerification model
type PhoneVerification struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key" json:"id"`
	User        *User
	UserID      *uuid.UUID `gorm:"type:uuid REFERENCES users(id);not null" json:"user_id"`
	Phone       string     `json:"phone"`
	CountryCode string     `json:"country_code"`
	Status      PVStatus   `json:"status"`
	Attempts    int        `json:"attempts"`
	Code        int
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

//BeforeSave for phone_verification table generating random uuid
func (pv *PhoneVerification) BeforeSave() {
	if pv.ID == nil {
		u := uuid.NewV4()
		pv.ID = &u
	}

	return
}

// NewPhoneVerifcation struct
func NewPhoneVerifcation() PhoneVerification {
	var pv PhoneVerification
	pv.Status = PVNew
	return pv
}
