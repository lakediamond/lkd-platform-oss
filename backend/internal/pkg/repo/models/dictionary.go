package models

import (
	"github.com/satori/go.uuid"
)

type DictionaryCountryPhoneCode struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key" json:"id"`
	Country     string     `json:"country"`
	CountryCode string     `json:"country_code"`
	PhoneCode   string     `json:"phone_code"`
}

// TableName overrides gorm built-in table name definition
func (DictionaryCountryPhoneCode) TableName() string {
	return "dictionary_country_phone_code"
}

//BeforeSave for dictionary_country_phone_code table generating random uuid
func (kt *DictionaryCountryPhoneCode) BeforeSave() {
	if kt.ID == nil {
		u := uuid.NewV4()
		kt.ID = &u
	}
	return
}
