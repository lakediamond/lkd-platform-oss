package models

type CustomerImageReUploadRequestInput struct {
	Current struct {
		ActiveFlag             bool        `json:"active_flag"`
		AddTime                string      `json:"add_time"`
		AssignedToUserID       int         `json:"assigned_to_user_id"`
		CompanyID              int         `json:"company_id"`
		CreatedByUserID        int         `json:"created_by_user_id"`
		DealDropboxBcc         string      `json:"deal_dropbox_bcc"`
		DealID                 int         `json:"deal_id"`
		DealTitle              string      `json:"deal_title"`
		Done                   bool        `json:"done"`
		DueDate                string      `json:"due_date"`
		DueTime                string      `json:"due_time"`
		Duration               string      `json:"duration"`
		GcalEventID            interface{} `json:"gcal_event_id"`
		GoogleCalendarEtag     interface{} `json:"google_calendar_etag"`
		GoogleCalendarID       interface{} `json:"google_calendar_id"`
		ID                     int         `json:"id"`
		LastNotificationTime   interface{} `json:"last_notification_time"`
		LastNotificationUserID interface{} `json:"last_notification_user_id"`
		MarkedAsDoneTime       string      `json:"marked_as_done_time"`
		NoGcal                 bool        `json:"no_gcal"`
		Note                   string      `json:"note"`
		NotificationLanguageID int         `json:"notification_language_id"`
		OrgID                  int         `json:"org_id"`
		OrgName                string      `json:"org_name"`
		OwnerName              string      `json:"owner_name"`
		Participants           []struct {
			PersonID    int  `json:"person_id"`
			PrimaryFlag bool `json:"primary_flag"`
		} `json:"participants"`
		PersonDropboxBcc    string      `json:"person_dropbox_bcc"`
		PersonID            int         `json:"person_id"`
		PersonName          string      `json:"person_name"`
		RecMasterActivityID interface{} `json:"rec_master_activity_id"`
		RecRule             interface{} `json:"rec_rule"`
		ReferenceID         interface{} `json:"reference_id"`
		ReferenceType       interface{} `json:"reference_type"`
		Subject             string      `json:"subject"`
		Type                string      `json:"type"`
		UpdateTime          string      `json:"update_time"`
		UpdateUserID        interface{} `json:"update_user_id"`
		UpdatesStoryID      int         `json:"updates_story_id"`
		UserID              int         `json:"user_id"`
	} `json:"current"`
	Event          string `json:"event"`
	MatchesFilters struct {
		Current []interface{} `json:"current"`
	} `json:"matches_filters"`
	Meta struct {
		Action                        string      `json:"action"`
		ActivityNotificationsLanguage interface{} `json:"activity_notifications_language"`
		CompanyID                     int         `json:"company_id"`
		Host                          string      `json:"host"`
		ID                            int         `json:"id"`
		IsBulkUpdate                  bool        `json:"is_bulk_update"`
		MatchesFilters                struct {
			Current []interface{} `json:"current"`
		} `json:"matches_filters"`
		Object                    string   `json:"object"`
		PermittedUserIds          []string `json:"permitted_user_ids"`
		PipedriveServiceName      bool     `json:"pipedrive_service_name"`
		SendActivityNotifications bool     `json:"send_activity_notifications"`
		Timestamp                 int      `json:"timestamp"`
		TimestampMicro            int      `json:"timestamp_micro"`
		TransPending              bool     `json:"trans_pending"`
		UserID                    int      `json:"user_id"`
		V                         int      `json:"v"`
		WebhookID                 string   `json:"webhook_id"`
	} `json:"meta"`
	Previous interface{} `json:"previous"`
	Retry    int         `json:"retry"`
	V        int         `json:"v"`
}
