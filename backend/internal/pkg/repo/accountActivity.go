package repo

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//AccountActivityRepository contains all using methods for account_activity table
type AccountActivityRepository interface {
	NewRecord(activity *models.AccountActivity) error
	GetAllActivities() ([]models.AccountActivity, error)
	LimitAllowedAuth(email, ipAddress string, timeSub time.Time) ([]models.AccountActivity, error)
}

//AccountActivityRepo is AccountActivityRepository main implementation
type AccountActivityRepo struct {
	db *gorm.DB
}

//NewRecord creating new record in table account_activity
func (repo *AccountActivityRepo) NewRecord(activity *models.AccountActivity) error {

	err := repo.db.Create(activity).Error
	if err != nil {
		return err
	}

	return nil
}

//GetAllActivities returns all activities
func (repo *AccountActivityRepo) GetAllActivities() ([]models.AccountActivity, error) {
	var activities []models.AccountActivity

	err := repo.db.Find(&activities).Error
	if err != nil {
		return activities, err
	}

	return activities, nil
}

func (repo *AccountActivityRepo) LimitAllowedAuth(email, ipAddress string, timeSub time.Time) ([]models.AccountActivity, error) {

	var activities []models.AccountActivity

	err := repo.db.
		Where("timestamp >= ?", timeSub).
		Where("content @> ?",
			fmt.Sprintf(`{"email": "%s", "ip_address": "%s"}`, email, ipAddress)).
		Find(&activities).Error
	if err != nil {
		return activities, err
	}

	return activities, nil

}
