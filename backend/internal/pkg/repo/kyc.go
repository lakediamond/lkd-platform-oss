package repo

import (
	"fmt"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/repo/models"
)

type KYCRepository interface {
	GetDictionaryCountryPhoneCodeRecords() ([]models.DictionaryCountryPhoneCode, error)
	DeleteKYCTierFlags(user *models.User, tierCode models.KYCTierCode, flagsToSkip []string) error
	CreateKYCTierFlag(kycTierFlag *models.KYCTierFlag) error
	GetKYCPlans(user *models.User) ([]models.KYCInvestmentPlan, error)
	GetKYCPlanByCode(planCode int) (models.KYCInvestmentPlan, error)
	GetKYCTierFailureRate(user *models.User, tierCode models.KYCTierCode) (int, error)
	GetKYCTierFailedFlags(user *models.User, tierCode models.KYCTierCode) ([]models.KYCTierFlag, error)
	UpdateKYCTierByModel(kycTier *models.KYCTier) error

	UpdateKYCTierStepByModel(kycTierStep *models.KYCTierStep) error
	UpdateKYCTierStep(user *models.User, tierCode int, stepCode string, contentItems map[string]string) error
	UpdateKYCTierStepCommonInfo(user *models.User, data *models.StepCommonInfoForm) error

	UpdateKYCTierStepComplianceReview(user *models.User) error

	UpdateKYCMediaItemStatus(user *models.User, itemType models.KYCMediaItemType, status models.KYCTierMediaItemStatus) error
	UpdateKYCMediaItemCRMFileID(user *models.User, itemType models.KYCMediaItemType, crmFileId uint64) error

	CompleteKYCTierStepIdentityScans(user *models.User) error

	UnlockKYCTierByCRMStageID(user *models.User, tierCode models.KYCTierCode) error
	UnlockKYCTierByCode(user *models.User, tierCode models.KYCTierCode) error
	UnlockKYCTierStep(user *models.User, tierCode models.KYCTierCode, stepCode models.KYCTierStepCode) error
	CompleteKYCTier(user *models.User, tierCode models.KYCTierCode) error

	SetReworkKYCTier(user *models.User, tierCode models.KYCTierCode) error

	UpdateKYCTierStatus(user *models.User, tierCode models.KYCTierCode, tierStatus models.KYCTierStatus) error

	GetKYCMediaItemStatus(user *models.User, itemType models.KYCMediaItemType) (models.KYCTierMediaItemStatus, error)
	GetKYCTiers(user *models.User) ([]models.KYCTier, error)
	GetKYCTierByCode(user *models.User, tierCode models.KYCTierCode) (models.KYCTier, error)
	GetKYCTierByStageID(user *models.User, stageID int) (models.KYCTier, error)
	GetKYCTierStep(user *models.User, tierCode int, stepCode string) (*models.KYCTierStep, error)
	GetKYCTierFlag(user *models.User, tierCode int) ([]models.KYCTierFlag, error)

	CreateKYCTemplateRecords(user *models.User) error
	GetKYCCountryBlacklistByCode(code string) (models.KYCCountryBlacklist, error)
	GetAllBlacklistedUserCountries(email string) ([]models.KYCCountryBlacklist, error)
	GetKYCTierStepContentItemByCode(kycTierStep *models.KYCTierStep, code string) (models.KYCTierStepContentItem, error)
	GetUserTierByStatus(user *models.User, tierStatuses []models.KYCTierStatus) (models.KYCTier, error)
	GetInvestmentPlanByCode(code int) (models.KYCInvestmentPlan, error)

	UpdateKYCTierStepContentItem(kycTierStepContentItem *models.KYCTierStepContentItem) error

	DeletePhoneVerification(kycTierStep *models.KYCTierStep) error
}

// KYCRepo dba layer
type KYCRepo struct {
	db *gorm.DB
}

func (repo *KYCRepo) GetKYCTierByCode(user *models.User, tierCode models.KYCTierCode) (models.KYCTier, error) {
	var tier models.KYCTier

	if err := repo.db.Model(&tier).Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps").Find(&tier).Error; err != nil {
		return tier, err
	}

	return tier, nil
}

func (repo *KYCRepo) GetDictionaryCountryPhoneCodeRecords() ([]models.DictionaryCountryPhoneCode, error) {
	var records []models.DictionaryCountryPhoneCode
	err := repo.db.Model(&records).Order("country asc").Find(&records).Error
	if err != nil {
		return records, err
	}

	return records, err
}

func (repo *KYCRepo) SetReworkKYCTier(user *models.User, tierCode models.KYCTierCode) error {
	err := repo.db.Model(&models.KYCTier{}).Where("code = ? and user_id = ?", tierCode, user.ID.String()).Select("status").Updates(map[string]interface{}{"status": models.TierRework}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) UnlockKYCTierStep(user *models.User, tierCode models.KYCTierCode, stepCode models.KYCTierStepCode) error {
	tier := models.KYCTier{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Find(&tier).Error
	if err != nil {
		return err
	}

	err = repo.db.Model(&models.KYCTierStep{}).Where("kyc_tier_id = ? and code = ?", tier.ID.String(), stepCode).Select("status").Updates(map[string]interface{}{"status": models.StepOpened}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) UnlockKYCTierByCRMStageID(user *models.User, stageID models.CRMStageID) error {
	tier := models.KYCTier{
		CRMStageID: stageID,
		UserID:     user.ID,
	}

	return repo.unlockKYCTier(user, tier)
}

func (repo *KYCRepo) unlockKYCTier(user *models.User, template models.KYCTier) error {
	tiers := []models.KYCTier{}
	var err error

	err = repo.db.Model(&template).Where("user_id = ? and code <= ?", template.UserID.String(), template.Code).Preload("Steps").Preload("Steps.ContentItems").Order("code asc").Find(&tiers).Error
	if err != nil {
		return err
	}

	tx := repo.db.Begin()
	if err := tx.Error; err != nil {
		return err
	}

	for idx, tier := range tiers {
		for _, step := range tier.Steps {
			if idx == 0 && step.StepOrder == 1 {
				if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepOpened}).Error; err != nil {
					tx.Rollback()
					return err
				}
			}
		}

		if err = tx.Model(&tier).Select("status").Updates(map[string]interface{}{"status": models.TierOpened}).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}

func (repo *KYCRepo) UnlockKYCTierByCode(user *models.User, tierCode models.KYCTierCode) error {
	tier := models.KYCTier{
		Code:   tierCode,
		UserID: user.ID,
	}

	return repo.unlockKYCTier(user, tier)
}

func (repo *KYCRepo) CompleteKYCTier(user *models.User, tierCode models.KYCTierCode) error {
	tier := models.KYCTier{}
	var err error

	err = repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps").
		Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	tx := repo.db.Begin()
	if err := tx.Error; err != nil {
		return err
	}

	for _, step := range tier.Steps {
		for _, stepContentItem := range step.ContentItems {
			if err = tx.Model(&stepContentItem).Select("status").Updates(map[string]interface{}{"status": models.StepVerified, "enabled": false, "enabled_at": time.Time{}}).Error; err != nil {
				tx.Rollback()
				return err
			}
		}
		if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepVerified}).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	if err = tx.Model(&tier).Select("status").Updates(map[string]interface{}{"status": models.TierGranted}).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

type KYCTierFailureRate struct {
	FailureRate int
}

func (repo *KYCRepo) GetKYCTierFailureRate(user *models.User, tierCode models.KYCTierCode) (int, error) {
	failureRate := KYCTierFailureRate{}
	err := repo.db.
		Raw(`
		select sum( 
			case
			when position(';' in value) > 0 then 1
			else 0
			end) as failure_rate
			from kyc_tier_flag 
			where 
			deleted_at is null and
			kyc_tier_id = (select id from kyc_tier where code = ? and user_id = ?)`, tierCode, user.ID.String()).Scan(&failureRate).Error
	if err != nil {
		return -1, err
	}
	return failureRate.FailureRate, nil
}

func (repo *KYCRepo) GetKYCTierFailedFlags(user *models.User, tierCode models.KYCTierCode) ([]models.KYCTierFlag, error) {
	var flags []models.KYCTierFlag
	err := repo.db.
		Raw(`
		select fl.id, fl.kyc_tier_id, fl.code, fl.value, fl.crm_pipeline_failure_stage, fl.created_at, fl.updated_at, fl.deleted_at
		from kyc_tier_flag fl
		inner join kyc_tier tr on tr.id = fl.kyc_tier_id
		where tr.user_id = ? and tr.code = ?
		and position(';' in fl.value) > 0
		and fl.deleted_at is null`, user.ID.String(), tierCode).Scan(&flags).Error
	if err != nil {
		return flags, err
	}
	return flags, nil
}

func (repo *KYCRepo) DeleteKYCTierFlags(user *models.User, tierCode models.KYCTierCode, flagsToSkip []string) error {
	var tier models.KYCTier

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Find(&tier).Error
	if err != nil {
		return err
	}

	err = repo.db.Where("kyc_tier_id = ? and code not in (?)", tier.ID.String(), flagsToSkip).Delete(models.KYCTierFlag{}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) UpdateKYCMediaItemCRMFileID(user *models.User, itemType models.KYCMediaItemType, crmFileId uint64) error {
	var tier models.KYCTier

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier0).Preload("Steps", "code = ?", models.IdentityScans).Preload("Steps.ContentItems", "code = ?", itemType).Find(&tier).Error
	if err != nil {
		return err
	}

	item := tier.Steps[0].ContentItems[0]
	crmFileIdValue := strconv.FormatUint(crmFileId, 10)
	err = repo.db.Model(&item).Select("value").Updates(map[string]string{"value": crmFileIdValue}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) UpdateKYCTierStatus(user *models.User, tierCode models.KYCTierCode, tierStatus models.KYCTierStatus) error {
	tier := &models.KYCTier{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Find(&tier).Error
	if err != nil {
		return err
	}

	err = repo.db.Model(&tier).Select("status").Updates(map[string]string{"status": tierStatus}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) GetInvestmentPlanByCode(code int) (models.KYCInvestmentPlan, error) {
	plan := models.KYCInvestmentPlan{}
	var err error

	err = repo.db.Where("code = ?", code).Find(&plan).Error
	if err != nil {
		return plan, err
	}

	return plan, nil
}

func (repo *KYCRepo) GetUserTierByStatus(user *models.User, tierStatuses []models.KYCTierStatus) (models.KYCTier, error) {
	var tier models.KYCTier
	queryOrder := ""

	if len(tierStatuses) == 1 && tierStatuses[0] == models.TierLocked {
		queryOrder = "code"
	} else {
		queryOrder = "code desc"
	}

	err := repo.db.Where("user_id = ? and status in (?) and code <= ?", user.ID.String(), tierStatuses, user.KYCInvestmentPlan.Code).Order(queryOrder).Limit(1).Find(&tier).Error
	if err != nil {
		return tier, err
	}

	return tier, nil
}

func (repo *KYCRepo) CompleteKYCTierStepIdentityScans(user *models.User) error {
	tier := models.KYCTier{}
	var err error

	err = repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier0).Preload("Steps", "code = ?", models.IdentityScans).
		Preload("Steps.ContentItems", "code in (?)", user.GetMediaTypes()).Find(&tier).Error
	if err != nil {
		return err
	}

	step := tier.Steps[0]

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	for _, item := range step.ContentItems {
		if err = tx.Model(&item).Select("status").Updates(map[string]interface{}{"status": models.MediaItemProcessed}).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepCustomerConfirmed}).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (repo *KYCRepo) GetKYCPlanByCode(planCode int) (models.KYCInvestmentPlan, error) {
	var plan models.KYCInvestmentPlan

	err := repo.db.Model(&plan).Where("code = ?", planCode).Find(&plan).Error

	return plan, err
}

func (repo *KYCRepo) GetKYCPlans(user *models.User) ([]models.KYCInvestmentPlan, error) {
	var plan []models.KYCInvestmentPlan
	var validTiers []models.KYCTier
	var validTierCodes []int

	err := repo.db.Where("user_id = ? and status in (?)", user.ID.String(), []string{models.TierLocked}).Select("code").Find(&validTiers).Error
	if err != nil {
		return nil, err
	}

	for _, tier := range validTiers {
		validTierCodes = append(validTierCodes, tier.Code)
	}
	err = repo.db.Model(&plan).Where("code in (?)", validTierCodes).Order("code").Find(&plan).Error

	return plan, err
}

func (repo *KYCRepo) GetKYCTierStep(user *models.User, tierCode int, stepCode string) (*models.KYCTierStep, error) {
	var tier models.KYCTier
	phoneVerificationEntry := models.PhoneVerification{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps", "code = ?", stepCode).Preload("Steps.ContentItems", func(db *gorm.DB) *gorm.DB {
		return db.Order("kyc_tier_step_item.item_order ASC")
	}).Find(&tier).Error
	if err != nil {
		return nil, err
	}

	step := tier.Steps[0]
	switch step.Code {
	case models.PhoneVerificationStep:
		if step.Status != models.StepVerified {
			if err = repo.db.Where("user_id = ?", user.ID).Order("updated_at desc").Limit(1).Find(&phoneVerificationEntry).Error; err != nil {
				switch err.Error() {
				case "record not found":
				default:
					return nil, err
				}
			}

			if phoneVerificationEntry.ID != nil {
				itemEnabled := false
				itemEnabledAt := phoneVerificationEntry.UpdatedAt.Add(time.Minute * 3)
				if !time.Now().Before(itemEnabledAt) {
					itemEnabled = true
					itemEnabledAt = time.Time{}
				}

				for _, item := range step.ContentItems {
					switch item.Code == "phone_number" || item.Code == "country_code" {
					case true:
						item.Enabled = itemEnabled
						item.EnabledAt = itemEnabledAt
						repo.db.Model(&item).Updates(map[string]interface{}{"enabled": itemEnabled, "enabled_at": itemEnabledAt})
					default:
					}
				}
			}
		}
	default:
		log.Info().Msg("reached default flow")
	}

	return &step, nil
}

func (repo *KYCRepo) UpdateKYCTierStep(user *models.User, tierCode int, stepCode string, contentItems map[string]string) error {
	tier := models.KYCTier{}
	phoneVerificationEntry := models.PhoneVerification{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps", "code = ? and status = ?", stepCode, models.StepOpened).Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	step := tier.Steps[0]

	if tierCode != models.Tier0 ||
		stepCode != models.PhoneVerificationStep {
		return nil
	}

	if len(contentItems["phone_verified"]) == 0 {
		err = repo.db.Where("user_id = ?", user.ID.String()).Order("updated_at desc").Limit(1).Find(&phoneVerificationEntry).Error
		if err != nil {
			return err
		}

		itemEnabled := true
		itemEnabledAt := phoneVerificationEntry.UpdatedAt.Add(time.Minute * 3)
		if time.Now().Before(itemEnabledAt) {
			itemEnabled = false
		}

		for _, item := range step.ContentItems {
			if item.Code == "phone_number" || item.Code == "country_code" {
				repo.db.Model(&item).Updates(map[string]interface{}{"status": phoneVerificationEntry.Status, "value": contentItems[item.Code], "enabled": itemEnabled, "enabled_at": itemEnabledAt})
			}
		}

		return nil
	}

	err = repo.db.Where("user_id = ? and status = ?", user.ID.String(), models.PVVerified).Order("updated_at desc").Limit(1).Find(&phoneVerificationEntry).Error
	if err != nil {
		return err
	}

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	if phoneVerificationEntry.ID != nil {
		for _, item := range step.ContentItems {
			if item.Code == "phone_number" || item.Code == "country_code" {
				tx.Model(&item).Updates(map[string]interface{}{"status": phoneVerificationEntry.Status, "enabled": false, "enabled_at": time.Time{}})
			}
		}
	}

	tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepVerified})
	tx.Model(&models.KYCTierStep{}).Where("kyc_tier_id = ? and code = ?", tier.ID, models.CommonInfo).Select("status").Updates(map[string]interface{}{"status": models.StepOpened})

	if err = tx.Commit().Error; err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (repo *KYCRepo) GetKYCMediaItemStatus(user *models.User, itemType models.KYCMediaItemType) (models.KYCTierMediaItemStatus, error) {
	err := repo.db.Preload("KYCTiers", func(db *gorm.DB) *gorm.DB {
		return db.Where("code  = ?", models.Tier0)
	}).Preload("KYCTiers.Steps", func(db *gorm.DB) *gorm.DB {
		return db.Where("code = ?", models.IdentityScans)
	}).Preload("KYCTiers.Steps.ContentItems", func(db *gorm.DB) *gorm.DB {
		return db.Where("code = ?", itemType)
	}).Find(&user).Error
	if err != nil {
		return "", err
	}

	item := user.KYCTiers[0].Steps[0].ContentItems[0]

	return item.Status, err
}

func (repo *KYCRepo) UpdateKYCMediaItemStatus(user *models.User, itemType models.KYCMediaItemType, status models.KYCTierMediaItemStatus) error {
	err := repo.db.Preload("KYCTiers", "code  = ?", models.Tier0).
		Preload("KYCTiers.Steps", "code = ?", models.IdentityScans).
		Preload("KYCTiers.Steps.ContentItems", "code = ?", itemType).Find(&user).Error

	item := user.KYCTiers[0].Steps[0].ContentItems[0]

	err = repo.db.Model(&item).Select("status").Updates(map[string]interface{}{"status": status}).Error

	if err != nil {
		return err
	}
	return nil
}

func (repo *KYCRepo) UpdateKYCTierStepComplianceReview(user *models.User) error {
	tier := models.KYCTier{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier1).Preload("Steps", "code = ? and status = ?", models.ComplianceReview, models.StepOpened).Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	step := tier.Steps[0]
	contentItem := step.ContentItems[0]

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	if err = tx.Model(&contentItem).Updates(map[string]interface{}{"status": models.StepCustomerConfirmed, "value": strconv.FormatBool(true), "enabled": false}).Error; err != nil {
		tx.Rollback()
		return err
	}

	if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepCustomerConfirmed}).Error; err != nil {
		tx.Rollback()
		return err
	}

	if err = tx.Model(&tier).Select("status").Updates(map[string]interface{}{"status": models.TierSubmitted}).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (repo *KYCRepo) UpdateKYCTierStepCommonInfo(user *models.User, data *models.StepCommonInfoForm) error {
	tier := models.KYCTier{}
	countryDictionaryRecord := models.DictionaryCountryPhoneCode{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier0).Preload("Steps", "code = ? and status = ?", models.CommonInfo, models.StepOpened).Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	step := tier.Steps[0]
	contentItems := step.ContentItems
	fmt.Println(len(contentItems))

	for _, item := range contentItems {
		switch item.Code {
		case "reg_address_country":
			switch len(data.RegAddressCountry) > 2 {
			case true:
				if err = repo.db.Where("country = ?", data.RegAddressCountry).Find(&countryDictionaryRecord).Error; err != nil {
					return err
				}
				item.Value = countryDictionaryRecord.CountryCode
			default:
				item.Value = data.RegAddressCountry
			}
		case "reg_address_zip":
			item.Value = data.RegAddressZIP
		case "reg_address_city":
			item.Value = data.RegAddressCity
		case "reg_address_details_1":
			item.Value = data.RegAddressDetails
		case "reg_address_details_2":
			item.Value = data.RegAddressDetailsExt
		case "contribution_option":
			item.Value = data.ContributionOption
		case "contribution_bank_name":
			if data.ContributionOption == models.OptFiat {
				item.Value = data.BankName
			}
		case "contribution_iban":
			if data.ContributionOption == models.OptFiat {
				item.Value = data.BankIBAN
			}
		case "contribution_bank_address":
			if data.ContributionOption == models.OptFiat {
				item.Value = data.BankAddress
			}
		}

		err = repo.db.Model(&item).Updates(map[string]interface{}{"status": models.StepCustomerConfirmed, "value": item.Value, "enabled": false}).Error
		if err != nil {
			return err
		}
	}

	step.Status = models.StepCustomerConfirmed
	err = repo.db.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepCustomerConfirmed}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) GetKYCTiers(user *models.User) ([]models.KYCTier, error) {
	var tiers []models.KYCTier
	var err error

	err = repo.db.Preload("KYCTiers", func(db *gorm.DB) *gorm.DB {
		return db.Where("code <= ? and status not in (?)", user.KYCInvestmentPlan.Code, []string{models.TierLocked}).Order("kyc_tier.code ASC")
	}).Preload("KYCTiers.Steps", func(db *gorm.DB) *gorm.DB {
		return db.Order("kyc_tier_step.step_order ASC")
	}).Find(&user).Error

	if err != nil {
		return tiers, err
	}
	return user.KYCTiers, nil
}

func (repo *KYCRepo) GetKYCTierByStageID(user *models.User, stageID int) (models.KYCTier, error) {
	var tier models.KYCTier

	err := repo.db.Where("user_id = ? and crm_stage_id = ?", user.ID, stageID).Preload("Steps").Find(&tier).Error
	if err != nil {
		return tier, err
	}

	return tier, nil
}

func (repo *KYCRepo) GetKYCTierFlag(user *models.User, tierCode int) ([]models.KYCTierFlag, error) {
	var flags []models.KYCTierFlag
	err := repo.db.
		Raw(`
		select fl.id, fl.kyc_tier_id, fl.code, fl.value, fl.created_at, fl.updated_at, fl.deleted_at
		from kyc_tier_flag fl
		inner join kyc_tier tr on tr.id = fl.kyc_tier_id
		where tr.user_id = ? and tr.code = ?`, user.ID.String(), tierCode).Scan(&flags).Error
	if err != nil {
		return flags, err
	}
	return flags, nil
}

func (repo *KYCRepo) CreateKYCTierFlag(kycTierFlag *models.KYCTierFlag) error {
	err := repo.db.Create(kycTierFlag).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) GetKYCCountryBlacklistByCode(code string) (models.KYCCountryBlacklist, error) {
	var kycCountryBlacklist models.KYCCountryBlacklist

	err := repo.db.Where("code = ?", code).Find(&kycCountryBlacklist).Error
	if err != nil {
		return kycCountryBlacklist, err
	}

	return kycCountryBlacklist, nil
}

func (repo *KYCRepo) CreateKYCTemplateRecords(user *models.User) error {

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	var kycTier models.KYCTier
	u := uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 0
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = 7

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	var kycTierStep models.KYCTierStep
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepOpened
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Mobile phone verification"
	kycTierStep.Code = "phone_verification"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	var kycTierStepContentItem models.KYCTierStepContentItem
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = "country_code"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 2
	kycTierStepContentItem.Code = "phone_number"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 2
	kycTierStep.Title = "Contributor information"
	kycTierStep.Code = "common_info"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = "reg_address_country"

	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 2
	kycTierStepContentItem.Code = "reg_address_zip"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 3
	kycTierStepContentItem.Code = "reg_address_city"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 4
	kycTierStepContentItem.Code = "reg_address_details_1"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 5
	kycTierStepContentItem.Code = "reg_address_details_2"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 6
	kycTierStepContentItem.Code = "contribution_option"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 7
	kycTierStepContentItem.Code = "contribution_bank_name"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 8
	kycTierStepContentItem.Code = "contribution_iban"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 9
	kycTierStepContentItem.Code = "contribution_bank_address"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 3
	kycTierStep.Title = "Identity document verification"
	kycTierStep.Code = "identity_scans"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = models.IDFrontSide
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 2
	kycTierStepContentItem.Code = models.IDBackSide
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 3
	kycTierStepContentItem.Code = models.IDSelfie
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTier = *new(models.KYCTier)
	u = uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 1
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = 8

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Compliance officer review"
	kycTierStep.Code = "compliance_review"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = "review_consent"
	kycTierStepContentItem.Value = strconv.FormatBool(false)
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTier = *new(models.KYCTier)
	u = uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 2
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = 9

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Proof of address verification"
	kycTierStep.Code = "address_ownership"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 2
	kycTierStep.Title = "UBO verification"
	kycTierStep.Code = "ubo"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 3
	kycTierStep.Title = "PEP verification"
	kycTierStep.Code = "pep"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 4
	kycTierStep.Title = "Source of funds verification"
	kycTierStep.Code = "funds_source"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTier = *new(models.KYCTier)
	u = uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 3
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = 10

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Video call verification"
	kycTierStep.Code = "video_call"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 2
	kycTierStep.Title = "Legal entity verification"
	kycTierStep.Code = "legal_entity"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (repo *KYCRepo) GetKYCTierStepContentItemByCode(kycTierStep *models.KYCTierStep, code string) (models.KYCTierStepContentItem, error) {
	var kycTierStepContentItem models.KYCTierStepContentItem

	err := repo.db.Where("kyc_tier_step_id = ? and code = ?", kycTierStep.ID, code).Find(&kycTierStepContentItem).Error
	if err != nil {
		return kycTierStepContentItem, err
	}

	return kycTierStepContentItem, nil
}

func (repo *KYCRepo) UpdateKYCTierStepContentItem(kycTierStepContentItem *models.KYCTierStepContentItem) error {
	err := repo.db.Save(kycTierStepContentItem).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) GetAllBlacklistedUserCountries(email string) ([]models.KYCCountryBlacklist, error) {
	var blacklist []models.KYCCountryBlacklist

	err := repo.db.
		Raw(`
		select bl.name
from kyc_country_blacklist bl
       inner join account_activities aa on bl.code = aa.cfip_country
where  aa.content ->> 'email' = ?`, email).Scan(&blacklist).Error
	if err != nil {
		return blacklist, err
	}

	return blacklist, nil
}

func (repo *KYCRepo) UpdateKYCTierStepByModel(kycTierStep *models.KYCTierStep) error {
	err := repo.db.Save(kycTierStep).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) UpdateKYCTierByModel(kycTier *models.KYCTier) error {
	err := repo.db.Save(kycTier).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *KYCRepo) DeletePhoneVerification(kycTierStep *models.KYCTierStep) error {
	item1, err := repo.GetKYCTierStepContentItemByCode(kycTierStep, "phone_number")
	if err != nil {
		return err
	}

	item1.Value = ""
	item1.Enabled = true
	item1.EnabledAt = time.Unix(0, 0)
	item1.Status = ""

	err = repo.db.Save(&item1).Error
	if err != nil {
		return err
	}

	item2, err := repo.GetKYCTierStepContentItemByCode(kycTierStep, "country_code")
	if err != nil {
		return err
	}

	item2.Value = ""
	item2.Enabled = true
	item2.EnabledAt = time.Unix(0, 0)
	item2.Status = ""

	err = repo.db.Save(&item2).Error
	if err != nil {
		return err
	}

	return nil
}
