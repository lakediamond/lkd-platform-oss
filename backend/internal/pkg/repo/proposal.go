package repo

import (
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//ProposalRepository contains all using methods for proposals table
type ProposalRepository interface {
	UpdateProposal(proposal *models.Proposal) error
	GetProposalByID(id uint) (models.Proposal, error)
	GetProposalByBlockchainID(blockchainID uint) (models.Proposal, error)
	GetProposalByTxHash(txHash string) (models.Proposal, error)
	GetAllProposals() ([]models.Proposal, error)
	GetAllUserProposals(ethAddress string) ([]models.Proposal, error)
	GetAllProposalsToMatch(price decimal.Decimal) ([]models.Proposal, error)
	GetAllProposalsToMatchSimulate(price decimal.Decimal) ([]models.Proposal, error)
	SetAllProposalsBannedByEthAddress(address string) error
}

//ProposalRepo is ProposalRepository main implementation
type ProposalRepo struct {
	db *gorm.DB
}

//UpdateProposal creating new or updating proposal
func (repo *ProposalRepo) UpdateProposal(proposal *models.Proposal) error {
	err := repo.db.Save(proposal)
	if err != nil {
		return err.Error
	}
	return nil
}

//GetProposalByID returns proposal finding by id, if no record with actual id - returns error
func (repo *ProposalRepo) GetProposalByID(id uint) (models.Proposal, error) {
	var proposal models.Proposal

	err := repo.db.Where("id = ?", id).Find(&proposal)
	if err != nil {
		return proposal, err.Error
	}

	return proposal, nil
}

//GetProposalByBlockchainID returns proposal finding by blockchain_id, if no record with actual blockchain_id - returns error
func (repo *ProposalRepo) GetProposalByBlockchainID(blockchainID uint) (models.Proposal, error) {
	var proposal models.Proposal

	err := repo.db.Where("blockchain_id = ?", blockchainID).Find(&proposal)
	if err != nil {
		return proposal, err.Error
	}

	return proposal, nil
}

//GetAllProposals returns all proposals
func (repo *ProposalRepo) GetAllProposals() ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

//GetAllUserProposals returns all user proposals
func (repo *ProposalRepo) GetAllUserProposals(ethAddress string) ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Where("created_by = ?", ethAddress).Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

//GetAllUserProposals returns all user proposals
func (repo *ProposalRepo) GetAllProposalsToMatch(price decimal.Decimal) ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Where("price <= ? AND status = ? AND banned = ?", price, "active", false).Order("price, blockchain_id").Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

//GetAllUserProposals returns all user proposals
func (repo *ProposalRepo) GetAllProposalsToMatchSimulate(price decimal.Decimal) ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Where("price <= ? AND status = ?", price, "active").Order("price, blockchain_id").Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

func (repo *ProposalRepo) GetProposalByTxHash(txHash string) (models.Proposal, error) {
	var proposal models.Proposal
	err := repo.db.Table("proposals").Where("tx_hash = ?", txHash).Find(&proposal).Error
	if err != nil {
		return proposal, err
	}

	return proposal, nil
}

func (repo *ProposalRepo) SetAllProposalsBannedByEthAddress(address string) error {
	err := repo.db.Table("proposals").Where("created_by = ?", address).Updates(map[string]interface{}{"banned": true}).Error
	if err != nil {
		return err
	}

	return nil
}
