package repo

import (
	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//UserRepository contains all using methods for users table
type VerificationRepository interface {
	Create(verificationResult *models.VerificationResult) error
	GetLastResultRecord(user *models.User) (models.VerificationResult, error)
}

//UserRepo is UserRepository main implementation
type VerificationRepo struct {
	db *gorm.DB
}

//CreateNewUser creating new user, if user exists or cannot create - returns error
func (repo *VerificationRepo) Create(verificationResult *models.VerificationResult) error {

	err := repo.db.Create(verificationResult).Error
	if err != nil {
		return err
	}

	return nil
}

//GetLastResultRecord returns last verification result received from KYC layer for given user, ordered by callback_date
func (repo *VerificationRepo) GetLastResultRecord(user *models.User) (models.VerificationResult, error) {
	var result models.VerificationResult
	err := repo.db.Model(&models.VerificationResult{}).Where("user_id = ?", user.ID.String()).Order("callback_date desc").Limit(1).Find(&result).Error
	if err != nil {
		return result, err
	}

	return result, nil
}
