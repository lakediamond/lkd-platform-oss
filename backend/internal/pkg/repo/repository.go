package repo

import (
	"github.com/jinzhu/gorm"
	//postgres database driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//Repo contains all table repositories
type Repo struct {
	DB                    *gorm.DB
	SupportData           SupportDataRepository
	User                  UserRepository
	JwtToken              JwtTokenRepository
	OrderTypes            OrderTypesRepository
	Order                 OrderRepository
	Proposal              ProposalRepository
	SubOwner              SubOwnerRepository
	AccountActivity       AccountActivityRepository
	Scopes                ScopeRepository
	Matches               MatchesRepository
	PhoneVerification     PhoneVerificationRepository
	IPLock                IPLockRepository
	KYC                   KYCRepository
	VerificationResult    VerificationRepository
	AccessRecoveryRequest UserAccessRecoveryRequestRepository
}
