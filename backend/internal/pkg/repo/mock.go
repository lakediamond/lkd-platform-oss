package repo

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/pkg/enviroment"
)

//GetDbMockClient initializing database with test params
func GetDbMockClient() *Repo {
	myEnv := enviroment.GetEnv()

	dbName := myEnv["DATABASE_NAME"]
	dbUser := myEnv["DATABASE_USER"]
	dbPass := myEnv["DATABASE_PASSWORD"]
	dbHost := myEnv["DATABASE_HOST"]
	dbPort := myEnv["DATABASE_PORT"]

	db, err := gorm.Open("postgres", fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		dbUser, dbPass, dbName, dbHost, dbPort))
	if err != nil {
		log.Panic().Msg(err.Error())
	}

	return &Repo{
		db,
		&SupportDataRepo{db},
		&UserRepo{db},
		&JwtTokenRepo{db},
		&OrderTypesRepo{db},
		&OrderRepo{db},
		&ProposalRepo{db},
		&SubOwnerRepo{db},
		&AccountActivityRepo{db},
		&ScopeRepo{db},
		&MatchesRepo{db},
		&PhoneVerificationRepo{db},
		&IPLockRepo{db},
		&KYCRepo{db},
		&VerificationRepo{db},
		&UserAccessRecoveryRequestRepo{db},
	}
}
