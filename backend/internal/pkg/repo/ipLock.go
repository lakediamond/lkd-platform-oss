package repo

import (
	"time"

	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

type IPLockRepository interface {
	CreateNewLock(lock *models.IPLock) error
	IsLocked(email, ipAddress string) bool
	GetLock(email, ipAddress string) (*models.IPLock, error)
}

type IPLockRepo struct {
	db *gorm.DB
}

func (repo *IPLockRepo) CreateNewLock(lock *models.IPLock) error {

	err := repo.db.Save(lock).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *IPLockRepo) IsLocked(email, ipAddress string) bool {
	var lock models.IPLock

	err := repo.db.Where("email = ?", email).
		Where("ip_address = ?", ipAddress).
		Find(&lock).Error
	if err != nil {
		return false
	}

	return lock.TimeTo.After(time.Now())
}

func (repo *IPLockRepo) GetLock(email, ipAddress string) (*models.IPLock, error) {
	var lock models.IPLock

	err := repo.db.Where("email = ?", email).
		Where("ip_address = ?", ipAddress).
		Find(&lock).Error
	if err != nil {
		return nil, err
	}

	return &lock, nil

}
