package repo

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//SubOwnerRepository contains all using methods for sub_owners table
type SubOwnerRepository interface {
	UpdateSubOwner(owner *models.SubOwner) error
	GetAllSubOwners() ([]models.SubOwner, error)
	GetAllSubOwnersJoinUsers() ([]Admin, error)
	IsSubOwner(address string) bool
}

//SubOwnerRepo is SubOwnerRepository main implementation
type SubOwnerRepo struct {
	db *gorm.DB
}

//UpdateSubOwner creating new or updating sub_owner
func (repo *SubOwnerRepo) UpdateSubOwner(owner *models.SubOwner) error {

	err := repo.db.Save(owner).Error
	if err != nil {
		return err
	}

	return nil
}

//GetAllSubOwners returns all sub_owners, returns err if nothing to return
func (repo *SubOwnerRepo) GetAllSubOwners() ([]models.SubOwner, error) {
	var owners []models.SubOwner

	err := repo.db.Find(&owners).Error
	if err != nil {
		return owners, err
	}

	return owners, nil
}

//GetAllSubOwnersJoinUsers returns all sub_owners with is_admin == true and join user table with some fields
func (repo *SubOwnerRepo) GetAllSubOwnersJoinUsers() ([]Admin, error) {

	var admins []Admin

	err := repo.db.Table("sub_owners").
		Select("users.id, users.first_name, users.last_name, users.email, sub_owners.eth_address").
		Where("sub_owners.is_owner = ?", true).
		Joins("left join users on sub_owners.eth_address = users.eth_address").
		Scan(&admins).Error
	if err != nil {
		return admins, err
	}

	return admins, nil
}

func (repo *SubOwnerRepo) IsSubOwner(address string) bool {

	var subOwner models.SubOwner
	err := repo.db.Where("eth_address = ? AND is_owner = ?", address, true).Find(&subOwner).Error
	if err != nil {
		return false
	}

	return true
}

//Admin struct made for output to frontend
type Admin struct {
	ID         uuid.UUID `json:"id"`
	FirstName  string    `json:"first_name"`
	LastName   string    `json:"last_name"`
	Email      string    `json:"email"`
	EthAddress string    `json:"eth_address"`
}
