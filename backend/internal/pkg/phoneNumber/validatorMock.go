package phoneNumber

import (
	"fmt"

	"github.com/rs/zerolog/log"
)

type validatorMock struct{}

//NewValidatorMock return new phone number validator mock instance
func NewValidatorMock() *validatorMock {
	return &validatorMock{}
}

//ValidateNumber mock for ValidateNumber function
func (config *validatorMock) ValidateNumber(phoneNumber string) (bool, error) {

	log.Debug().Msg(fmt.Sprintf("Number is valid"))

	return true, nil
}
