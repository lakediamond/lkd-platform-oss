package matcherBackend

import (
	"encoding/hex"
	"errors"
	"math/big"
	"strconv"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//event ProposalCompleted(uint indexed id, uint indexed tokenAmount, uint indexed orderId, uint ethAmount, bool isActive);
func completeProposal(repo *repo.Repo, ethLog types.Log) error {

	id, err := strconv.ParseUint(hex.EncodeToString(ethLog.Topics[1].Bytes()), 16, 64)
	if err != nil {
		log.Debug().Msg("cannot parse uint from id")
		return err
	}

	var tokenAmount = new(big.Int)
	tokenAmount, ok := tokenAmount.SetString(hex.EncodeToString(ethLog.Topics[2].Bytes()), 16)
	if !ok {
		log.Debug().Msg("cannot parse uint from token amount")
		return errors.New("cannot parse uint from token amount")
	}

	orderID, err := strconv.ParseUint(hex.EncodeToString(ethLog.Topics[3].Bytes()), 16, 64)
	if err != nil {
		log.Debug().Msg("cannot parse uint from id")
		return err
	}

	var ethAmount = new(big.Int)
	ethAmount, ok = ethAmount.SetString(hex.EncodeToString(ethLog.Data[:32]), 16)
	if !ok {
		log.Debug().Msg("cannot parse uint from ether amount")
		return errors.New("cannot parse uint from ether amount")
	}

	isActive := ethLog.Data[63] == 1

	proposal, err := repo.Proposal.GetProposalByBlockchainID(uint(id))
	if err != nil {
		return err
	}

	proposal.Amount = proposal.Amount.Sub(decimal.NewFromBigInt(tokenAmount, 0))
	if !isActive {
		proposal.Status = models.Closed
		accountActivity.ProposalClosed(repo, &proposal)
	}

	err = repo.Proposal.UpdateProposal(&proposal)
	if err != nil {
		return err
	}

	var match models.Matches
	match.TxHash = ethLog.TxHash.String()
	match.ProposalBlockchainID = proposal.BlockchainID
	match.OrderBlockchainID = uint(orderID)
	match.Tokens = decimal.NewFromBigInt(tokenAmount, 0)
	match.Ether = decimal.NewFromBigInt(ethAmount, 0)

	err = repo.Matches.CreateNewMatch(&match)
	if err != nil {
		log.Error().Err(err).Msg("cannot save match")
	}

	log.Debug().Msgf("proposal successfully completed. BlockchainId: %d, amount rest: %s, proposal status: %s",
		proposal.BlockchainID, proposal.Amount.String(), proposal.Status)

	return nil
}
