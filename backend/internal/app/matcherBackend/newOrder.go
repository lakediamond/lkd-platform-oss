package matcherBackend

import (
	"encoding/hex"
	"math/big"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/matcherApplication"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//NewOrder(_from, _price, msg.value, _tknAmount, _type, _metadata);
func newOrder(app *matcherApplication.Application, ethLog types.Log) error {

	var from = "0x" + ethLog.Topics[1].String()[26:]

	var price = new(big.Int)
	price, ok := price.SetString(hex.EncodeToString(ethLog.Topics[2].Bytes()), 16)
	if !ok {
		log.Debug().Msg("cannot parse bigint from price")
	}

	var ethAmount = new(big.Int)
	ethAmount, ok = ethAmount.SetString(hex.EncodeToString(ethLog.Topics[3].Bytes()), 16)
	if !ok {
		log.Debug().Msg("cannot parse bigint from ethAmount")
	}

	myAbi, err := abi.JSON(strings.NewReader(storageAbi))
	if err != nil {
		log.Fatal().Msg("invalid storageContract abi")
		return err
	}

	var orderStruct OrderStruct
	myAbi.Unpack(&orderStruct, "NewOrder", ethLog.Data)

	var order models.Order

	order.CreatedBy = from
	order.Price = decimal.NewFromBigInt(price, 0)
	order.EthAmount = decimal.NewFromBigInt(ethAmount, 0)

	order.TokenAmount = decimal.NewFromBigInt(orderStruct.TknAmount, 0)

	orderType, err := strconv.ParseUint(orderStruct.Type.String(), 10, 64)
	if err != nil {
		log.Debug().Msg("cannot parse uint from orderType")
		return err
	}

	order.TypeID = uint(orderType)

	orderTypeDB, err := app.Repo.OrderTypes.GetOrderTypeByID(uint(orderType))
	if err != nil {
		log.Error().Err(err).Msg("cannot get orderType from DB")
		return err
	}
	order.Types = orderTypeDB

	order.Metadata = orderStruct.Metadata

	orderID, err := strconv.ParseUint(orderStruct.OrderID.String(), 10, 64)
	if err != nil {
		log.Debug().Msg("cannot parse uint from orderID")
		return err
	}

	order.TxHash = ethLog.TxHash.String()

	order.BlockchainID = uint(orderID)

	order.CreatedOn = app.EthClient.Internal.GetBlockTimestamp(big.NewInt(int64(ethLog.BlockNumber)), 0)

	if !order.EthAmount.Equal(decimal.New(0, 0)) {
		coefficient, _ := strconv.Atoi(order.Price.Mul(order.TokenAmount).Div(order.EthAmount).Ceil().String())
		order.Coefficient = coefficient
	} else {
		order.Coefficient = 0
	}

	err = app.Repo.Order.UpdateOrder(&order)
	if err != nil {
		return err
	}

	err = banProposals(app.Repo, orderStruct.BannedAddresses)
	if err != nil {
		log.Error().Err(err).Msg("banProposals error")
	}

	defer func() {
		if err := recover(); err != nil {
			log.Error().Msgf("Recovered after add order fail, err: %s", err)
		}
	}()
	accountActivity.OrderAdded(app.Repo, &order)

	log.Info().Msgf("new order successfully stored. "+
		"TxHash: %s, orderBlockchainID: %d, from: %s, price: %s, ether amount: %s, token amount: %s, type: %d, metadata: %s",
		order.TxHash, order.BlockchainID, order.CreatedBy, order.Price.String(), order.EthAmount.String(), order.TokenAmount.String(), order.TypeID, order.Metadata)

	log.Info().Msg("waiting 20 second to process banned accounts")
	time.Sleep(20 * time.Second)

	defer func() {
		if err := recover(); err != nil {
			log.Error().Msgf("Recovered after match order fail, err: %s", err)
		}
	}()
	MathOrder(app, &order)

	return nil
}

//OrderStruct need to parse ethereum data from NewOrder event. DO NOT CHANGE OrderId to OrderID
type OrderStruct struct {
	TknAmount       *big.Int
	Type            *big.Int
	Metadata        string
	OrderID         *big.Int
	BannedAddresses []common.Address
}

func banProposals(repo *repo.Repo, bannedAddresses []common.Address) error {
	var err error
	for _, address := range bannedAddresses {
		err = repo.Proposal.SetAllProposalsBannedByEthAddress(address.String())
		if err != nil {
			log.Error().Err(err).Msgf("banProposals error for address: %s", address.String())
		}
	}
	if err != nil {
		return err
	}

	return nil
}
