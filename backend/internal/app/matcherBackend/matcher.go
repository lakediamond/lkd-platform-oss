package matcherBackend

import (
	"errors"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/matcherApplication"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func MathOrder(app *matcherApplication.Application, order *models.Order) error {
	log.Info().Msg("Matcher starts")

	proposals, err := app.Repo.Proposal.GetAllProposalsToMatch(order.Price)
	if err != nil {
		log.Debug().Err(err).Msg("cannot get proposals from DB")
		return err
	}

	if order.EthAmount == decimal.New(0, 0) {
		return errors.New("order eth amount = 0, dividing by zero")
	}

	var ids []*big.Int
	var tokenValues []*big.Int
	var etherValues []*big.Int

	orderAmountBuf := order.TokenAmount

	rest := order.EthAmount

	coefficient := decimal.New(int64(order.Coefficient), 0)

	log.Debug().Msgf("chfCoefficient: %s", coefficient.String())

	for _, proposal := range proposals {
		// x<y
		ids = append(ids, big.NewInt(int64(proposal.BlockchainID)))
		if orderAmountBuf.Cmp(proposal.Amount) == -1 {
			x, _ := new(big.Int).SetString(orderAmountBuf.String(), 10)
			tokenValues = append(tokenValues, x)

			valueBuf := orderAmountBuf.Mul(proposal.Price).Div(coefficient).Floor()

			x, _ = new(big.Int).SetString(valueBuf.String(), 10)

			etherValues = append(etherValues, x)

			rest = rest.Sub(valueBuf)

			break
		} else {
			x, _ := new(big.Int).SetString(proposal.Amount.String(), 10)
			tokenValues = append(tokenValues, x)

			orderAmountBuf = orderAmountBuf.Sub(proposal.Amount)

			valueBuf := proposal.Amount.Mul(proposal.Price).Div(coefficient).Floor()

			x, _ = new(big.Int).SetString(valueBuf.String(), 10)

			etherValues = append(etherValues, x)

			rest = rest.Sub(valueBuf)
		}

		if rest.Equal(decimal.New(0, 0)) {
			break
		}
	}

	io, err := ioContract.NewIoContract(common.HexToAddress(app.EthClient.MatcherData.IOContractAddress), app.EthClient)

	k, err := app.EthClient.Internal.GetOwnerPrivateKey()
	if err != nil {
		log.Error().Err(err).Msg("getOwnerPrivateKey error")
		return err
	}

	key, err := crypto.HexToECDSA(k)
	if err != nil {
		log.Error().Err(err).Msg("invalid private key")
	}
	auth := bind.NewKeyedTransactor(key)

	x, _ := new(big.Int).SetString(rest.String(), 10)
	log.Debug().Msgf("order blockchain id: %d", big.NewInt(int64(order.BlockchainID)))
	log.Debug().Msgf("proposal ids: %v", ids)
	log.Debug().Msgf("token values: %v", tokenValues)
	log.Debug().Msgf("ether values: %v", etherValues)
	log.Debug().Msgf("rest: %d", x)

	var iterator int

sendTx:
	tx, err := io.MatchOrder(auth, big.NewInt(int64(order.BlockchainID)), ids)
	if err != nil {
		if iterator < 3 {
			log.Error().Err(err).Msgf("%d retry", iterator+1)
			iterator++
			time.Sleep(time.Second)
			goto sendTx
		}

		return err
	}

	k = ""
	key = nil

	log.Info().Msgf("Matcher action completed, tx: %s", tx.Hash().String())
	log.Print("matcher finished")
	return nil
}
