package matcherBackend

import (
	"encoding/hex"
	"strconv"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func newOrderType(repo *repo.Repo, ethLog types.Log) error {
	var orderType models.OrderType

	id, err := strconv.ParseUint(hex.EncodeToString(ethLog.Topics[1].Bytes()), 16, 64)
	if err != nil {
		log.Error().Err(err).Msg("cannot parse number from new order type event")
		return err
	}
	orderType.ID = uint(id)

	myAbi, err := abi.JSON(strings.NewReader(definitionString))
	if err != nil {
		log.Fatal().Msg("invalid definition for ethereum string")
		return err
	}

	var description string
	myAbi.Unpack(&description, "definitionString", ethLog.Data)

	orderType.Name = description

	_, err = repo.OrderTypes.GetOrderTypeByID(orderType.ID)
	if err != nil {
		accountActivity.OrderTypeAdded(repo, &orderType)
	} else {
		accountActivity.OrderTypeUpdated(repo, &orderType)
	}

	err = repo.OrderTypes.SaveOrderType(&orderType)
	if err != nil {
		return err
	}

	log.Debug().Msgf("new order type successfully stored. Id: %d, description: %s", orderType.ID, orderType.Name)

	return nil
}

var definitionString = `[{ "name" : "definitionString", "constant" : false, "outputs": [ { "name": "String", "type": "string" } ] }]`
