package matcherBackend

import (
	"context"
	"encoding/hex"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/matcherApplication"
)

func handleNodeErr(app *matcherApplication.Application) {
	app.EthClient = etherPkg.InitializePlatformEthereumApplication(app.EthClient.MatcherData)
	RunListener(app)
}

//RunListener starting event listener after start program or reconnect to ethereum node
func RunListener(app *matcherApplication.Application) {
	go listenPastEvents(app)
	ListenEvents(app)
}

func listenPastEvents(app *matcherApplication.Application) {
	lastBlock := app.Repo.SupportData.GetLastBlock()

	var filter ethereum.FilterQuery

	filter.Addresses = []common.Address{common.HexToAddress(app.EthClient.MatcherData.LakeDiamond)}
	filter.FromBlock = big.NewInt(int64(lastBlock + 1))

	logs, filterErr := app.EthClient.FilterLogs(context.Background(), filter)

	if filterErr != nil {
		log.Error().Err(filterErr).Msgf("failed to get filter logs for contract ")
		return
	}

	for _, result := range logs {
		checkLakeDiamondLog(app, result)
		lastBlock = uint(result.BlockNumber)
	}
	if lastBlock != 0 {
		dbLastBlock := app.Repo.SupportData.GetLastBlock()
		if lastBlock > dbLastBlock {
			app.Repo.SupportData.SetLastBlock(lastBlock)
		}
	}

	log.Debug().Msg("listenSubOwnerPastEvents successfully finished")
}

//ListenEvents listen events from IO smart contract
func ListenEvents(app *matcherApplication.Application) {
	var eventChannel = make(chan types.Log)
	var filter ethereum.FilterQuery

	filter.Addresses = []common.Address{common.HexToAddress(app.EthClient.MatcherData.LakeDiamond)}

	s, err := app.EthClient.SubscribeFilterLogs(context.Background(), filter, eventChannel)
	if err != nil {
		log.Error().Err(err).Msgf("Unable to start event listener, retrying")
		time.Sleep(5 * time.Second)
		goto loop
	}
	log.Info().Msg("Main contract event listener started")
	for {
		select {
		case err := <-s.Err():
			log.Error().Err(err).Msgf("Logs subscription error. Resetting connection")
			goto loop
		case l := <-eventChannel:
			go checkLakeDiamondLog(app, l)
		}
	}

loop:
	handleNodeErr(app)
}

func checkLakeDiamondLog(app *matcherApplication.Application, ethLog types.Log) {
	switch ethLog.Topics[0].String() {
	case app.EthClient.MatcherData.NewSubOwner:
		err := newSubOwner(app.Repo, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("new subOwner event error")
		}

	case app.EthClient.MatcherData.RemovedSubOwner:
		err := removedSubOwner(app.Repo, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("removed subOwner event error")
		}

	case app.EthClient.MatcherData.NewOrder:
		err := newOrder(app, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("new order event error")
		}

	case app.EthClient.MatcherData.NewProposal:
		err := newProposal(app, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("new proposal event error")
		}

	case app.EthClient.MatcherData.WithdrawProposal:
		err := withdrawProposal(app, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("withdraw proposal event error")
		}

	case app.EthClient.MatcherData.NewOrderType:
		err := newOrderType(app.Repo, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("new order type event error")
		}

	case app.EthClient.MatcherData.CompleteProposal:
		err := completeProposal(app.Repo, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("complete proposal event error")
		}

	case app.EthClient.MatcherData.CompleteOrder:
		err := closeOrder(app, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("complete order event error")
		}

	case app.EthClient.MatcherData.BannedAddress:
		err := banAllUserProposals(app, ethLog)
		if err != nil {
			log.Debug().Err(err).Msg("banAllUserProposals event error")
		}

	default:
		log.Debug().Msgf("unhandled with topic[0]: %s", hex.EncodeToString(ethLog.Topics[0].Bytes()))
	}

	log.Debug().Msgf("setting last block by %d", ethLog.BlockNumber)
	app.Repo.SupportData.SetLastBlock(uint(ethLog.BlockNumber))
}
