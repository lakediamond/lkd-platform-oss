package matcherBackend

import (
	"encoding/hex"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func newSubOwner(repo *repo.Repo, ethLog types.Log) error {
	userAddress := "0x" + (hex.EncodeToString(ethLog.Data))[24:]

	repo.SubOwner.UpdateSubOwner(&models.SubOwner{EthAddress: userAddress, IsOwner: true})
	accountActivity.SubOwnerAdded(repo, userAddress)

	user, err := repo.User.GetUserByEthAddress(userAddress)
	if err != nil {
		log.Debug().Msgf("New subOwner with eth address %s received, but no such address id DB", userAddress)
		return nil
	} else {
		log.Debug().Msgf("User %s role successfully changed to admin", user.Email)
	}

	user.Role = models.AdminRole

	err = repo.User.UpdateUser(&user)
	if err != nil {
		return err
	}

	return nil
}

func removedSubOwner(repo *repo.Repo, ethLog types.Log) error {
	var userAddress = "0x" + (hex.EncodeToString(ethLog.Data))[24:]

	repo.SubOwner.UpdateSubOwner(&models.SubOwner{EthAddress: userAddress})
	accountActivity.SubOwnerRemoved(repo, userAddress)

	user, err := repo.User.GetUserByEthAddress(userAddress)
	if err != nil {
		log.Debug().Msgf("New subOwner with eth address %s received, but no such address id DB", userAddress)
		return nil
	} else {
		log.Debug().Msgf("User %s role successfully changed to user", user.Email)
	}

	user.Role = models.UserRole

	err = repo.User.UpdateUser(&user)
	if err != nil {
		return err
	}

	return nil
}
