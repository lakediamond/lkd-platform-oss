package matcherBackend

import (
	"encoding/hex"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/matcherApplication"
)

func banAllUserProposals(app *matcherApplication.Application, ethLog types.Log) error {
	ethAddress := hex.EncodeToString(ethLog.Data)

	err := app.Repo.Proposal.SetAllProposalsBannedByEthAddress(ethAddress)
	if err != nil {
		return err
	}

	log.Debug().Msgf("User proposals with eth address: %s banned.", ethAddress)

	return nil
}
