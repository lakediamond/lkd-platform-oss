package matcherBackend

import (
	"encoding/hex"
	"errors"
	"math/big"
	"strconv"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/matcherApplication"
)

//event OrderProcessed(uint indexed id, uint rest);
func closeOrder(app *matcherApplication.Application, ethLog types.Log) error {

	id, err := strconv.ParseUint(hex.EncodeToString(ethLog.Topics[1].Bytes()), 16, 64)
	if err != nil {
		log.Debug().Msg("cannot parse uint from id")
		return err
	}

	order, err := app.Repo.Order.GetOrderByBlockchainID(uint(id))
	if err != nil {
		log.Debug().Err(err).Msg("get order error")
		return err
	}

	var rest = new(big.Int)
	rest, ok := rest.SetString(hex.EncodeToString(ethLog.Data), 16)
	if !ok {
		log.Debug().Msg("cannot parse decimal from rest")
		return errors.New("cannot parse decimal from rest")
	}

	order.EthAmountLeft = decimal.NewFromBigInt(rest, 0)
	order.CompletedOn = app.EthClient.Internal.GetBlockTimestamp(big.NewInt(int64(ethLog.BlockNumber)), 0)

	err = app.Repo.Order.UpdateOrder(&order)
	if err != nil {
		return err
	}

	accountActivity.OrderMatched(app.Repo, &order)

	log.Debug().Msgf("new order successfully closed. "+
		"orderBlockchainID: %d",
		order.BlockchainID)

	return nil
}
