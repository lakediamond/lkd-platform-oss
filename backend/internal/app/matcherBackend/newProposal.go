package matcherBackend

import (
	"encoding/hex"
	"errors"
	"math/big"
	"strconv"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/matcherApplication"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//event ProposalCreated(address indexed from, uint indexed price, uint indexed amount, uint id);
func newProposal(app *matcherApplication.Application, ethLog types.Log) error {
	var from = "0x" + ethLog.Topics[1].String()[26:]

	var price = new(big.Int)
	price, ok := price.SetString(hex.EncodeToString(ethLog.Topics[2].Bytes()), 16)
	if !ok {
		log.Debug().Msg("cannot parse uint from price")
		return errors.New("cannot parse uint from price")
	}

	var tokenAmount = new(big.Int)
	tokenAmount, ok = tokenAmount.SetString(hex.EncodeToString(ethLog.Topics[3].Bytes()), 16)
	if !ok {
		log.Debug().Msg("cannot parse uint from token amount")
		return errors.New("cannot parse uint from token amount")
	}

	id, err := strconv.ParseUint(hex.EncodeToString(ethLog.Data), 16, 64)
	if err != nil {
		log.Debug().Msg("cannot parse uint from blockchain id")
		return err
	}

	var proposal models.Proposal

	proposal.BlockchainID = uint(id)
	proposal.TxHash = ethLog.TxHash.String()
	proposal.Price = decimal.NewFromBigInt(price, 0)
	proposal.Amount = decimal.NewFromBigInt(tokenAmount, 0)
	proposal.Status = models.Active

	proposal.OriginalTokens = decimal.NewFromBigInt(tokenAmount, 0)

	proposal.CreatedBy = from

	proposal.CreatedOn = app.EthClient.Internal.GetBlockTimestamp(big.NewInt(int64(ethLog.BlockNumber)), 0)

	err = app.Repo.Proposal.UpdateProposal(&proposal)
	if err != nil {
		return err
	}

	accountActivity.ProposalCreated(app.Repo, &proposal)

	log.Debug().Msgf("new proposal successfully stored. "+
		"Tx hash: %s, blockchainId: %d,  from: %s, price: %s, amount %s",
		proposal.TxHash, proposal.BlockchainID, from, proposal.Price.String(), proposal.Amount.String())

	return nil
}
