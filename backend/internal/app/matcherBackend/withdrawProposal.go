package matcherBackend

import (
	"encoding/hex"
	"errors"
	"math/big"
	"strconv"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/matcherApplication"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//event ProposalWithdrawn(uint indexed amount, uint indexed id, bool isActive);
func withdrawProposal(app *matcherApplication.Application, ethLog types.Log) error {

	var amount = new(big.Int)
	amount, ok := amount.SetString(hex.EncodeToString(ethLog.Topics[1].Bytes()), 16)
	if !ok {
		log.Debug().Msg("cannot parse uint from amount")
		return errors.New("cannot parse uint from amount")
	}

	id, err := strconv.ParseUint(hex.EncodeToString(ethLog.Topics[2].Bytes()), 16, 64)
	if err != nil {
		log.Debug().Msg("cannot parse uint from id")
		return err
	}

	isActive := ethLog.Data[31] == 1

	proposal, err := app.Repo.Proposal.GetProposalByBlockchainID(uint(id))
	if err != nil {
		return err
	}

	proposal.WithdrawnAmount = decimal.NewFromBigInt(amount, 0)
	proposal.Amount = proposal.Amount.Sub(decimal.NewFromBigInt(amount, 0))

	if !isActive {
		proposal.Status = models.Withdrawn
	}

	time := app.EthClient.Internal.GetBlockTimestamp(big.NewInt(int64(ethLog.BlockNumber)), 0)
	proposal.WithdrawnOn = &time

	err = app.Repo.Proposal.UpdateProposal(&proposal)
	if err != nil {
		return err
	}

	accountActivity.ProposalWithdrawn(app.Repo, &proposal)

	log.Debug().Msgf("proposal successfully withdrawn. BlockchainId: %d, amount withdrawn: %s, proposal status: %s",
		proposal.BlockchainID, proposal.WithdrawnAmount.String(), proposal.Status)

	return nil
}
