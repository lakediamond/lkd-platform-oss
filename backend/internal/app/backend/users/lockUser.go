package users

import (
	"lkd-platform-backend/internal/pkg/repo"
)

func lockAccount(repo *repo.Repo, userStr string) error {
	user, err := repo.User.GetUserByEmail(userStr)
	if err != nil {
		return err
	}

	user.Locked = true
	repo.User.UpdateUser(&user)

	return nil
}
