package users

import (
	"errors"
	"fmt"
	"net/url"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	mailjetAdapter "lkd-platform-backend/internal/pkg/mailjet"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type (
	ErrEmailAlreadyVerified struct {
		ErrBase
	}
)

var (
	errEmailAlreadyVerified = ErrEmailAlreadyVerified{ErrBaseNew("user email has been already verified")}
)

func sendVerificationEmail(app *application.Application, user *models.User, loginChallenge string) error {
	token := CreateEmailToken(app.Config.JwtSalt, user.Email)

	cfgTemplate := app.Mailjet.GetConfigTemplate("verification")

	callbackUrl, err := GenerateCallbackUrl(cfgTemplate.Variables["url"].(string),
		map[string]string{"token": token, "login_challenge": loginChallenge})

	if err != nil {
		return err
	}

	cfgTemplate.Variables["url"] = callbackUrl.String()
	cfgTemplate.Variables["firstname"] = user.FirstName
	cfgTemplate.Variables["lastname"] = user.LastName

	cfgTemplate.ToName = fmt.Sprintf("%s %s", user.FirstName, user.LastName)
	cfgTemplate.ToEmail = user.Email

	message, err := mailjetAdapter.BuildMessageByConfig(cfgTemplate)
	if err != nil {
		return err
	}

	log.Fatal()
	messagesInfo := []mailjet.InfoMessagesV31{*message}

	MailjetSendEmail(app.Mailjet, messagesInfo)

	return nil
}

func GenerateCallbackUrl(templateUrl string, paramValues map[string]string) (*url.URL, error) {
	urlParsed, err := url.Parse(templateUrl)

	if err != nil {
		return nil, err
	}

	urlQueryParams := urlParsed.Query()
	for key, value := range paramValues {
		urlQueryParams.Set(key, value)
	}
	urlParsed.RawQuery = urlQueryParams.Encode()

	log.Debug().Msg("email verification url " + urlParsed.String())

	return urlParsed, nil
}

func MailjetSendEmail(sender *mailjetAdapter.Sender, messagesInfo []mailjet.InfoMessagesV31) {
	res, err := sender.Send(messagesInfo)
	if err != nil {
		log.Error().Err(err).Msg("Mailjet response error")
	}
	log.Debug().Msgf("Data: %+v\n", res)
}

//CreateEmailToken creating JWT token to verify email
func CreateEmailToken(jwtSalt, email string) string {

	expire := time.Now().UTC().Add(24 * time.Hour).Unix()

	claims := &jwt.MapClaims{
		"exp":   expire,
		"email": email,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	ss, _ := token.SignedString([]byte(jwtSalt))

	log.Debug().Msgf("Created new email token for: %s, token: %s", email, ss)
	return ss
}

func verifyEmailToken(app *application.Application, tokenString string) (*models.User, error) {

	claims := jwt.MapClaims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(app.Config.JwtSalt), nil
	})
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("invalid token")
	}

	if !claims.VerifyExpiresAt(time.Now().UTC().Unix(), true) {
		return nil, errors.New("expired token")
	}

	email := claims["email"].(string)

	user, err := app.Repo.User.GetUserByEmail(email)
	if err != nil {
		return nil, err
	}

	if user.EmailVerified == true {
		err = errors.New(errEmailAlreadyVerified.Error())
		log.Debug().Msgf("user %s already verified", email)
		return nil, err
	}

	user.EmailVerified = true
	err = app.Repo.User.UpdateUser(&user)
	if err != nil {
		return &user, err
	}

	return &user, nil
}
