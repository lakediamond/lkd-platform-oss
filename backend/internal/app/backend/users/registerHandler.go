package users

import (
	"errors"
	"net/http"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//RegisterData JSON request body to registration endpoint
type RegisterData struct {
	FirstName      string `json:"first_name"`
	LastName       string `json:"last_name"`
	Email          string `json:"email"`
	Password       string `json:"password"`
	EthAddress     string `json:"hash"`
	BirthDate      string `json:"birth_date"`
	LoginChallenge string `json:"login_challenge"`
	InvestmentPlan int    `json:"investment_plan"`
}

//RegisterAccountHandler handling user registration endpoint
var RegisterAccountHandler = handlers.ApplicationHandler(registerAccount)

func registerAccount(w http.ResponseWriter, r *http.Request) {
	log.Debug().Msg("registering new account")
	w.Header().Set("Content-Type", "application/json")

	var registerData RegisterData

	err := httputil.ParseBody(w, r.Body, &registerData)
	if err != nil {
		return
	}

	if registerData.LoginChallenge == "" {
		err := errors.New("login_challenge request body parameter not provided or empty")
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	app := application.GetApplicationContext(r)

	ipConfig := httputil.GetIPDetails(r)

	errs := createUser(app, &registerData, ipConfig.Address, ipConfig.Country)
	if len(errs) > 0 {
		log.Error().Msgf("%v", errs)
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteMultiplyErrorMsg(w, errs)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
