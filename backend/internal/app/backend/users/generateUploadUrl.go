package users

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

var GetGenerateUrlHandler = handlers.ApplicationHandler(gcGetGenerateUrl)

func gcGetGenerateUrl(w http.ResponseWriter, r *http.Request) {

	routeParams := httprouter.ParamsFromContext(r.Context())
	imageType := routeParams.ByName("image_type")

	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	exists, err := app.GC.ExistsImage(user.ID.String(), imageType)
	if err != nil {
		log.Error().Err(err).Msg("ExistsImage error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if exists {
		w.WriteHeader(http.StatusNotModified)
		return
	}

	content := r.Header.Get("response-content-disposition")

	lines := strings.Split(strings.ToLower(content), "filename=")
	if len(lines) != 2 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	presignedURL, err := app.GC.GeneratePresignedURL(user.ID.String(), lines[1])
	if err != nil {
		log.Error().Err(err).Msg("getGenerateUrlService error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = app.Repo.KYC.UpdateKYCMediaItemStatus(&user, imageType, "")
	log.Debug().Msgf("Successfully generated presigned URL ", presignedURL)

	resp, _ := json.Marshal(map[string]string{"url": presignedURL})

	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

//
//func gUrl() {
//	pkey, err := ioutil.ReadFile("my-private-key.pem")
//	if err != nil {
//		// TODO: handle error.
//	}
//	url, err := storage.SignedURL("my-bucket", "my-object", &storage.SignedURLOptions{
//		GoogleAccessID: "xxx@developer.gserviceaccount.com",
//		PrivateKey:     pkey,
//		Method:         "GET",
//		Expires:        time.Now().Add(48 * time.Hour),
//	})
//	if err != nil {
//		// TODO: handle error.
//	}
//	fmt.Println(url)
//}
//
//func gcStorage() {
//
//	var bucket storage.BucketHandle
//
//	c, _ := storage.NewClient()
//	c.Bucket().Objects()
//
//	io.WriteString(d.w, "\nListbucket result:\n")
//
//	query := &storage.Query{Prefix: "foo"}
//	it := d.bucket.Objects(d.ctx, query)
//	for {
//		obj, err := it.Next()
//		if err == iterator.Done {
//			break
//		}
//		if err != nil {
//			d.errorf("listBucket: unable to list bucket %q: %v", d.bucketName, err)
//			return
//		}
//		d.dumpStats(obj)
//	}
//}
