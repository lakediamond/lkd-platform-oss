package kyc

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

//BTCSFlowHandler handling BTCS flow endpoint
var BTCSFlowHandler = handlers.ApplicationHandler(btcsFlowHandler)

func btcsFlowHandler(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	routeParams := httprouter.ParamsFromContext(r.Context())
	pathParam := routeParams.ByName("user_id")

	userID, err := uuid.FromString(pathParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if !app.Repo.User.IsExistsByID(&userID) {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	err = updateKYCProgress(app, &userID)
	if err != nil {
		log.Error().Err(err).Msg("btcsFlowHandlerError")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
