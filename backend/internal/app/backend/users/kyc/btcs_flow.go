package kyc

import (
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"

	"github.com/satori/go.uuid"
)

func updateKYCProgress(app *application.Application, userID *uuid.UUID) error {

	user, err := app.Repo.User.GetUserByID(userID)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.CompleteKYCTierStepIdentityScans(&user)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.UpdateKYCTierStatus(&user, models.Tier0, models.TierSubmitted)
	if err != nil {
		return err
	}

	if user.KYCInvestmentPlan.Code == models.Tier1 {
		err = app.Repo.KYC.UnlockKYCTierByCode(&user, models.Tier1)
		if err != nil {
			return err
		}
	}

	return nil
}
