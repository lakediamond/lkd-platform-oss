package kyc

import (
	"log"
	"strings"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func handleProcessingError(app *application.Application, userID *uuid.UUID, verificationResult *models.VerificationResult) error {
	splitter := strings.Split(verificationResult.IdentityVerification, "is invalid, ")
	if len(splitter) != 2 {
		return errors.New("invalid verificationResult")
	}

	log.Print("original data from kyc ", verificationResult.IdentityVerification)
	imageName := splitter[1]
	user, err := app.Repo.User.GetUserByID(userID)

	log.Print("image name ", imageName)
	imageMetainfo, err := app.Storage.GetImageMetainfo(&user, imageName)
	if err != nil {
		return err
	}

	log.Print("image metainfo ", imageMetainfo)

	if err = app.Storage.InvalidateImage(&user, imageMetainfo); err != nil {
		return err
	}

	return nil
}
