package kyc

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

var SubmitSuccessImageValidationHandler = handlers.ApplicationHandler(submitSuccessImageValidation)

func submitSuccessImageValidation(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	routeParams := httprouter.ParamsFromContext(r.Context())
	id := routeParams.ByName("user_id")

	userID, err := uuid.FromString(id)
	if err != nil {
		log.Error().Err(err).Msg("invalid userID")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	user, err := app.Repo.User.GetUserByID(&userID)
	if err != nil {
		log.Info().Err(err).Msg("invalid userID")
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	imageType := routeParams.ByName("image_type")

	err = submitSuccessImageValidationService(app.Repo, &user, imageType)
	if err != nil {
		log.Error().Err(err).Msg("submitSuccessImageValidationService error")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

var SubmitInvalidImageValidationHandler = handlers.ApplicationHandler(submitInvalidImageValidation)

func submitInvalidImageValidation(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	routeParams := httprouter.ParamsFromContext(r.Context())
	id := routeParams.ByName("user_id")

	userID, err := uuid.FromString(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var verificationResult models.VerificationResult
	err = httputil.ParseBody(w, r.Body, &verificationResult)
	if err != nil {
		return
	}

	defer r.Body.Close()

	if verificationResult.VerificationStatus != "PROCESSING_ERROR" {
		err := errors.New("invalid request verification status for this route")
		log.Error().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, err)
		return
	}

	user, err := app.Repo.User.GetUserByID(&userID)
	if err != nil {
		log.Info().Err(err).Msg("invalid userID")
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	imageType := routeParams.ByName("image_type")

	err = submitFailedImageValidationService(app.Repo, &user, imageType)
	if err != nil {
		log.Error().Err(err).Msg("submitFailedImageValidationService error")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	err = handleProcessingError(app, &userID, &verificationResult)
	if err != nil {
		log.Error().Err(err).Msg("handleProcessingError")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
