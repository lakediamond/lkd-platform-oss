package users

import (
	"errors"
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/app/backend/users/kyc"
	"lkd-platform-backend/internal/app/backend/users/pipeDrive"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

var (
	errFailedToSetTier = errors.New("failed to set target tier on user record")
)

func updateInvestmentPlanService(app *application.Application, user *models.User, payload *UpdateInvestmentPlanInput) error {
	if !user.IsValidTier(payload.InvestmentPlan) {
		err := errors.New("required parameter `investment_plan` is missing or invalid")
		return err
	}

	plan, err := app.Repo.KYC.GetKYCPlanByCode(payload.InvestmentPlan)
	if err != nil {
		err := errors.New(fmt.Sprintf("failed to get investment plan for request - %v", payload))
		return err
	}

	if err = app.Repo.User.UpdateTargetTier(user, plan); err != nil {
		return errFailedToSetTier
	}

	if verificationResult, err := app.Repo.VerificationResult.GetLastResultRecord(user); err != nil {
		log.Warn().Msg(fmt.Sprintf("There was no previous verification records for user ID - %s", user.ID.String()))
	} else {
		if err = kyc.CreateKYCPostIdentityVerificationFlags(app, &verificationResult); err != nil {
			log.Error().Err(err).Msg("createKYCPostIdentityVerificationFlags error")
			return err
		}
	}

	if user.CRMDealID > 0 {
		err = pipeDrive.UpdateCustomerCRMDeal(app, user)
		if err != nil {
			err = fmt.Errorf("update CRM deal for customer - %s", err.Error())
			return err
		}
	}

	return nil
}
