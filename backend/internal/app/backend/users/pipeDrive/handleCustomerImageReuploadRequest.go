package pipeDrive

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"

	"github.com/Jeffail/gabs"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/crm"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func CustomerImageReUploadService(app *application.Application, customerImageReUploadRequestInput *models.CustomerImageReUploadRequestInput) ([]string, error) {
	processedImages := []string{}

	user, err := app.Repo.User.GetUserByDealID(customerImageReUploadRequestInput.Current.DealID)
	if err != nil {
		return processedImages, err
	}

	str := strings.Replace(customerImageReUploadRequestInput.Current.Subject, " ", "", -1)
	taskSubjectItems := strings.Split(str, ",")

	for _, taskSubjectItem := range taskSubjectItems {
		imageMetainfo, err := app.Storage.GetImageMetainfo(&user, taskSubjectItem)
		if err != nil {
			return processedImages, err
		}

		if imageMetainfo != nil {
			if err = app.Storage.InvalidateImage(&user, imageMetainfo); err != nil {
				return processedImages, err
			}
			processedImages = append(processedImages, taskSubjectItem)
		}
	}

	if len(processedImages) > 0 {
		if err = app.Repo.KYC.UnlockKYCTierStep(&user, models.Tier0, models.IdentityScans); err != nil {
			return processedImages, err
		}

		if err = app.Repo.KYC.SetReworkKYCTier(&user, models.Tier0); err != nil {
			return processedImages, err
		}
	}

	return processedImages, nil
}

func SetActivityDone(crmData crm.DataInterface, customerImageReUploadRequestInput *models.CustomerImageReUploadRequestInput, processedImages []string) error {
	//https://api.pipedrive.com/v1/activities/1?api_token=01f1329bf98c5c8e5d9ffe0970c09445b7fea1e9

	url := fmt.Sprintf("https://api.pipedrive.com/v1/activities/%d?api_token=%s", customerImageReUploadRequestInput.Current.ID, crmData.GetToken())

	jsonObj := gabs.New()
	jsonObj.Set(customerImageReUploadRequestInput.Current.ID, "id")
	jsonObj.Set("1", "done")
	jsonObj.Set(fmt.Sprintf("Number of images processed - %d", len(processedImages)), "note")

	client := &http.Client{}

	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(jsonObj.Bytes()))
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("setActivityDone returns %d response", resp.StatusCode)
	}

	return nil
}
