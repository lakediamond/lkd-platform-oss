package pipeDrive

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/httputil"
)

type CreateCRMEntityResponse struct {
	Success bool `json:"success"`
	Data    struct {
		ID uint64 `json:"id"`
	} `json:"data"`
	Error string `json:"error"`
}

//UpdateCustomerCRMDeal updates or creates if doesn't exist CRM deal for customer
func UpdateCustomerCRMDeal(app *application.Application, user *models.User) error {
	var crmPipelineStage int64
	crmDealID := ""
	isCreateDeal := false
	if user.CRMDealID == 0 {
		isCreateDeal = true
	}

	rate, err := app.Repo.KYC.GetKYCTierFailureRate(user, models.Tier0)
	if err != nil {
		return err
	}

	switch rate {
	case 0:
		if user.KYCInvestmentPlan.Code == 0 {
			crmPipelineStage = 7
		} else {
			err = app.Repo.KYC.UnlockKYCTierByCode(user, models.Tier1)
			if err != nil {
				return err
			}

			crmPipelineStage = 8
		}
	case 1:
		failureFlags, err := app.Repo.KYC.GetKYCTierFailedFlags(user, models.Tier0)
		if err != nil {
			return err
		}

		crmPipelineStage = failureFlags[0].CRMPipelineFailureStage
	default:
		crmPipelineStage = 26
	}

	if !isCreateDeal {
		crmDealID = fmt.Sprintf("/%s", strconv.FormatUint(user.CRMDealID, 10))
	}
	url := fmt.Sprintf("https://api.pipedrive.com/v1/deals%s?api_token=%s", crmDealID, app.CRMData.GetToken())

	t := time.Now()

	jsonObj := gabs.New()
	jsonObj.Set(fmt.Sprintf("%s %s - LD-KYC-%s", user.LastName, user.FirstName, t.Format("20060102150405")), "title")
	jsonObj.Set(user.CRMPersonID, "person_id")
	jsonObj.Set(2, "org_id")
	jsonObj.Set(crmPipelineStage, "stage_id")
	jsonObj.Set("open", "status")
	jsonObj.Set(3, "visible_to")

	kycInvestmentPlan, err := app.Repo.KYC.GetInvestmentPlanByCode(user.InvestmentPlan)
	if err != nil {
		return err
	}

	jsonObj.Set(kycInvestmentPlan.MaxLimit, "value")

	kycTierFlags, err := app.Repo.KYC.GetKYCTierFlag(user, 0)
	if err != nil {
		return err
	}

	for _, kycTierFlag := range kycTierFlags {
		switch kycTierFlag.Code {
		case app.CRMData.GetDeal().NameSanctionScreeningValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().NameSanctionScreeningValue)
		case app.CRMData.GetDeal().TelephoneFromAllowedCountryValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().TelephoneFromAllowedCountryValue)
		case app.CRMData.GetDeal().ResidentialAddressFromAllowedCountryValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().ResidentialAddressFromAllowedCountryValue)
		case app.CRMData.GetDeal().IDDocumentBitaccessVerificationValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().IDDocumentBitaccessVerificationValue)
		case app.CRMData.GetDeal().IDDocumentFromAllowedCountryValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().IDDocumentFromAllowedCountryValue)
		case app.CRMData.GetDeal().SelfieBitaccessVerificationValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().SelfieBitaccessVerificationValue)
		case app.CRMData.GetDeal().EthereumAddressScorechainScreeningValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().EthereumAddressScorechainScreeningValue)
		case app.CRMData.GetDeal().IPConnectionFromAllowedCountryValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetDeal().IPConnectionFromAllowedCountryValue)
		}
	}
	log.Info().Msgf("url: %s", url)
	log.Info().Msgf("object: %s", jsonObj.String())

	r := bytes.NewReader(jsonObj.Bytes())
	resp := &http.Response{}
	if isCreateDeal {
		log.Debug().Msgf("url: %s", url)
		log.Debug().Msgf("object: %s", jsonObj.String())

		resp, err = http.Post(url, "application/json", r)
		if err != nil {
			return err
		}

		if resp.StatusCode != http.StatusCreated {
			return errors.New(fmt.Sprintf("UpdateCustomerCRMDeal returns %d response", resp.StatusCode))
		}
	} else {
		client := &http.Client{}
		req, updateDealErr := http.NewRequest("PUT", url, r)
		if updateDealErr != nil {
			return updateDealErr
		}

		req.Header.Add("Content-Type", "application/json")
		resp, err = client.Do(req)
		if err != nil {
			return err
		}

		if resp.StatusCode != http.StatusOK {
			return errors.New(fmt.Sprintf("UpdateCustomerCRMDeal returns %d response", resp.StatusCode))
		}
	}
	defer resp.Body.Close()

	data := httputil.ReadBody(resp.Body)
	// defer resp.Body.Close()

	var createCustomerApplicationResponse CreateCRMEntityResponse
	err = json.Unmarshal(data, &createCustomerApplicationResponse)
	if err != nil {
		return err
	}

	if !createCustomerApplicationResponse.Success {
		return errors.New(createCustomerApplicationResponse.Error)
	}

	user.CRMDealID = createCustomerApplicationResponse.Data.ID
	err = app.Repo.User.UpdateUserCRMDeal(user)
	if err != nil {
		return err
	}

	err = uploadIDScanImages(app, user)
	if err != nil {
		return err
	}

	//err = gcUploadIDScanImages(app, user)
	//if err != nil {
	//	log.Error().Err(err).Msg("gcUploadIDScanImages error")
	//}

	return nil
}

//func gcUploadIDScanImages(app *application.Application, user *models.User) error {
//	images, err := app.GC.GetKYCImages(user.ID.String())
//	if err != nil {
//		return err
//	}
//
//	for _, image := range images {
//		err := app.GC.CopyImageToDeal(user.ID.String(), string(user.CRMDealID), image)
//		if err != nil {
//			log.Error().Err(err).Msg("CopyImageToDeal error")
//		}
//
//		err = app.Repo.KYC.UpdateKYCMediaItemCRMFileID(user, strings.Split(image.Name, ".")[0], crmResponse.Data.ID)
//		if err != nil {
//			return err
//		}
//	}
//
//}

func uploadIDScanImages(app *application.Application, user *models.User) error {
	images, err := app.Storage.GetKYCImages(user)
	if err != nil {
		return err
	}

	for _, image := range images {
		err := addMinioPhoto(app, user, strconv.Itoa(int(user.CRMDealID)), image.Name)
		if err != nil {
			log.Error().Err(err).Msg("addMinioPhoto error")
		}
	}

	return nil
}

func addMinioPhoto(app *application.Application, user *models.User, dealID, objName string) error {

	//sending file to deal
	imageName := strings.Split(objName, "/")[2]
	imageNameParts := strings.Split(imageName, ".")

	//downloading file from minio client
	imageReader, err := app.Storage.GetImageReader(user, imageName)
	if err != nil {
		return err
	}

	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	fileWriter, err := bodyWriter.CreateFormFile("file", fmt.Sprintf("%s-%s.%s", imageNameParts[0], time.Now().Format("2006-01-02 15:04:05"), imageNameParts[1]))
	if err != nil {
		return err
	}

	_, err = io.Copy(fileWriter, imageReader)
	if err != nil {
		return err
	}

	contentType := bodyWriter.FormDataContentType()

	bodyWriter.WriteField("deal_id", dealID)
	bodyWriter.Close()

	url := fmt.Sprintf("https://api.pipedrive.com/v1/files?api_token=%s", app.CRMData.GetToken())

	resp, err := http.Post(url, contentType, bodyBuf)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		return errors.New(fmt.Sprintf("addMinioPhoto returns status, %d", resp.StatusCode))
	}

	body := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	var crmResponse CreateCRMEntityResponse
	err = json.Unmarshal(body, &crmResponse)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.UpdateKYCMediaItemCRMFileID(user, strings.Split(imageName, ".")[0], crmResponse.Data.ID)
	if err != nil {
		return err
	}

	return nil
}
