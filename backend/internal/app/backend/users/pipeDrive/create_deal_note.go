package pipeDrive

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/Jeffail/gabs"
	"github.com/satori/go.uuid"
	"github.com/tidwall/pretty"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/httputil"
)

type CreateDealNoteResponse struct {
	Success bool   `json:"success"`
	Error   string `json:"error"`
}

func CreateDealNote(app *application.Application, btcsResult *models.VerificationResult, userID *uuid.UUID) error {

	user, err := app.Repo.User.GetUserByID(userID)
	if err != nil {
		return err
	}

	url := fmt.Sprintf("https://api.pipedrive.com/v1/notes?api_token=%s", app.CRMData.GetToken())

	jsonObj := gabs.New()
	jsonObj.Set(user.CRMDealID, "deal_id")

	screeningBytes, err := json.Marshal(btcsResult.VerifiedNameSanctionsScreen)
	jsonObj.Set(string(pretty.Pretty(screeningBytes)), "content")

	r := bytes.NewReader(jsonObj.Bytes())
	resp, err := http.Post(url, "application/json", r)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body := httputil.ReadBody(resp.Body)

	var createDealNoteResponse CreateDealNoteResponse
	err = json.Unmarshal(body, &createDealNoteResponse)
	if err != nil {
		return err
	}

	if !createDealNoteResponse.Success {
		return fmt.Errorf("error: %s, cannot create new deal note, userID: %v", createDealNoteResponse.Error, user.ID)
	}

	return nil
}
