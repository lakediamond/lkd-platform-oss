package pipeDrive

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/Jeffail/gabs"
	"github.com/pkg/errors"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/httputil"
)

//CreatePersonResponse is a response which receiving from CRM
type CreatePersonResponse struct {
	Success                  bool                     `json:"success"`
	Error                    string                   `json:"error, omitempty"`
	CreatePersonResponseData CreatePersonResponseData `json:"data"`
}

//CreatePersonResponseData is a person response data from CreatePersonResponse
type CreatePersonResponseData struct {
	ID uint `json:"id"`
}

//CreateNewPerson creating new person in CRM
func CreateNewPerson(app *application.Application, user *models.User, commonInfo *models.StepCommonInfoForm) error {

	err := duplicatePersonCheck(app, user)
	if err != nil {
		return err
	}

	jsonObj := gabs.New()
	jsonObj.Set(fmt.Sprintf("%s %s", user.LastName, user.FirstName), "name")
	jsonObj.Set(2, "org_id")
	jsonObj.Set(user.Email, "email")

	verification, err := app.Repo.PhoneVerification.GetVerifiedUserPhone(user)
	if err != nil {
		return errors.New("cannot set phone")
	}

	jsonObj.Set(verification.Phone, "phone")

	jsonObj.Set(3, "visible_to")

	modelAddr := fmt.Sprintf("%s , %s , %s, %s, %s", commonInfo.RegAddressCountry, commonInfo.RegAddressZIP, commonInfo.RegAddressCity, commonInfo.RegAddressDetails, commonInfo.RegAddressDetailsExt)

	jsonObj.Set(modelAddr, app.CRMData.GetPerson().ResidentialAddressValue)
	jsonObj.Set(user.FirstName, app.CRMData.GetPerson().FirstNameValue)
	jsonObj.Set(user.LastName, app.CRMData.GetPerson().LastNameValue)
	jsonObj.Set(user.EthAddress, app.CRMData.GetPerson().EthereumAddressValue)
	jsonObj.Set(user.BirthDate, app.CRMData.GetPerson().BirthDateValue)
	jsonObj.Set(commonInfo.BankName, app.CRMData.GetPerson().BankNameValue)
	jsonObj.Set(commonInfo.BankAddress, app.CRMData.GetPerson().BankAddressValue)
	jsonObj.Set(commonInfo.BankIBAN, app.CRMData.GetPerson().BankIbanValue)

	kycTierFlags, err := app.Repo.KYC.GetKYCTierFlag(user, 0)
	if err != nil {
		return err
	}

	for _, kycTierFlag := range kycTierFlags {
		switch kycTierFlag.Code {
		case app.CRMData.GetPerson().AgeOver18VerifiedValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetPerson().AgeOver18VerifiedValue)
		case app.CRMData.GetPerson().TelephoneUserVerificationValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetPerson().TelephoneUserVerificationValue)
		case app.CRMData.GetPerson().EmailUserVerificationValue:
			jsonObj.Set(kycTierFlag.Value, app.CRMData.GetPerson().EmailUserVerificationValue)
		}
	}

	if user.CRMPersonID != 0 {
		err = updatePerson(app, user, jsonObj)
		if err != nil {
			return nil
		}
		return nil

	}

	err = createPerson(app, user, jsonObj)
	if err != nil {
		return err
	}

	return nil
}

type PersonCheckResponse struct {
	Data []struct {
		Email   string `json:"email"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
		OrgID   int    `json:"org_id"`
		OrgName string `json:"org_name"`
		Phone   string `json:"phone"`
		Picture struct {
			Height int    `json:"height"`
			ID     int    `json:"id"`
			URL    string `json:"url"`
			Width  int    `json:"width"`
		} `json:"picture"`
		VisibleTo string `json:"visible_to"`
	} `json:"data"`
	Success bool `json:"success"`
}

func duplicatePersonCheck(app *application.Application, user *models.User) error {

	email := strings.Replace(user.Email, "@", "%40", -1)
	url := fmt.Sprintf("https://api.pipedrive.com/v1/persons/find?term=%s&start=0&search_by_email=1&api_token=%s", email, app.CRMData.GetToken())

	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("get request to: %s returns response: %d", url, resp.StatusCode)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var personCheckResponse PersonCheckResponse

	err = json.Unmarshal(body, &personCheckResponse)
	if err != nil {
		return err
	}

	if !personCheckResponse.Success {
		return fmt.Errorf("get request to: %s returns success = %v", url, personCheckResponse.Success)
	}

	if personCheckResponse.Data == nil {
		return nil
	}

	user.CRMPersonID = uint(personCheckResponse.Data[0].ID)
	err = app.Repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}

func updatePerson(app *application.Application, user *models.User, jsonObj *gabs.Container) error {
	url := fmt.Sprintf("https://api.pipedrive.com/v1/persons/%d?api_token=%s", user.CRMPersonID, app.CRMData.GetToken())
	r := bytes.NewReader(jsonObj.Bytes())

	client := &http.Client{}
	req, err := http.NewRequest("PUT", url, r)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	body := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	var createPersonResponse CreatePersonResponse
	err = json.Unmarshal(body, &createPersonResponse)
	if err != nil {
		return err
	}

	if !createPersonResponse.Success {
		return errors.New(fmt.Sprintf("error: %s, cannot create new person, userID: %v", createPersonResponse.Error, user.ID))
	}

	user.CRMPersonID = createPersonResponse.CreatePersonResponseData.ID
	err = app.Repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}

func createPerson(app *application.Application, user *models.User, jsonObj *gabs.Container) error {
	url := fmt.Sprintf("https://api.pipedrive.com/v1/persons?api_token=%s", app.CRMData.GetToken())
	r := bytes.NewReader(jsonObj.Bytes())
	resp, err := http.Post(url, "application/json", r)
	if err != nil {
		return err
	}

	body := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	var createPersonResponse CreatePersonResponse
	err = json.Unmarshal(body, &createPersonResponse)
	if err != nil {
		return err
	}

	if !createPersonResponse.Success {
		return errors.New(fmt.Sprintf("error: %s, cannot create new person, userID: %v", createPersonResponse.Error, user.ID))
	}

	user.CRMPersonID = createPersonResponse.CreatePersonResponseData.ID
	err = app.Repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}
