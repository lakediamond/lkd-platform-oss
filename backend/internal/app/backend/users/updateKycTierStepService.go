package users

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func updateKycTierStepService(app *application.Application, user *models.User, body []byte, tierCode int, step string) error {
	ks := NewKYCService(app)
	var commonInfo models.StepCommonInfoForm

	switch fmt.Sprintf("%d-%s", tierCode, step) {
	case fmt.Sprintf("%d-%s", models.Tier0, models.CommonInfo):
		if err := json.Unmarshal(body, &commonInfo); err != nil {
			log.Debug().Err(err).Msg("invalid data format " + models.CommonInfo)
			return errors.New("invalid data format")
		}

		log.Debug().Msgf("step common info form: %v", commonInfo)

		if commonInfo.BankIBAN != "" {
			if isIBANValid, validationFailures := ks.isValidIBAN(&commonInfo); !isIBANValid {
				log.Debug().Err(validationFailures[0]).Msg(validationFailures[0].Error())
				return validationFailures[0]
			}
		}

		if err := ks.updateKYCTierStepCommonInfo(app, user, &commonInfo); err != nil {
			log.Debug().Err(err).Msg("failure during execution " + models.CommonInfo)
			return errors.New("invalid data format")
		}

		// if err = ks.updateKYCTierStepCommonInfo(app, &user, &commonInfo); err != nil {
		// 	log.Debug().Err(err).Msg("failure during execution " + models.CommonInfo)
		// 	w.WriteHeader(http.StatusForbidden)
		// 	w.Write([]byte("wrong data format"))
		// 	return
		// }
	case fmt.Sprintf("%d-%s", models.Tier1, models.ComplianceReview):
		var complianceReview models.StepComplianceReviewForm
		if err := json.Unmarshal(body, &complianceReview); err != nil {
			log.Debug().Err(err).Msg("invalid data format " + models.ComplianceReview)
			return errors.New("invalid data format")
		}

		if err := ks.updateKYCTierStepComplianceReview(app, user); err != nil {
			log.Debug().Err(err).Msg("failure during execution " + models.ComplianceReview)
			return errors.New("invalid data format")
		}
	}

	return nil
}
