package httpserver

import (
	"fmt"
	"lkd-platform-backend/pkg/handlers"
	"net/http"

	"github.com/justinas/alice"

	"lkd-platform-backend/internal/pkg/application"

	"github.com/facebookgo/grace/gracehttp"
	"github.com/rs/zerolog/log"
)

// Serve will run HTTP Server on configured application PORT
func Serve(app *application.Application) {
	router := NewHTTPRouter(app)

	routes := alice.New(handlers.JSONContentTypeHandler, handlers.LoggingHandler, handlers.RecoveryHandler).Then(router)

	addr := fmt.Sprintf(":%s", app.Config.Port)
	log.Info().Msgf("Serving API on http://localhost%s", addr)
	if err := gracehttp.Serve(&http.Server{Addr: addr, Handler: routes}); err != nil {
		log.Fatal().Err(err).Msg("")
	}
}
