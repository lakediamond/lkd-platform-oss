package httpserver

import (
	"net/http"

	"github.com/justinas/alice"
	"gopkg.in/DataDog/dd-trace-go.v1/contrib/julienschmidt/httprouter"

	"lkd-platform-backend/internal/app/backend/events"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/admins"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/changePassword"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/matchSimulator"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/oauth"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/orderPrice"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/orderTypes"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/orders"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/proposals"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/recoveryPassword"
	"lkd-platform-backend/internal/app/backend/google2fa"
	"lkd-platform-backend/internal/app/backend/users"
	"lkd-platform-backend/internal/app/backend/users/kyc"
	"lkd-platform-backend/internal/app/backend/users/pipeDrive"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/httputil"
)

func protectedRoute(app *application.Application, h http.Handler) http.Handler {
	return alice.New(application.RequestID, app.WithApplicationContext, app.RequireTokenAuthentication).Then(h)
}

func adminRoute(app *application.Application, h http.Handler) http.Handler {
	return alice.New(application.RequestID, app.WithApplicationContext, app.RequireTokenAuthentication, app.RequireAdminAccess).Then(h)
}

func defaultRoute(app *application.Application, h http.Handler) http.Handler {
	return alice.New(application.RequestID, app.WithApplicationContext, httputil.WithIPConfig).Then(h)
}

func handleCors(app *application.Application, h http.Handler) http.Handler {
	return alice.New(application.RequestID, app.WithApplicationContext).ThenFunc(app.HandleCORS)
}

func basicAuth(app *application.Application, h http.Handler) http.Handler {
	return alice.New(application.RequestID, app.WithApplicationContext, app.BasicAuth).Then(h)
}

// NewHTTPRouter will return application router.
// Require instance of application to properly mount routes to application and pass application context to HTTP Handlers
func NewHTTPRouter(app *application.Application) *httprouter.Router {
	router := httprouter.New()

	routes := application.Routes{

		application.Route{Alias: "register_account", Method: "POST", Path: "/user/register", Handler: defaultRoute(app, users.RegisterAccountHandler)},
		// application.Route{Alias: "register_account", Method: "OPTIONS", Path: "/user/register", Handler: handleCors(app, users.RegisterAccountHandler)},

		application.Route{Alias: "authenticate_account", Method: "POST", Path: "/user/authenticate", Handler: defaultRoute(app, users.AuthenticateAccountHandler)},
		// application.Route{Alias: "authenticate_account", Method: "OPTIONS", Path: "/user/authenticate", Handler: handleCors(app, users.AuthenticateAccountHandler)},

		application.Route{Alias: "user_logout", Method: "POST", Path: "/user/logout", Handler: protectedRoute(app, users.UserLogoutHandler)},
		// application.Route{Alias: "user_logout", Method: "OPTIONS", Path: "/user/logout", Handler: handleCors(app, users.UserLogoutHandler)},

		application.Route{Alias: "email_confirmation", Method: "GET", Path: "/user/email_confirmation", Handler: defaultRoute(app, users.ConfirmEmailHandler)},
		// application.Route{Alias: "email_confirmation", Method: "OPTIONS", Path: "/user/email_confirmation", Handler: handleCors(app, users.ConfirmEmailHandler)},

		application.Route{Alias: "get_all_admins", Method: "GET", Path: "/admin/subowner", Handler: adminRoute(app, admins.GetAllAdminsHandler)},
		// application.Route{Alias: "get_all_admins", Method: "OPTIONS", Path: "/admin/subowner", Handler: handleCors(app, admins.GetAllAdminsHandler)},

		application.Route{Alias: "get_all_proposals", Method: "GET", Path: "/user/proposal/all", Handler: adminRoute(app, proposals.GetAllProposalsHandler)},
		// application.Route{Alias: "get_all_proposals", Method: "OPTIONS", Path: "/user/proposal/all", Handler: handleCors(app, proposals.GetAllProposalsHandler)},

		application.Route{Alias: "get_all_user_proposals", Method: "GET", Path: "/user/proposal", Handler: protectedRoute(app, proposals.GetAllUserProposalsHandler)},
		// application.Route{Alias: "get_all_user_proposals", Method: "OPTIONS", Path: "/user/proposal", Handler: handleCors(app, proposals.GetAllUserProposalsHandler)},

		application.Route{Alias: "check_user_phone_format", Method: "GET", Path: "/user/phone/format", Handler: protectedRoute(app, users.ValidatePhoneFormatHandler)},
		// application.Route{Alias: "check_user_phone_format", Method: "OPTIONS", Path: "/user/phone/format", Handler: handleCors(app, users.ValidatePhoneFormatHandler)},

		application.Route{Alias: "create_user_phone_verification", Method: "POST", Path: "/user/phone", Handler: protectedRoute(app, users.CreatePhoneVerificationHandler)},
		application.Route{Alias: "create_user_phone_verification", Method: "DELETE", Path: "/user/phone", Handler: protectedRoute(app, users.DeletePhoneVerificationHandler)},
		// application.Route{Alias: "create_user_phone_verification", Method: "OPTIONS", Path: "/user/phone", Handler: handleCors(app, users.CreatePhoneVerificationHandler)},

		application.Route{Alias: "verify_user_phone_verification", Method: "POST", Path: "/user/phone/verify", Handler: protectedRoute(app, users.VerifyPhoneHandler)},
		// application.Route{Alias: "verify_user_phone_verification", Method: "OPTIONS", Path: "/user/phone/verify", Handler: handleCors(app, users.VerifyPhoneHandler)},

		application.Route{Alias: "resend_user_phone_verification", Method: "POST", Path: "/user/phone/resend", Handler: protectedRoute(app, users.ResendPhoneVerificationHandler)},
		// application.Route{Alias: "resend_user_phone_verification", Method: "OPTIONS", Path: "/user/phone/resend", Handler: handleCors(app, users.ResendPhoneVerificationHandler)},

		application.Route{Alias: "get_all_orders", Method: "GET", Path: "/admin/order", Handler: adminRoute(app, orders.GetAllOrdersHandler)},
		// application.Route{Alias: "get_all_orders", Method: "OPTIONS", Path: "/admin/order", Handler: handleCors(app, orders.GetAllOrdersHandler)},

		application.Route{Alias: "get_orders_cumulative_data", Method: "GET", Path: "/admin/orders_cumulative_data", Handler: adminRoute(app, orders.GetOrdersCumulativeDataHandler)},
		// application.Route{Alias: "get_orders_cumulative_data", Method: "OPTIONS", Path: "/admin/orders_cumulative_data", Handler: handleCors(app, orders.GetOrdersCumulativeDataHandler)},

		application.Route{Alias: "get_all_order_types", Method: "GET", Path: "/admin/order_type", Handler: adminRoute(app, orderTypes.GetAllOrderTypesHandler)},
		// application.Route{Alias: "get_all_order_types", Method: "OPTIONS", Path: "/admin/order_type", Handler: handleCors(app, orderTypes.GetAllOrderTypesHandler)},

		application.Route{Alias: "user_lock", Method: "POST", Path: "/user/lock", Handler: adminRoute(app, users.LockUserHandler)},
		// application.Route{Alias: "user_lock", Method: "OPTIONS", Path: "/user/lock", Handler: handleCors(app, users.LockUserHandler)},

		application.Route{Alias: "user_profile", Method: "GET", Path: "/user/profile", Handler: protectedRoute(app, users.GetUserProfile)},
		// application.Route{Alias: "user_profile", Method: "OPTIONS", Path: "/user/profile", Handler: handleCors(app, users.GetUserProfile)},

		application.Route{Alias: "get_investment_plan_options", Method: "GET", Path: "/user/profile/kyc/plan", Handler: protectedRoute(app, users.GetInvestmentPlanOptionsHandler)},
		// application.Route{Alias: "get_investment_plan_options", Method: "OPTIONS", Path: "/user/profile/kyc/plan", Handler: handleCors(app, users.GetInvestmentPlanOptionsHandler)},

		application.Route{Alias: "get_user_kyc_profile", Method: "GET", Path: "/user/profile/kyc", Handler: protectedRoute(app, users.GetKycTiers)},
		application.Route{Alias: "get_user_kyc_profile", Method: "POST", Path: "/user/profile/kyc", Handler: protectedRoute(app, users.UpdateInvestmentPlan)},
		// application.Route{Alias: "get_user_kyc_profile", Method: "OPTIONS", Path: "/user/profile/kyc", Handler: handleCors(app, users.GetKycTiers)},

		application.Route{Alias: "kyc_tier_step", Method: "GET", Path: "/user/profile/kyc/tier/:tier/step/:step", Handler: protectedRoute(app, users.GetKycTierStep)},
		application.Route{Alias: "kyc_tier_step", Method: "POST", Path: "/user/profile/kyc/tier/:tier/step/:step", Handler: protectedRoute(app, users.UpdateKycTierStep)},
		// application.Route{Alias: "kyc_tier_step", Method: "OPTIONS", Path: "/user/profile/kyc/tier/:tier/step/:step", Handler: handleCors(app, users.GetKycTierStep)},

		application.Route{Alias: "s3_notification", Method: "POST", Path: "/events/s3", Handler: defaultRoute(app, events.S3EventsHandler)},

		application.Route{Alias: "check_oauth_login_request", Method: "GET", Path: "/oauth/requests/:type", Handler: defaultRoute(app, oauth.GetOAuthLoginRequest)},
		// application.Route{Alias: "check_oauth_login_request", Method: "OPTIONS", Path: "/oauth/requests/:type", Handler: handleCors(app, oauth.GetOAuthLoginRequest)},

		application.Route{Alias: "start_oauth_flow", Method: "GET", Path: "/oauth/flow/:flow", Handler: defaultRoute(app, oauth.StartOAuthFlow)},
		// application.Route{Alias: "start_oauth_flow", Method: "OPTIONS", Path: "/oauth/flow/:flow", Handler: handleCors(app, oauth.StartOAuthFlow)},

		application.Route{Alias: "process_consent", Method: "POST", Path: "/oauth/consent", Handler: defaultRoute(app, oauth.ProcessUserConsent)},
		// application.Route{Alias: "process_consent", Method: "OPTIONS", Path: "/oauth/consent", Handler: handleCors(app, oauth.ProcessUserConsent)},

		application.Route{Alias: "oauth_code", Method: "GET", Path: "/oauth/callback", Handler: defaultRoute(app, oauth.ProcessOAuthCallback)},
		// application.Route{Alias: "oauth_code", Method: "OPTIONS", Path: "/oauth/callback", Handler: handleCors(app, oauth.ProcessOAuthCallback)},

		application.Route{Alias: "order_eth_price", Method: "GET", Path: "/admin/order/price", Handler: adminRoute(app, orderPrice.GetOrderEthExchangeRateHandler)},
		// application.Route{Alias: "order_eth_price", Method: "OPTIONS", Path: "/admin/order/price", Handler: handleCors(app, orderPrice.GetOrderEthExchangeRateHandler)},

		application.Route{Alias: "user_change_password", Method: "POST", Path: "/user/change_password", Handler: protectedRoute(app, changePassword.ChangePasswordHandler)},
		// application.Route{Alias: "user_change_password", Method: "OPTIONS", Path: "/user/change_password", Handler: handleCors(app, changePassword.ChangePasswordHandler)},

		application.Route{Alias: "user_2fa_setup", Method: "POST", Path: "/user/2fa", Handler: protectedRoute(app, google2fa.EnableGoogle2FAHandler)},
		// application.Route{Alias: "user_2fa_setup", Method: "OPTIONS", Path: "/user/2fa", Handler: handleCors(app, google2fa.EnableGoogle2FAHandler)},

		application.Route{Alias: "user_2fa_disable", Method: "POST", Path: "/user/2fa/disable", Handler: protectedRoute(app, google2fa.DisableGoogle2FaHandler)},
		// application.Route{Alias: "user_2fa_disable", Method: "OPTIONS", Path: "/user/2fa/disable", Handler: handleCors(app, google2fa.DisableGoogle2FaHandler)},

		application.Route{Alias: "user_2fa_complete", Method: "POST", Path: "/user/2fa/complete", Handler: protectedRoute(app, google2fa.CompleteGoogle2FaHandler)},
		// application.Route{Alias: "user_2fa_complete", Method: "OPTIONS", Path: "/user/2fa/complete", Handler: handleCors(app, google2fa.CompleteGoogle2FaHandler)},

		application.Route{Alias: "id_verification_result", Method: "POST", Path: "/user/kyc/verification/:user_id/id", Handler: defaultRoute(app, kyc.VerificationUserHandler)},
		// application.Route{Alias: "id_verification_result", Method: "OPTIONS", Path: "/user/kyc/verification/:user_id/id", Handler: handleCors(app, kyc.VerificationUserHandler)},

		application.Route{Alias: "id_verification_url", Method: "GET", Path: "/user/kyc/media/id/:image_type", Handler: protectedRoute(app, users.GetGenerateUrlHandler)},
		// application.Route{Alias: "id_verification_url", Method: "OPTIONS", Path: "/user/kyc/media/id/:image_type", Handler: handleCors(app, users.GetGenerateUrlHandler)},

		application.Route{Alias: "btcs_flow_event", Method: "POST", Path: "/user/kyc/flow/:user_id", Handler: defaultRoute(app, kyc.BTCSFlowHandler)},
		// application.Route{Alias: "btcs_flow_event", Method: "OPTIONS", Path: "/user/kyc/flow/:user_id", Handler: protectedRoute(app, kyc.BTCSFlowHandler)},

		application.Route{Alias: "reupload_image_task", Method: "POST", Path: "/user/kyc/crm/reupload_image_task", Handler: basicAuth(app, users.CustomerImageReUploadRequestHandler)},
		application.Route{Alias: "tier_changes", Method: "POST", Path: "/user/kyc/crm/tier_changes", Handler: basicAuth(app, pipeDrive.TierAchievedHandler)},

		application.Route{Alias: "access_recovery", Method: "POST", Path: "/user/access_recovery", Handler: defaultRoute(app, recoveryPassword.RecoveryPasswordHandler)},
		// application.Route{Alias: "access_recovery", Method: "OPTIONS", Path: "/user/access_recovery", Handler: handleCors(app, recoveryPassword.RecoveryPasswordHandler)},

		application.Route{Alias: "access_recovery_activation", Method: "GET", Path: "/user/access_recovery_activation", Handler: defaultRoute(app, recoveryPassword.RecoveryPasswordFromEmailHandler)},
		// application.Route{Alias: "access_recovery_activation", Method: "OPTIONS", Path: "/user/access_recovery_activation", Handler: handleCors(app, recoveryPassword.RecoveryPasswordFromEmailHandler)},

		application.Route{Alias: "access_recovery_new_password", Method: "POST", Path: "/user/access_recovery_activation", Handler: defaultRoute(app, recoveryPassword.NewPasswordHandler)},
		// application.Route{Alias: "access_recovery_new_password", Method: "OPTIONS", Path: "/user/access_recovery_activation", Handler: defaultRoute(app, recoveryPassword.NewPasswordHandler)},

		application.Route{Alias: "verify_recovery_token", Method: "POST", Path: "/user/access_recovery_activation/check", Handler: defaultRoute(app, recoveryPassword.VerifyRecoveryTokenHandler)},
		// application.Route{Alias: "verify_recovery_token", Method: "OPTIONS", Path: "/user/access_recovery_activation/check", Handler: handleCors(app, recoveryPassword.VerifyRecoveryTokenHandler)},

		application.Route{Alias: "match_simulation", Method: "POST", Path: "/match_simulation", Handler: defaultRoute(app, matchSimulator.MatchSimulationHandler)},
		// application.Route{Alias: "match_simulation", Method: "OPTIONS", Path: "/match_simulation", Handler: handleCors(app, matchSimulator.MatchSimulationHandler)},

		application.Route{Alias: "submit_success_image_validation", Method: "PATCH", Path: "/user/kyc/verification/:user_id/id/:image_type", Handler: defaultRoute(app, kyc.SubmitSuccessImageValidationHandler)},
		application.Route{Alias: "submit_success_image_validation", Method: "DELETE", Path: "/user/kyc/verification/:user_id/id/:image_type", Handler: defaultRoute(app, kyc.SubmitInvalidImageValidationHandler)},
		// application.Route{Alias: "submit_success_image_validation", Method: "OPTIONS", Path: "/user/kyc/verification/:user_id/id/:image_type", Handler: handleCors(app, kyc.SubmitSuccessImageValidationHandler)},
	}

	for _, route := range routes {
		router.Handler(route.Method, route.Path, route.Handler)
	}

	app.MountRoutes(routes)

	return router
}
