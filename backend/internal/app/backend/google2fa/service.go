package google2fa

import (
	"lkd-platform-backend/internal/pkg/crypto"
	. "lkd-platform-backend/internal/pkg/errs"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func completeGoogle2FaService(repo *repo.Repo, user *models.User, completeGoogle2FaInput *CompleteGoogle2FaInput) error {
	if !Check2FA(user, completeGoogle2FaInput.Password) {
		return ErrInvalid2FACode
	}

	user.Google2FAEnabled = true

	err := repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}

func disableGoogle2FaService(repo *repo.Repo, user *models.User, disableGoogle2FaInput *DisableGoogle2FaInput) error {
	if !crypto.Verify(disableGoogle2FaInput.Password, user.Password) {
		return ErrInvalidPassword
	}

	if !Check2FA(user, disableGoogle2FaInput.Google2FAPass) {
		return ErrInvalid2FACode
	}

	user.Google2FAEnabled = false

	err := repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}
