package proposals

import (
	"math/big"
	"time"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type ProposalResponse struct {
	ID                    uint       `json:"id"`
	TokensAmount          *big.Int   `json:"tokens_amount"`
	CreatedOn             time.Time  `json:"created_on"`
	Price                 *big.Int   `json:"price"`
	WithdrawnOn           *time.Time `json:"withdrawn_on"`
	WithdrawnTokensAmount *big.Int   `json:"withdrawn_tokens_amount"`
}

func getProposals(repo repo.ProposalRepository) ([]ProposalResponse, error) {

	var proposalResponses []ProposalResponse

	proposals, err := repo.GetAllProposals()
	if err != nil {
		return proposalResponses, err
	}

	proposalResponses, err = combineProposalResponse(proposals)
	if err != nil {
		return proposalResponses, err
	}

	return proposalResponses, nil
}

func getUserProposals(repo repo.ProposalRepository, ethAddress string) ([]ProposalResponse, error) {

	var proposalResponses []ProposalResponse

	proposals, err := repo.GetAllUserProposals(ethAddress)
	if err != nil {
		return proposalResponses, err
	}

	proposalResponses, err = combineProposalResponse(proposals)
	if err != nil {
		return proposalResponses, err
	}

	return proposalResponses, nil
}

func combineProposalResponse(proposals []models.Proposal) ([]ProposalResponse, error) {
	var proposalResponses []ProposalResponse
	var proposalResponse ProposalResponse

	for _, proposal := range proposals {
		proposalResponse.ID = proposal.BlockchainID
		proposalResponse.TokensAmount = proposal.Amount.Coefficient()
		proposalResponse.CreatedOn = proposal.CreatedOn
		proposalResponse.WithdrawnOn = proposal.WithdrawnOn
		proposalResponse.WithdrawnTokensAmount = proposal.WithdrawnAmount.Coefficient()

		var price = new(big.Int)
		price.SetString(proposal.Price.String(), 10)

		proposalResponse.Price = price

		proposalResponses = append(proposalResponses, proposalResponse)
	}

	return proposalResponses, nil
}
