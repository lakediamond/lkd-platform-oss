package proposals

import (
	"net/http"
	"reflect"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

//GetAllProposalsHandler handling get all proposals request
var GetAllProposalsHandler = handlers.ApplicationHandler(getAllProposals)

func getAllProposals(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)


	conn, err := app.Config.WSUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error().Err(err).Msg("websocket upgrade error")
		return
	}

	proposals, err := getProposals(app.Repo.Proposal)
	if err != nil {
		log.Debug().Err(err).Msg("Get all proposals error")
		return
	}
	conn.WriteJSON(proposals)

	var lastData []ProposalResponse

	for {
		i, _, err := conn.ReadMessage()
		if i == 1000 || err != nil {
			log.Print(i)
			log.Print(err)
			//conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(1000, "closing WS connection"))
			conn.Close()
			return
		}

		proposals, err := getProposals(app.Repo.Proposal)
		if err != nil {
			log.Debug().Err(err).Msg("Get all proposals error")
			return
		}

		if reflect.DeepEqual(lastData, proposals) {
			//time.Sleep(30 * time.Second)
			continue
		}

		lastData = proposals

		if err = conn.WriteJSON(proposals); err != nil {
			return
		}

		//time.Sleep(30 * time.Second)
	}
}

//GetAllUserProposalsHandler handling get user proposals request
var GetAllUserProposalsHandler = handlers.ApplicationHandler(getAllUserProposals)

func getAllUserProposals(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	user := application.GetUser(r)

	conn, err := app.Config.WSUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error().Err(err).Msg("websocket upgrade error")
		return
	}

	proposals, err := getUserProposals(app.Repo.Proposal, user.EthAddress)
	if err != nil {
		log.Debug().Err(err).Msg("Get all proposals error")
		return
	}

	conn.WriteJSON(proposals)

	var lastData []ProposalResponse

	for {
		i, msg, err := conn.ReadMessage()
		log.Print("msg: ", msg)
		if i == 1000 || err != nil {
			log.Print("i: ", i)
			log.Print(msg)
			log.Print(err)
			//conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(1000, "closing WS connection"))
			conn.Close()
			return
		}

		proposals, err := getUserProposals(app.Repo.Proposal, user.EthAddress)
		if err != nil {
			log.Debug().Err(err).Msg("Get all proposals error")
			return
		}

		if reflect.DeepEqual(lastData, proposals) {
			//time.Sleep(30 * time.Second)
			continue
		}

		lastData = proposals

		if err = conn.WriteJSON(proposals); err != nil {
			return
		}

		//time.Sleep(30 * time.Second)
	}
}
