package matchSimulator_test

import (
	"math/big"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/backend/frontEndpoints/matchSimulator"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type MatchSimulatorSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
}

func (suite *MatchSimulatorSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_MatchSimulatorSuite(t *testing.T) {
	suite.Run(t, new(MatchSimulatorSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *MatchSimulatorSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	clearDB(suite)
}

func (suite *MatchSimulatorSuite) AfterTest() {
	clearDB(suite)
}

func clearDB(suite *MatchSimulatorSuite) {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
}

func (suite *MatchSimulatorSuite) TestHapyPath() {
	repository := suite.App.Repo

	for i := 0; i < 6; i++ {
		proposal := &models.Proposal{
			ID:             uint(i + 1),
			Amount:         decimal.New(int64(18+i), 18),
			BlockchainID:   uint(i + 1),
			CreatedBy:      "0x777",
			OriginalTokens: decimal.New(int64(18+i), 18),
			Price:          decimal.New(int64(90+i), 16),
			Status:         "active",
		}
		err := repository.Proposal.UpdateProposal(proposal)
		if err != nil {
			log.Error().Err(err)
		}
	}

	repository.OrderTypes.SaveOrderType(&models.OrderType{ID: 1, Name: "test"})

	order := &models.Order{
		BlockchainID:  1,
		TxHash:        "0x0123",
		Metadata:      "test",
		TypeID:        1,
		Price:         decimal.New(1, 18),
		EthAmount:     decimal.New(1, 18),
		TokenAmount:   decimal.New(120, 18),
		CreatedBy:     "0x111",
		EthAmountLeft: decimal.New(1, 18),
	}

	orderPrice, _ := new(big.Int).SetString(order.Price.String(), 10)
	orderData := &matchSimulator.OrderData{Rest: orderPrice, Order: *order}

	matchData, err := matchSimulator.MatchSimulationService(repository, *orderData)

	if err != nil {
		log.Print(err)
	} else {
		log.Print(matchData)
	}
}

func (suite *MatchSimulatorSuite) TestWithBan() {
	repository := suite.App.Repo

	for i := 0; i < 6; i++ {
		proposal := &models.Proposal{
			ID:             uint(i + 1),
			Amount:         decimal.New(int64(18+i), 18),
			BlockchainID:   uint(i + 1),
			CreatedBy:      "0x777" + strconv.Itoa(i),
			OriginalTokens: decimal.New(int64(18+i), 18),
			Price:          decimal.New(int64(90+i), 16),
			Status:         "active",
		}
		err := repository.Proposal.UpdateProposal(proposal)
		if err != nil {
			log.Error().Err(err)
		}
	}

	repository.OrderTypes.SaveOrderType(&models.OrderType{ID: 1, Name: "test"})

	order := &models.Order{
		BlockchainID:  1,
		TxHash:        "0x0123",
		Metadata:      "test",
		TypeID:        1,
		Price:         decimal.New(1, 18),
		EthAmount:     decimal.New(1, 18),
		TokenAmount:   decimal.New(120, 18),
		CreatedBy:     "0x111",
		EthAmountLeft: decimal.New(1, 18),
	}

	orderPrice, _ := new(big.Int).SetString(order.Price.String(), 10)
	orderData := &matchSimulator.OrderData{Rest: orderPrice, Order: *order}

	matchData, _ := matchSimulator.MatchSimulationService(repository, *orderData)

	matchData.Investors[0].Banned = true
	matchData.Investors[4].Banned = true

	matchData, _ = matchSimulator.MatchSimulationService(repository, matchData)

	suite.Equal("357750000000000001", matchData.Rest.String())
}
