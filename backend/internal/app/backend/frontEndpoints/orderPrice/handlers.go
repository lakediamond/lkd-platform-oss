package orderPrice

import (
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//EtherPriceRateResponse is a response for get ether price rate endpoint
type EtherPriceRateResponse struct {
	EtherPriceRate decimal.Decimal `json:"ether_price_rate"`
}

//GetOrderEthExchangeRateHandler handling get order eth exchange rate request
var GetOrderEthExchangeRateHandler = handlers.ApplicationHandler(getOrderEthExchangeRate)

func getOrderEthExchangeRate(w http.ResponseWriter, r *http.Request) {

	tokens := r.URL.Query()["tokens"]
	tokenPrice := r.URL.Query()["tokenPrice"]
	orderEthPrice := r.URL.Query()["orderEthPrice"]

	if len(tokens) != 1 || len(tokenPrice) != 1 || len(orderEthPrice) != 1 {
		err := errors.New("invalid input data")
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, err)
		return
	}

	result, err := getOrderEthExchangeRateService(tokens[0], tokenPrice[0], orderEthPrice[0])
	if err != nil {
		log.Debug().Err(err).Msg("getOrderEthExchangeRateService error")
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, err)
		return
	}

	respData, _ := json.Marshal(result)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}
