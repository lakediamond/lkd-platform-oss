package orderPrice

import (
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
)

func getOrderEthExchangeRateService(tokensStr, tokenPriceStr, orderEthAmountStr string) (EtherPriceRateResponse, error) {

	var ethExchangeRateResponse EtherPriceRateResponse

	tokens, err := decimal.NewFromString(tokensStr)
	if err != nil {
		return ethExchangeRateResponse, err
	}

	tokenPrice, err := decimal.NewFromString(tokenPriceStr)
	if err != nil {
		return ethExchangeRateResponse, err
	}

	orderEthAmount, err := decimal.NewFromString(orderEthAmountStr)
	if err != nil {
		return ethExchangeRateResponse, err
	}

	if orderEthAmount.Equal(decimal.New(0, 0)) {
		return ethExchangeRateResponse, errors.New("dividing by zero")
	}

	if tokens.IsNegative() || tokenPrice.IsNegative() || orderEthAmount.IsNegative() {
		return ethExchangeRateResponse, errors.New("invalid data")
	}

	ethExchangeRateResponse.EtherPriceRate = tokenPrice.Mul(tokens).Div(orderEthAmount)

	return ethExchangeRateResponse, nil
}
