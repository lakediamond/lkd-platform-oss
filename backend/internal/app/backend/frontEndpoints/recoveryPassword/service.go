package recoveryPassword

import (
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/app/backend/users"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/crypto"
	mailjetAdapter "lkd-platform-backend/internal/pkg/mailjet"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func recoveryPasswordService(app *application.Application, user *models.User) error {
	//check previous recovery
	_, err := app.Repo.AccessRecoveryRequest.GetRequestByUserID(user.ID)
	if err == nil {
		return errors.New("recovery request already created")
	}

	var accessRecoveryRequest models.UserAccessRecoveryRequest
	accessRecoveryRequest.UserID = user.ID
	accessRecoveryRequest.Status = "PENDING"
	accessRecoveryRequest.BeforeSave()

	token := createRestoreToken(app.Config.JwtSalt, user.Email, accessRecoveryRequest.ID)

	accessRecoveryRequest.Token = token
	err = app.Repo.AccessRecoveryRequest.Save(&accessRecoveryRequest)
	if err != nil {
		return err
	}

	cfgTemplate, err := setupEmailConfig(app, token, user)
	if err != nil {
		return err
	}

	message, err := mailjetAdapter.BuildMessageByConfig(cfgTemplate)
	if err != nil {
		return err
	}

	messagesInfo := []mailjet.InfoMessagesV31{*message}

	users.MailjetSendEmail(app.Mailjet, messagesInfo)

	return nil
}

func setupEmailConfig(app *application.Application, token string, user *models.User) (*mailjetAdapter.EmailConfig, error) {
	cfgTemplate := app.Mailjet.GetConfigTemplate("recovery")

	callbackUrl, err := users.GenerateCallbackUrl(cfgTemplate.Variables["url"].(string),
		map[string]string{"token": token})
	if err != nil {
		return nil, err
	}

	cfgTemplate.Variables["url"] = callbackUrl.String()
	cfgTemplate.Variables["firstname"] = user.FirstName
	cfgTemplate.Variables["lastname"] = user.LastName
	cfgTemplate.ToName = fmt.Sprintf("%s %s", user.FirstName, user.LastName)
	cfgTemplate.ToEmail = user.Email

	return cfgTemplate, nil
}

func recoveryPasswordFromEmailService(repo *repo.Repo, user *models.User, accessRecoveryRequest *models.UserAccessRecoveryRequest) error {
	if accessRecoveryRequest.Status == "COMPLETED" {
		return errors.New("recovery request already completed")
	}

	accessRecoveryRequest.Status = "ACTIVATED"
	err := repo.AccessRecoveryRequest.Save(accessRecoveryRequest)
	if err != nil {
		return err
	}

	user.Locked = true

	err = repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}

func newPasswordService(repo *repo.Repo, user *models.User, accessRecoveryRequest *models.UserAccessRecoveryRequest, newPassword string) error {
	if accessRecoveryRequest.Status == "COMPLETED" {
		return errors.New("recovery already completed")
	}

	user.Password = crypto.EncodePass(newPassword)
	user.Locked = false

	err := repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	accessRecoveryRequest.Status = "COMPLETED"

	err = repo.AccessRecoveryRequest.Save(accessRecoveryRequest)
	if err != nil {
		return err
	}

	return nil
}

func createRestoreToken(jwtSalt, email string, recoveryID *uuid.UUID) string {

	expire := time.Now().UTC().Add(30 * time.Minute).Unix()

	claims := &jwt.MapClaims{
		"exp":         expire,
		"email":       email,
		"recovery_id": recoveryID.String(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	ss, _ := token.SignedString([]byte(jwtSalt))

	log.Debug().Msgf("Created new restore token for: %s, token: %s", email, ss)
	return ss
}

func verifyRestoreToken(app *application.Application, tokenString string) (models.User, models.UserAccessRecoveryRequest, error) {

	var user models.User
	var request models.UserAccessRecoveryRequest

	claims := jwt.MapClaims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(app.Config.JwtSalt), nil
	})
	if err != nil {
		return user, request, err
	}

	if !token.Valid {
		return user, request, errors.New("invalid token")
	}

	if !claims.VerifyExpiresAt(time.Now().UTC().Unix(), true) {
		return user, request, errors.New("expired token")
	}

	email := claims["email"].(string)

	log.Print("email: ", email)

	user, err = app.Repo.User.GetUserByEmail(email)
	if err != nil {
		return user, request, err
	}

	req := claims["recovery_id"].(string)

	log.Print("req ", req)

	reqUuid, err := uuid.FromString(req)
	if err != nil {
		return user, request, err
	}

	request, err = app.Repo.AccessRecoveryRequest.GetRequestByID(&reqUuid)
	if err != nil {
		return user, request, err
	}

	return user, request, nil
}
