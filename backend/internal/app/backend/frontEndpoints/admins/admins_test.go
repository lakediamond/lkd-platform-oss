package admins_test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type FrontEndpointsAdminSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
	User       *models.User
}

func (suite *FrontEndpointsAdminSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_FrontEndpointsAdminSuite(t *testing.T) {
	suite.Run(t, new(FrontEndpointsAdminSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *FrontEndpointsAdminSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	//key, _ := crypto.GenerateKey()
	//app.EthClient = etherPkg.InitializePlatformEthereumMock(key)

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM sub_owners;")

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"

	suite.App.Repo.User.CreateNewUser(&user)

	suite.User = &user
}

func (suite *FrontEndpointsAdminSuite) AfterTest() {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM sub_owners;")
}

func (suite *FrontEndpointsAdminSuite) TestGetEmptyAdmins() {

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"
	suite.App.Repo.User.CreateNewUser(&user)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", suite.MockServer.URL+"/admin/subowner", nil)

	req.Header.Add("user_email", user.Email)
	resp, err := client.Do(req)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	suite.Equal(http.StatusOK, resp.StatusCode)

	data, _ := ioutil.ReadAll(resp.Body)

	var admins []repo.Admin

	json.Unmarshal(data, &admins)

	log.Print(admins)

	suite.Equal(0, len(admins))
}

func (suite *FrontEndpointsAdminSuite) TestGetOneAdmin() {

	subOwner := models.SubOwner{EthAddress: suite.User.EthAddress, IsOwner: true}
	suite.App.Repo.SubOwner.UpdateSubOwner(&subOwner)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", suite.MockServer.URL+"/admin/subowner", nil)

	req.Header.Add("user_email", suite.User.Email)
	resp, err := client.Do(req)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	suite.Equal(http.StatusOK, resp.StatusCode)

	data, _ := ioutil.ReadAll(resp.Body)

	var admins []repo.Admin

	json.Unmarshal(data, &admins)

	log.Print(admins)

	suite.Equal(1, len(admins))
}

func (suite *FrontEndpointsAdminSuite) TestGetTwoAdmins() {
	subOwner := models.SubOwner{EthAddress: suite.User.EthAddress, IsOwner: true}
	suite.App.Repo.SubOwner.UpdateSubOwner(&subOwner)

	subOwner2 := models.SubOwner{EthAddress: "0x040334a07f601350cAFAc8AbE5d9E21968d31522", IsOwner: true}
	suite.App.Repo.SubOwner.UpdateSubOwner(&subOwner2)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", suite.MockServer.URL+"/admin/subowner", nil)

	req.Header.Add("user_email", suite.User.Email)
	resp, err := client.Do(req)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	suite.Equal(http.StatusOK, resp.StatusCode)

	data, _ := ioutil.ReadAll(resp.Body)

	var admins []repo.Admin

	json.Unmarshal(data, &admins)

	log.Print(admins)

	suite.Equal(2, len(admins))
}

func (suite *FrontEndpointsAdminSuite) TestCheckIsOwnerFalse() {
	subOwner := models.SubOwner{EthAddress: suite.User.EthAddress, IsOwner: true}
	suite.App.Repo.SubOwner.UpdateSubOwner(&subOwner)

	subOwner2 := models.SubOwner{EthAddress: "0x040334a07f601350cAFAc8AbE5d9E21968d31522", IsOwner: false}
	suite.App.Repo.SubOwner.UpdateSubOwner(&subOwner2)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", suite.MockServer.URL+"/admin/subowner", nil)

	req.Header.Add("user_email", suite.User.Email)
	resp, err := client.Do(req)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	suite.Equal(http.StatusOK, resp.StatusCode)

	data, _ := ioutil.ReadAll(resp.Body)

	var admins []repo.Admin

	json.Unmarshal(data, &admins)

	log.Print(admins)

	suite.Equal(1, len(admins))

}
