package orders_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/backend/frontEndpoints/orders"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
	"lkd-platform-backend/pkg/httputil"
)

type FrontEndpointsOrdersSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
	User       *models.User
}

func (suite *FrontEndpointsOrdersSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_FrontEndpointsOrdersSuite(t *testing.T) {
	suite.Run(t, new(FrontEndpointsOrdersSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *FrontEndpointsOrdersSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM orders;")
	suite.App.Repo.DB.Exec("DELETE FROM order_types;")

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"

	suite.App.Repo.User.CreateNewUser(&user)

	suite.User = &user
}

func (suite *FrontEndpointsOrdersSuite) AfterTest() {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM orders;")
	suite.App.Repo.DB.Exec("DELETE FROM order_types;")
}

func (suite *FrontEndpointsOrdersSuite) TestZeroOrdersData() {
	body := sendGetRequestAndGetResponseBody(suite, "/admin/order")

	var ords []orders.OrderResponse
	json.Unmarshal(body, &ords)

	suite.Equal(0, len(ords))
}

func (suite *FrontEndpointsOrdersSuite) TestOneOrderData() {
	addNewOrderType(suite, 1, "test")

	createNewOrder(suite, decimal.New(100, 0), decimal.New(200, 0), 1, 1)

	body := sendGetRequestAndGetResponseBody(suite, "/admin/order")

	var ords []orders.OrderResponse
	json.Unmarshal(body, &ords)

	suite.Equal(1, len(ords))
}

func (suite *FrontEndpointsOrdersSuite) TestTwoOrderData() {
	addNewOrderType(suite, 1, "test")

	createNewOrder(suite, decimal.New(100, 0), decimal.New(200, 0), 1, 1)
	createNewOrder(suite, decimal.New(100, 0), decimal.New(200, 0), 1, 2)

	body := sendGetRequestAndGetResponseBody(suite, "/admin/order")

	var ords []orders.OrderResponse

	json.Unmarshal(body, &ords)

	suite.Equal(2, len(ords))
}

func sendGetRequestAndGetResponseBody(suite *FrontEndpointsOrdersSuite, route string) []byte {
	resp, _ := sendGetRequest(suite, route)

	suite.Equal(http.StatusOK, resp.StatusCode)

	defer resp.Body.Close()
	return httputil.ReadBody(resp.Body)
}

func sendGetRequest(suite *FrontEndpointsOrdersSuite, route string) (*http.Response, error) {
	url, _ := url.Parse(suite.MockServer.URL + route)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	return client.Do(req)
}

func addNewOrderType(suite *FrontEndpointsOrdersSuite, id uint, name string) {
	var orderType models.OrderType
	orderType.ID = id
	orderType.Name = name

	suite.App.Repo.OrderTypes.SaveOrderType(&orderType)
}

func createNewOrder(suite *FrontEndpointsOrdersSuite, price decimal.Decimal, ethAmount decimal.Decimal, typeId uint, blockchainID uint) {
	var order models.Order
	order.Price = price
	order.EthAmount = ethAmount
	order.TypeID = typeId
	order.BlockchainID = blockchainID
	suite.App.Repo.Order.UpdateOrder(&order)
}

func createNewProposal(suite *FrontEndpointsOrdersSuite, price decimal.Decimal, amount decimal.Decimal, blockchainID uint) {
	var proposal models.Proposal
	proposal.Price = price
	proposal.Amount = amount
	proposal.BlockchainID = blockchainID
	suite.App.Repo.Proposal.UpdateProposal(&proposal)
}

func createNewMatch(suite *FrontEndpointsOrdersSuite, price decimal.Decimal, amount decimal.Decimal, blockchainID uint) {
	var proposal models.Proposal
	proposal.Price = price
	proposal.Amount = amount
	proposal.BlockchainID = blockchainID
	suite.App.Repo.Proposal.UpdateProposal(&proposal)
}
