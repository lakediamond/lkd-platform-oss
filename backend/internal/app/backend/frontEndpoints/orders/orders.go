package orders

import (
	"time"

	"github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/repo"
)

func getOrders(repo *repo.Repo) ([]OrderResponse, error) {

	var orderResponses []OrderResponse

	orders, err := repo.Order.GetOrdersDetails()
	if err != nil {
		return orderResponses, err
	}

	orderResponses = combineOrderResponse(orders)

	return orderResponses, nil
}

func getOrdersCumulativeData(repo *repo.Repo, daysToLoad int) ([]repo.OrdersCumulativeDataDB, error) {
	return repo.Order.GetOrdersCumulativeData(daysToLoad)
}

func combineOrderResponse(orderResponsesDB []repo.OrderResponseDB) []OrderResponse {

	var orderResponses []OrderResponse

	for _, orderResponseDB := range orderResponsesDB {

		var orderResponse OrderResponse
		var existsID = -1

		for i, orderResponseIter := range orderResponses {
			if orderResponseIter.ID == orderResponseDB.ID {
				orderResponse = orderResponseIter
				existsID = i
				break
			}
		}

		//if not set yet
		if orderResponse.Metadata == "" {
			orderResponse.ID = orderResponseDB.ID
			orderResponse.CreatedOn = orderResponseDB.CreatedOn
			orderResponse.CreatedBy = orderResponseDB.CreatedBy
			orderResponse.Type = orderResponseDB.Type
			orderResponse.Metadata = orderResponseDB.Metadata
			orderResponse.TokenPrice = orderResponseDB.TokenPrice
			orderResponse.TokensAmount = orderResponseDB.TokenAmount
			orderResponse.EthAmount = orderResponseDB.EthAmount
		}

		var investment Investment

		if orderResponseDB.TxHash != "" {
			investment.ID = orderResponseDB.InvestmentID
			investment.TxHash = orderResponseDB.TxHash
			investment.TokensInvested = orderResponseDB.TokensInvested
			investment.EthReceived = orderResponseDB.EthReceived
			if orderResponseDB.EthAccount != "" {
				investment.FirstName = orderResponseDB.FirstName
				investment.LastName = orderResponseDB.LastName
				investment.Email = orderResponseDB.Email
				investment.EthAccount = orderResponseDB.EthAccount
			}
		}

		if orderResponseDB.TxHash != "" {
			orderResponse.Investments = append(orderResponse.Investments, investment)
		}

		if existsID == -1 {
			orderResponses = append(orderResponses, orderResponse)
		} else {
			orderResponses[existsID] = orderResponse
		}

	}

	return orderResponses
}

//OrderResponse contains response to getOrders request
type OrderResponse struct {
	ID           uint            `json:"id"`
	CreatedBy    string          `json:"created_by"`
	CreatedOn    time.Time       `json:"created_on"`
	Type         string          `json:"type"`
	Metadata     string          `json:"metadata"`
	TokenPrice   decimal.Decimal `json:"token_price"`
	TokensAmount decimal.Decimal `json:"tokens_amount"`
	EthAmount    decimal.Decimal `json:"eth_amount"`
	Investments  []Investment    `json:"investments"`
}

//Investment contains information about investor
type Investment struct {
	ID             *uuid.UUID      `json:"id"`
	TxHash         string          `json:"tx_hash"`
	TokensInvested decimal.Decimal `json:"tokens_invested"`
	EthReceived    decimal.Decimal `json:"eth_received"`
	FirstName      string          `json:"first_name"`
	LastName       string          `json:"last_name"`
	Email          string          `json:"email"`
	EthAccount     string          `json:"eth_account"`
}
