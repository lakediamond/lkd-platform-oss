package orders

import (
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/gorilla/websocket"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//GetAllOrdersHandler handling get all orders request
var GetAllOrdersHandler = handlers.ApplicationHandler(getAllOrders)

func getAllOrders(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	conn, err := app.Config.WSUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error().Err(err).Msg("webSocket upgrade error")
		return
	}

	orders, err := getOrders(app.Repo)
	if err != nil {
		log.Debug().Err(err).Msg("Get all orders error")
		return
	}

	conn.WriteJSON(orders)

	var lastData []OrderResponse

	for {

		i, _, err := conn.ReadMessage()
		if i == 1000 || err != nil {
			conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(1000, "closing WS connection"))
			return
		}

		orders, err := getOrders(app.Repo)
		if err != nil {
			log.Debug().Err(err).Msg("Get all orders error")
			return
		}

		if reflect.DeepEqual(lastData, orders) {
			time.Sleep(30 * time.Second)
			continue
		}

		lastData = orders

		if err = conn.WriteJSON(orders); err != nil {
			return
		}

		time.Sleep(30 * time.Second)
	}
}

//GetOrdersCumulativeDataHandler handling get all cumulative data request
var GetOrdersCumulativeDataHandler = handlers.ApplicationHandler(getOrdersCumulativeDataByDays)

func getOrdersCumulativeDataByDays(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	conn, err := app.Config.WSUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error().Err(err).Msg("webSocket upgrade error")
		return
	}

	var lastData []repo.OrdersCumulativeDataDB

	for {

		i, _, err := conn.ReadMessage()
		if i == 1000 || err != nil {
			conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(1000, "closing WS connection"))
			return
		}

		const daysToLoadValueKey = "daysToLoad"
		daysToLoadValue, ok := r.URL.Query()["daysToLoad"]

		if !ok || len(daysToLoadValue[0]) < 1 {
			errorMessage := fmt.Errorf("url Param %s is missing", daysToLoadValueKey)
			log.Error().Msg(errorMessage.Error())
			httputil.WriteErrorMsg(w, errorMessage)
			return
		}

		daysToLoad, err := strconv.Atoi(daysToLoadValue[0])
		if err != nil {
			errorMessage := fmt.Errorf("url Param %s must be a number", daysToLoadValueKey)
			log.Error().Err(err).Msg(errorMessage.Error())
			httputil.WriteErrorMsg(w, errorMessage)
			return
		}

		ordersCumulativeData, err := getOrdersCumulativeData(app.Repo, daysToLoad)
		if err != nil {
			log.Error().Err(err).Msg("Get orders cumulative data error")
			return
		}

		if reflect.DeepEqual(lastData, ordersCumulativeData) {
			time.Sleep(30 * time.Second)
			continue
		}

		lastData = ordersCumulativeData

		if err = conn.WriteJSON(ordersCumulativeData); err != nil {
			return
		}

		time.Sleep(30 * time.Second)
	}
}
