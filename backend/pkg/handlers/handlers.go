package handlers

import (
	"bufio"
	"net"
	"net/http"
	"runtime/debug"
	"time"

	"github.com/gorilla/handlers"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type recoveryLogger struct {
	zerolog.Logger
}

func (log recoveryLogger) Println(v ...interface{}) {
	log.Error().Str("error.stack", string(debug.Stack())).Str("error.message", v[0].(string)).Msg("")
}

var RecoveryHandler = handlers.RecoveryHandler(
	handlers.RecoveryLogger(recoveryLogger{log.Logger}),
	handlers.PrintRecoveryStack(false))

type httpLoggingHandler struct {
	handler http.Handler
}

type loggingResponseWriter struct {
	w          http.ResponseWriter
	statusCode int
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.w.WriteHeader(code)
}

func (lrw *loggingResponseWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	hj := lrw.w.(http.Hijacker)
	return hj.Hijack()
}

func (h httpLoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	lrw := NewLoggingResponseWriter(w)
	h.handler.ServeHTTP(lrw.w, r)
	elapsed := time.Since(start)
	log.Info().Int("http.status_code", lrw.statusCode).Str("http.url_details.path", r.URL.Path).Str("http.method", r.Method).Str("duration", elapsed.String()).Msg("")
}

func LoggingHandler(h http.Handler) http.Handler {
	return httpLoggingHandler{h}
}

type applicationHandler struct {
	handler http.HandlerFunc
}

func JSONContentTypeHandler(h http.Handler) http.Handler {
	return handlers.ContentTypeHandler(h, "application/json")
}

func (c applicationHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.handler(w, r)
}

func ApplicationHandler(h http.HandlerFunc) http.Handler {
	return applicationHandler{h}
}
