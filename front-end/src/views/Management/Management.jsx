import React from "react";
import { connect } from "react-redux";
import { Row, Col, Alert, message, Skeleton, Card } from "antd";
import {
  SubOwnersTable,
  OrdersTypeTable,
  OrderTypeDataWidget,
  SubownerDataWidget,
  MetamaskNotExistCard
} from "components";
import { Request } from "services";
import { GET_ORDER_TYPES } from "redux/constants";
import {
  isMetamaskInstalled,
  checkContractOwner,
  get
} from "../../redux/actions";

class Management extends React.Component {
  state = {};

  getOrdersTypeData = async () => {
    const orderTypes = await Request(GET_ORDER_TYPES);

    if (!orderTypes) {
      message.error("Looks like it's a problem with network");
      return false;
    }

    if (orderTypes.status !== 403) {
      orderTypes.json().then(data => {
        this.setState({ ordersTypeData: data });
        return true;
      });
    }
    // TODO What to do with error fetch? refresh a page?
    // console.log(result);
  };

  addSubOwnersToContract = addresses => {
    const {
      metaMask: { ioContractInstance }
    } = this.props;

    ioContractInstance.addSubOwners(addresses, (err, res) => {
      if (!err) {
        message.success(
          "You successfully added new sub owners to the contract"
        );
        return true;
      }
      message.error("Ooops, something went wrong");
      return false;
    });
  };

  removeSubOwnersFromContract = addresses => {
    const {
      metaMask: { ioContractInstance }
    } = this.props;

    ioContractInstance.removeSubOwners(addresses, (err, res) => {
      if (!err) {
        message.success("You successfully removed sub owners to the contract");
        return true;
      }
      message.error("Ooops, something went wrong");
      return false;
    });
  };

  addOrderTypesToContract = (id, orderType) => {
    const {
      metaMask: { ioContractInstance }
    } = this.props;

    ioContractInstance.setOrderType(Number(id), orderType, (err, res) => {
      if (!err) {
        message.success(
          "You successfully added new order type to the contract"
        );
        return true;
      }
      message.error("Ooops, something went wrong");
      return false;
    });
  };

  componentDidMount() {
    const {
      user: { role },
      history,
      metaMask: {
        metaMaskAccount,
        isInstalled,
        ioContractInstance,
        isContractOwner
      },
      checkContractOwner,
      isMetamaskInstalled,
      get
    } = this.props;

    if (!!role && role !== "admin") {
      history.push("/profile/overview");
    }

    if (isInstalled === null) {
      isMetamaskInstalled();
    }

    if (!!metaMaskAccount) {
      // this.getSubownersData();
      get("GET_SUB_OWNERS");
      this.getOrdersTypeData();
    }

    if (!!ioContractInstance && isContractOwner === null) {
      checkContractOwner(ioContractInstance, metaMaskAccount);
    }
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const {
      metaMask: { metaMaskAccount, ioContractInstance, isContractOwner },
      checkContractOwner,
      get
    } = this.props;

    if (
      prevProps.metaMask.metaMaskAccount !== metaMaskAccount &&
      !!metaMaskAccount
    ) {
      get("GET_SUB_OWNERS");
      this.getOrdersTypeData();
    }

    if (
      prevProps.metaMask.ioContractInstance !== ioContractInstance &&
      !!ioContractInstance &&
      isContractOwner === null
    ) {
      checkContractOwner(ioContractInstance, metaMaskAccount);
    }
  }

  render() {
    const {
      metaMask: { isInstalled }
    } = this.props;

    switch (isInstalled) {
      case null:
        return <Skeleton active />;
      case true:
        return this.renderContent();
      case false:
        return <MetamaskNotExistCard />;
      default:
        return null;
    }
  }

  renderAlert = () => {
    return this.state.open_alert ? (
      <Row gutter={16} className="container_row">
        <Col span={12}>
          <Alert
            message={this.state.alert_text}
            type={this.state.alert_type}
            showIcon
            closeText="X"
          />
        </Col>
      </Row>
    ) : null;
  };

  renderContent = () => {
    const {
      metaMask: { storageContractInstance, isLoggedIn, isContractOwner },
      subOwnersList,
      proposalsList
    } = this.props;

    if (!!!isLoggedIn) {
      return (
        <React.Fragment>
          <Row gutter={16} className="container_row">
            <Col span={16}>
              <Card>
                <Skeleton active />
              </Card>
            </Col>
            <Col span={8}>
              <Card>
                <Skeleton active />
              </Card>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Card>
                <Skeleton active />
              </Card>
            </Col>
            <Col span={12}>
              <Card>
                <Skeleton active />
              </Card>
            </Col>
          </Row>
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        {this.renderAlert()}
        <Row
          gutter={16}
          type="flex"
          justify="space-between"
          className="container_row"
        >
          <Col lg={14} xl={12}>
            <SubownerDataWidget action={this.addSubOwnersToContract} />
          </Col>
          <Col lg={10} xl={12}>
            <OrderTypeDataWidget action={this.addOrderTypesToContract} />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col lg={24} xl={12} className="container_row">
            <SubOwnersTable
              collection={subOwnersList}
              action={this.removeSubOwnersFromContract}
              isContractOwner={isContractOwner}
            />
          </Col>
          <Col lg={24} xl={12}>
            <OrdersTypeTable
              collection={this.state.ordersTypeData}
              action={this.addOrderTypesToContract}
              isContractOwner={isContractOwner}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    metaMask: state.metaMask,
    subOwnersList: state.subOwnersList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isMetamaskInstalled: () => {
      dispatch(isMetamaskInstalled());
    },
    checkContractOwner: (contractInstance, account) => {
      dispatch(checkContractOwner(contractInstance, account));
    },
    get: type => {
      dispatch(get(type));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Management);
