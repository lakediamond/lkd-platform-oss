import React from "react";
import { connect } from "react-redux";
import { Row, Col, Skeleton, Card } from "antd";
import {
  OrdersTable,
  CreateOrderDataWidget,
  MetamaskNotExistCard,
  OrderMatchingSimulation,
  Spinner,
  FinishMessage
} from "components";
import { get, isMetamaskInstalled, webSocketData } from "redux/actions";
import { startAuth } from "services";
import { ORDERS_SOCKET } from "redux/constants";

class Orders extends React.Component {
  componentWillMount() {
    this.timeouts = [];
  }

  componentDidMount() {
    this.fetchOrders();

    const {
      user: { role },
      history,
      metaMask: {
        metaMaskAccount,
        isInstalled,
        ioContractInstance,
        isContractOwner,
        isLoggedIn
      },
      isMetamaskInstalled
    } = this.props;

    //TODO For production or demo

    if (!!role && role !== "admin") {
      history.push("/profile/overview");
    }

    if (isInstalled === null) {
      isMetamaskInstalled();
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      ordersList: { error }
    } = this.props;

    if (prevProps.ordersList.error == null && !!error) {
      if (error.code === 401) {
      }
    }

    // ordersList:
    //   error:
    //     code: 401
    // message: "[recovery] Failed to get token from session cookie"
  }

  componentWillUnmount() {
    // this.props.webSocketData(ORDERS_SOCKET, "finish");
  }

  fetchOrders = () => {
    // this.props.fetchOrdersList();
    this.props.get("GET_ORDERS");
    // this.props.webSocketData(ORDERS_SOCKET, "start");
    // this.timeouts.push(setTimeout(this.fetchOrders, 1000));
  };

  renderContent = () => {
    const {
      metaMask: { isLoggedIn, metaMaskAccount },
      simulatedData: { showSimulate },
      ordersList: { orders }
    } = this.props;

    if (!!!isLoggedIn) {
      return (
        <React.Fragment>
          <Row className="container_row">
            <Col span={12}>
              <Card style={{ height: "512px" }}>
                <Skeleton active />
                <Skeleton active />
                <Skeleton active />
              </Card>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Card>
                <Skeleton active />
              </Card>
            </Col>
          </Row>
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        <Row className="container_row" gutter={48}>
          <Col span={12}>
            <CreateOrderDataWidget />
          </Col>
          {showSimulate ? (
            <Col span={12}>
              <OrderMatchingSimulation />
            </Col>
          ) : null}
        </Row>
        <Row>
          <Col span={24}>
            <OrdersTable
              collection={orders}
              account={metaMaskAccount}
              title={"Orders"}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  };

  render() {
    const {
      metaMask: { isInstalled },
      ordersList: { error }
    } = this.props;

    if (isInstalled === false) {
      return <MetamaskNotExistCard />;
    }

    if (!!error) {
      if (error.code === 401) {
        return (
          <React.Fragment>
            <FinishMessage
              success={false}
              message={"logged in"}
              method={startAuth}
            />
          </React.Fragment>
        );
      }
    }

    return <React.Fragment>{this.renderContent()}</React.Fragment>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ordersList: state.ordersList,
    user: state.user,
    metaMask: state.metaMask,
    simulatedData: state.simulatedData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isMetamaskInstalled: () => {
      dispatch(isMetamaskInstalled());
    },
    get: type => {
      dispatch(get(type));
    },
    // webSocketData: (type, action) => {
    //   dispatch(webSocketData(type, action));
    // }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orders);
