import React from "react";
import { connect } from "react-redux";
import { Card, Button, Row, Col, message, Spin, Icon } from "antd";
import { Request, startAuth } from "services";
import { GET_OAUTH_CONSENT_REQUEST } from "redux/constants";
import { disclaimer } from "lib/disclaimer";
import { confirmConsent } from "redux/actions";
import { Spinner } from "components";

class Consent extends React.Component {
  state = {
    showDisclaimer: null
  };

  handleSubmit = e => {
    e.preventDefault();
    this.postConsent();
  };

  CheckOauthConsentRequest = async param => {
    const data = await Request(GET_OAUTH_CONSENT_REQUEST, null, param);
    if (!data) {
      message.error("Looks like it's a network error");
      return false;
    }

    if (data.status === 200) {
      const headers = data.headers;
      const location = headers.get("Location");
      window.location.href = location;
      return true;
    }
    
    if(data.status === 204){
      this.setState({showDisclaimer: true})
    }
  };

  postConsent = async () => {
    const data = {
      decision: "approve",
      consent_challenge: this.state.challenge
    };
    this.props.confirmConsent(data);
  };

  componentDidMount() {
    const {
      history: {
        location: { search }
      }
    } = this.props;

    if (!!!search) {
      startAuth();
    }

    if (search.includes("consent_challenge")) {
      this.setState({ challenge: search.slice(19) });
      const param = `${this.props.history.location.pathname}${
        this.props.history.location.search
      }`;
      this.CheckOauthConsentRequest(param);
    }
  }

  render() {
    const {
      history: {
        location: { search }
      },
      user: { request }
    } = this.props;

    const { showDisclaimer } = this.state;

    if (!!!search || showDisclaimer === null) {
      return <Spinner />;
    }

    if (!!showDisclaimer) {
      return (
        <Row className="consent">
          <Col span={12} offset={6}>
            <Card title={"Consent"}>
              <Row>
                <Col className="container_row">
                  <div className="disclaimer">{disclaimer}</div>
                </Col>
                <Col span={12} offset={6}>
                  <Button
                    type="primary"
                    name="submit"
                    block
                    onClick={e => this.handleSubmit(e)}
                  >
                    {request ? (
                      <Icon type="sync" spin style={{ color: "fff" }} />
                    ) : (
                      "Approve"
                    )}
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    confirmConsent: data => {
      dispatch(confirmConsent(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Consent);