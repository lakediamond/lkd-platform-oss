import React from "react";
import { connect } from "react-redux";
import { Row, Col, message } from "antd";
import ChartistGraph from "react-chartist";
import { dailySalesChart } from "variables/charts";
import { StatsCard, ChartCard, OrdersTable } from "components";

import {
  get,
  showAlert,
  webSocketData
} from "redux/actions";
import { ORDERS_SOCKET } from "redux/constants";

class Dashboard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  fetchTables = () => {
    this.props.get("GET_ORDERS");
    this.props.get("GET_PROPOSALS_USER");
    // this.props.webSocketData(ORDERS_SOCKET, "start");

    // this.timeouts.push(setTimeout(this.fetchTables, 3000));
  };

  componentWillMount() {
    this.timeouts = [];
  }

  componentWillUnmount() {
    // this.props.webSocketData(ORDERS_SOCKET, "finish");
  }

  componentDidMount() {
    this.fetchTables();
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      proposalsList: { error }
    } = this.props;

    if (error) {
      message.error(error);
    }
  }

  getWeeklyOrderChartData = orders => {
    const currentDate = new Date();

    if (!("dayLabels" in this.state)) {
      const days = ["M", "T", "W", "T", "F", "S", "S"];
      const currentDay = currentDate.getDay();
      let labels = [];
      for (let i = currentDay; i < currentDay + 7; i++) {
        labels.push(days[i % 7]);
      }
      this.setState({ dayLabels: labels });
    }

    let weeklyOrders = [0, 0, 0, 0, 0, 0, 0];
    const day = 24 * 60 * 60 * 1000;
    const today = currentDate.setHours(0, 0, 0, 0);
    let orderDate;
    for (let i = 0; i < orders.length; i++) {
      orderDate = new Date(orders[i].date).setHours(0, 0, 0, 0);
      for (let j = 0; j < 7; j++) {
        if (orderDate === today - j * day) {
          weeklyOrders[weeklyOrders.length - 1 - j] += 1;
          break;
        }
      }
    }

    let options = dailySalesChart.options;
    const minPrice = Math.min.apply(Math, weeklyOrders);
    const maxPrice = Math.max.apply(Math, weeklyOrders);
    options.low = minPrice - 0.1 * minPrice;
    options.high = maxPrice + 0.1 * maxPrice;

    return {
      data: {
        labels: this.state.dayLabels,
        series: [weeklyOrders]
      },
      options: options
    };
  };

  render() {
    const { ordersList: {orders}, proposalsList, user } = this.props;

    const achieved = user.kyc_summary
      ? user.kyc_summary["achieved"]
      : null;

    const showOrdersChart = ordersList => {
      return ordersList ? (
        <Col span={8}>
          <a href="/orders">
            <ChartCard
              chart={
                <ChartistGraph
                  className="ct-chart"
                  data={this.getWeeklyOrderChartData(ordersList)["data"]}
                  type="Line"
                  options={this.getWeeklyOrderChartData(ordersList)["options"]}
                  listener={dailySalesChart.animation}
                />
              }
              chartColor="blue"
              title="Weekly Orders"
              text=""
              statText=""
            />
          </a>
        </Col>
      ) : null;
    };

    const renderUserProposals = () => {
      return !!achieved ? (
        <Col span={8}>
          <a href="/bids">
            <StatsCard
              iconColor="green"
              title="My open proposals"
              description={proposalsList["user"] ? proposalsList["user"].length : 0}
              statText="Bids"
            />
          </a>
        </Col>
      ) : null;
    };
    
    return (
      <React.Fragment>
        <Row className="container_row" gutter={48}>
          {showOrdersChart(orders)}
          {renderUserProposals()}
        </Row>
        <Row>
          <Col span={24}>
            <OrdersTable
              title="Latest Orders"
              collection={orders}
              limitRows={5}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ordersList: state.ordersList,
    proposalsList: state.proposalsList,
    metaMask: state.metaMask,
    alertMessage: state.alertMessage,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: (type) => {
      dispatch(get(type));
    },
    // webSocketData: (type, action) => {
    //   dispatch(webSocketData(type, action));
    // },
    showAlert: message => {
      dispatch(showAlert(message));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
