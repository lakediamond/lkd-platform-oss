import React from "react";
import { connect } from "react-redux";
import { SHA3 } from 'sha3';
import {Card, Button, Input, Row, Col, message, Form, Skeleton, Popover, Icon} from "antd";
import { changePassword } from "redux/actions/index";
import { Title, Spinner, PasswordHint } from "components";
import { checkPasswordOWASP } from "services";
const FormItem = Form.Item;

class Password extends React.Component {
  
  handleSubmit = e => {
    e.preventDefault();
    const {
      changePassword,
      user: { google_2fa_enabled },
      form
    } = this.props;
    
    form.validateFields(async (err, values) => {
      if (!err) {
  
        const hashOldBrowser = new SHA3(256);
        const hashNewBrowser = new SHA3(256);
        
        const value = {
          old_password: hashOldBrowser.update(values["old_password"]).digest('hex'),
          new_password: hashNewBrowser.update(values["new_password"]).digest('hex')
        };
        
        if (google_2fa_enabled) {
          value["google_2_fa_pass"] = values["google_2_fa_pass"]
        }
        
        changePassword(value);
        form.resetFields();
        return true;
      }
    });
  };
  
  
  validateNewPassword = (rule, value, callback) => {
    if (value) {
      if(checkPasswordOWASP(value) !== 'valid'){
        callback(checkPasswordOWASP(value));
      }
    }
    callback();
  };
  
  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("new_password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };
  
  renderForm = () => {
    const {
      form: { getFieldDecorator },
      fetch: { state },
      user: { google_2fa_enabled }
    } = this.props;
    
    return (
      <Form onSubmit={e => this.handleSubmit(e)}>
        <FormItem label={"Old password"}>
          {getFieldDecorator("old_password", {
            validateTrigger: "onBlur",
            initialValue: "",
            rules: [
              {
                required: true,
                message: "Please input your password!"
              },
            ]
          })(
            <Input type="password" />
          )}
        </FormItem>
  
        <FormItem label={"New password"}>
          {getFieldDecorator("new_password", {
            validateTrigger: "onBlur",
            initialValue: "",
            rules: [
              {
                required: true,
                message: "Please confirm new password!"
              },
              {
                validator: this.validateNewPassword
              }
            ]
          })(<Input type="password" />)}
        </FormItem>
  
        <FormItem label={"Confirm password"} className={!google_2fa_enabled ? "card_row" : null}>
          {getFieldDecorator("new_password_confirm", {
            validateTrigger: "onBlur",
            initialValue: "",
            rules: [
              {
                required: true,
                message: "Please confirm your password!"
              },
              {
                validator: this.compareToFirstPassword
              }
            ]
          })(<Input type="password" />)}
        </FormItem>
  
        { google_2fa_enabled ? <FormItem label={"2FA code"} className={"card_row"}>
          {getFieldDecorator("google_2_fa_pass", {
            validateTrigger: "onBlur",
            rules: [
              {
                required: true,
                message: "Please input code from Google 2FA"
              }
            ]
          })(<Input />)}
        </FormItem> : null }
        
        
        <Button type="primary" htmlType="submit" block>
          {!state ? (
            "Change password"
          ) : (
            <Icon type="sync" spin style={{ color: "fff" }} />
          )}
        </Button>
      </Form>
    );
  };
  
  render() {
    return (
      <Row className="container_row">
        <Col span={12}>
          <Card>
            {this.renderForm()}
          </Card>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    fetch: state.fetch,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changePassword: (value) => {
      dispatch(changePassword(value));
    }
  };
};

const PasswordComponent = Form.create()(Password);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PasswordComponent);
