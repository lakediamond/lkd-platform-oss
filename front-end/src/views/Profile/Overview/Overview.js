import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { Card, Col, Input, Row, Button } from "antd";
import { setInvestmentPlan, get } from "redux/actions";
import introJs from "intro.js";
import "intro.js/introjs.css";

class Overview extends React.Component {
  componentDidMount() {
  
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      user: { target_tier },
      history
    } = this.props;
    
    if(prevProps.user.target_tier === null && target_tier === null){
      introJs().start();
    }
  
    if(prevProps.user.target_tier === null && prevProps.user.target_tier !== target_tier){
      this.props.get("GET_TIERS_INFO");
      history.push("/profile/kyc");
    }
    
  }
  
  componentWillUnmount() {
    introJs().exit();
  }
  
  showKYCSummary = () => {
    const { kyc_summary: kyc, target_tier } = this.props.user;

    return target_tier && kyc ? (
      <Col span={12}>
        <Card title={"KYC progress"}>
          <Row gutter={8}>
            <Col span={10}>Tier achieved</Col>
            <Col span={10}>
              {kyc["achieved"] ? kyc["achieved"]["name"] : "None"}
            </Col>
          </Row>
          <Row gutter={8}>
            <Col span={10}>Tier which requires input</Col>
            <Col span={10}>
              {kyc.ongoing ? (
                <NavLink to={`/profile/kyc/tier/${kyc.ongoing["id"]}`}>
                  {kyc.ongoing["name"]}
                </NavLink>
              ) : (
                "None"
              )}
            </Col>
          </Row>
          {kyc.upcoming ? (
            <Row gutter={8}>
              <Col span={10}>Upcoming tier</Col>
              <Col span={10}>{kyc.upcoming["name"]}</Col>
            </Row>
          ) : null}
        </Card>
      </Col>
    ) : null;
  };

  showInvestmentPlan = () => {
    const { target_tier } = this.props.user;
  
    {/*<Input*/}
      {/*addonBefore="Investment plan"*/}
      {/*id="investment_plan"*/}
      {/*defaultValue={target_tier.description}*/}
      {/*disabled={true}*/}
    {/*/>*/}
    
    return target_tier !== null ? (
      null
    ) : (
      <Button
        to={"/profile/kyc"}
        className={"ant-btn ant-btn-primary ant-btn-block"}
        data-intro="Button!"
        data-step="2"
        onClick={() => this.props.setInvestmentPlan(0)}
      >
        Please, proceed to create your KYC application
      </Button>
    );
  };

  render() {
    const {
      first_name,
      last_name,
      email,
      eth_address,
      birth_date
    } = this.props.user;

    return (
      <React.Fragment>
        <Row className="container_row">
          <Col span={24}>
            <Card>
              <Row className="card_row" gutter={48}>
                <Col span={12}>
                  <Input
                    addonBefore="First name"
                    id="first_name"
                    defaultValue={first_name}
                    disabled={true}
                  />
                </Col>
                <Col span={12}>
                  <Input
                    addonBefore="Last name"
                    id="last_name"
                    defaultValue={last_name}
                    disabled={true}
                  />
                </Col>
              </Row>
              <Row className="card_row" gutter={48}>
                <Col span={12}>
                  <Input
                    addonBefore="Email address"
                    id="email-address"
                    defaultValue={email}
                    disabled={true}
                  />
                </Col>
                <Col span={12}>
                  <Input
                    addonBefore="ETH address"
                    id="eth-address"
                    defaultValue={eth_address}
                    disabled={true}
                  />
                </Col>
              </Row>
              <Row className="container_row" gutter={48}>
                <Col span={12}>
                  <Input
                    addonBefore="Birth date"
                    id="birth_date_input"
                    defaultValue={birth_date}
                    disabled={true}
                  />
                </Col>
                <Col span={12}>{this.showInvestmentPlan()}</Col>
              </Row>
              <Row gutter={48}>
                <Col span={12} data-intro="Hello step one!" data-step="1">
                  All users must complete identity verification to participate in LKD tokensale and
                  use the LKD platform to place orders
                </Col>
                {this.showKYCSummary()}
              </Row>
            </Card>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setInvestmentPlan: data => {
      dispatch(setInvestmentPlan(data));
    },
    get: type => {
      dispatch(get(type));
    }
  };
};



export default connect(mapStateToProps, mapDispatchToProps)(Overview);
