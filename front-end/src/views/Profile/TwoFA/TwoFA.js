import React from "react";
import { connect } from "react-redux";
import { SHA3 } from "sha3";
import { Card, Button, Input, Row, Col, message, Form, Skeleton } from "antd";
import { CountDown } from "ant-design-pro";
import { get2FAQRCode, enable2FA, disable2FA } from "redux/actions/index";
import { startAuth } from "services";
import { FinishMessage } from "components";
const FormItem = Form.Item;
const QRCode = require("qrcode.react");

class TwoFA extends React.Component {
  state = {
    requestForDisable2FA: false
  };

  handleClickGetQR = e => {
    e.preventDefault();
    const { get2FAQRCode } = this.props;
    get2FAQRCode();
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      enable2FA,
      user: { google_2fa_enabled },
      disable2FA
    } = this.props;

    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const hashPassword = new SHA3(256);

        if (!google_2fa_enabled) {
          const value = { password: values["2fa_code_field"] };
          enable2FA(value);
          return true;
        }

        const value = {
          google_2_fa_pass: values["2fa_code_field"],
          password: hashPassword.update(values["password"]).digest("hex")
        };

        disable2FA(value);
        this.setState({ requestForDisable2FA: false });
        return true;
      }
    });
  };

  handleClickDisableQR = e => {
    e.preventDefault();
    this.setState({ requestForDisable2FA: true });
  };

  render2FAComponent = () => {
    const {
      user: { google_2fa_enabled, google_2fa_code }
    } = this.props;

    const { requestForDisable2FA } = this.state;

    return !google_2fa_enabled ? (
      !!!google_2fa_code ? (
        <React.Fragment>
          <Row>
            To add more security to your account, you may enable Second Factor Authentication (2FA).
            Our platform relies on Google Authenticator tool.
            You can easily install it on your mobile device, please follow the setup links below:
            <ul>
              <li>
                <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">
                  Android based devices
                </a>
              </li>
              <li>
                <a href="https://itunes.apple.com/ua/app/google-authenticator/id388497605">
                  iOS based devices
                </a>
              </li>
            </ul>
          </Row>
          <Row>
            <Button
              type="primary"
              onClick={e => this.handleClickGetQR(e)}
              disabled={!!google_2fa_code}
            >
              Enable 2FA
            </Button>
          </Row>
        </React.Fragment>
      ) : null
    ) : !requestForDisable2FA ? (
      <React.Fragment>
        <Row className={"card_row"}>
          In order to remove 2FA from your account, the platform will ask a current 2FA code and your current password
        </Row>
        <Button
          type="primary"
          onClick={e => this.handleClickDisableQR(e)}
          disabled={requestForDisable2FA}
        >
          Disable 2FA
        </Button>
      </React.Fragment>
    ) : null;
  };

  renderCodeField = () => {
    const {
      form: { getFieldDecorator },
      user: { google_2fa_code }
    } = this.props;

    const { requestForDisable2FA } = this.state;

    return !!google_2fa_code || requestForDisable2FA ? (
      <Row gutter={24}>
        <Form onSubmit={e => this.handleSubmit(e)}>
          <Row type="flex" justify="start" align="bottom" gutter={24}>
            <Col lg={12} xl={6}>
              <FormItem label={"Confirmation code"}>
                {getFieldDecorator("2fa_code_field", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please input code from Google 2FA"
                    }
                  ]
                })(<Input />)}
              </FormItem>
            </Col>
            {requestForDisable2FA ? (
              <Col lg={12} xl={6}>
                <FormItem label={"Password"}>
                  {getFieldDecorator("password", {
                    validateTrigger: "onBlur",
                    rules: [
                      {
                        required: true,
                        message: "Please input your password"
                      }
                    ]
                  })(<Input type="password" />)}
                </FormItem>
              </Col>
            ) : null}
            <Col lg={12} xl={8}>
              <FormItem>
                <Button htmlType={"submit"} type="primary">
                  {requestForDisable2FA ? "Deactivate 2FA" : "Send 2FA code"}
                </Button>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Row>
    ) : null;
  };

  renderQRcode = () => {
    const {
      user: { google_2fa_code }
    } = this.props;

    return !!google_2fa_code ? (
      <React.Fragment>
        <Row className="card_row" span={12}>
          <QRCode
            value={google_2fa_code}
            size={128}
            bgColor={"#ffffff"}
            fgColor={"#000000"}
            level={"L"}
            includeMargin={false}
            renderAs={"svg"}
          />
        </Row>
      </React.Fragment>
    ) : null;
  };

  render() {
    const {
      fetch: { state },
      user: { google_2fa_enabled, google_2fa_code }
    } = this.props;

    if (google_2fa_enabled && google_2fa_code !== null) {
      return (
        <Row className="container_row">
          <Col span={18}>
            <FinishMessage
              success={true}
              message={"enabled 2FA"}
              method={startAuth}
            />
          </Col>
        </Row>
      );
    }

    return (
      <Row className="container_row">
        <Col span={18}>
          <Card>
            {!state ? (
              <React.Fragment>
                {this.render2FAComponent()}
                {this.renderQRcode()}
                {this.renderCodeField()}
              </React.Fragment>
            ) : (
              <Skeleton active />
            )}
          </Card>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get2FAQRCode: () => {
      dispatch(get2FAQRCode());
    },
    enable2FA: value => {
      dispatch(enable2FA(value));
    },
    disable2FA: value => {
      dispatch(disable2FA(value));
    }
  };
};

const TwoFAComponent = Form.create()(TwoFA);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TwoFAComponent);
