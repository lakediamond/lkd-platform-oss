import React from "react";
import { Request } from "services";
import { GET_OAUTH_TOKEN_EXCHANGE } from "redux/constants";
import { Spin, message } from "antd";
import { Spinner } from "components";

class Final extends React.Component {
  async componentDidMount() {
    const {
      history: {
        location: { search }
      }
    } = this.props;

    if (search.indexOf("?code") !== -1) {
      const data = await Request(GET_OAUTH_TOKEN_EXCHANGE, null, search);

      if (!data) {
        message.error("Looks like it's a network error");
        return false;
      }

      const headers = data.headers;
      const location = headers.get("Location");
      window.location.href = location;
    } else {
      window.location.href = "/auth";
    }
  }

  render() {
    return(<Spinner />)
  }
}

export default Final;
