import React from "react";
import { connect } from "react-redux";
import { Row, Col, message, Card, Skeleton } from "antd";
import {
  ProposalsTable,
  CreateProposalDataWidget,
  MetamaskNotExistCard,
  Spinner
} from "components";

import {
  checkAllowance,
  get,
  fetchProposalsList_all,
  fetchProposalsList_user,
  isMetamaskInstalled,
  webSocketData
} from "redux/actions";
import { PROPOSALS_USER_SOCKET, PROPOSALS_ALL_SOCKET } from "redux/constants";


class Proposals extends React.Component {
  state = {
    allowanceRequest: 0
  };

  componentWillMount() {
    // this.timeouts = [];
  }

  componentDidMount() {
    const {
      user: { kyc_summary },
      history,
      metaMask: { metaMaskAccount, isInstalled },
      isMetamaskInstalled,
      webSocketData,
      get
    } = this.props;
  
    // TODO It's right, but it's not necessary for develop
    // if(!!!kyc_summary){
    //   history.push("/profile");
    // }
    
    
    if (isInstalled === null) {
      isMetamaskInstalled();
    }

    if (!!metaMaskAccount) {
      get("GET_PROPOSALS_ALL");
      get("GET_PROPOSALS_USER");
      // webSocketData(PROPOSALS_ALL_SOCKET, "start");
      // webSocketData(PROPOSALS_USER_SOCKET, "start");
    }
  }

  componentWillUnmount() {
    const {
      webSocketData
    } = this.props;
    
    // webSocketData(PROPOSALS_ALL_SOCKET, "finish");
    // webSocketData(PROPOSALS_USER_SOCKET, "finish");
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const {
      metaMask: { metaMaskAccount, allowance },
      checkAllowance,
      proposalsList: { error },
      webSocketData,
      get
    } = this.props;

    const { allowanceRequest } = this.state;
    

    if (
      prevProps.metaMask.metaMaskAccount !== metaMaskAccount &&
      !!metaMaskAccount
    ) {
      get("GET_PROPOSALS_ALL");
      get("GET_PROPOSALS_USER");
      // webSocketData(PROPOSALS_ALL_SOCKET, "start");
      // webSocketData(PROPOSALS_USER_SOCKET, "start");
    }

    if (
      !!metaMaskAccount &&
      prevProps.metaMask.allowance === null &&
      allowance === null &&
      allowanceRequest === 0
    ) {
      this.setState({ allowanceRequest: 1 });
      checkAllowance(metaMaskAccount);
    }

    //TODO CHECK THIS
    if (error) {
      message.error(error);
    }
  }

  render() {
    const {
      metaMask: { isInstalled }
    } = this.props;

    switch (isInstalled) {
      case null:
        return <Skeleton active />;
      case true:
        return this.renderContent();
      case false:
        return <MetamaskNotExistCard />;
      default:
        return <Skeleton active />;
    }
  }

  renderCreateProposal = () => {
    const {
      metaMask: { allowance, ioContractInstance }
    } = this.props;

    if (allowance === null) {
      return (
        <Card className="card_allowance_skeleton">
          <Skeleton active />
        </Card>
      );
    }
    return allowance ? (
      <CreateProposalDataWidget contractInstance={ioContractInstance} />
    ) : (
      <Card className="card_allowance_skeleton">
        "You need to get an allowance"
      </Card>
    );
  };
  //todo create button for get an allowance if false

  renderContent = () => {
    const {
      metaMask: { ioContractInstance, isLoggedIn },
      proposalsList
    } = this.props;
  
    if(!!!isLoggedIn){
  
      return (
        <React.Fragment>
          <Row className="container_row" gutter={48}>
            <Col lg={8} xl={6}>
              <Card>
                <Skeleton active/>
              </Card>
            </Col>
            <Col lg={16} xl={18}>
              <Card>
                <Skeleton active/>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Card>
                <Skeleton active/>
              </Card>
            </Col>
          </Row>
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        <Row className="container_row" gutter={48}>
          <Col lg={8} xl={6}>
            {this.renderCreateProposal()}
          </Col>
          <Col lg={16} xl={18}>
            <ProposalsTable
              collection={proposalsList["user"]}
              title={"My Proposals"}
              type={"user"}
              contractInstance={ioContractInstance}
            />
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <ProposalsTable
              collection={proposalsList["all"]}
              title={"All Proposals"}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    proposalsList: state.proposalsList,
    metaMask: state.metaMask,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isMetamaskInstalled: () => {
      dispatch(isMetamaskInstalled());
    },
    get: (type) => {
      dispatch(get(type));
    },
    checkAllowance: account => {
      dispatch(checkAllowance(account));
    },
    // webSocketData: (type, action) => {
    //   dispatch(webSocketData(type, action));
    // }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Proposals);
