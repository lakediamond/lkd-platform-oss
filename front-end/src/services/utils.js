import moment from "moment";
import { BLOCKCHAIN_EXPLORER_URL } from "redux/constants";
const OWASP = require("owasp-password-strength-test");

OWASP.config({
  allowPassphrases: false,
  maxLength: 128,
  minLength: 10,
  minPhraseLength: 0,
  minOptionalTestsToPass: 4
});

export const startAuth = () => (window.location.href = "/auth");

export const increaseValueForBlockchain = value =>
  Number(value) * Math.pow(10, 18);
export const decreaseValueForBlockchain = value => {
  const result = Number(value) / Math.pow(10, 18);
  return result % 1 === 0 ? result : result.toFixed(4);
};
export const formatData = value =>
  moment(value)
    .utc()
    .format("YYYY/MM/DD  HH:mm:ss");
export const createAddressLink = value =>
  `${BLOCKCHAIN_EXPLORER_URL}/address/${value}`;

export const createTXLink = value => `${BLOCKCHAIN_EXPLORER_URL}/tx/${value}`;

export const checkPasswordOWASP = value => {
  const result = OWASP.test(value);
  return !result["errors"].length
    ? "valid"
    : result["errors"].map(item => item.padStart(item.length + 1)).join(",");
};
