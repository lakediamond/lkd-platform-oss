import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Request,
  decreaseValueForBlockchain,
  increaseValueForBlockchain
} from "services";
import { Button, Col, Form, Input, Row, Card, Select, message } from "antd";

import {
  GET_ORDER_TYPES,
  GET_ETH_PRICE,
  DIGIT_REG_EXP,
  LETTERS_EXP,
  SPECSYMBOLS_NOT_ALLOWED
} from "redux/constants";

import { postDataForSimulate, hideSimulate } from "redux/actions";

const Option = Select.Option;
const FormItem = Form.Item;

class CreateOrderDataWidget extends React.Component {
  state = {
    ordersTypeData: null
  };

  handleSubmit = e => {
    const { simulatedData } = this.props;

    const banned_addresses = simulatedData.data
      ? simulatedData.data.investors
          .filter(item => item.banned)
          .map(item => item.address)
      : [];

    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        const price = increaseValueForBlockchain(values["price_value"]);
        const ether_amount_value = increaseValueForBlockchain(
          values["ether_amount_value"]
        );

        this.createOrderInContract(
          values["type_value"],
          values["metadata_value"],
          price,
          values["token_amount_value"],
          banned_addresses,
          ether_amount_value
        );
      }
    });
  };

  handleClickReset = e => {
    e.preventDefault();
    this.props.form.resetFields();
  };

  handleBlur = async e => {
    const fieldsAreNotEmpty = () => {
      return (
        this.props.form.getFieldValue("price_value") > 0 &&
        this.props.form.getFieldValue("token_amount_value") > 0 &&
        this.props.form.getFieldValue("ether_amount_value") > 0
      );
    };

    if (e.target.value.length < 1) {
      return false;
    }

    if (fieldsAreNotEmpty()) {
      const price = increaseValueForBlockchain(
        this.props.form.getFieldValue("price_value")
      );
      const token_amount = this.props.form.getFieldValue("token_amount_value");
      const ether_amount = increaseValueForBlockchain(
        this.props.form.getFieldValue("ether_amount_value")
      );
      const fetchString = `tokens=${token_amount}&tokenPrice=${price}&orderEthPrice=${ether_amount}`;
      const value = await Request(GET_ETH_PRICE, null, fetchString);

      if (!value) {
        message.error("Looks like it's a network error");
      }

      value.json().then(data =>
        this.props.form.setFieldsValue({
          ["ether_price_rate"]: data["ether_price_rate"]
        })
      );
    }
    return false;
  };

  //TODO REDUX!!!
  getOrdersTypeData = async () => {
    const data = await Request(GET_ORDER_TYPES);

    if (!data) {
      message.error("Looks like it's a network error");
    }

    if (data.status !== 403 || data.status !== 401) {
      data.json().then(data => {
        this.setState({ ordersTypeData: data });
        return true;
      });
    } else {
      message.error(
        "Ooops, something went wrong. Looks like you need to login again"
      );
    }
  };

  createOrderInContract = (
    type_value,
    metadata_value,
    price_value,
    token_amount_value,
    banned_addresses,
    ether_amount_value
  ) => {
    const {
      metaMask: { ioContractInstance },
      hideSimulate
    } = this.props;

    ioContractInstance.createOrder(
      type_value,
      metadata_value,
      price_value,
      token_amount_value,
      banned_addresses,
      { value: ether_amount_value },
      (err, res) => {
        if (!err) {
          message.success("You successfully added new order to the contract");
          this.props.form.resetFields();
          hideSimulate();
          return;
        }
        message.error("Ooops, something went wrong");
        return;
      }
    );
  };

  createSimulate = e => {
    e.preventDefault();
    const {
      postDataForSimulate,
      form: { validateFields }
    } = this.props;
    validateFields((err, values) => {
      if (!err) {
        const data = {
          order: {
            price: `${increaseValueForBlockchain(values["price_value"])}`,
            eth_amount: `${increaseValueForBlockchain(
              values["ether_amount_value"]
            )}`,
            token_amount: `${values["token_amount_value"]}`
          }
        };

        postDataForSimulate(data);
      }
    });
  };

  componentDidMount() {
    this.getOrdersTypeData();
  }

  renderOptionsOrderTypes = () => {
    const { ordersTypeData } = this.state;
    return ordersTypeData != null
      ? ordersTypeData.map((item, index) => {
          return (
            <Option value={item["ID"]} key={index}>
              {item["Name"]}
            </Option>
          );
        })
      : null;
  };

  render() {
    const {
      form: { getFieldDecorator },
      simulatedData
    } = this.props;

    return (
      <Card title={"Create order"}>
        <Row>
          <Col>
            <Form onSubmit={e => this.handleSubmit(e)}>
              <FormItem label={"Select a type"}>
                {getFieldDecorator("type_value", {
                  rules: [{ required: true, message: "Please select a type!" }]
                })(<Select>{this.renderOptionsOrderTypes()}</Select>)}
              </FormItem>
              <FormItem label={"Metadata"}>
                {getFieldDecorator("metadata_value", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      pattern: SPECSYMBOLS_NOT_ALLOWED,
                      message: "Please input metadata"
                    }
                  ]
                })(<Input />)}
              </FormItem>
              <FormItem label={"Price"}>
                {getFieldDecorator("price_value", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please type price value",
                      pattern: DIGIT_REG_EXP
                    }
                  ]
                })(
                  <Input
                    placeholder="0"
                    name={"price_value"}
                    onBlur={e => this.handleBlur(e)}
                  />
                )}
              </FormItem>
              <FormItem label={"Token amount"}>
                {getFieldDecorator("token_amount_value", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please type token amount",
                      pattern: DIGIT_REG_EXP
                    }
                  ]
                })(
                  <Input
                    placeholder="0"
                    name={"token_amount_value"}
                    onBlur={e => this.handleBlur(e)}
                  />
                )}
              </FormItem>
              <FormItem label={"Ethereum amount"}>
                {getFieldDecorator("ether_amount_value", {
                  rules: [
                    {
                      required: true,
                      message: "Please type ether amount",
                      pattern: DIGIT_REG_EXP
                    }
                  ]
                })(
                  <Input
                    name="ether_amount_value"
                    placeholder="0"
                    onBlur={e => this.handleBlur(e)}
                  />
                )}
              </FormItem>
              <FormItem className="card_row" label={"Ethereum price rate"}>
                {getFieldDecorator("ether_price_rate", {
                  rules: [{ message: "This field has to be full" }]
                })(<Input placeholder="0" disabled={true} />)}
              </FormItem>
              <Row gutter={16} className="card_row">
                <Col span={8}>
                  <Button type="primary" htmlType="submit" block>
                    Create
                  </Button>
                </Col>

                <Col span={8}>
                  <Button
                    type="danger"
                    block
                    onClick={e => this.handleClickReset(e)}
                  >
                    Decline
                  </Button>
                </Col>
                <Col span={8}>
                  <Button
                    type="dashed"
                    block
                    onClick={e => this.createSimulate(e)}
                    disabled={simulatedData.showSimulate}
                  >
                    Simulate
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    metaMask: state.metaMask,
    simulatedData: state.simulatedData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postDataForSimulate: data => {
      dispatch(postDataForSimulate(data));
    },
    hideSimulate: () => {
      dispatch(hideSimulate());
    }
  };
};

const CreateOrderComponent = Form.create()(CreateOrderDataWidget);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateOrderComponent);
