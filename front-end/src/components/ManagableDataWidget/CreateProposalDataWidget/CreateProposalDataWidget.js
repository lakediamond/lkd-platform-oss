import React from "react";
import { Button, Col, Form, Input, Row, Card, message } from "antd";
import { DIGIT_REG_EXP } from "redux/constants";
import { increaseValueForBlockchain } from "services";
const FormItem = Form.Item;

class CreateProposalDataWidget extends React.Component {
  
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if(!err){
        this.createProposalInContract(values['price_value'], values['amount_value']);
      }
    })
  };

  handleClickReset = e => {
    e.preventDefault();
    this.props.form.resetFields();
  };

  createProposalInContract = (price_value, amount_value) => {
    const { contractInstance } = this.props;
    const price = increaseValueForBlockchain(price_value);
    contractInstance.createProposal(price, amount_value, (err, res) => {
      if (!err) {
        message.success("You successfully created a proposal");
        this.props.form.resetFields();
        return;
      }
      message.error("Ooops, something went wrong");
      return;
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Card title={"Create proposal"}>
        <Row>
          <Col>
            <Form onSubmit={e => this.handleSubmit(e)}>
              <FormItem label={"Price"}>
                {getFieldDecorator("price_value", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please input price!",
                      pattern: DIGIT_REG_EXP
                    }
                  ]
                })(<Input placeholder="0" />)}
              </FormItem>
              <FormItem className="card_row" label={"Amount"}>
                {getFieldDecorator("amount_value", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please input amount!",
                      pattern: DIGIT_REG_EXP
                    }
                  ]
                })(<Input placeholder="0" />)}
              </FormItem>
              <Row gutter={16} className="card_row">
                <Col span={12}>
                  <Button type="primary" htmlType="submit" block>
                    Create
                  </Button>
                </Col>
                <Col span={12}>
                  <Button
                    type="danger"
                    block
                    onClick={e => this.handleClickReset(e)}
                  >
                    Decline
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Card>
    );
  }
}

export default Form.create()(CreateProposalDataWidget);
