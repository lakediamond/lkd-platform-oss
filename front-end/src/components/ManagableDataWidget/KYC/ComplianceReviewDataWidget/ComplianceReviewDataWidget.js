import React from "react";
import { Card, Row, Col, Form, Checkbox, Button, Icon, Divider } from "antd";

import { connect } from "react-redux";
import { getComplianceReview, postComplianceReview } from "redux/actions";

const FormItem = Form.Item;

class ComplianceReviewDataWidget extends React.Component {
  state = {
    approved: false,
    id: null
  };

  componentDidMount() {
    this.props.getComplianceReview();
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const { complianceReview } = this.props;

    if (prevProps.complianceReview !== complianceReview && complianceReview) {
      const { id } = complianceReview;
      this.setState({ id: id });
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    const { postComplianceReview } = this.props;
    const data = {id: this.state.id};
    postComplianceReview(data);
  };

  renderButton = () => {
    const {
      complianceReview,
      fetch: { state }
    } = this.props;

    const { approved } = this.state;

    if (!complianceReview) {
      return null;
    }

    const status = complianceReview ? complianceReview["status"] : null;

    if (status === "CUSTOMER_CONFIRMED") {
      return (
        <React.Fragment>
          <Row gutter={24}>
            <Col offset={18}>
              <span className={"confirmed"}>
                <Icon type="check-circle" theme="filled" /> Customer confirmed
              </span>
            </Col>
          </Row>
        </React.Fragment>
      );
    }

    return (
      <Row>
        <Col>
          <Button disabled={!approved} htmlType={"submit"} type="primary">
            {!state ? (
              "Approve"
            ) : (
              <Icon type="sync" spin style={{ color: "fff" }} />
            )}
          </Button>
        </Col>
      </Row>
    );
  };

  render() {
    const {
      complianceReview,
      form: { getFieldDecorator }
    } = this.props;

    const { approved } = this.state;

    if (!complianceReview) {
      return null;
    }

    const code = complianceReview.content_items[0]["code"];
    const value = complianceReview.content_items[0]["value"] === "true";
    const enabled = complianceReview.content_items[0]["enabled"];
  

    return (
      <Card title="Compliance officer review">
        <Form onSubmit={e => this.handleSubmit(e)}>
          <Row>
            <Col>
              <FormItem>
                {getFieldDecorator(code, {
                  validateTrigger: "onBlur",
                  initialValue: value,
                  rules: [
                    {
                      required: true
                    }
                  ]
                })(
                  <Checkbox
                    onChange={() => this.setState({ approved: !approved })}
                    checked={value ? value : this.state.approved}
                    disabled={!enabled}
                  >
                    I acknowledge the need for my KYC application to be reviewed
                    by compliance officer
                  </Checkbox>
                )}
              </FormItem>
            </Col>
          </Row>
          {this.renderButton()}
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    complianceReview: state.complianceReview,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postComplianceReview: value => {
      dispatch(postComplianceReview(value));
    },
    getComplianceReview: () => {
      dispatch(getComplianceReview());
    }
  };
};

const complianceReviewComponent = Form.create()(ComplianceReviewDataWidget);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(complianceReviewComponent);
