import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Button, Icon, Upload, message, Row, Col, Skeleton } from "antd";
import {
  GET_GENERIC_UPLOAD_IMAGE_URL,
  CHECK_POSSIBILITY_TO_UPLOAD_PHOTO,
  GET_IDENTITY_SCANS,
  GET_TIERS_INFO
} from "redux/constants";

import { updateIdentityScansStatus, get } from "redux/actions";

import { Request } from "services";
import { Spinner } from "components";

class UploadDataWidget extends React.Component {
  static propTypes = {
    type: PropTypes.string.isRequired
  };

  state = {
    file: null,
    isUploaded: false,
    error: null
  };

  componentWillMount() {
    this.timeouts = [];
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      type,
      order,
      identityScans,
      updateIdentityScansStatus,
      openedTab
    } = this.props;

    const fileName = `/${type}`;

    const status = identityScans
      ? identityScans.content_items[order]["status"]
      : null;

    if (!!!status || status === "INVALID") {
      const isPhotoExists = await Request(
        CHECK_POSSIBILITY_TO_UPLOAD_PHOTO,
        null,
        fileName
      );

      if (!isPhotoExists) {
        message.error("Network error");
        return false;
      }

      if (isPhotoExists.status === 304) {
        if (status === "UPLOADED") {
          return updateIdentityScansStatus(order, "UPLOADED");
        }
      }

      if (isPhotoExists.status === 204) {
        return true;
      }
    }

    if (
      (openedTab == order && status === "UPLOADED") ||
      (prevProps.openedTab != openedTab &&
        openedTab == order &&
        status === "UPLOADED")
    ) {
      this.timeouts.forEach(clearTimeout);
      this.timeouts = [];
      if (!this.timeouts.length) {
        this.timeouts.push(
          setInterval(() => {
            this.props.get(GET_IDENTITY_SCANS);
          }, 7000)
        );
      }
    }

    if (status !== "UPLOADED" || openedTab != order) {
      this.timeouts.forEach(clearTimeout);
      this.timeouts = [];
    }
  }

  componentWillUnmount() {
    this.timeouts.forEach(clearTimeout);
    this.timeouts = [];
  }

  // async componentDidMount() {
  //   // const { type, order, identityScans } = this.props;
  // }

  beforeUpload = async file => {
    const { type, updateIdentityScansStatus, order } = this.props;
    const filePath = `/${type}`;

    const rowFile = file.slice(0, file.size, file.type);
    const rowExt = file.name.split(".");
    const ext = rowExt[rowExt.length - 1];

    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error("Image must be less than 2MB");
      this.setState({ error: true });
    }

    const newFile = new File([rowFile], `${type}.${ext}`, {
      type: rowFile.type
    });

    const pathForNewFileCreated = await Request(
      GET_GENERIC_UPLOAD_IMAGE_URL,
      newFile.name,
      filePath
    );

    if (!pathForNewFileCreated) {
      message.error("Network error");
    }

    if (pathForNewFileCreated.status === 200) {
      pathForNewFileCreated
        .json()
        .then(url => this.setState({ url: url["url"], file: newFile }));
    }

    return await false;
  };

  handleChange = () => {
    return false;
  };

  handleUpload = async () => {
    const { updateIdentityScansStatus, order } = this.props;
    const { file } = this.state;

    this.setState({
      uploading: true
    });

    const config = {
      method: "PUT",
      headers: new Headers({ "Content-type": `${file.type}` }),
      body: file,
      mode: "cors"
    };

    const data = await fetch(this.state.url, config);

    if (!data) {
      message.error("This is a network error");
    }

    if (data.status === 200) {
      message.success("Your image successfully upload");
      updateIdentityScansStatus(order, "UPLOADED");
    }

    this.setState({
      uploading: false
    });

    return;
  };

  handleRemove = () => {
    this.setState({ isUploaded: false, file: null, error: null });
  };

  renderUploadComponent = () => {
    const { file } = this.state;
    const { order, identityScans } = this.props;
    const status = identityScans
      ? identityScans.content_items[order]["status"]
      : null;

    if (!!!status || status === "INVALID") {
      return (
        <Upload
          accept=".jpg, .png, .jpeg"
          name="avatar"
          listType="picture-card"
          beforeUpload={this.beforeUpload}
          onChange={this.handleChange}
          className="card_row"
          showUploadList={true}
          onRemove={() => this.handleRemove()}
        >
          {file ? null : <Icon type="plus" />}
        </Upload>
      );
    }

    return null;
  };

  renderTextStatus = () => {
    const { order, identityScans, get } = this.props;
    const status = identityScans
      ? identityScans.content_items[order]["status"]
      : null;

    if (status === "UPLOADED") {
      return (
        <React.Fragment>
          <div className={"card_row"}>
            Photo has been uploaded, please wait while we check it...
          </div>
          <Spinner type={"in_block"} />
        </React.Fragment>
      );
    }

    if (status === "INVALID") {
      return (
        <React.Fragment>
          <div>Your previous image was corrupted.Please try again</div>
        </React.Fragment>
      );
    }

    if (status === "VALID" || status === "PROCESSED") {
      get(GET_TIERS_INFO);
      return (
        <React.Fragment>
          <span className={"confirmed"}>
            <Icon type="check-circle" theme="filled" /> Upload successful!
          </span>
        </React.Fragment>
      );
    }

    return null;
  };

  renderButton = () => {
    const { uploading, file, error } = this.state;
    const { order, identityScans } = this.props;
    const status = identityScans["content_items"][order]["status"];

    if (status === "" || status === "INVALID") {
      return (
        <Row>
          <Col>
            <Button
              type="primary"
              onClick={this.handleUpload}
              disabled={!file || !!error}
              className="card_row"
            >
              {uploading ? "Uploading" : "Start Upload"}
            </Button>
          </Col>
        </Row>
      );
    }

    return null;
  };

  render() {
    const { order, identityScans } = this.props;
    const status = identityScans
      ? identityScans.content_items[order]["status"]
      : null;

    if (status === null) {
      return (
        <React.Fragment>
          <Skeleton active />
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        <Row>
          <Col>
            {this.renderTextStatus()}
            {this.renderUploadComponent()}
          </Col>
        </Row>
        {this.renderButton()}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    identityScans: state.identityScans
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateIdentityScansStatus: (order, value) => {
      dispatch(updateIdentityScansStatus(order, value));
    },
    get: type => {
      dispatch(get(type));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadDataWidget);
