import React from "react";
import PropTypes from "prop-types";
import { Menu, Dropdown, Icon } from "antd";

const SimpleMenu = props => {
  const { handleOptionChange, options, selectedLabel } = props;

  const handleMenuItemClick = (event, index, value) => {
    handleOptionChange(value);
  };

  const menuItems = () => {
    return options.map((option, index) => (
      <Menu.Item
        key={index}
        onClick={event => handleMenuItemClick(event, index, option.name)}
      >
        {option.label}
      </Menu.Item>
    ));
  };

  const menu = <Menu id="lock-menu">{menuItems()}</Menu>;

  return (
    <Dropdown overlay={menu} trigger={["click"]}>
      <a href="#">
        {selectedLabel}
        <Icon type="down" />
      </a>
    </Dropdown>
  );
};

SimpleMenu.propTypes = {
  options: PropTypes.array.isRequired,
  selectedLabel: PropTypes.string.isRequired
};

export default SimpleMenu;
