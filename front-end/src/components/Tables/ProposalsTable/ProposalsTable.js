import React from "react";
import { Table, Card, Input, Button } from "antd";
import { decreaseValueForBlockchain, formatData } from "services";
const { Column } = Table;

const ProposalsTable = props => {
  const { collection, title, type, contractInstance } = props;

  const withdrawProposal = id => {
    contractInstance.withdrawProposal(id, (err, res) => {
      if (!err) {
        return;
      }
      return;
    });
  };

  const handleClickSubmit = e => {
    withdrawProposal(e.target.value);
  };

  return (
    <Card title={title}>
      <Table dataSource={collection} pagination={true}>
        <Column title="Id" dataIndex="id" key={`${"id"+Math.random()}`} />
        <Column
          title="Price"
          dataIndex="price"
          render={text => {
            return <span>{decreaseValueForBlockchain(text)}</span>;
          }}
          key={`${"price"+Math.random()}`}
        />
        <Column
          title="Tokens amount"
          dataIndex="tokens_amount"
          key={`${"tokens_amount"+Math.random()}`}
        />
        <Column
          title="Created on"
          dataIndex="created_on"
          key={`${"created_on"+Math.random()}`}
          render={text => <span>{formatData(text)}</span>}
        />
        <Column
          title="Withdrawn on"
          key={`${"withdrawn_on"+Math.random()}`}
          dataIndex="withdrawn_on"
          render={text => <span>{text ? formatData(text) : "-"}</span>}
        />
        <Column
          title="Withdrawn tokens amount"
          dataIndex="withdrawn_tokens_amount"
          key={`${"withdrawn_tokens_amount"+Math.random()}`}
        />
        {type === "user" ? (
          <Column
            title="Withdraw"
            key={`${"id"+Math.random()}`}
            render={text => (
              <Button
                type="primary"
                shape="circle"
                icon="check"
                onClick={e => handleClickSubmit(e)}
                size="small"
                value={text["id"]}
                key={`${text["id"]+Math.random()}`}
                disabled={
                  text["withdrawn_tokens_amount"] != 0 ||
                  Number(text["tokens_amount"]) === 0
                }
              />
            )}
          />
        ) : null}
      </Table>
    </Card>
  );
};

export default ProposalsTable;
