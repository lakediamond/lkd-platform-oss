import { BLOCKCHAIN_EXPLORER_URL } from "redux/constants";

const tableColumns =  [
  { title: "First Name", dataIndex: "first_name", key: "first_name" },
  { title: "Last Name", dataIndex: "last_name", key: "last_name" },
  { title: "Email", dataIndex: "email", key: "email" },
  { title: "ETH Address", dataIndex: "eth_address", key: "eth_address",  }
];

export default tableColumns;