import React from "react";
import { Table, Card, Row } from "antd";
import { decreaseValueForBlockchain, formatData, createAddressLink, createTXLink } from "services";

class OrdersTable extends React.Component {
  state = {
    filteredInfo: null,
    sortedInfo: null
  };

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  expandedRowRender = (record, index) => {
    
    const {collection} = this.props;
  
    const investments = collection.map(item => {
      if(!!item["investments"]){
        return item["investments"];
      }
      return false;
    });
  
    const columns = [
      {
        title: "TX Hash",
        key: "tx_hash",
        render: text => {
          return (
            <a href={createTXLink(["tx_hash"])}>
              {text["tx_hash"]}
            </a>
          );
        }
      },
      {
        title: "Tokens invested",
        dataIndex: "tokens_invested",
        key: "tokens_invested"
      },
      {
        title: "Eth received",
        key: "eth_received",
        render: (text, record, index) => {
          return (
            <span>{decreaseValueForBlockchain(text["eth_received"])}</span>
          );
        }
      },
      {title: "Last Name", dataIndex: "last_name", key: "last_name"},
      {title: "First Name", dataIndex: "first_name", key: "first_name"},
      {title: "Email", dataIndex: "email", key: "email"},
      {
        title: "ETH Account",
        key: "eth_account",
        render: text => {
          return (
            <a
              href={createAddressLink(text["eth_account"])}
            >
              {text["eth_account"]}
            </a>
          );
        }
      }
    ];
    
    return (
      <Table
        className="table_expanded"
        columns={columns}
        dataSource={investments[index]}
        pagination={false}
      />
    )
  };

  render() {
    const { collection, account, title } = this.props;
    let { filteredInfo, sortedInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};

    const tableColumns = [
      {
        title: "Id",
        dataIndex: "id",
        key: "id",
        sorter: (a, b) => a.id - b.id
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        sorter: (a, b) => a.type - b.type
      },
      {
        title: "Created by",
        key: "created_by",
        filters: [{ text: "By me", value: `${account}` }],
        filteredValue: filteredInfo.created_by || null,
        onFilter: (value, record) => record.created_by == value,
        sorter: (a, b) => a.created_by - b.created_by,
        sortOrder: sortedInfo.columnKey === "created_by" && sortedInfo.order,
        render: text => {
          return (
            <a
              href={createAddressLink(text["created_by"])}
            >
              {text["created_by"]}
            </a>
          );
        }
        // filteredValue: accountForCheck || null,
        // sortOrder: sortedInfo.columnKey === "created_by" && sortedInfo.order,
        // sorter: (a, b) => a.created_by - b.created_by
      },
      {
        title: "Created on",
        key: "created_on",
        render: (text, record, index) => {
          return <span>{formatData(text["created_on"])}</span>;
        },
        sorter: (a, b) => a.created_on - b.created_on
      },
      {
        title: "Ethereum amount",
        dataIndex: "eth_amount",
        key: "eth_amount",
        sorter: (a, b) => a.eth_amount - b.eth_amount
      },
      {
        title: "Metadata",
        dataIndex: "metadata",
        key: "metadata",
        sorter: (a, b) => a.metadata - b.metadata
      },
      {
        title: "Token amount",
        dataIndex: "tokens_amount",
        key: "tokens_amount",
        sorter: (a, b) => a.tokens_amount - b.tokens_amount
      },
      {
        title: "Token price",
        key: "token_price",
        sorter: (a, b) => a.token_price - b.token_price,
        render: (text, record, index) => {
          return <span>{decreaseValueForBlockchain(text["token_price"])}</span>;
        }
      }
    ];

    return (
      <Card title={title}>
        <Row className="card_row">
          {/*<Col span={4}>*/}
          {/*<Switch onChange={e => this.handleChange(e)} />*/}
          {/*</Col>*/}
          {/*<Col span={4}>Orders created by me</Col>*/}
        </Row>
        <Table
          className={"table_small"}
          dataSource={collection}
          columns={tableColumns}
          pagination={true}
          onChange={this.handleChange}
          expandedRowRender={this.expandedRowRender}
        />
      </Card>
    );
  }
}

export default OrdersTable;
