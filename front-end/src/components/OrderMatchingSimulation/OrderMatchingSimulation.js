import React from "react";
import {
  Table,
  Card,
  Row,
  Col,
  Button,
  Form,
  Input,
  Checkbox,
  message,
  Skeleton,
  Icon
} from "antd";
import { decreaseValueForBlockchain, formatData } from "services";

import { connect } from "react-redux";
import { hideSimulate, postDataForSimulate } from "redux/actions";

class OrderMatchingSimulation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newData: JSON.parse(JSON.stringify(this.props.simulatedData.data))
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      JSON.stringify(prevProps.simulatedData.data) !==
      JSON.stringify(this.props.simulatedData.data)
    ) {
      this.setState({
        newData: JSON.parse(JSON.stringify(this.props.simulatedData.data))
      });
    }
  }

  handleCheckBox = (e, text, index) => {
    const {
      newData: { investors }
    } = this.state;

    let user = Object.assign({}, investors);
    investors[`${index}`]["banned"] = e.target.checked;
    return this.setState({ user });
  };

  handleConfirm = e => {
    const { postDataForSimulate } = this.props;
    e.preventDefault();

    if (
      JSON.stringify(this.props.simulatedData.data["investors"]) ===
      JSON.stringify(this.state.newData["investors"])
    ) {
      message.error("Please change the data");
      return false;
    }

    postDataForSimulate(this.state.newData);
  };

  handleReset = e => {
    e.preventDefault();
    this.setState({
      newData: JSON.parse(JSON.stringify(this.props.simulatedData.data))
    });
  };

  expandedRowRender = (record, index) => {
    const {
      newData: { investors }
    } = this.state;

    const data = investors.map(item => {
      if (!!item["proposals"]) {
        return item["proposals"];
      }
      return false;
    });

    const columns = [
      {
        title: "Created at",
        key: "created_at",
        render: text => {
          return <span>{formatData(text["created_at"])}</span>;
        }
      },
      {
        title: "Ethereum",
        dataIndex: "eth",
        key: "eth"
      },
      { title: "Price", dataIndex: "price", key: "price" }
    ];

    return (
      <Table
        className="table_expanded"
        columns={columns}
        dataSource={data[index]}
        pagination={false}
      />
    );
  };

  render() {
    // const { collection, account } = this.props;
    const {
      hideSimulate,
      simulatedData: {
        data: { chf_coefficient, rest }
      },
      fetch: { state }
    } = this.props;

    const {
      newData: { investors }
    } = this.state;

    const tableColumns = [
      {
        title: "Address",
        dataIndex: "address",
        key: "address"
      },
      {
        title: "Banned",
        key: "banned",
        render: (text, record, index) => {
          return <span>{text ? "No" : "Yes"}</span>;
        }
      },
      {
        title: "Block",
        key: "created_at",
        render: (text, record, index) => {
          return (
            <Checkbox
              defaultChecked={investors[`${index}`]["banned"]}
              checked={investors[`${index}`]["banned"]}
              onChange={e => this.handleCheckBox(e, text, index)}
            />
          );
        }
      }
    ];

    return (
      <Card title={"Matching simulation"}>
        <Row className="card_row">
          <Input
            addonBefore="Exchange rate"
            disabled={true}
            value={chf_coefficient}
          />
        </Row>
        <Row className="container_row">
          <Input addonBefore="ETH remained" disabled={true} value={rest} />
        </Row>
        <Row className="card_row">
          {!state ? (
            <Table
              className={"table_small"}
              dataSource={investors}
              columns={tableColumns}
              pagination={false}
              onChange={this.handleChange}
              expandedRowRender={this.expandedRowRender}
            />
          ) : (
            <Skeleton active />
          )}
        </Row>
        <Row gutter={16} className="card_row">
          <Col span={8}>
            <Button
              type="primary"
              block
              onClick={e => this.handleConfirm(e)}
              disabled={state}
            >
              {state ? (
                <Icon type="sync" spin style={{ color: "fff" }} />
              ) : (
                "Confirm"
              )}
            </Button>
          </Col>
          <Col span={8}>
            <Button
              type="danger"
              block
              onClick={e => this.handleReset(e)}
              disabled={state}
            >
              {state ? (
                <Icon type="sync" spin style={{ color: "fff" }} />
              ) : (
                "Decline"
              )}
            </Button>
          </Col>
          <Col span={8}>
            <Button
              type="dashed"
              block
              onClick={e => hideSimulate(e)}
              disabled={state}
            >
              {state ? (
                <Icon type="sync" spin style={{ color: "fff" }} />
              ) : (
                "Close"
              )}
            </Button>
          </Col>
        </Row>
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    simulatedData: state.simulatedData,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    hideSimulate: () => {
      dispatch(hideSimulate());
    },
    postDataForSimulate: data => {
      dispatch(postDataForSimulate(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderMatchingSimulation);
