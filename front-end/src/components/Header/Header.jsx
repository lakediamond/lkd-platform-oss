import React from "react";
import HeaderLinks from "./HeaderLinks";
import { Layout } from "antd";

const HeaderComp = ({ ...props }) => {
  
  const { history: {location: {pathname}} } = props;
  const { Header } = Layout;
  
  const showCurrentLocation =() => {
    return props.routes.map((prop, key) => {
      if (prop.path === pathname) {
        return prop.navbarName;
      }
      return null;
    });
  };
  
  return (pathname !== "/consent" && pathname !== "/auth") ? (
    <Header>
      <div>{showCurrentLocation()}</div>
      <HeaderLinks history={props.history} />
    </Header>
  ) : null;
}

export default HeaderComp;
