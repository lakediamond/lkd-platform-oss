import React from "react";
import { Menu, Dropdown, Icon } from "antd";
import { connect } from "react-redux";
import { logoutUser } from "redux/actions";

const HeaderLinks = (props) => {
  
  const { history: {location: {pathname}}, logoutUser } = props;

  const menu = (
    <Menu>
      <Menu.Item key="0" onClick={logoutUser}>
        Log out
      </Menu.Item>
    </Menu>
  );

  return ( pathname !== "/consent" ?
    <React.Fragment>
      <Dropdown overlay={menu} trigger={["click"]}>
          <Icon type="user" className={"link"}/>
      </Dropdown>
    </React.Fragment> : null
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => {
      dispatch(logoutUser());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderLinks);
