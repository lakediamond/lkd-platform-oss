import React from "react";
import { NavLink } from "react-router-dom";
import { Icon, Menu, message } from "antd";
import connect from "react-redux/es/connect/connect";
const SubMenu = Menu.SubMenu;

const Navigation = props => {
  // state = {
  //   selectedItem: null
  // };

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   const { tierInfo } = this.props;
  //
  //   if (prevProps.tierInfo !== tierInfo && !!tierInfo.tierInfo) {
  //     this.setState({
  //       selectedItem: tierInfo.tierInfo[0]["steps"].filter(
  //         item => item["status"] === "OPENED"
  //       )[0]["code"]
  //     });
  //   }
  //
  //   if(prevState.selectedItem !== this.state.selectedItem){
  //     this.forceUpdate();
  //     console.log("sdsdsdsdsd");
  //   }
  //
  // }

  const changeStepComponent = e => {
    const { changeStep } = props;
    changeStep(e.key);
  };

  // showErrorMessage = () => {
  //   message.error("This field is blocked, please confirm previous field");
  // };

  const linkStyle = {
    position: "absolute",
    width: "100%"
  };

  const renderMenuItems = () => {
    const {
      tierInfo: { tierInfo },
      defaultSelectedStep
    } = props;

    const menu = tierInfo
      ? tierInfo.map(item => {
          return (
            <SubMenu
              key={item["code"]}
              title={
                <NavLink
                  to={`/profile/kyc/tier/${item["code"]}`}
                  style={linkStyle}
                  className={
                    item["status"] === "LOCKED"
                      ? "navigation__item_locked"
                      : item["status"] === "OPENED" ||
                        item["status"] === "REWORK"
                      ? "navigation__item_opened"
                      : "navigation__item_confirmed"
                  }
                >
                  <Icon type="security-scan" />
                  Detailed personal information
                </NavLink>
              }
            >
              {item["steps"].map(step => {
                return (
                  <Menu.Item
                    // onClick={
                    //   step["status"] === "LOCKED"
                    //     ? () => this.showErrorMessage()
                    //     : e => this.changeStepComponent(e)
                    // }
                    onClick={e => changeStepComponent(e)}
                    key={step["code"]}
                    className={[
                      step["status"] === "LOCKED"
                        ? "navigation__item_locked"
                        : step["status"] === "OPENED"
                        ? "navigation__item_opened"
                        : "navigation__item_confirmed",
                      defaultSelectedStep == step["code"] &&
                      step["status"] !== "LOCKED"
                        ? "ant-menu-item-selected"
                        : null
                    ]}
                    disabled={step["status"] === "LOCKED"}
                  >
                    {step["title"]}{" "}
                    {step["status"] === "LOCKED" ? (
                      <Icon type="close-circle" />
                    ) : step["status"] === "OPENED" ? (
                      <Icon type="login" />
                    ) : (
                      <Icon type="check-circle" />
                    )}
                  </Menu.Item>
                );
              })}
            </SubMenu>
          );
        })
      : null;
    return menu;
  };
  return (
    <Menu
      defaultOpenKeys={[props.defaultOpenKey]}
      mode="inline"
      className="navigation"
    >
      {renderMenuItems()}
    </Menu>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    tierInfo: state.tierInfo
  };
};

export default connect(
  mapStateToProps,
  null
)(Navigation);
