import React from "react";
import { Spin } from "antd";

const Spinner = (props) => {
  const spinnerStyleForContainer = {
    position: "absolute",
    marginLeft: "48vw",
    marginTop: "48vh"
  };
  
  const spinnerStyleForBlock = {
    position: "relative",
    marginLeft: "30px",
  };

  return (
    <div>
      <Spin size="large" style={props.type === "in_block" ? spinnerStyleForBlock : spinnerStyleForContainer } />
    </div>
  );
};

export default Spinner;
