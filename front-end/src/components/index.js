// ##############################
// // // Cards
// #############################

import ChartCard from "./Cards/ChartCard.jsx";
import StatsCard from "./Cards/StatsCard.jsx";
import MetamaskNotExistCard from "./Cards/MetamaskNotExistCard.js";

// ##############################
// // // SimpleMenu
// #############################

import SimpleMenu from "./SimpleMenu/SimpleMenu.jsx";

// ##############################
// // // Header
// #############################

import HeaderComp from "./Header/Header.jsx";
import HeaderLinks from "./Header/HeaderLinks.jsx";

// ##############################
// // // Sidebar
// #############################

import Sidebar from "./Sidebar/Sidebar.jsx";

// ##############################
// // // Forms
// #############################

import OrdersTypeTable from "./Tables/OrdersTypeTable/OrdersTypeTable";
import SubOwnersTable from "./Tables/SubOwnersTable/SubOwnersTable";
import OrdersTable from "./Tables/OrdersTable/OrdersTable";
import ProposalsTable from "./Tables/ProposalsTable/ProposalsTable"
import OrderTypeDataWidget from "./ManagableDataWidget/OrdertypeDataWidget/OrderTypeDataWidget";
import SubownerDataWidget from "./ManagableDataWidget/SubownerDataWidget/SubownerDataWidget";
import CreateOrderDataWidget from "./ManagableDataWidget/CreateOrderDataWidget/CreateOrderDataWidget";
import CreateProposalDataWidget from "./ManagableDataWidget/CreateProposalDataWidget/CreateProposalDataWidget";
import PhoneDataWidget from "./ManagableDataWidget/KYC/PhoneDataWidget/PhoneDataWidget"
import Navigation from "./Navigation/Navigation";
import UploadDataWidget from "./ManagableDataWidget/KYC/UploadDataWidget/UploadDataWidget";
import TierStepDataWidget from "./ManagableDataWidget/KYC/TierStepDataWidget/TierStepDataWidget";
import DocumentDataWidget from "./ManagableDataWidget/KYC/DocumentDataWidget/DocumentDataWidget";
import CommonInfoDataWidget from "./ManagableDataWidget/KYC/CommonInfoDataWidget/CommonInfoDataWidget";
import TargetTierDataWidget from "./ManagableDataWidget/KYC/TargetTierDataWidget/TargetTierDataWidget";
import ComplianceReviewDataWidget from "./ManagableDataWidget/KYC/ComplianceReviewDataWidget/ComplianceReviewDataWidget";
import Spinner from "./Spinner/Spinner";
import Title from "./Title/Title";
import PasswordHint from "./PasswordHint/PasswordHint"
import FinishMessage from "./FinishMessage/FinishMessage";
import OrderMatchingSimulation from "./OrderMatchingSimulation/OrderMatchingSimulation";


export {
  // Cards
  ChartCard,
  StatsCard,
  // SimpleMenu
  SimpleMenu,
  // Header
  HeaderComp,
  HeaderLinks,
  // Sidebar
  Sidebar,
  OrdersTable,
  SubOwnersTable,
  OrdersTypeTable,
  ProposalsTable,
  OrderTypeDataWidget,
  SubownerDataWidget,
  CreateOrderDataWidget,
  CreateProposalDataWidget,
  Navigation,
  PhoneDataWidget,
  TierStepDataWidget,
  DocumentDataWidget,
  UploadDataWidget,
  CommonInfoDataWidget,
  TargetTierDataWidget,
  ComplianceReviewDataWidget,
  MetamaskNotExistCard,
  Spinner,
  Title,
  PasswordHint,
  FinishMessage,
  OrderMatchingSimulation
};
