import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { HeaderComp, Sidebar, Spinner } from "components";
import appRoutes from "routes/app.jsx";
import { Login, Register, Auth, Consent, Final, Recovery } from "views";
import { Layout, Alert, notification } from "antd";
import {
  showAlert,
  loginUser,
  closeAlert,
  isMetamaskLoggedIn,
  enableMetamask,
  createContractInstance,
  signInUser
} from "redux/actions/index";

const switchRoutes = (
  <Switch>
    {appRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      return (
        <Route exact path={prop.path} component={prop.component} key={key} />
      );
    })}
  </Switch>
);

class App extends React.Component {
  state = {};

  componentDidMount() {
    const {
      location: { pathname },
      signInUser
    } = this.props;

    const loginPath = pathname === "/login";
    const registerPath = pathname === "/register";
    const authPath = pathname === "/auth";
    const consentPath = pathname === "/consent";
    const recoveryPath = pathname === "/user/access_recovery_activation";
    const finalPath = pathname === "/";

    if (
      !loginPath &&
      !registerPath &&
      !authPath &&
      !consentPath &&
      !finalPath &&
      !recoveryPath
    ) {
      signInUser();
    }
  }

  componentWillMount() {
    this.timeouts = [];
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      metaMask: {
        metaMaskAccount,
        ioContractInstance,
        isLoggedIn,
        isInstalled
      },
      enableMetamask,
      isMetamaskLoggedIn,
      createContractInstance,
      location: { pathname },
      user: { eth_address }
    } = this.props;

    if (this.refs.mainPanel) {
      this.refs.mainPanel.scrollTop = 0;
    }

    if (
      prevProps.metaMask.isInstalled === null &&
      isInstalled &&
      isLoggedIn === null &&
      !!!metaMaskAccount &&
      (pathname === "/proposals" ||
        pathname === "/wallet" ||
        pathname === "/management" ||
        pathname === "/orders")
    ) {
      isMetamaskLoggedIn();
    }

    if (
      isInstalled === true &&
      isLoggedIn === false &&
      !!!metaMaskAccount &&
      (pathname === "/proposals" ||
        pathname === "/wallet" ||
        pathname === "/management" ||
        pathname === "/orders")
    ) {
      this.renderUserNotLoggedInMetamaskNotification();
    }

    if (isLoggedIn && metaMaskAccount === null) {
      enableMetamask();
    }

    if (
      prevProps.metaMask.metaMaskAccount !== metaMaskAccount &&
      !!metaMaskAccount &&
      !!eth_address
    ) {
      this.checkCurrentAccount(metaMaskAccount);
      setInterval(() => this.isAccountChanged(metaMaskAccount), 1000);
    }

    if (
      !!metaMaskAccount &&
      !!!prevProps.metaMask.ioContractInstance &&
      !!!ioContractInstance
    ) {
      createContractInstance(metaMaskAccount);
    }
  }

  renderUserNotLoggedInMetamaskNotification = () => {
    notification.open({
      message: "We are so sorry!",
      description:
        "Because you have metamask extension in your browser, you have to log in to it",
      duration: 5
    });
  };

  render() {
    const { Content } = Layout;
    const {
      alertMessage: { open_alert, alert_message },
      location: { pathname },
      metaMask: { isLoggedIn },
      ...rest
    } = this.props;

    const {
      user: { in_progress }
    } = rest;

    const loginPath = pathname === "/login";
    const registerPath = pathname === "/register";
    const authPath = pathname === "/auth";
    const consentPath = pathname === "/consent";
    const recoveryPath = pathname === "/user/access_recovery_activation";
    const finalPath = pathname === "/";

    const renderOAUTHViews = pathname => {
      switch (pathname) {
        case "/login":
          return <Login history={rest.history} />;
        case "/register":
          return <Register history={rest.history} />;
        case "/auth":
          return <Auth history={rest.history} />;
        case "/consent":
          return <Consent history={rest.history} />;
        case "/":
          return <Final history={rest.history} />;
        case "/user/access_recovery_activation":
          return <Recovery history={rest.history} />;
        default:
          return null;
      }
    };

    if (
      loginPath ||
      registerPath ||
      authPath ||
      consentPath ||
      finalPath ||
      recoveryPath
    ) {
      return <React.Fragment>{renderOAUTHViews(pathname)}</React.Fragment>;
    }

    if (in_progress || open_alert === null) {
      return <Spinner />;
    }

    const metaMaskIsNeededPath = pathname => {
      return (
        pathname === "/proposals" ||
        pathname === "/wallet" ||
        pathname === "/management" ||
        pathname === "/orders"
      );
    };

    return (
      <React.Fragment>
        {open_alert ? (
        <Alert
        message="Uncorrect metamask account"
        description={alert_message}
        type="error"
        />
        ) : null}
        <Layout
          style={{ minHeight: "100vh" }}
          
        >
          <Sidebar
            routes={appRoutes}
            linksDisabled={this.state.modal_open}
            {...rest}
          />
          <Layout
            className={
              isLoggedIn === false && metaMaskIsNeededPath(pathname)
                ? "metamask_not_logged_in"
                : null
            }
          >
            <HeaderComp routes={appRoutes} {...rest} />
            <Content className={open_alert ? "layout_blocked" : null}>
              {switchRoutes}
            </Content>
          </Layout>
        </Layout>
      </React.Fragment>
    );
  }

  isAccountChanged(account) {
    if (window.web3.eth.accounts[0] !== account) {
      window.location.reload();
    }
  }

  checkCurrentAccount(account) {
    const {
      user: { eth_address },
      showAlert,
      closeAlert
    } = this.props;

    if (eth_address.toLowerCase() !== account.toLowerCase()) {
      showAlert(
        "Please choose Ethereum account address you specified during registration"
      );
    } else {
      closeAlert();
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    alertMessage: state.alertMessage,
    metaMask: state.metaMask,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    showAlert: message => {
      dispatch(showAlert(message));
    },
    closeAlert: () => {
      dispatch(closeAlert());
    },
    isMetamaskLoggedIn: () => {
      dispatch(isMetamaskLoggedIn());
    },
    enableMetamask: () => {
      dispatch(enableMetamask());
    },
    createContractInstance: () => {
      dispatch(createContractInstance());
    },
    signInUser: () => {
      dispatch(signInUser());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
