import {
  FETCH_IN_PROGRESS,
  FETCH_IS_FINISHED
} from "../constants";

export const fetchInProgress = (payload) => {
  return { type: FETCH_IN_PROGRESS, payload };
};

export const fetchIsFinished = () => {
  return { type: FETCH_IS_FINISHED };
};
