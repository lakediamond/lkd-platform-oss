import {
  METAMASK_INSTALLED,
  METAMASK_NOT_INSTALLED,
  METAMASK_USER_LOGGED_IN_SUCCESS,
  METAMASK_USER_LOGGED_IN_FAILED,
  METAMASK_ENABLED,
  METAMASK_NOT_ENABLED,
  USER_IS_CONTRACT_OWNER,
  USER_NOT_CONTRACT_OWNER,
  ETH_BALANCE_RECEIVE_SUCCESS,
  ETH_BALANCE_RECEIVE_FAILED,
  TOKEN_BALANCE_RECEIVE_SUCCESS,
  TOKEN_BALANCE_RECEIVE_FAILED,
  CONTRACT_INSTANCE_CREATED,
  TOKEN_CONTRACT,
  IO_CONTRACT,
  ALLOWANCE_RECEIVED,
  ALLOWANCE_FAILED,
  SHOW_SIMULATE,
  HIDE_SIMULATE,
  POST_SIMULATE,
  POST_SIMULATE_SUCCESS,
  POST_SIMULATE_FAILED
} from "../constants";
import { Request } from "services";

import Contract from "../../lib/Contract";
import { fetchInProgress, fetchIsFinished } from "./fetch";

const isNewVersion = () => !!window.ethereum._metamask;

const metamaskAccountExist = acc => {
  return {
    type: METAMASK_ENABLED,
    payload: { isEnabled: true, account: acc.join("") }
  };
};

const metamaskAccountNotExist = () => {
  return { type: METAMASK_NOT_ENABLED };
};

const metamaskETHBalanceSuccess = value => {
  return { type: ETH_BALANCE_RECEIVE_SUCCESS, payload: value };
};

const metamaskETHBalanceError = () => {
  return { type: ETH_BALANCE_RECEIVE_FAILED, payload: 0 };
};

const metamaskTOKENBalanceSuccess = value => {
  return { type: TOKEN_BALANCE_RECEIVE_SUCCESS, payload: value };
};

const metamaskTOKENBalanceError = () => {
  return { type: TOKEN_BALANCE_RECEIVE_FAILED, payload: 10000 };
};

const allowanceRecieved = () => {
  return { type: ALLOWANCE_RECEIVED, payload: true };
};

const allowanceFailed = () => {
  return { type: ALLOWANCE_FAILED, payload: false };
};

const metamaskUserLoggedInSuccess = () => {
  return { type: METAMASK_USER_LOGGED_IN_SUCCESS };
};

const metamaskUserLoggedInFailed = () => {
  return { type: METAMASK_USER_LOGGED_IN_FAILED };
};

const userIsContractOwner = () => {
  return { type: USER_IS_CONTRACT_OWNER };
};

const userNotContractOwner = () => {
  return { type: USER_NOT_CONTRACT_OWNER };
};

export const hideSimulate = () => {
  return { type: HIDE_SIMULATE };
};

const postSimulateSuccess = payload => {
  return { type: POST_SIMULATE_SUCCESS, payload };
};

const postSimulateFailed = payload => {
  return { type: POST_SIMULATE_FAILED, payload };
};

export const isMetamaskInstalled = () => {
  return !!window.web3
    ? { type: METAMASK_INSTALLED, payload: true }
    : { type: METAMASK_NOT_INSTALLED };
};

export const isMetamaskLoggedIn = () => {
  return async dispatch => {
    window.web3.eth.getAccounts(function(err, accounts) {
      if (err != null || accounts.length === 0) {
        return dispatch(metamaskUserLoggedInFailed());
      }
      return dispatch(metamaskUserLoggedInSuccess());
    });
  };
};

export const enableMetamask = () => {
  return async dispatch => {
    if (!isNewVersion) {
      const acc = window.web3.eth.accounts[0];
      return dispatch(metamaskAccountExist(acc));
    }
    try {
      const acc = await window.ethereum.enable();
      return dispatch(metamaskAccountExist(acc));
    } catch (e) {
      return dispatch(metamaskAccountNotExist());
    }
  };
};

export const getETHBalance = account => {
  return dispatch => {
    window.web3.eth.getBalance(account, (error, result) => {
      if (!error) {
        const res = JSON.stringify(result).replace(/"/g, "");
        const balance = Number(res);
        const ETHBalance = window.web3.fromWei(balance, "ether");
        return dispatch(metamaskETHBalanceSuccess(ETHBalance));
      } else {
        return dispatch(metamaskETHBalanceError);
      }
    });
  };
};

export const getTokenBalance = account => {
  return dispatch => {
    const contractInstance = new Contract(account, TOKEN_CONTRACT);
    contractInstance.balanceOf(account, (error, balance) => {
      if (!error) {
        const res = JSON.stringify(balance).replace(/"/g, "");
        const BalanceD = Number(res);
        const tokenBalance = window.web3.fromWei(BalanceD, "wei");
        return dispatch(metamaskTOKENBalanceSuccess(tokenBalance));
      }
      return dispatch(metamaskTOKENBalanceError(0));
    });
  };
};

export const createContractInstance = account => {
  const tokenContractInstance = new Contract(account, TOKEN_CONTRACT);
  const ioContractInstance = new Contract(account, IO_CONTRACT);
  return {
    type: CONTRACT_INSTANCE_CREATED,
    payload: {
      tokenContractInstance: tokenContractInstance,
      ioContractInstance: ioContractInstance
    }
  };
};

export const checkContractOwner = (contractInstance, account) => {
  return dispatch => {
    return contractInstance.owner.call((err, res) => {
      res == account
        ? dispatch(userIsContractOwner())
        : dispatch(userNotContractOwner());
    });
  };
};

export const checkAllowance = account => {
  const numberForCheck = Math.pow(10, 20);
  const numberAllowance = Math.pow(10, 22);
  const tokenContractInstance = new Contract(account, TOKEN_CONTRACT);
  const ioContractInstance = new Contract(account, IO_CONTRACT);

  return dispatch => {
    return tokenContractInstance.allowance(
      account,
      ioContractInstance.address,
      (err, result) => {
        if (!err) {
          const res = JSON.stringify(result).replace(/"/g, "");
          const Balance = Number(res);
          if (Balance < numberForCheck) {
            return tokenContractInstance.approve(
              ioContractInstance.address,
              numberAllowance,
              (err, result) => {
                if (!err) {
                  return dispatch(allowanceRecieved());
                } else {
                  return dispatch(allowanceFailed());
                }
              }
            );
          } else {
            return dispatch(allowanceRecieved());
          }
        }
        dispatch(allowanceFailed());
      }
    );
  };
};

export const postDataForSimulate = value => {
  return async dispatch => {
    dispatch(fetchInProgress("POST_DATA_SIMULATE"));
    const data = await Request(POST_SIMULATE, value);
    if (!data) {
      dispatch(fetchIsFinished());
      dispatch(postSimulateFailed("FAILED"));
      //TODO ERRORS IMPROVED
      return false;
    }

    if (data.status === 200) {
      dispatch(fetchIsFinished());
      data.json().then(res => {
        return dispatch(postSimulateSuccess(res));
      });
      return true;
    }
    dispatch(fetchIsFinished());
    dispatch(postSimulateFailed("FAILED"));
  };
};
