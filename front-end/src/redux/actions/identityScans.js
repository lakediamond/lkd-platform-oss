import {
  UPGRADE_IDENTITY_SCANS_INFO,
} from "../constants";

export const updateIdentityScansStatus = (order, value) => {
  return { type: UPGRADE_IDENTITY_SCANS_INFO, payload: {order, value} };
};



