import {
  postPhoneNumber,
  postCode,
  getPhoneInfo,
  resendPhoneNumber,
  deletePhoneNumber
} from "./phone";
import { postCommonInfo, getCommonInfo } from "./commonInfo";
import { closeAlert, showAlert } from "./alert";
import {
  signInUser,
  setInvestmentPlan,
  logoutUser,
  confirmConsent,
  get2FAQRCode,
  enable2FA,
  disable2FA
} from "./user";
import { get, webSocketData } from "./get";
import {
  isMetamaskInstalled,
  enableMetamask,
  getETHBalance,
  getTokenBalance,
  createContractInstance,
  checkAllowance,
  isMetamaskLoggedIn,
  checkContractOwner,
  postDataForSimulate,
  hideSimulate
} from "./ether";

// import { postInvestmentPlan } from "./kyc";
import { fetchInProgress, fetchIsFinished } from "./fetch";
import { getComplianceReview, postComplianceReview } from "./complianceReview";
import { checkChallenge, restorePassword, sendEmailForRestore, changePassword } from "./auth";
import { updateIdentityScansStatus } from "./identityScans"

export {
  closeAlert,
  showAlert,
  signInUser,
  logoutUser,
  confirmConsent,
  isMetamaskInstalled,
  isMetamaskLoggedIn,
  enableMetamask,
  getETHBalance,
  getTokenBalance,
  createContractInstance,
  checkContractOwner,
  checkAllowance,
  postPhoneNumber,
  resendPhoneNumber,
  postCode,
  // postInvestmentPlan,
  getPhoneInfo,
  postCommonInfo,
  getCommonInfo,
  setInvestmentPlan,
  fetchInProgress,
  fetchIsFinished,
  getComplianceReview,
  postComplianceReview,
  get2FAQRCode,
  enable2FA,
  disable2FA,
  checkChallenge,
  restorePassword,
  changePassword,
  sendEmailForRestore,
  get,
  webSocketData,
  hideSimulate,
  postDataForSimulate,
  updateIdentityScansStatus,
  deletePhoneNumber
};
