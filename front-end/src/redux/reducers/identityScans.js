import {
  GET_IDENTITY_SCANS_FAILED,
  GET_IDENTITY_SCANS_SUCCESS,
  UPGRADE_IDENTITY_SCANS_INFO
} from "../constants";

const defaultState = null;

export default (state = defaultState, action) => {
  switch (action.type) {
    case GET_IDENTITY_SCANS_FAILED:
      return defaultState;
    case GET_IDENTITY_SCANS_SUCCESS:
      return action.payload;
    case UPGRADE_IDENTITY_SCANS_INFO:
      return {
        ...state,
        state: state["content_items"][action.payload.order]["status"] =
          action.payload.value
      };
    default:
      return state;
  }
};
