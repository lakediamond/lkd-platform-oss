import {
  POST_SIMULATE_SUCCESS,
  HIDE_SIMULATE, POST_SIMULATE_FAILED,
} from "../constants";

const defaultState = {
  showSimulate: null,
  data: null,
  error: null
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case POST_SIMULATE_SUCCESS:
      return { ...state, showSimulate: true, data: action.payload };
    case POST_SIMULATE_FAILED:
      return { ...state, showSimulate: false, data: defaultState['data'], error: action.payload };
    case HIDE_SIMULATE:
      return { ...state, showSimulate: false, data: defaultState['data'], error: defaultState['error'] };
    default:
      return state;
  }
};

