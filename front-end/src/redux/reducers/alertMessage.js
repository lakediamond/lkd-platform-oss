const defaultState = {
  open_alert: false,
  alert_message: ""
};

const alertMessage = (state = defaultState, action) => {
  switch (action.type) {
    case "CLOSE_ALERT":
      return { ...state, open_alert: false };
    case "SHOW_ALERT":
      return { ...state, open_alert: true, alert_message: action.message };
    default:
      return state;
  }
};

export default alertMessage;
