import {
  SEND_EMAIL_FOR_RESTORE_SUCCESS,
  SEND_EMAIL_FOR_RESTORE_FAILED,
  CHECK_CHALLENGE_SUCCESS,
  CHECK_CHALLENGE_FAILED,
  RESTORE_PASSWORD_SUCCESS,
  RESTORE_PASSWORD_FAILED,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILED
} from "../constants";

const defaultState = {
  challenge_is_valid: null,
  email_for_restore_send: null,
  password_changed: null
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case SEND_EMAIL_FOR_RESTORE_SUCCESS:
      return {...state, email_for_restore_send: true};
    case SEND_EMAIL_FOR_RESTORE_FAILED:
      return {...state, email_for_restore_send: false};
    case CHECK_CHALLENGE_SUCCESS:
      return {...state, challenge_is_valid: true};
    case CHECK_CHALLENGE_FAILED:
      return {...state, challenge_is_valid: false};
    case RESTORE_PASSWORD_SUCCESS:
      return {...state, password_changed: true};
    case RESTORE_PASSWORD_FAILED:
      return {...state, password_changed: false};
    case CHANGE_PASSWORD_SUCCESS:
      return {...state, password_changed: true};
    case CHANGE_PASSWORD_FAILED:
      return {...state, password_changed: false};
    default:
      return state;
  }
}