import { combineReducers } from 'redux';
import ordersList from './ordersList';
import proposalsList from './proposalsList';
import subOwnersList from "./subOwners";
import alertMessage from './alertMessage';
import user from './user';
import metaMask from './metaMask';
import simulatedData from './simulatedData';
import phone from './phone';
import commonInfo from './commonInfo';
import tierInfo from "./kyc";
import fetch from "./fetch";
import complianceReview from "./complianceReview";
import auth from "./auth";
import identityScans from "./identityScans";

const tokensiteApp = combineReducers({
    ordersList,
    proposalsList,
    subOwnersList,
    alertMessage,
    user,
    metaMask,
    phone,
    commonInfo,
    tierInfo,
    complianceReview,
    auth,
    fetch,
    simulatedData,
    identityScans
});

export default tokensiteApp;
