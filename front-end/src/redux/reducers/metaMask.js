import {
  METAMASK_INSTALLED,
  METAMASK_NOT_INSTALLED,
  METAMASK_USER_LOGGED_IN_SUCCESS,
  METAMASK_USER_LOGGED_IN_FAILED,
  METAMASK_ENABLED,
  METAMASK_NOT_ENABLED,
  USER_IS_CONTRACT_OWNER,
  USER_NOT_CONTRACT_OWNER,
  ETH_BALANCE_RECEIVE_SUCCESS,
  ETH_BALANCE_RECEIVE_FAILED,
  TOKEN_BALANCE_RECEIVE_SUCCESS,
  TOKEN_BALANCE_RECEIVE_FAILED,
  CONTRACT_INSTANCE_CREATED,
  ALLOWANCE_RECEIVED,
  ALLOWANCE_FAILED,
  SHOW_SIMULATE,
  HIDE_SIMULATE
} from "../constants";

const defaultState = {
  isInstalled: null,
  isLoggedIn: null,
  metaMaskAccount: null,
  isContractOwner: null,
  ethBalance: null,
  tokenBalance: null,
  tokenContractInstance: null,
  ioContractInstance: null,
  storageContractInstance: null,
  allowance: null,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case METAMASK_INSTALLED:
      return { ...state, isInstalled: action.payload };
    case METAMASK_NOT_INSTALLED:
      return { ...state, isInstalled: false };
    case METAMASK_USER_LOGGED_IN_FAILED:
      return { ...state, isLoggedIn: false };
    case METAMASK_USER_LOGGED_IN_SUCCESS:
      return { ...state, isLoggedIn: true };
    case METAMASK_ENABLED:
      return {
        ...state,
        isEnabled: action.payload["isEnabled"],
        metaMaskAccount: action.payload["account"],
        isLoggedIn: true
      };
    case USER_IS_CONTRACT_OWNER:
      return { ...state, isContractOwner: true };
    case USER_NOT_CONTRACT_OWNER:
      return { ...state, isContractOwner: false };
    case METAMASK_NOT_ENABLED:
      return { ...state, isEnabled: false, metaMaskAccount: "" };
    case ETH_BALANCE_RECEIVE_SUCCESS:
      return { ...state, ethBalance: action.payload };
    case ETH_BALANCE_RECEIVE_FAILED:
      return { ...state, ethBalance: 0 };
    case TOKEN_BALANCE_RECEIVE_SUCCESS:
      return { ...state, tokenBalance: action.payload };
    case TOKEN_BALANCE_RECEIVE_FAILED:
      return { ...state, tokenBalance: 0 };
    case CONTRACT_INSTANCE_CREATED:
      return {
        ...state,
        tokenContractInstance: action.payload["tokenContractInstance"],
        ioContractInstance: action.payload["ioContractInstance"],
        storageContractInstance: action.payload["storageContractInstance"]
      };
    case ALLOWANCE_RECEIVED:
      return { ...state, allowance: action.payload };
    case ALLOWANCE_FAILED:
      return { ...state, allowance: action.payload };
    default:
      return state;
  }
};
