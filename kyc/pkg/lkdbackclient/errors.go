package lkdbackclient

import (
	"encoding/json"
	"fmt"
	"strconv"
)

// LkdBackClientErrors implements error interface for LKD Backend errors
type LkdBackClientErrors struct {
	Code    int    `json:"error"`
	Message string `json:"message"`
}

func (e *LkdBackClientErrors) Error() string {
	return fmt.Sprintf("LKD Backend Error! Code: %+v. Message: %+v", e.Code, e.Message)
}

func (e LkdBackClientErrors) MarshalJSON() ([]byte, error) {
	errors := make(map[string]string)
	errors["Error"] = strconv.Itoa(e.Code)
	errors["Message"] = e.Error()
	return json.Marshal(errors)
}
