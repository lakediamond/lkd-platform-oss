package lkdbackclient

import (
	"kyc-service/pkg/btcsclient"
	"net/http"
)

type LKDBackClient interface {
	NotifyBTCSFlowStarted(customerID string) error
	SubmitIDProcessingError(message, customerID string) error
	SubmitSuccessImageValidation(userID, imageType string) error
	SubmitIDVerificationResults(results *btcsclient.BtcsScansEventIDVerificationsResult, customerID string) error
	do(req *http.Request, target interface{}) (*http.Response, error)
	makeRequest(method, path string, body interface{}, query map[string]string) (*http.Request, error)
}
