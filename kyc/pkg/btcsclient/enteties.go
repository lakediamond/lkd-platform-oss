package btcsclient

import "time"

type CreateCustomerRequest struct {
	FirstName      string   `json:"first_name"`
	LastName       string   `json:"last_name"`
	Email          string   `json:"contact_info"`
	Phone          []string `json:"phone"`
	Dob            string   `json:"dob"`
	Organizations  []string `json:"organizations"`
	PrimaryContact string   `json:"primary_contact"`
	LookupKeys     []string `json:"lookup_keys"`
}

type UpdateCustomerRequest struct {
	FirstName      string   `json:"first_name,omitempty"`
	LastName       string   `json:"last_name,omitempty"`
	Email          string   `json:"contact_info,omitempty"`
	Phone          []string `json:"phone,omitempty"`
	PostalCode     string   `json:"postal_code,omitempty"`
	Country        string   `json:"country,omitempty"`
	State          string   `json:"state,omitempty"`
	Address1       string   `json:"address1,omitempty"`
	Dob            string   `json:"dob,omitempty"`
	Address2       string   `json:"address2,omitempty"`
	Organizations  []string `json:"organizations,omitempty"`
	PrimaryContact string   `json:"primary_contact,omitempty"`
	CustomFlag1    bool     `json:"custom_flag1,omitempty"`
	CustomFlag2    bool     `json:"custom_flag2,omitempty"`
	CustomFlag3    bool     `json:"custom_flag3,omitempty"`
	CustomFlag4    bool     `json:"custom_flag4,omitempty"`
	CustomFlag5    bool     `json:"custom_flag5,omitempty"`
	CustomFlag6    bool     `json:"custom_flag6,omitempty"`
	Tags           []string `json:"tags"`
}

// UploadIDScansRequest represents request to submit scans to btcs
type UploadIDScansRequest struct {
	FaceImage   string `json:"face_image,omitempty"`
	FrontImage  string `json:"front_image,omitempty"`
	BackImage   string `json:"back_image,omitempty"`
	CallBackURL string `json:"callback_url"`
}

type GetCustomerResponse struct {
	Address1       string   `json:"address1"`
	Address2       string   `json:"address2,omitempty"`
	AllowVerify    bool     `json:"-"`
	BlockedByOrgs  []string `json:"-"`
	CandidateID    string   `json:"-"`
	City           string   `json:"city"`
	Country        string   `json:"country"`
	DateCreated    string   `json:"date_created"`
	DateUpdated    string   `json:"date_updated"`
	Dob            string   `json:"dob"`
	FirstName      string   `json:"first_name"`
	ForReview      bool     `json:"-"`
	ID             int      `json:"id"`
	IDExpiry       string   `json:"-"`
	IDNumber       string   `json:"id_number"`
	IDType         string   `json:"id_type"`
	IsDeleted      bool     `json:"-"`
	LastName       string   `json:"last_name"`
	LookupKeys     []string `json:"lookup_keys"`
	Organizations  []string `json:"organizations"`
	PersonID       string   `json:"-"`
	Phone          []string `json:"phone"`
	PlaceID        string   `json:"-"`
	PostalCode     string   `json:"postal_code"`
	PrimaryContact string   `json:"primary_contact"`
	ScanID         string   `json:"scan_id"`
	Ssn            string   `json:"-"`
	State          string   `json:"state"`
	Status         string   `json:"status"`
	Tags           []string `json:"tags"`
}

type CustomerLevels struct {
	BitmaskLevel int      `json:"bitmask_level"`
	BitmaskStr   string   `json:"bitmask_str"`
	CheckedFlags []string `json:"checked_flags"`
}

type CustomerLimits struct {
	AllowVerify     bool     `json:"allow_verify"`
	CheckedFlags    []string `json:"checked_flags"`
	CustomerBitmask int      `json:"customer_bitmask"`
	KycLevel        struct {
		Bitmask int    `json:"bitmask"`
		Name    string `json:"name"`
	} `json:"kyc_level"`
	Limits struct {
		BuyCurrent    float64 `json:"buy_current"`
		BuyDaily      float64 `json:"buy_daily"`
		BuyLifetime   float64 `json:"buy_lifetime"`
		BuyMonthly    float64 `json:"buy_monthly"`
		BuyPerTxn     float64 `json:"buy_per_txn"`
		BuyWeekly     float64 `json:"buy_weekly"`
		BuyYearly     float64 `json:"buy_yearly"`
		NoOfTxnsDaily float64 `json:"no_of_txns_daily"`
		SellCurrent   float64 `json:"sell_current"`
		SellDaily     float64 `json:"sell_daily"`
		SellLifetime  float64 `json:"sell_lifetime"`
		SellMonthly   float64 `json:"sell_monthly"`
		SellPerTxn    float64 `json:"sell_per_txn"`
		SellWeekly    float64 `json:"sell_weekly"`
		SellYearly    float64 `json:"sell_yearly"`
		TotalDaily    float64 `json:"total_daily"`
		TotalLifetime float64 `json:"total_lifetime"`
		TotalMonthly  float64 `json:"total_monthly"`
		TotalWeekly   float64 `json:"total_weekly"`
		TotalYearly   float64 `json:"total_yearly"`
	} `json:"limits"`
	ProjectedLevel string `json:"projected_level"`
}

type OrganizationLevels struct {
	Count  int `json:"count"`
	Levels []struct {
		Bitmask       int      `json:"bitmask"`
		BitmaskStr    string   `json:"bitmask_str"`
		BuyDaily      float64  `json:"buy_daily"`
		BuyLifetime   float64  `json:"buy_lifetime"`
		BuyMonthly    float64  `json:"buy_monthly"`
		BuyPerTxn     float64  `json:"buy_per_txn"`
		BuyWeekly     float64  `json:"buy_weekly"`
		BuyYearly     float64  `json:"buy_yearly"`
		CheckedFlags  []string `json:"checked_flags"`
		ID            int64    `json:"id"`
		Name          string   `json:"name"`
		NoOfTxnsDaily int      `json:"no_of_txns_daily"`
		SellDaily     float64  `json:"sell_daily"`
		SellLifetime  float64  `json:"sell_lifetime"`
		SellMonthly   float64  `json:"sell_monthly"`
		SellPerTxn    float64  `json:"sell_per_txn"`
		SellWeekly    float64  `json:"sell_weekly"`
		SellYearly    float64  `json:"sell_yearly"`
		TotalDaily    float64  `json:"total_daily"`
		TotalLifetime float64  `json:"total_lifetime"`
		TotalMonthly  float64  `json:"total_monthly"`
		TotalWeekly   float64  `json:"total_weekly"`
		TotalYearly   float64  `json:"total_yearly"`
	} `json:"levels"`
}

type ScanAccessResponse struct {
	BackFilename  string `json:"back_filename"`
	BackURL       string `json:"back_url"`
	FaceFilename  string `json:"face_filename"`
	FaceURL       string `json:"face_url"`
	FrontFilename string `json:"front_filename"`
	FrontURL      string `json:"front_url"`
}

type SubmitAddress struct {
	Address     string `json:"address"`
	CallbackURL string `json:"callback_url"`
}

var KYCFlags = map[string]int{
	"ACTIVE":                         10,
	"BLOCKED":                        11,
	"CUSTOM_FLAG1":                   31,
	"CUSTOM_FLAG2":                   32,
	"CUSTOM_FLAG3":                   33,
	"CUSTOM_FLAG4":                   34,
	"CUSTOM_FLAG5":                   35,
	"CUSTOM_FLAG6":                   36,
	"SUBMITTED_NAME":                 5,
	"SUBMITTED_PHONE":                13,
	"SUBMITTED_EMAIL":                14,
	"SUBMITTED_ADDRESS":              6,
	"SUBMITTED_DOB":                  15,
	"SUBMITTED_TID":                  16,
	"SUBMITTED_ZIP":                  17,
	"SUBMITTED_ID":                   12,
	"SUBMITTED_FACE":                 18,
	"VERIFIED_PHONE_SMS":             19,
	"VERIFIED_PHONE_MOBILE":          3,
	"VERIFIED_NAME_PHONE":            4,
	"VERIFIED_EMAIL":                 20,
	"VERIFIED_NAME_SANCTIONS_SCREEN": 7,
	"VERIFIED_NAME_ID":               22,
	"VERIFIED_ADDRESS_ID":            23,
	"VERIFIED_ADDRESS_DOCUMENT":      24,
	"VERIFIED_DOB_ID":                25,
	"VERIFIED_ID":                    8,
	"VERIFIED_ID_WITH_FACE":          9,
	"FAILED_SANCTIONS":               28,
	"SUBMITTED_SSN":                  29,
	"VERIFIED_SSN":                   30,
}

// BtcsScansEventIDVerificationsResult struct that represents ID verification response (BTCSIDVerificationEventsResponse) results
type BtcsScansEventIDVerificationsResult struct {
	Duplicates                  []uint64  `json:"duplicates"`
	CallBackType                string    `json:"callBackType"`
	CallbackDate                time.Time `json:"callbackDate"`
	CallbackURL                 string    `json:"callback_url"`
	FirstAttemptDate            time.Time `json:"firstAttemptDate"`
	IDCheckDataPositions        string    `json:"idCheckDataPositions"`
	IDCheckDocumentValidation   string    `json:"idCheckDocumentValidation"`
	IDCheckHologram             string    `json:"idCheckHologram"`
	IDCheckMRZcode              string    `json:"idCheckMRZcode"`
	IDCheckMicroprint           string    `json:"idCheckMicroprint"`
	IDCheckSecurityFeatures     string    `json:"idCheckSecurityFeatures"`
	IDCheckSignature            string    `json:"idCheckSignature"`
	IDCountry                   string    `json:"idCountry"`
	IDDob                       string    `json:"idDob"`
	IDExpiry                    string    `json:"idExpiry"`
	IDFirstName                 string    `json:"idFirstName"`
	IDLastName                  string    `json:"idLastName"`
	IDNumber                    string    `json:"idNumber"`
	IDScanImage                 string    `json:"idScanImage"`
	IDScanImageFace             string    `json:"idScanImageFace"`
	IDScanSource                string    `json:"idScanSource"`
	IDScanStatus                string    `json:"idScanStatus"`
	IDType                      string    `json:"idType"`
	IdentityVerification        string    `json:"identityVerification"`
	JumioIDScanReference        string    `json:"jumioIdScanReference"`
	MerchantIDScanReference     string    `json:"merchantIdScanReference"`
	PersonalNumber              string    `json:"personalNumber"`
	TransactionDate             time.Time `json:"transactionDate"`
	VerificationStatus          string    `json:"verificationStatus"`
	VerifiedNameSanctionsScreen struct {
		Matches []interface{} `json:"matches"`
	} `json:"verified_name_sanctions_screen"`
}

// BTCSIDVerificationEventsResponse struct that represents ID verification respons
type BTCSIDVerificationEventsResponse struct {
	Count           int    `json:"count"`
	Cursor          string `json:"cursor"`
	IDVerifications []struct {
		CustomerID    int64                               `json:"customer_id"`
		DateCompleted string                              `json:"date_completed"`
		DateSubmitted string                              `json:"date_submitted"`
		ID            int64                               `json:"id"`
		RefID         string                              `json:"ref_id"`
		Remarks       string                              `json:"remarks"`
		Result        BtcsScansEventIDVerificationsResult `json:"result"`
		Status        string                              `json:"status"`
		VerifiedBy    string                              `json:"verified_by"`
	} `json:"id_verifications"`
	More bool `json:"more"`
}

type CustomerTraces struct {
	Cursor string `json:"cursor"`
	More   bool   `json:"more"`
	Traces []struct {
		Action      string      `json:"action"`
		DateCreated string      `json:"date_created"`
		ID          int64       `json:"id"`
		Kind        string      `json:"kind"`
		OldData     interface{} `json:"old_data"`
		PerformedBy string      `json:"performed_by"`
		RawData     struct {
			Duplicates []uint64      `json:"duplicates"`
			Matches    []interface{} `json:"matches"`
		} `json:"raw_data"`
		Remarks string   `json:"remarks"`
		Tag     []string `json:"tag"`
	} `json:"traces"`
}
