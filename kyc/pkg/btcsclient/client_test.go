package btcsclient_test

import (
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/testutils"
	"kyc-service/internal/pkg/utils"
	"kyc-service/pkg/btcsclient"
	"net/http/httptest"
	"testing"

	// "github.com/stretchr/testify/assert"
	_ "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type BtcsClientTestSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
	BTCSClient btcsclient.BTCSClient
}

func TestCheckIdentitySuite(t *testing.T) {
	suite.Run(t, new(BtcsClientTestSuite))
}

func (suite *BtcsClientTestSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	suite.App = app
	suite.MockServer = mockServer
	suite.BTCSClient = utils.NewBTCSClient()
}

func (suite *BtcsClientTestSuite) TestGetTraces() {
	resp, err := suite.BTCSClient.GetCustomerTaces("5630286201094144")
	suite.Nil(err)
	suite.NotNil(resp.Traces[0].RawData.Matches)
	suite.Equal(resp.Traces[0].Remarks, "VERIFIED_NAME_SANCTIONS_SCREEN set to False")
	matches := btcsclient.FindSanctionsLogsInCustomerTraces(resp)
	suite.NotNil(matches)
}
