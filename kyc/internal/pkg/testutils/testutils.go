package testutils

import (
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"os"
	"path/filepath"

	"kyc-service/internal/app/service/httpserver"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo/models"

	"gopkg.in/DataDog/dd-trace-go.v1/contrib/julienschmidt/httprouter"
)

// NewHTTPMock return mocked application, router and server for tests
func NewHTTPMock() (*application.Application, *httprouter.Router, *httptest.Server) {
	app := application.NewKycService()
	router := httpserver.NewHTTPRouter(app)
	mockServer := httptest.NewServer(router)
	return app, router, mockServer
}

func FindTestDataPath() (string, error) {
	fixtures_dir := "test_data"
	path, err := os.Getwd()
	// var fixturesFolder []os.FileInfo
	if err != nil {
		return "", err
	}
	for {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			return "", err
		}
		for _, f := range files {
			if f.IsDir() && f.Name() == fixtures_dir {
				// fixturesFolder, err = ioutil.ReadDir(path)
				// if err != nil {
				// 	return "", err
				// }
				fp := filepath.Join(path, "test_data")
				return fp, nil
			}
		}
		path = filepath.Dir(path)
	}
}

func UploadFixtures(app *application.Application, fixturesFilename string) error {
	path, err := FindTestDataPath()
	if err != nil {
		return err
	}
	fp := filepath.Join(path, fixturesFilename)
	jsonFile, err := os.Open(fp)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	customer := new(models.Customer)
	err = json.NewDecoder(jsonFile).Decode(customer)
	if err != nil {
		panic(err)
	}
	err = app.Repo.Customer.CreateNewCustomer(customer)
	if err != nil {
		panic(err)
	}
	return nil
}
