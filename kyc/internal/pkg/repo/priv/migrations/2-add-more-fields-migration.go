package migrations

import (
	"kyc-service/internal/pkg/repo/models"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddImagesUploadedFieldsToCustomerModel = gormigrate.Migration{
	ID: "536d6d73-34fb-4750-9922-ec8f66dcd8fc",
	Migrate: func(tx *gorm.DB) error {
		error := tx.AutoMigrate(&models.Customer{}).Error
		if error != nil {
			return error
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers 
		DROP COLUMN face_uploaded, 
		DROP COLUMN front_uploaded, 
		DROP COLUMN back_uploaded, 
		DROP COLUMN submited_at;`
		return tx.Exec(req).Error
	},
}
