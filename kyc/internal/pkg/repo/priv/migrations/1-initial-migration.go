package migrations

import (
	"kyc-service/internal/pkg/repo/models"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var CreateCustomerTable = gormigrate.Migration{
	ID: "7aa0c5e1-dbbe-4f4e-bd53-e208984e48ac",
	Migrate: func(tx *gorm.DB) error {
		error := tx.AutoMigrate(&models.Customer{}).Error
		if error != nil {
			return error
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("customers").Error
	},
}
