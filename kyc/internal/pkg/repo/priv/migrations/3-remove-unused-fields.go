package migrations

import (
	"kyc-service/internal/pkg/repo/models"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var RemoveUnusedFieldsMigration = gormigrate.Migration{
	ID: "f0fd733b-c02b-4fe6-a8ec-a49c575e7d04",
	Migrate: func(tx *gorm.DB) error {
		err := tx.Model(&models.Customer{}).DropColumn("passed_all_tiers").Error
		if err != nil {

		}
		err = tx.Model(&models.Customer{}).DropColumn("bit_mask_level").Error
		if err != nil {

		}
		err = tx.Model(&models.Customer{}).DropColumn("bit_mask").Error
		if err != nil {

		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		return nil
	},
}
