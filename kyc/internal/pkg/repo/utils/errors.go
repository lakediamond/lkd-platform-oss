package utils

import (
	"encoding/json"
	"fmt"
)

// DuplicateConstraintError is the struct that represents duplicate constraint error in db
type DuplicateConstraintError struct {
	Table string
	Field string
}

// DoesNotExistsError is the struct that represents doesnotexists error in db
type DoesNotExistsError struct {
	Table string
	Field string
}

// NotNullConstrainError is the struct that represents not-null constraint error in db
type NotNullConstrainError struct {
	Table string
	Field string
}

func (e *NotNullConstrainError) Error() string {
	return fmt.Sprintf("'%v' in '%v' can't be nil", e.Field, e.Table)
}

func (e *DuplicateConstraintError) Error() string {
	return fmt.Sprintf("'%v' in '%v' already exists", e.Field, e.Table)
}

func (e *DoesNotExistsError) Error() string {
	return fmt.Sprintf("'%v' in '%v' does not exists", e.Field, e.Table)
}

func (e DoesNotExistsError) MarshalJSON() ([]byte, error) {
	errors := make(map[string]string)
	errors["Error"] = "DoesNotExists"
	errors["Message"] = e.Error()
	return json.Marshal(errors)
}

func (e DuplicateConstraintError) MarshalJSON() ([]byte, error) {
	errors := make(map[string]string)
	errors["Error"] = "DuplicateConstraint"
	errors["Message"] = e.Error()
	return json.Marshal(errors)
}

func (e NotNullConstrainError) MarshalJSON() ([]byte, error) {
	errors := make(map[string]string)
	errors["Error"] = "NotNullConstraint"
	errors["Message"] = e.Error()
	return json.Marshal(errors)
}
