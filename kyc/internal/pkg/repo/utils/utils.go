package utils

import "github.com/lib/pq"

// IsUniqueConstraintError defines does provided error is Postgreses unique constraint error
func IsUniqueConstraintError(err error) bool {
	if pqErr, ok := err.(*pq.Error); ok {
		return pqErr.Code == "23505"
	}
	return false
}

// IsNotNullConstraintError defines does provided error is Postgreses not-nul constraint error
func IsNotNullConstraintError(err error) bool {
	if pqErr, ok := err.(*pq.Error); ok {
		return pqErr.Code == "23502"
	}
	return false
}
