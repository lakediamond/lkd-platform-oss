package models

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

type Customer struct {
	ID                           string `gorm:"primary_key"`
	CreatedAt                    time.Time
	UpdatedAt                    time.Time
	KycID                        string `gorm:"unique"`
	FaceUploaded                 bool
	FrontUploaded                bool
	BackUploaded                 bool
	FaceFormat                   string
	FrontFormat                  string
	BackFormat                   string
	SubmitedAt                   *time.Time
	ScansCheckResultReceived     bool
	ScansCheckResultReceivedAt   *time.Time
	ScansCheckResult             string
	ScansCheckSubmitedAt         *time.Time
	AddressCheckResultReceived   bool
	AddressCheckResultReceivedAt *time.Time
	AddressCheckResult           string
}

func (Customer) IsValidMediaType(token string) bool {
	for _, item := range []MediaType{FrontImage, BackImage, FaceImage} {
		if item == token {
			return true
		}
	}

	return false
}

func (Customer) GetMediaTypeDBFlagFieldName(mediaType MediaType) (string, error) {
	if len(strings.Split(mediaType, "_")) != 2 {
		return "", errors.New("Invalid media type")
	}
	mediaTypePrefix := strings.Split(mediaType, "_")[0]
	return fmt.Sprintf("%s_uploaded", mediaTypePrefix), nil
}

type MediaType = string

const (
	UniqueConstraintKycID = "customers_kyc_id_key"
	UniqueConstraintID    = "customers_pkey"

	FrontImage MediaType = "front_image"
	BackImage  MediaType = "back_image"
	FaceImage  MediaType = "face_image"
)

