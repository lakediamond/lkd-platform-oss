package context

import (
	"kyc-service/internal/pkg/application"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

//GetApplicationContext returns application from http request
func GetApplicationContext(r *http.Request) *application.Application {
	return r.Context().Value(application.ApplicationKey).(*application.Application)
}

//GetRequestParams returns params from http request context
func GetRequestParams(r *http.Request) httprouter.Params {
	return httprouter.ParamsFromContext(r.Context())
}

//HandlingContext contains needed handling context
type HandlingContext struct {
	Configuration *application.Application
}
