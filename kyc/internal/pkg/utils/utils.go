package utils

import (
	"context"
	"fmt"
	"kyc-service/pkg/btcsclient"
	"kyc-service/pkg/env"
	"kyc-service/pkg/lkdbackclient"
	"log"
	"strconv"
	"strings"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

type Storage struct {
	gcpProjectID           string
	gcpServiceAccountEmail string
	kycBucket              *storage.BucketHandle
	kycBucketName          string
	Client                 *storage.Client
}

func NewStorage() *Storage {
	myEnv := env.GetEnv()
	ctx := context.Background()

	creds, err := google.FindDefaultCredentials(ctx, storage.ScopeReadOnly)
	if err != nil {
		log.Fatal(err)
	}
	client, err := storage.NewClient(ctx, option.WithCredentials(creds))
	if err != nil {
		log.Fatal(err)
	}

	bucket := client.Bucket(myEnv["SCANS_BUCKET_NAME"])
	return &Storage{
		gcpProjectID:           myEnv["GCP_PROJECT_ID_FULL"],
		gcpServiceAccountEmail: strings.Split(myEnv["GCP_PROJECT_ID_FULL"], "/")[3],
		kycBucket:              bucket,
		kycBucketName:          myEnv["SCANS_BUCKET_NAME"],
		Client:                 client,
	}
}

func (s *Storage) GetImageMetainfo(userID, imageName string) (*storage.ObjectAttrs, error) {
	query := &storage.Query{
		Prefix:    fmt.Sprintf("%s/identity-scans/", userID),
		Delimiter: "/"}
	it := s.kycBucket.Objects(context.Background(), query)

	for {
		imageAttributes, err := it.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}
			return nil, err
		}
		log.Print(imageAttributes.Name)

		if imageAttributes.Name == imageName || strings.Split(imageAttributes.Name, ".")[0] == imageName {
			return imageAttributes, nil
		}
	}
	return nil, nil
}

func (s *Storage) GetImageReader(userID, imageName string) (*storage.Reader, error) {
	log.Println("image name - ", imageName)
	log.Println(fmt.Sprintf("%s/identity-scans/%s", userID, imageName))
	objectHandle := s.kycBucket.Object(imageName)
	return objectHandle.NewReader(context.Background())
}

func NextBtcsPhoneNumber(customersNb int64) []string {
	myEnv := env.GetEnv()
	template := []rune(myEnv["KYC_CUSTOMER_PHONE_NUMBER_TEMPLATE"])
	btcsCustomerPhoneSuffix := strconv.FormatInt(customersNb, 10)
	template = template[:len(template)-len(btcsCustomerPhoneSuffix)]
	for _, ch := range btcsCustomerPhoneSuffix {
		template = append(template, ch)
	}
	return []string{string(template)}
}

// NewBTCSClient returns instance of configured with env variables BTCSClient
func NewBTCSClient() btcsclient.BTCSClient {
	myEnv := env.GetEnv()

	baseURL, _ := myEnv["KYC_API_URL"]
	orgName := myEnv["KYC_ORGANIZATION"]
	btcsAPIKey := myEnv["KYC_API_KEY"]
	btcsAPISecret := myEnv["KYC_API_SECRET"]

	return btcsclient.NewBTCSClient(baseURL, orgName, btcsAPIKey, btcsAPISecret)
}

// NewLKDBackClient is the fabrick thet returns structure to implement LKDBackClient interface
func NewLKDBackClient() lkdbackclient.LKDBackClient {
	myEnv := env.GetEnv()
	baseURL := myEnv["LKD_BACK_URL"]
	return lkdbackclient.NewLKDBackCLient(baseURL)
}
