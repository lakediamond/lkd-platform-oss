package events

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/rs/zerolog/log"

	customer_service "kyc-service/internal/app/service/customer"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo/models"
	"kyc-service/pkg/btcsclient"
)

// UpdateFileFlagBasedOnEvent parses event and set curresponding FileUploaded flag
func UpdateFileFlagBasedOnEvent(app *application.Application, event *S3EventNotification) error {
	customer, err := FindCustomerS3EventBelongsTo(app, event)
	if err != nil {
		return err
	}
	err = SetCustomerFileUploadedFalgBasedOnEvent(app, customer, event)
	if err != nil {
		return err
	}
	if app.Repo.Customer.DoesAllImageFlagsAreSet(customer) {
		go customer_service.SubmitCustomerIDScansFromS3Service(app, customer.ID)
		go app.LKDBackClient.NotifyBTCSFlowStarted(customer.ID)
	}

	return err
}

// FindCustomerS3EventBelongsTo function pars S3 events Key value to find existing customer in DB based on file path
func FindCustomerS3EventBelongsTo(app *application.Application, event *S3EventNotification) (*models.Customer, error) {
	i := strings.Split(event.Key, "/")
	customerID := i[1]
	customer, err := app.Repo.Customer.GetCustomerByID(customerID)
	if err != nil {
		log.Error().Msgf("Parsing S3 event error when setting finding correspondet user. Error: %+v. customerID: %+v", err, customerID)
		return nil, err
	}
	return customer, nil
}

// SetCustomerFileUploadedFalgBasedOnEvent function parses S3 events Key's file name to set curresponding flag
func SetCustomerFileUploadedFalgBasedOnEvent(app *application.Application, customer *models.Customer, event *S3EventNotification) error {
	i := strings.Split(event.Key, "/")
	uploadedFile := i[3]
	tmp := strings.Split(uploadedFile, ".")
	fileName := tmp[0]
	log.Print("fileName: ", fileName)
	fileFormat := tmp[1]
	log.Print("fileFormat: ", fileFormat)
	var err error

	// TODO: refactoring required (atomic transactions, functions abstraction)
	switch fileName {
	case "face_image":
		err := checkFaceImageValidity(app, customer, fileFormat)
		if err != nil {
			log.Error().Msgf("checkFaceImageValidity error, %s", err)
		}
	case "back_image":
		err := checkBackImageValidity(app, customer, fileFormat)
		if err != nil {
			log.Error().Msgf("checkFaceImageValidity error, %s", err)
		}
	case "front_image":
		err := checkFrontImageValidity(app, customer, fileFormat)
		if err != nil {
			log.Error().Msgf("checkFrontImageValidity error, %s", err)
		}

	default:
		log.Print("find: ", fileName)
	}
	if err != nil {
		log.Error().Msgf("Parsing BTCS event error when setting image flag. Error: %+v. Event: %+v", err, event)
		return err
	}
	return nil
}

// FindCustomerBtcsScanEventBelongsTo function pars BTCS scans events MerchantIDScanReference value to find existing customer in DB based on file path
func FindCustomerBtcsScanEventBelongsTo(app *application.Application, event *BtcsScansEvent) (*models.Customer, error) {
	customerID := strconv.FormatUint(event.CustomerID, 10)
	customer, err := app.Repo.Customer.GetCustomerByKycID(customerID)
	if err != nil {
		log.Error().Msgf("Parsing BTCS event error when finding correspponding user. Error: %+v. Event: %+v", err, event)
		return nil, err
	}
	return customer, nil
}

// ParseBtcsScansEvent parses BtcsScansEvent and sets correspinding flags for corresponding customer
func ParseBtcsScansEvent(app *application.Application, event *BtcsScansEvent) error {
	customer, err := FindCustomerBtcsScanEventBelongsTo(app, event)
	if err != nil {
		log.Error().Msgf("Parsing BTCS event Error: %+v. Event: %+v", err, event)
		return err
	}
	status := event.Status
	if event.Details == "FACE_FAIL" {
		status = "REJECT"
	}
	err = app.Repo.Customer.SetScansCheck(status, customer)
	if err != nil {
		log.Error().Msgf("Parsing BTCS event error when set DB flags. Error: %+v. Event: %+v", err, event)
		return err
	}
	log.Info().Msgf("GORUTINE TO NOTIFY LKD BACK STARTING OK for customer ID %v", customer.ID)
	go NotifyLKDBackednAboutIDVereficationEvent(app, customer, event)
	return nil
}

// NotifyLKDBackednAboutIDVereficationEvent fetch verefication id traces and push latest to LKD backend
func NotifyLKDBackednAboutIDVereficationEvent(app *application.Application, customer *models.Customer, event *BtcsScansEvent) {
	// lastVereficationResult := btcsclient.BtcsScansEventIDVerificationsResult{}
	resp, err := app.BTCSClient.GetIDVerificationTraceLogs(customer.KycID)
	if err != nil {
		log.Error().Msgf("Notify LKD back error fetching ID verefication logs: %+v", err)
		panic(err)
	}
	lastVereficationResult := resp.IDVerifications[0].Result

	// if len(lastVereficationResult.Duplicates) != 0 {
	// log.Info().Msgf("original customer id - %s", strconv.FormatUint(lastVereficationResult.Duplicates[0], 10))

	if event.Details == "ERROR_DUPLICATE_ID" {
		err = GetDuplicateDetails(app, customer.KycID, &lastVereficationResult)
		if err != nil {
			log.Error().Msgf("Notify LKD back error when inserting duplicate details to request: %+v", err)
			panic(err)
		}

		log.Info().Msgf("Duplicates nb - %d", len(lastVereficationResult.Duplicates))
		resp, err = app.BTCSClient.GetIDVerificationTraceLogs(strconv.FormatUint(lastVereficationResult.Duplicates[0], 10))
		if err != nil {
			log.Error().Msgf("Notify LKD back error fetching ID verefication logs: %+v", err)
			panic(err)
		}
		lastVereficationResult = resp.IDVerifications[0].Result
	}

	err = InsertSanctionsMatchesInToIDVereficationResults(app, customer.KycID, &lastVereficationResult)
	if err != nil {
		log.Error().Msgf("Notify LKD back error when inserting sanctions math to request: %+v", err)
		panic(err)
	}

	log.Info().Msgf("GORUTINE RECEIVED TRACES AND SUBMITS for customer %v", customer.ID)
	go app.LKDBackClient.SubmitIDVerificationResults(&lastVereficationResult, customer.ID)

	app.Repo.Customer.SetScansCheckResultSubmitedAt(customer)

	log.Info().Msgf("VEREFICATION RESULTS SENT: %+v", lastVereficationResult)
}

// InsertSanctionsMatchesInToIDVereficationResults fetches Customer traces from btcs, extract sanctions match results and inserts in to ID verefication results
func InsertSanctionsMatchesInToIDVereficationResults(app *application.Application, customerID string, results *btcsclient.BtcsScansEventIDVerificationsResult) error {
	traces, err := app.BTCSClient.GetCustomerTaces(customerID)
	if err != nil {
		return err
	}
	matches := btcsclient.FindSanctionsLogsInCustomerTraces(traces)
	results.VerifiedNameSanctionsScreen.Matches = matches
	return nil
}

// GetDuplicateDetails fetches Customer traces from btcs, extract duplicate customer matches, inserts them to ID verification results
func GetDuplicateDetails(app *application.Application, customerID string, results *btcsclient.BtcsScansEventIDVerificationsResult) error {
	traces, err := app.BTCSClient.GetCustomerTaces(customerID)
	if err != nil {
		return err
	}
	duplicates := btcsclient.FindDuplicateDetailsInCustomerTraces(traces)
	results.Duplicates = duplicates
	return nil
}

func checkFaceImageValidity(app *application.Application, customer *models.Customer, fileFormat string) error {
	log.Print("finding face_image")

	keyFace := fmt.Sprintf("%v/identity-scans/%v.%v", customer.ID, "face_image", fileFormat)
	faceImageReader, err := app.Storage.GetImageReader(customer.ID, keyFace)
	if err != nil {
		return err
	}

	imageProcessor := &btcsclient.MultiPartImagesProcessor{
		FaceImage:  faceImageReader,
		BtcsClient: app.BTCSClient,
	}

	err = imageProcessor.CheckFaceImageValidity()
	if err != nil {
		_, ok := err.(*btcsclient.ImageProcessorError)
		if ok {
			app.LKDBackClient.SubmitIDProcessingError(err.Error()+fileFormat, customer.ID)
		}
		return err
	}

	_ = app.Repo.Customer.SetFaceImageFlag(true, customer)
	_ = app.Repo.Customer.SetFaceFormat(fileFormat, customer)

	err = app.LKDBackClient.SubmitSuccessImageValidation(customer.ID, "face_image")
	if err != nil {
		return err
	}

	return nil
}

func checkFrontImageValidity(app *application.Application, customer *models.Customer, fileFormat string) error {
	log.Print("finding front_image")

	keyFront := fmt.Sprintf("%v/identity-scans/%v.%v", customer.ID, "front_image", fileFormat)
	frontImageReader, err := app.Storage.GetImageReader(customer.ID, keyFront)
	if err != nil {
		return err
	}

	imageProcessor := &btcsclient.MultiPartImagesProcessor{
		FrontImage: frontImageReader,
		BtcsClient: app.BTCSClient,
	}

	err = imageProcessor.CheckFrontImageValidity()
	if err != nil {
		_, ok := err.(*btcsclient.ImageProcessorError)
		if ok {
			app.LKDBackClient.SubmitIDProcessingError(err.Error()+fileFormat, customer.ID)
		}
		return err
	}

	_ = app.Repo.Customer.SetFrontImageFlag(true, customer)
	_ = app.Repo.Customer.SetFrontFormat(fileFormat, customer)

	err = app.LKDBackClient.SubmitSuccessImageValidation(customer.ID, "front_image")
	if err != nil {
		return err
	}

	return nil
}

func checkBackImageValidity(app *application.Application, customer *models.Customer, fileFormat string) error {
	log.Print("finding back_image")

	keyBack := fmt.Sprintf("%v/identity-scans/%v.%v", customer.ID, "back_image", fileFormat)
	backImageReader, err := app.Storage.GetImageReader(customer.ID, keyBack)
	if err != nil {
		return err
	}

	imageProcessor := &btcsclient.MultiPartImagesProcessor{
		BackImage:  backImageReader,
		BtcsClient: app.BTCSClient,
	}

	err = imageProcessor.CheckBackImageValidity()
	if err != nil {
		_, ok := err.(*btcsclient.ImageProcessorError)
		if ok {
			app.LKDBackClient.SubmitIDProcessingError(err.Error()+fileFormat, customer.ID)
		}
		return err
	}

	_ = app.Repo.Customer.SetBackImageFlag(true, customer)
	_ = app.Repo.Customer.SetBackFormat(fileFormat, customer)

	err = app.LKDBackClient.SubmitSuccessImageValidation(customer.ID, "back_image")
	if err != nil {
		return err
	}

	return nil
}
