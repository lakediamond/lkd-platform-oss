package events

import (
	"encoding/json"
	"fmt"
)

// EventsError represents errors struct happens when parsing S3 events
type EventsError struct {
	Code         string `json:"error"`
	ErrorMessage string `json:"message"`
}

func (e *EventsError) Error() string {
	return fmt.Sprintf("%+v", e.ErrorMessage)
}

func (e EventsError) MarshalJSON() ([]byte, error) {
	errors := make(map[string]string)
	errors["Error"] = e.Code
	errors["Message"] = e.Error()
	return json.Marshal(errors)
}
