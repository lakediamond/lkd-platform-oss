package events

import (
	"context"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo/utils"
	"strings"

	"github.com/rs/zerolog/log"

	"cloud.google.com/go/pubsub"
)

func Subscribe(app *application.Application) {
	client := gcpClient(app.Config.ProjectID)
	defer func() {
		err := client.Close()
		if err != nil {
			log.Fatal().Err(err).Msg("Failed to create client")
		}
	}()

	t := client.Topic(app.Config.EventSubscriptionTopic)
	defer t.Stop()

	sub := client.Subscription(app.Config.EventSubscriptionTopic)
	err := sub.Receive(context.Background(), func(ctx context.Context, m *pubsub.Message) {
		if m.Attributes["eventType"] == "OBJECT_FINALIZE" {
			key := strings.Join(
				[]string{m.Attributes["bucketId"],
					m.Attributes["objectId"]},
				"/")
			eventData := &S3EventNotification{Key: key}
			err := UpdateFileFlagBasedOnEvent(app, eventData)
			if err != nil {
				log.Error().Err(err).Msg("Error during updating file flag")
				switch err.(type) {
				case *utils.DoesNotExistsError:
					m.Ack()
				default:
					return
				}
				return
			}
		}
		m.Ack()
	})
	if err != context.Canceled {
		log.Error().Err(err)
	}
}
func gcpClient(projectID string) *pubsub.Client {
	client, err := pubsub.NewClient(context.Background(), projectID)
	if err != nil {
		log.Fatal().Err(err).Msg("Subscriber init failed")
	}

	return client
}
