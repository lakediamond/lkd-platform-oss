package events

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"

	"kyc-service/internal/pkg/context"
	"kyc-service/pkg/handlers"
)

// S3EventsHandler responsible for handling & validation http reqeusts from AWS SNS with S3 notification events
var S3EventsHandler = handlers.ApplicationHandler(s3Events)

// TODO: add security header check
func s3Events(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	eventData := new(S3EventNotification)
	err := json.NewDecoder(r.Body).Decode(eventData)
	if err != nil {
		log.Error().Msgf("New S3 event notification, payload decoding error: %+v", eventData)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !(eventData.EventName == S3Put) {
		if eventData.EventName == "" {
			log.Error().Msgf("New S3 event notification, no EventName error")
			w.WriteHeader(http.StatusBadRequest)
		} else {
			w.WriteHeader(http.StatusOK)
		}
		return
	}
	log.Info().Msgf("New S3 event notification, with payload: %+v", eventData)
	err = UpdateFileFlagBasedOnEvent(app, eventData)
	if err != nil {
		log.Error().Msgf("New S3 event notification, DB flag update ERROR: %+v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

// BtcsScansEventsHandler responsible for handling notifications about scans parsing from BTCS
var BtcsScansEventsHandler = handlers.ApplicationHandler(btcsScansEvents)

// TODO: add security header checks
func btcsScansEvents(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	eventData := new(BtcsScansEvent)
	err := json.NewDecoder(r.Body).Decode(eventData)
	if err != nil {
		log.Error().Msgf("New BTCS scans verification event, decodding ERROR: %+v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if eventData.CustomerID == 0 {
		log.Error().Msgf("New BTCS scans verification event ERROR, no Customer ID")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	log.Info().Msgf("New BTCS scans verification event, with payload: %+v", eventData)

	err = ParseBtcsScansEvent(app, eventData)
	if err != nil {
		log.Error().Msgf("New BTCS scans verification event parse ERROR: %+v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	return
}

// BtcsAddressEventsHandler responsible for handling notifications about address validation from BTCS
var BtcsAddressEventsHandler = handlers.ApplicationHandler(btcsAddressEvents)

func btcsAddressEvents(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusAccepted)
	return
}
