package events_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"kyc-service/internal/app/service/events"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo"
	repo_utils "kyc-service/internal/pkg/repo/utils"
	"kyc-service/internal/pkg/testutils"
	"kyc-service/pkg/btcsclient"
	"kyc-service/pkg/lkdbackclient"

	"github.com/stretchr/testify/suite"
)

type EventsTestSuite struct {
	suite.Suite
	App                       *application.Application
	MockServer                *httptest.Server
	S3Event1                  *events.S3EventNotification
	BtcsScansEvent1           *events.BtcsScansEvent
	BTCSIDVerificationResults *btcsclient.BTCSIDVerificationEventsResponse
	BtcsCustomerTraces        *btcsclient.CustomerTraces
}

func TestEventsTestSuite(t *testing.T) {
	suite.Run(t, new(EventsTestSuite))
}

// SetupTest will be called before each test
func (suite *EventsTestSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbClient()
	suite.App = app
	suite.MockServer = mockServer
	err := testutils.UploadFixtures(app, "customer_fixtures.json")
	if err != nil {
		panic(err)
	}
	s3Event1 := returnS3Event1FromFile()
	iDVereficationResults := returnBTCSIDVerificationResults()
	btcsScansEvent1 := returnBtcsScansEvent1()
	btcsCustomerTraces := returnBTCSCustomerTraces()
	suite.BtcsCustomerTraces = btcsCustomerTraces
	suite.BTCSIDVerificationResults = iDVereficationResults
	suite.S3Event1 = s3Event1
	suite.BtcsScansEvent1 = btcsScansEvent1
}

// TearDownTest will be called after each test
func (suite *EventsTestSuite) TearDownTest() {
	// TODO: replace with DB transactions
	suite.App.Repo.DB.Exec("DELETE FROM customers;")
}

// TODO: use goled files https://medium.com/@povilasve/go-advanced-tips-tricks-a872503ac859
func returnS3Event1FromFile() *events.S3EventNotification {
	path, err := testutils.FindTestDataPath()
	if err != nil {
		panic(err)
	}
	fp := filepath.Join(path, "s3_test_event_notification.json")
	jsonFile, err := os.Open(fp)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	event := new(events.S3EventNotification)
	_ = json.NewDecoder(jsonFile).Decode(event)
	return event
}

func returnBTCSIDVerificationResults() *btcsclient.BTCSIDVerificationEventsResponse {
	path, err := testutils.FindTestDataPath()
	if err != nil {
		panic(err)
	}
	fp := filepath.Join(path, "btcs_scans_event_trace_response.json")
	jsonFile, err := os.Open(fp)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	traces := new(btcsclient.BTCSIDVerificationEventsResponse)
	_ = json.NewDecoder(jsonFile).Decode(traces)
	return traces
}

func returnBTCSCustomerTraces() *btcsclient.CustomerTraces {
	path, err := testutils.FindTestDataPath()
	if err != nil {
		panic(err)
	}
	fp := filepath.Join(path, "btcs_traces_with_sanctions_match.json")
	jsonFile, err := os.Open(fp)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	traces := new(btcsclient.CustomerTraces)
	_ = json.NewDecoder(jsonFile).Decode(traces)
	return traces
}

// TODO: use gold files https://medium.com/@povilasve/go-advanced-tips-tricks-a872503ac859
func returnBtcsScansEvent1() *events.BtcsScansEvent {
	path, err := testutils.FindTestDataPath()
	if err != nil {
		panic(err)
	}
	fp := filepath.Join(path, "btcs_scans_event_notification.json")
	jsonFile, err := os.Open(fp)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	event := new(events.BtcsScansEvent)
	_ = json.NewDecoder(jsonFile).Decode(event)
	return event
}

func (suite *EventsTestSuite) TestWrongEventFormatReturnsBadRequest() {
	request := make(map[string]int)
	request["route"] = 66
	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("s3_notification"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response Status Code Shoulde Be 400")
}

func (suite *EventsTestSuite) TestRightEventFormatReturnsStatusOk() {

	// Marhsling sttruct to json
	tmp, _ := json.Marshal(suite.S3Event1)
	// Creadting Reader from json
	requestData := bytes.NewReader(tmp)

	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("s3_notification"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusOK, resp.StatusCode, "Response Status Code Shoulde Be 200")
}

func (suite *EventsTestSuite) TestRightEventSetsRightStatusInDB() {
	// Marhsling sttruct to json
	tmp, _ := json.Marshal(suite.S3Event1)
	// Creadting Reader from json
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("s3_notification"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}

	suite.Equal(http.StatusOK, resp.StatusCode, "Response Status Code Shoulde Be")
	customer, _ := suite.App.Repo.Customer.GetCustomerByID("9b131a6a-f81b-4300-b3a3-9e32ad41b9c3")
	suite.Nil(customer.SubmitedAt)
	suite.True(customer.FaceUploaded, "Face should be uploaded")
}

func (suite *EventsTestSuite) TestRightEventSetsRightSFileFormat() {
	// Marhsling sttruct to json
	tmp, _ := json.Marshal(suite.S3Event1)
	// Creadting Reader from json
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("s3_notification"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}

	suite.Equal(http.StatusOK, resp.StatusCode, "Response Status Code Shoulde Be")
	customer, _ := suite.App.Repo.Customer.GetCustomerByID("9b131a6a-f81b-4300-b3a3-9e32ad41b9c3")
	suite.Nil(customer.SubmitedAt)
	suite.True(customer.FaceUploaded, "Face should be uploaded")
	suite.Equal(customer.FaceFormat, "jpeg")
}

func (suite *EventsTestSuite) TestRightEventSetsRightSFileFormat2() {
	suite.S3Event1.Key = "lkd/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3/back_image.png"
	// Marhsling sttruct to json
	tmp, _ := json.Marshal(suite.S3Event1)
	// Creadting Reader from json
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("s3_notification"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}

	suite.Equal(http.StatusOK, resp.StatusCode, "Response Status Code Shoulde Be")
	customer, _ := suite.App.Repo.Customer.GetCustomerByID("9b131a6a-f81b-4300-b3a3-9e32ad41b9c3")
	suite.Nil(customer.SubmitedAt)
	suite.True(customer.BackUploaded, "Face should be uploaded")
	suite.Equal("png", customer.BackFormat)
}

func (suite *EventsTestSuite) TestFindCustomerEventBelongsTo() {
	customer, err := events.FindCustomerS3EventBelongsTo(suite.App, suite.S3Event1)
	if err != nil {
		suite.FailNow("Find customer error")
	}
	if customer == nil {
		suite.FailNow("Customer is Nil")
	}
	suite.Equal(customer.ID, "9b131a6a-f81b-4300-b3a3-9e32ad41b9c3", "Customer ID should match PATH to the file inseide bucket")
}

func (suite *EventsTestSuite) TestSetCustomerFileUploadedFalgBasedOnEventFaceImage() {
	customer, err := events.FindCustomerS3EventBelongsTo(suite.App, suite.S3Event1)
	if err != nil {
		suite.FailNow("Find customer error")
	}
	if customer == nil {
		suite.FailNow("Customer is Nil")
	}
	suite.Equal(customer.ID, "9b131a6a-f81b-4300-b3a3-9e32ad41b9c3", "Customer ID should match PATH to the file inseide bucket")
	err = events.SetCustomerFileUploadedFalgBasedOnEvent(suite.App, customer, suite.S3Event1)
	// customer, err = events.FindCustomerS3EventBelongsTo(suite.App, event)
	suite.True(customer.FaceUploaded, "FaceUploaded should be true")
}

func (suite *EventsTestSuite) TestSetCustomerFileUploadedFalgBasedOnEventFrontImage() {
	suite.S3Event1.Key = "lkd/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3/front_image.jpeg"
	customer, err := events.FindCustomerS3EventBelongsTo(suite.App, suite.S3Event1)
	if err != nil {
		suite.FailNow("Find customer error")
	}
	if customer == nil {
		suite.FailNow("Customer is Nil")
	}
	suite.Equal(customer.ID, "9b131a6a-f81b-4300-b3a3-9e32ad41b9c3", "Customer ID should match PATH to the file inseide bucket")
	err = events.SetCustomerFileUploadedFalgBasedOnEvent(suite.App, customer, suite.S3Event1)
	// customer, err = events.FindCustomerS3EventBelongsTo(suite.App, event)
	suite.True(customer.FrontUploaded, "FrontUploaded should be true")
}

func (suite *EventsTestSuite) TestSetCustomerFileUploadedFalgBasedOnEventBackImage() {
	suite.S3Event1.Key = "lkd/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3/back_image.jpeg"
	customer, err := events.FindCustomerS3EventBelongsTo(suite.App, suite.S3Event1)
	if err != nil {
		suite.FailNow("Find customer error")
	}
	if customer == nil {
		suite.FailNow("Customer is Nil")
	}
	suite.Equal(customer.ID, "9b131a6a-f81b-4300-b3a3-9e32ad41b9c3", "Customer ID should match PATH to the file inseide bucket")
	err = events.SetCustomerFileUploadedFalgBasedOnEvent(suite.App, customer, suite.S3Event1)
	// customer, err = events.FindCustomerS3EventBelongsTo(suite.App, event)
	suite.True(customer.BackUploaded, "BackUploaded should be true")
}

func (suite *EventsTestSuite) TestRightBtcsScanEventFromatReturnsStatusAccepted() {
	mocks := make(map[string]interface{})
	mocks["GetIDVereficationTraceLogs"] = suite.BTCSIDVerificationResults
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}
	suite.App.LKDBackClient = &lkdbackclient.MockedLKDBackClient{}

	// Marhsling struct to json
	tmp, _ := json.Marshal(suite.BtcsScansEvent1)
	// Creadting Reader from json
	requestData := bytes.NewReader(tmp)

	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("btcs_scan_events"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusAccepted, resp.StatusCode, "Response Status Code Shoulde Be 200")
}

func (suite *EventsTestSuite) TestWrongBtcsScanEventFromatReturnsBadRequest() {
	mocks := make(map[string]interface{})
	mocks["GetIDVereficationTraceLogs"] = suite.BTCSIDVerificationResults
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}
	suite.App.LKDBackClient = &lkdbackclient.MockedLKDBackClient{}

	request := make(map[string]int)
	request["route"] = 66
	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("btcs_scan_events"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response Status Code Shoulde Be 400")
}

func (suite *EventsTestSuite) TestRightBtcsScanEventFromatWithUnexistingCustomerReturnsBadRequest() {
	mocks := make(map[string]interface{})
	mocks["GetIDVereficationTraceLogs"] = suite.BTCSIDVerificationResults
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}
	suite.App.LKDBackClient = &lkdbackclient.MockedLKDBackClient{}

	// Marhsling sttruct to json
	suite.BtcsScansEvent1.CustomerID = 5702092954009611
	tmp, _ := json.Marshal(suite.BtcsScansEvent1)
	// Creadting Reader from json
	requestData := bytes.NewReader(tmp)

	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("btcs_scan_events"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response Status Code Shoulde Be 400")
}

func (suite *EventsTestSuite) TestFindCustomerBtcsScanEventBelongsToExists() {
	customer, err := events.FindCustomerBtcsScanEventBelongsTo(suite.App, suite.BtcsScansEvent1)
	suite.NotNil(customer)
	suite.Nil(err, "Customer should be found")
	suite.Equal(customer.ID, "9b131a6a-f81b-4300-b3a3-9e32ad41b9c3")
	suite.Equal(customer.KycID, "5702092954009600")
}
func (suite *EventsTestSuite) TestFindCustomerBtcsScanEventBelongsToNotExists() {
	suite.BtcsScansEvent1.CustomerID = 5702092954009611
	_, err := events.FindCustomerBtcsScanEventBelongsTo(suite.App, suite.BtcsScansEvent1)
	suite.NotNil(err, "Customer must not be found")
	if suite.Error(err) {
		suite.Equal(&repo_utils.DoesNotExistsError{Table: "customer", Field: "kyc_id"}, err, "Error should be DoesNotExists")
	}
}

func (suite *EventsTestSuite) TestRightBtcsScanEventFromatChangesDBStatusParsing() {
	mocks := make(map[string]interface{})
	mocks["GetIDVereficationTraceLogs"] = suite.BTCSIDVerificationResults
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}
	suite.App.LKDBackClient = &lkdbackclient.MockedLKDBackClient{}

	err := events.ParseBtcsScansEvent(suite.App, suite.BtcsScansEvent1)
	suite.Nil(err)
	customer, err := events.FindCustomerBtcsScanEventBelongsTo(suite.App, suite.BtcsScansEvent1)
	suite.Nil(err, "There must not be error")
	suite.True(customer.ScansCheckResultReceived, "Flag about received results must be set")
	suite.Equal(customer.ScansCheckResult, "APPROVE", "Event result must be")
	suite.NotNil(customer.ScansCheckResultReceivedAt, "Event result received at must not be nil")
}

func (suite *EventsTestSuite) TestBTCSEventVerefeicationFaceFailDetails() {
	mocks := make(map[string]interface{})
	mocks["GetIDVereficationTraceLogs"] = suite.BTCSIDVerificationResults
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}
	suite.App.LKDBackClient = &lkdbackclient.MockedLKDBackClient{}

	customer, _ := suite.App.Repo.Customer.GetCustomerByID("9b131a6a-f81b-4300-b3a3-9e32ad41b9c3")
	suite.App.Repo.Customer.SetAllImagesFlags(true, customer)
	suite.BtcsScansEvent1.Details = "FACE_FAIL"
	err := events.ParseBtcsScansEvent(suite.App, suite.BtcsScansEvent1)
	suite.Nil(err, "No error must appear")
	customer, err = events.FindCustomerBtcsScanEventBelongsTo(suite.App, suite.BtcsScansEvent1)
	suite.Nil(err, "Customer should be found")
	suite.NotNil(customer)
	suite.False(customer.FaceUploaded, "Face uploaded flag must be set to false")
	suite.False(customer.FrontUploaded, "Front uploaded flag must be set to false")
	suite.False(customer.BackUploaded, "Back uploaded flag must be set to false")
}

func (suite *EventsTestSuite) TestBTCSEventVerefeicationRejectStatus() {
	mocks := make(map[string]interface{})
	mocks["GetIDVereficationTraceLogs"] = suite.BTCSIDVerificationResults
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}
	suite.App.LKDBackClient = &lkdbackclient.MockedLKDBackClient{}

	customer, _ := suite.App.Repo.Customer.GetCustomerByID("9b131a6a-f81b-4300-b3a3-9e32ad41b9c3")
	suite.App.Repo.Customer.SetAllImagesFlags(true, customer)
	suite.BtcsScansEvent1.Details = "ERROR_NOT_READABLE_ID"
	suite.BtcsScansEvent1.Status = "REJECTED"
	err := events.ParseBtcsScansEvent(suite.App, suite.BtcsScansEvent1)
	suite.Nil(err, "No error must appear")
	customer, err = events.FindCustomerBtcsScanEventBelongsTo(suite.App, suite.BtcsScansEvent1)
	suite.Nil(err, "Customer should be found")
	suite.NotNil(customer)
	suite.False(customer.FaceUploaded, "Face uploaded flag must be set to false")
	suite.False(customer.FrontUploaded, "Front uploaded flag must be set to false")
	suite.False(customer.BackUploaded, "Back uploaded flag must be set to false")
}

func (suite *EventsTestSuite) TestNotifyLKDBackednAboutIDVereficationEvent() {
	mocks := make(map[string]interface{})
	mocks["GetIDVereficationTraceLogs"] = suite.BTCSIDVerificationResults
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}
	suite.App.LKDBackClient = &lkdbackclient.MockedLKDBackClient{}

	customer, _ := suite.App.Repo.Customer.GetCustomerByID("9b131a6a-f81b-4300-b3a3-9e32ad41b9c3")
	events.NotifyLKDBackednAboutIDVereficationEvent(suite.App, customer)
	suite.NotNil(customer.ScansCheckSubmitedAt, "Date must not be nil")
}

func (suite *EventsTestSuite) TestInsertSanctionsMatchesInToIDVereficationResults() {
	mocks := make(map[string]interface{})
	mocks["GetCustomerTaces"] = suite.BtcsCustomerTraces
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}

	lastVereficationResult := suite.BTCSIDVerificationResults.IDVerifications[0].Result
	err := events.InsertSanctionsMatchesInToIDVereficationResults(suite.App, "5702092954009600", &lastVereficationResult)
	suite.Nil(err, "There must not be error")
	suite.NotNil(lastVereficationResult.VerifiedNameSanctionsScreen.Matches, "Matches must be inserted")
}
