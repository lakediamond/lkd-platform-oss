package httpserver

import (
	"gopkg.in/DataDog/dd-trace-go.v1/contrib/julienschmidt/httprouter"

	"kyc-service/internal/app/service/customer"
	"kyc-service/internal/app/service/events"
	"kyc-service/internal/pkg/application"
)

// func multiTypeRoute(app *application.Application, h http.Handler) http.Handler {
// 	return alice.New(handlers.MultiPartContentTypeHandler, app.WithApplicationContext).Then(h)
// }

// KYC-service main routes
func NewHTTPRouter(app *application.Application) *httprouter.Router {
	router := httprouter.New()

	routes := application.Routes{
		application.Route{Alias: "create_customer", Method: "POST", Path: "/customer", Handler: app.WithApplicationContext(customer.CreateCustomerHandler)},
		application.Route{Alias: "update_customer", Method: "PATCH", Path: "/customer/:customer_id", Handler: app.WithApplicationContext(customer.UpdateCustomerHandler)},
		application.Route{Alias: "reset_photo_upload_status", Method: "PATCH", Path: "/customer/:customer_id/media/:type", Handler: app.WithApplicationContext(customer.ResetMediaUploadStatus)},
		application.Route{Alias: "submit_ids", Method: "PUT", Path: "/customer/:customer_id", Handler: app.WithApplicationContext(customer.SubmitIDScansHandler)},
		application.Route{Alias: "verify_customer", Method: "GET", Path: "/customer/:customer_id", Handler: app.WithApplicationContext(customer.VerifyCustomerHandler)},

		application.Route{Alias: "s3_notification", Method: "POST", Path: "/api/events/s3", Handler: app.WithApplicationContext(events.S3EventsHandler)},
		application.Route{Alias: "btcs_scan_events", Method: "POST", Path: "/api/events/btcs/scans", Handler: app.WithApplicationContext(events.BtcsScansEventsHandler)},
		application.Route{Alias: "btcs_address_events", Method: "POST", Path: "/api/events/btcs/address", Handler: app.WithApplicationContext(events.BtcsAddressEventsHandler)},
	}

	for _, route := range routes {
		router.Handler(route.Method, route.Path, route.Handler)
	}

	app.MountRoutes(routes)

	return router
}
