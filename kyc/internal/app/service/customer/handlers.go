package customer

import (
	"encoding/json"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"

	"kyc-service/internal/pkg/context"
	repo_utils "kyc-service/internal/pkg/repo/utils"
	"kyc-service/pkg/btcsclient"
	"kyc-service/pkg/handlers"
)

// CreateCustomerData  is the structure that represents createCustomer request json data
type CreateCustomerData struct {
	FirstName  string   `json:"first_name"`
	LastName   string   `json:"last_name"`
	Email      string   `json:"email"`
	Phone      []string `json:"phone"`
	Dob        string   `json:"dob"`
	EthAddress string   `json:"eth_address"`
	ID         string   `json:"id"`
}

// VerifyCustomerHandler responsible for handling & validating HTTP request
var VerifyCustomerHandler = handlers.ApplicationHandler(verifyCustomer)

func verifyCustomer(w http.ResponseWriter, r *http.Request) {
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")
	app := context.GetApplicationContext(r)

	customerLevels, err := getCustomerLevels(app, customerID)
	if err != nil {
		jsonedError, _ := json.Marshal(err)
		if _, ok := err.(*repo_utils.DoesNotExistsError); ok {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonedError)
		return
	}

	jsonString, _ := json.Marshal(customerLevels)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonString)
	w.WriteHeader(http.StatusOK)
	return
}

// CreateCustomerHandler responsible for handling & validating HTTP request
var CreateCustomerHandler = handlers.ApplicationHandler(createCustomer)

func createCustomer(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	customerData := new(CreateCustomerData)
	err := json.NewDecoder(r.Body).Decode(customerData)
	if err != nil {
		log.Error().Msgf("Error decoding create customer data: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	log.Info().Msgf("New customer create request, with data: %+v", customerData)
	err = createCustomerService(app, customerData)
	if err != nil {
		jsonedError, _ := json.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(jsonedError)
		log.Error().Msgf("Error creating customer: %+v", err)
		return
	}

	log.Info().Msgf("Customer with ID: %+v, created", customerData.ID)
	w.WriteHeader(http.StatusCreated)
	return
}

// UpdateCustomerHandler responsible for handling & validating HTTP request
var UpdateCustomerHandler = handlers.ApplicationHandler(updateCustomer)

func updateCustomer(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")

	customerData := new(btcsclient.UpdateCustomerRequest)
	err := json.NewDecoder(r.Body).Decode(customerData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = updateCustomerService(app, customerData, customerID)
	if err != nil {
		jsonedError, _ := json.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		if _, ok := err.(*repo_utils.DoesNotExistsError); ok {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
		w.Write(jsonedError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	return
}

// ResetMediaUploadStatus reset upload status for given media type to allow re-submission flow
var ResetMediaUploadStatus = handlers.ApplicationHandler(resetMediaUploadStatus)

func resetMediaUploadStatus(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")
	mediaType := routeParams.ByName("type")

	err := resetMediaUploadStatusService(app, customerID, mediaType)
	if err != nil {
		jsonedError, _ := json.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonedError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	return
}

// VerifyCustomerPhoneSmsHandler responsible for handling & validating HTTP request
var VerifyCustomerPhoneSmsHandler = handlers.ApplicationHandler(verifyCustomerPhoneSms)

func verifyCustomerPhoneSms(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")

	err := setCustomerPhoneSmsService(app, customerID)
	if err != nil {
		jsonedError, _ := json.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		if _, ok := err.(*repo_utils.DoesNotExistsError); ok {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
		w.Write(jsonedError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	return
}

// SubmitIDScansHandler responsible for handling & validating HTTP request
var SubmitIDScansHandler = handlers.ApplicationHandler(submitIDScans)

func submitIDScans(w http.ResponseWriter, r *http.Request) {
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")
	app := context.GetApplicationContext(r)
	// TODO: Think about siz of the Form
	err := r.ParseMultipartForm(100000)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	m := r.MultipartForm

	err = submitCustomerIDScansService(app, m, customerID)

	if err != nil {
		jsonedError, _ := json.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(jsonedError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
