module kyc-service

require (
	cloud.google.com/go v0.34.0
	github.com/aws/aws-sdk-go v1.16.7
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	github.com/gorilla/handlers v1.4.0
	github.com/jinzhu/gorm v1.9.2
	github.com/joho/godotenv v1.3.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/justinas/alice v0.0.0-20171023064455-03f45bd4b7da
	github.com/lib/pq v1.0.0
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/rs/zerolog v1.11.0
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.2.2
	github.com/tinylib/msgp v1.1.0 // indirect
	go.opencensus.io v0.19.0 // indirect
	golang.org/x/crypto v0.0.0-20181127143415-eb0de9b17e85 // indirect
	golang.org/x/oauth2 v0.0.0-20190130055435-99b60b757ec1
	google.golang.org/api v0.1.0
	google.golang.org/genproto v0.0.0-20190201180003-4b09977fb922 // indirect
	google.golang.org/grpc v1.18.0 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.9.0
	gopkg.in/gormigrate.v1 v1.3.0
)
