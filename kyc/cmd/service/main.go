package main

import (
	_ "image/jpeg"
	"net"
	"os"

	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"

	"kyc-service/internal/app/service/events"
	"kyc-service/internal/app/service/httpserver"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo"
	"kyc-service/internal/pkg/utils"
)

func main() {
	addr := net.JoinHostPort(
		os.Getenv("DD_AGENT_HOST"),
		os.Getenv("DD_TRACE_AGENT_PORT"),
	)
	tracer.Start(tracer.WithAgentAddr(addr), tracer.WithServiceName("lkd-platform-kyc"))
	defer tracer.Stop()

	app := application.NewKycService()
	app.Repo = repo.GetDbClient()
	app.BTCSClient = utils.NewBTCSClient()
	app.LKDBackClient = utils.NewLKDBackClient()
	app.Storage = utils.NewStorage()

	sub, present := os.LookupEnv("RUN_SUBSCRIBERS")
	if present && sub == "true" {
		go events.Subscribe(app)
	}
	httpserver.Serve(app)

}
