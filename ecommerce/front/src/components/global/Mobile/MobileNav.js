import React from 'react';
import { connect } from 'react-redux';

function mapStatetoProps(state) {
  return state;
}

class Nav extends React.Component {
  toggleMainMenu = () => {
    var $hamburger = document.querySelector('.hamburger');
    var $nav = document.querySelector('.main-navigation');
    var $cart = document.querySelector('.push .secondary-nav');

    $hamburger.classList.toggle('is-active');
    $nav.classList.toggle('is-active');

    if ($cart) {
      $cart.classList.toggle('is-hidded');
    }
  };

  render() {
    const linkPrefix =
      process.env.REACT_APP_LD_LINK_PREFIX || 'https://lakediamond.ch';
    return (
      <div className="page-top">
        <div className="page-top__logo">
          <a href={`${linkPrefix}/`}>
            <img
              src="img/logo_lake_home@2x.png"
              alt="LakeDiamond - The brilliant side of technology"
            />
          </a>
        </div>
        <div className="page-top__hamburger main-menu-wrapper">
          <button
            className="hamburger hamburger--spin"
            type="button"
            onClick={this.toggleMainMenu}>
            <span className="hamburger-box">
              <span className="hamburger-inner" />
              <div className="menu-text">
                <span className="menu-op">Menu</span>
                <span className="menu-cl">Close</span>
              </div>
            </span>
          </button>
          <div className="nav main-navigation">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-16">
                  <ul className="nav navbar-nav navbar-left">
                    <li>
                      <a href={`${linkPrefix}/`}>Home</a>
                    </li>
                    <li>
                      <a href={`${linkPrefix}/about-us`}>About Us</a>
                    </li>
                    <li>
                      <a href={`${linkPrefix}/ourtechnologies`}>
                        Our technologies
                      </a>
                    </li>
                    <li>
                      <a href={`${linkPrefix}/explore`}>Our applications</a>
                    </li>
                    <li className="active">
                      <a href={`${linkPrefix}/products`}>Our products</a>
                    </li>
                    <li>
                      <a href={`${linkPrefix}/tokensale`}>Token sale</a>
                    </li>
                    <li>
                      <a href={`${linkPrefix}/contact`}>Contact</a>
                    </li>
                  </ul>
                </div>
                <div className="col-md-8">
                  <div className="social-links--wrapper">
                    <div className="social-links__twitter social-links">
                      <a target="_blank" href="https://twitter.com/LakeDiamond">
                        <i className="fa fa-twitter" />
                      </a>
                    </div>
                    <div className="social-links__linkedin social-links">
                      <a
                        target="_blank"
                        href="https://www.linkedin.com/company/lakediamond/">
                        <i className="fa fa-linkedin" />
                      </a>
                    </div>
                    <div className="social-links__medium social-links">
                      <a
                        target="_blank"
                        href="https://medium.com/@LakeDiamond/">
                        <i className="fa fa-medium" />
                      </a>
                    </div>
                    <div className="social-links__youtube social-links">
                      <a
                        target="_blank"
                        href="https://www.youtube.com/channel/UCax1IHyIqGPtK-XpebQAXLw">
                        <i className="fa fa-youtube" />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStatetoProps)(Nav);
