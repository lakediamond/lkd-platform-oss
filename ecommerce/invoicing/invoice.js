var fs = require('fs');
var https = require('https');
var exports = (module.exports = {});

exports.generateInvoice = function(filename, invoiceTo, invoiceCurrency, invoiceItems, success, error) {
  const invoice = {
    logo: 'http://invoiced.com/img/logo-invoice.png',
    from: 'Lake Diamond Store',
    to: invoiceTo,
    currency: invoiceCurrency,
    payment_terms: 'Auto-Billed - Do Not Pay',
    items: invoiceItems,
    notes: 'Thanks for being an awesome customer!',
    terms: 'No need to submit payment. You will be auto-billed for this invoice.'
  };
  var postData = JSON.stringify(invoice);
  var options = {
    hostname: 'invoice-generator.com',
    port: 443,
    path: '/',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(postData)
    }
  };

  console.log('DATAINVOICE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  console.log(postData);
  console.log('DATAINVOICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');

  var file = fs.createWriteStream(filename);

  var req = https.request(options, function(res) {
    res
      .on('data', function(chunk) {
        file.write(chunk);
      })
      .on('end', function() {
        file.end();

        if (typeof success === 'function') {
          success();
        }
      });
  });
  req.write(postData);
  req.end();

  if (typeof error === 'function') {
    req.on('error', error);
  }
};
